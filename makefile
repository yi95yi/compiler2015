all:
	mkdir -p bin
	cd src && javac -cp "../antlr-4.5-complete.jar": -d ../bin **/*.java

clean:
	rm -rf bin
