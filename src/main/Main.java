package main;

import java.io.File;
import java.util.Scanner;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import cst.cLexer;
import cst.cParser;
import util.*;
import ast.*;

public class Main implements CommonEnv {
	
	static String fileName = "d:/tgh/New/kmp.c";
	static SourceCode sourcecode = null;
	static Trans trs = null;
	static String fileContent = "";

	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {
		
		fileContent = new Scanner(new BufferedInputStream(System.in)).next();
		//File file = new File(fileName);
		//Scanner scanner = new Scanner(file);
		//scanner = scanner.useDelimiter("\\Z");
		//fileContent = scanner.next();
			
		ANTLRInputStream src = new ANTLRInputStream(Rep.Trans(fileContent));
		cLexer lexer = new cLexer(src);
		CommonTokenStream tockens = new CommonTokenStream(lexer);

		cParser psr = new cParser(tockens);
		tockens.fill();

		sourcecode = psr.program().ret;
		
		trs = new Trans();
		sourcecode.translate(trs);

		mipsGen();
		System.exit(0);
	}

	public static void mipsGen() throws Exception {
		Analy analyzer = new Analy();
		LabElim lEliminator = new LabElim();
		
		for (CompUnit u : trs.cUnits) 
		{
			boolean bContinue = lEliminator.eliminate(u);
			while (bContinue)
			{
				bContinue = lEliminator.eliminate(u);
			}
			u.findBranches(analyzer);
			u.findBasicBlocks(analyzer);
			u.findLiveness(analyzer);
		}

		MipsCode mipscode = new MipsCode();

		mipscode.codeGen(new MipsAsm(".data"));
		for (DFrag data : trs.dataFrags)
			mipscode.gen(data.gen());
		mipscode.CommGen(trs.maxArgumentNum, trs.topLevelSize, wordSize);
		mipscode.print(trs.cUnits.get(0), new LinearScan(trs.cUnits.get(0),analyzer));
		mipscode.codeGen(new MipsAsm("jal exit"));

		for (int i = 1; i < trs.cUnits.size(); ++i) {
			mipscode.print(trs.cUnits.get(i),
					new LinearScan(trs.cUnits.get(i), analyzer));
		}

		mipscode.flush(System.out);
		//PrintStream asm = new PrintStream(fileName + ".s");
		//mipscode.flush(asm);
		String rt = "########################################\n############### RUN-TIME ###############\n########################################\n\n#malloc\nTag1:\n\t# a0 -- size in bytes (already x4)\n\tli $v0, 9\n\tsyscall\n\tjr $ra\n\n#getchar\nTag2:\n\tli $v0, 12\n\tsyscall\n\tjr $ra\n\t\nexit:\n\tli $v0, 10\n\tsyscall\n\tjr $ra\n\n## Daniel J. Ellard -- 03/13/94\n## printf.asm--\n## an implementation of a simple printf work-alike.\n\n## printf--\n## A simple printf-like function. Understands just the basic forms\n## of the %s, %d, %c, and %% formats, and can only have 3 embedded\n## formats (so that all of the parameters are passed in registers).\n## If there are more than 3 embedded formats, all but the first 3 are\n## completely ignored (not even printed).\n## Register Usage:\n## $a0,$s0 - pointer to format string\n## $a1,$s1 - format argument 1 (optional)\n## $a2,$s2 - format argument 2 (optional)\n## $a3,$s3 - format argument 3 (optional)\n## $s4 - count of formats processed.\n## $s5 - char at $s4.\n## $s6 - pointer to printf buffer\n##\nTag0:\n\tsubu $sp, $sp, 56 # set up the stack frame,\n\tsw $ra, 32($sp) # saving the local environment.\n\tsw $fp, 28($sp)\n\tsw $s0, 24($sp)\n\tsw $s1, 20($sp)\n\tsw $s2, 16($sp)\n\tsw $s3, 12($sp)\n\tsw $s4, 8($sp)\n\tsw $s5, 4($sp)\n\tsw $s6, 0($sp)\n\tsw $s7, 36($sp)\n\tsw $t0, 40($sp)\n\tsw $t1, 44($sp)\n\tsw $t2, 48($sp)\n\tsw $t3, 52($sp)\n\taddu $fp, $sp, 52\n\n# grab the arguments:\n\tmove $s0, $a0 # fmt string\n\tmove $s1, $a1 # arg1 (optional)\n\tmove $s2, $a2 # arg2 (optional)\n\tmove $s3, $a3 # arg3 (optional)\n\tlw $s7, 16($v1)# arg4 (optional)\n\tlw $t0, 20($v1)# arg5 (optional)\n\n\tli $s4, 0 # set # of formats = 0\n\tla $s6, printf_buf # set s6 = base of printf buffer.\n\nprintf_loop: # process each character in the fmt:\n\tlb $s5, 0($s0) # get the next character, and then\n\taddu $s0, $s0, 1 # bump up $s0 to the next character.\n\n\tbeq $s5, '%', printf_fmt # if the fmt character, then do fmt.\n\tbeq $0, $s5, printf_end # if zero, then go to end.\n\nprintf_putc:\n\tsb $s5, 0($s6) # otherwise, just put this char\n\tsb $0, 1($s6) # into the printf buffer,\n\tmove $a0, $s6 # and then print it with the\n\tli $v0, 4 # print_str syscall\n\tsyscall\n\n\tb printf_loop # loop on.\n\nprintf_fmt:\n\tlb $s5, 0($s0) # see what the fmt character is,\n\taddu $s0, $s0, 1 # and bump up the pointer.\n\n\tbeq $s4, 5, printf_loop # if we've already processed 3 args,\n# then *ignore* this fmt.\n\tbeq $s5, '0', printf_pre\n\tbeq $s5, 'd', printf_int # if 'd', print as a decimal integer.\n\tbeq $s5, 's', printf_str # if 's', print as a string.\n\tbeq $s5, 'c', printf_char # if 'c', print as a ASCII char.\n\tbeq $s5, '%', printf_perc # if '%', print a '%'\n\tb printf_loop # otherwise, just continue.\n\nprintf_shift_args: # shift over the fmt args,\n\tmove $s1, $s2 # $s1 = $s2\n\tmove $s2, $s3 # $s2 = $s3\n\tmove $s3, $s7 # $s3 = $s7\n\tmove $s7, $t0\n\n\tadd $s4, $s4, 1 # increment # of args processed.\n\n\tb printf_loop # and continue the main loop.\n\t\nprintf_pre:\n\tlb $s5, ($s0)\n\tsub $s5, $s5, 48\n\tadd $s0, $s0, 2\n\t\n\tli $t1, 0\n\tmove $t2, $s1\nprintf_digits_begin:\n\tbeqz $t2, printf_digits_end\n\tadd $t1, $t1, 1\n\tdiv $t2, $t2, 10\n\tb printf_digits_begin\n\t\nprintf_digits_end:\n\tsub $t1, $s5, $t1\n\t\nprintf_pre_output_zero_begin:\n\tbeqz $t1, printf_pre_output_zero_end\n\tsub $t1, $t1, 1\n\tli $a0, 0\n\tli $v0, 1\n\tsyscall\n\tb printf_pre_output_zero_begin\n\t\nprintf_pre_output_zero_end:\n\tmove $a0, $s1\n\tli $v0, 1\n\tsyscall\n\tb printf_shift_args\n\t\n\nprintf_int: # deal with a %d:\n\tmove $a0, $s1 # do a print_int syscall of $s1.\n\tli $v0, 1\n\tsyscall\n\tb printf_shift_args # branch to printf_shift_args\n\nprintf_str: # deal with a %s:\n\tmove $a0, $s1 # do a print_string syscall of $s1.\n\tli $v0, 4\n\tsyscall\n\tb printf_shift_args # branch to printf_shift_args\n\nprintf_char: # deal with a %c:\n\tsb $s1, 0($s6) # fill the buffer in with byte $s1,\n\tsb $0, 1($s6) # and then a null.\n\tmove $a0, $s6 # and then do a print_str syscall\n\tli $v0, 4 # on the buffer.\n\tsyscall\n\tb printf_shift_args # branch to printf_shift_args\n\nprintf_perc: # deal with a %%:\n\tli $s5, '%' # (this is redundant)\n\tsb $s5, 0($s6) # fill the buffer in with byte %,\n\tsb $0, 1($s6) # and then a null.\n\tmove $a0, $s6 # and then do a print_str syscall\n\tli $v0, 4 # on the buffer.\n\tsyscall\n\tb printf_loop # branch to printf_loop\n\nprintf_end:\n\tlw $t1, 44($sp)\n\tlw $t2, 48($sp)\n\tlw $t3, 52($sp)\n\tlw $s7, 36($sp)\n\tlw $t0, 40($sp)\n\tlw $ra, 32($sp) # restore the prior environment:\n\tlw $fp, 28($sp)\n\tlw $s0, 24($sp)\n\tlw $s1, 20($sp)\n\tlw $s2, 16($sp)\n\tlw $s3, 12($sp)\n\tlw $s4, 8($sp)\n\tlw $s5, 4($sp)\n\tlw $s6, 0($sp)\n\taddu $sp, $sp, 56 # release the stack frame.\n\tjr $ra # return.\n\n.data\n\tprintf_buf: .space 2\n";
		//asm.println(rt);
        System.out.println(rt);
		
	}

}
