package ast;

import java.util.List;

import util.*;

public class SourceCode implements Node {
	private final List<Node> nodeList;

	public SourceCode(List<Node> _nodeList) {
		this.nodeList = _nodeList;
	}

	public void semantic(SourceCode sourcecode) throws Exception {
		for (Node node : sourcecode.nodeList) {
			if (node instanceof Declaration)
				semantic((Declaration) node);
			if (node instanceof Fun_Definition)
				semantic((Fun_Definition) node);
		}
	}

	public void semantic(Declaration declaration) throws Exception {
	}

	public void semantic(Fun_Definition funcdefinition) throws Exception {
	}

	public void translate(Trans trs) throws Exception {
		for (Node i : nodeList) {
			i.translate(trs);
		}
		trs.emit_Call();
	}

}
