package ast;

import java.util.List;

import type.TYPE;
import util.Trans;

public class InitDeclarators {
	private final List<InitDeclarator> initDeclaratorList;
	public void semantic(InitDeclarator initd, TYPE type) { }

	public InitDeclarators(List<InitDeclarator> _initDeclaratorList) {
		super();
		this.initDeclaratorList = _initDeclaratorList;
	}
	
	public void translateInitDeclarators(Trans trs, TYPE type) throws Exception {
		for (InitDeclarator initd : this.initDeclaratorList)
			initd.translateInitDeclarator(trs, type, trs.isTopLevel);
	}
}
