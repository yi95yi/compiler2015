package ast;

import ast.expr.Identifier;

public class PlainDeclarator {
	public int numOfStars;
	public Identifier identifier;

	public PlainDeclarator(int _numOfStars, Identifier _identifier) {
		super();
		this.numOfStars = _numOfStars;
		this.identifier = _identifier;
	}
	
}
