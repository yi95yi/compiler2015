package ast;

import util.Trans;

public interface Node {
	public void translate(Trans trs) throws Exception;
}
