package ast.type;

import type.CHAR;
import type.TYPE;
import util.Trans;

public class Type_Char implements Type_Specifier {
	@Override
	public TYPE translateType(Trans trs) throws Exception {
		return CHAR.getInstance();
	}

}