package ast.type;

import type.TYPE;
import util.Trans;

public interface Type_Specifier {
	public TYPE translateType(Trans trs) throws Exception;

}
