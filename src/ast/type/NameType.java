package ast.type;

import ast.Type_defName;
import type.TYPE;
import util.Trans;

public class NameType implements Type_Specifier {
	private final Type_defName typedefName;

	public NameType(Type_defName _typedefName) {
		super();
		this.typedefName = _typedefName;
	}
	
	public TYPE translateType(Trans trs) 
	{
		Object obj = trs.env.getByTypedefName(typedefName.name);
		return (TYPE) obj;
	}
}
