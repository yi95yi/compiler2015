package ast.type;

import type.INT;
import type.TYPE;
import util.Trans;

public class Type_Int implements Type_Specifier {
	public TYPE translateType(Trans trs) throws Exception {
		return INT.getInstance();
	}

}
