package ast.type;

import type.TYPE;
import type.VOID;
import util.Trans;

public class Type_Void implements Type_Specifier {

	public TYPE translateType(Trans trs) throws Exception {
		return VOID.getInstance();
	}

}
