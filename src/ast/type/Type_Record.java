package ast.type;

import ir.address.Address;
import ir.address.IntegerConst;

import java.util.List;

import ast.Declarators;
import ast.StructUnion;
import ast.expr.Identifier;
import ir.ArithmeticExpr;
import type.RECORD;
import type.STRUCT;
import type.TYPE;
import type.UNION;
import util.Pair;
import util.Symbol;
import util.Trans;

public class Type_Record implements Type_Specifier {
	public final StructUnion structUnion;
	public final Identifier identifier;
	public final List<Pair<Type_Specifier, Declarators>> recordTypeList;

	public Type_Record(StructUnion _structUnion, Identifier _identifier,
			List<Pair<Type_Specifier, Declarators>> _recordTypeList) {
		super();
		this.structUnion = _structUnion;
		this.identifier = _identifier;
		this.recordTypeList = _recordTypeList;
	}

	public TYPE translateType(Trans trs) throws Exception {

		Type_Record rType = this;
		if (rType.identifier != null
				&& (rType.identifier.symbol.equals(Symbol.symbol("DDD")))) {
			trs.hasRecord = true;
		}

		if (rType.identifier != null && rType.recordTypeList == null)
			return trs.env.getByRecordName(rType.identifier.symbol);

		Symbol symbol = rType.identifier == null ? null
				: rType.identifier.symbol;
		RECORD record = rType.structUnion == StructUnion.STRUCT ? new STRUCT(
				symbol) : new UNION(symbol);
		trs.env.putType(record, symbol);

		Address size = new IntegerConst(0);
		for (Pair<Type_Specifier, Declarators> pair : rType.recordTypeList) {
			TYPE type = pair.first.translateType(trs);
			List<Pair<TYPE, Symbol>> pairs = pair.second.translateDeclarators(
					trs, type);

			for (Pair<TYPE, Symbol> p : pairs) {
				Address offset = null;
				if (size instanceof IntegerConst) {
					offset = ((IntegerConst) size).clone();
				} else {
					offset = trs.allocate(new IntegerConst(4));
					trs.emitMove(offset, size);
				}
				record.addField(p.first, p.second, offset);
				size = trs.emitArithmeticOp(trs.newTemp(), size, p.first.size,
						ArithmeticExpr.ArithmeticOp.ADD);
			}
		}

		record.size = size;
		return record;

	}
}
