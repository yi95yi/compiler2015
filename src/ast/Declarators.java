package ast;

import java.util.ArrayList;
import java.util.List;

import ast.decl.Declarator;
import type.TYPE;
import util.Pair;
import util.Symbol;
import util.Trans;

public class Declarators {
	private final List<Declarator> declaratorList;

	public Declarators(List<Declarator> _declaratorList) {
		super();
		this.declaratorList = _declaratorList;
	}

	public List<Pair<TYPE, Symbol>> translateDeclarators(Trans trs, TYPE type)
			throws Exception {
		List<Pair<TYPE, Symbol>> list = new ArrayList<Pair<TYPE, Symbol>>();

		for (Declarator decl : this.declaratorList) {
			list.add(decl.translateDeclarator(trs, type));
		}

		return list;
	}
}
