package ast.expr;

import ir.ArithmeticExpr;

import java.util.List;

public class ExprRelation extends ExprBinary<ExprShift> {
	public ExprRelation(List<ExprShift> _shiftExpr, List<ArithmeticExpr.ArithmeticOp> _binaryOp) {
		super(_shiftExpr, _binaryOp);
	}
}
