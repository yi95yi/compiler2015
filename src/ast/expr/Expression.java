package ast.expr;

import ir.address.Address;
import type.*;
import util.Pair;
import util.Trans;

public interface Expression {
	public Pair<Address, TYPE> translateExpr(Trans trs) throws Exception;
}
