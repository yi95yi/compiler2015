package ast.expr;

import ir.ArithmeticExpr;
import ir.address.Address;
import ir.address.Temp;
import type.STRUCT;
import type.TYPE;
import util.Pair;
import util.Trans;

public class ExprAssign implements Expression {
	public LogicalOrExpr logicalOrExpr;
	public ExprUnary unaryExpr;
	public ArithmeticExpr.ArithmeticOp binaryOp;
	public ExprAssign assignExpr;

	public ExprAssign(LogicalOrExpr _logicalOrExpr, ExprUnary _unaryExpr,
			ArithmeticExpr.ArithmeticOp _binaryOp, ExprAssign _assignExpr) {
		super();
		this.logicalOrExpr = _logicalOrExpr;
		this.unaryExpr = _unaryExpr;
		this.binaryOp = _binaryOp;
		this.assignExpr = _assignExpr;
	}
	
	public Pair<Address, TYPE> translateExpr(Trans trs) throws Exception 
	{
		if (this.logicalOrExpr != null)
			return this.logicalOrExpr.translateExpr(trs);

		Pair<Address, TYPE> lExpr = this.unaryExpr.translateExpr(trs);
		Pair<Address, TYPE> rExpr = this.assignExpr.translateExpr(trs);
		if (lExpr == null || rExpr == null)
			return null;

		if (this.binaryOp == ArithmeticExpr.ArithmeticOp.ASSIGN) {
			if (lExpr.second instanceof STRUCT)
				trs.copyStruct(lExpr.first, rExpr.first, (STRUCT) lExpr.second);
			else
				trs.emitMove(lExpr.first, rExpr.first);
		} else {
			Temp temp = trs.newTemp();
			ArithmeticExpr.ArithmeticOp op = ArithmeticExpr.ArithmeticOp.values()[this.binaryOp.ordinal()
					- (ArithmeticExpr.ArithmeticOp.ADDASSIGN.ordinal() - ArithmeticExpr.ArithmeticOp.ADD.ordinal())];

			trs.emitArithmeticOp(temp, lExpr.first, rExpr.first, op);
			trs.emitMove(lExpr.first, temp);
		}
		return lExpr;
	}

}
