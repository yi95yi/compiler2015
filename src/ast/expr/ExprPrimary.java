package ast.expr;

import ir.address.Address;
import type.TYPE;
import util.Pair;
import util.Trans;

public class ExprPrimary implements Expression {
	private final Expression expression;

	public ExprPrimary(Expression _expression) {
		super();
		this.expression = _expression;
	}
	
	public Pair<Address, TYPE> translateExpr(Trans trs) throws Exception {
		return expression.translateExpr(trs);
	}

}
