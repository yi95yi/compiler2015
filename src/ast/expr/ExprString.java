package ast.expr;

import ir.address.Address;
import ir.address.IntegerConst;
import ir.address.Label;
import type.ARRAY;
import type.CHAR;
import type.TYPE;
import util.DFrag;
import util.Pair;
import util.Trans;

public class ExprString implements Expression {
	private final String value;

	public ExprString(String _string) {
		value = _string;
	}

	@Override
	public Pair<Address, TYPE> translateExpr(Trans trs) throws Exception {
		if (value == null) {
			return null;
		}
		Label l = new Label();
		String str = value;
		str = str.substring(1, str.length() - 1);
		str = str.replace("\\\\n", "\n");
		
		if (str.equals("%d")) l.stringType = 5;
		if (str.equals("%d ")) l.stringType = 6;
		if (str.equals("%d\\n")) l.stringType = 7;
		if (str.equals("%d %d\\n")) l.stringType = 8;
		if (str.equals("%s")) l.stringType = 9;

		DFrag data = new DFrag(l, str);
		trs.dataFrags.add(data);
		return new Pair<Address, TYPE>(l, new ARRAY(CHAR.getInstance(),
				new IntegerConst(value.length() * 4)));
	}
}
