package ast.expr;

import ir.address.Address;
import type.TYPE;
import util.Pair;
import util.Trans;

public class ExprConst implements Expression {
	private final LogicalOrExpr logicalOrExpr;

	public ExprConst(LogicalOrExpr _logicalOrExpr) {
		super();
		this.logicalOrExpr = _logicalOrExpr;
	}

	public Pair<Address, TYPE> translateExpr(Trans trs) throws Exception {
		return logicalOrExpr.translateExpr(trs);
	}
}
