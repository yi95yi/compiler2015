package ast.expr;

import ir.ArithmeticExpr;

import java.util.List;

public class InclusiveOrExpr extends ExprBinary<ExclusiveOrExpr> {
	public InclusiveOrExpr(List<ExclusiveOrExpr> _exlusiveExpr,
			List<ArithmeticExpr.ArithmeticOp> _binaryOp) {
		super(_exlusiveExpr, _binaryOp);
	}

	@Override
	protected ArithmeticExpr.ArithmeticOp getBinaryOp() {
		return ArithmeticExpr.ArithmeticOp.OR;
	}
}
