package ast.expr;


import ir.ArithmeticExpr;

import java.util.List;

public class ExprMultiply extends ExprBinary<ExprCast> {
	public ExprMultiply(List<ExprCast> _castExpr, List<ArithmeticExpr.ArithmeticOp> _binaryOp) {
		super(_castExpr, _binaryOp);
	}
}
