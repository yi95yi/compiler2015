package ast.expr;

import ir.ArithmeticExpr;
import ir.Goto;
import ir.IfTrue;
import ir.LabelQuad;
import ir.address.Address;
import ir.address.IntegerConst;
import ir.address.Label;
import ir.address.Temp;

import java.util.List;

import type.INT;
import type.TYPE;
import util.Pair;
import util.Trans;

public class LogicalOrExpr extends ExprBinary<LogicalAndExpr> {
	public LogicalOrExpr(List<LogicalAndExpr> _logicalAndExpr,
			List<ArithmeticExpr.ArithmeticOp> _binaryOp) {
		super(_logicalAndExpr, _binaryOp);
	}

	public Pair<Address, TYPE> translateExpr(Trans trs) throws Exception {
		if (this.exprList.size() == 1) {
			return this.exprList.get(0).translateExpr(trs);
		}

		Label successLabel = new Label(), endLabel = new Label();
		for (LogicalAndExpr expr : this.exprList) {
			Pair<Address, TYPE> pair = expr.translateExpr(trs);
			trs.emit(new IfTrue(pair.first, successLabel));
		}

		Temp temp = trs.newTemp();
		trs.emitMove(temp, new IntegerConst(0));
		trs.emit(new Goto(endLabel));

		trs.emit(new LabelQuad(successLabel));
		trs.emitMove(temp, new IntegerConst(1));
		trs.emit(new LabelQuad(endLabel));
		return new Pair<Address, TYPE>(temp, INT.getInstance());
	}
}
