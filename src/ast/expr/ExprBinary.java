package ast.expr;

import ir.ArithmeticExpr;
import ir.address.Address;
import ir.address.Temp;

import java.util.List;

import type.INT;
import type.TYPE;
import util.Pair;
import util.Trans;

public abstract class ExprBinary<ExprType extends Expression> implements Expression {
	protected final List<ExprType> exprList;
	protected final List<ArithmeticExpr.ArithmeticOp> binaryOpList;

	public ExprBinary(List<ExprType> _exprList, List<ArithmeticExpr.ArithmeticOp> _binaryOpList) {
		super();
		this.exprList = _exprList;
		this.binaryOpList = _binaryOpList;
	}
	
	public Pair<Address, TYPE> translateExpr(Trans trs) throws Exception {
		if (exprList.size() == 1) {
			return (exprList.get(0)).translateExpr(trs);
		}

		Address addr = (exprList.get(0)).translateExpr(trs).first;

		for (int i = 1; i < exprList.size(); i++) {
			ExprType e = exprList.get(i);
			ArithmeticExpr.ArithmeticOp op = binaryOpList == null ? getBinaryOp() : binaryOpList.get(i - 1);

			Pair<Address, TYPE> pair = e.translateExpr(trs);

			Temp tmp = trs.newTemp();
			addr = trs.emitArithmeticOp(tmp, addr, pair.first, op);
		}

		return new Pair<Address, TYPE>(addr, INT.getInstance());
	}
	
	protected ArithmeticExpr.ArithmeticOp getBinaryOp() {
		return null;
	}
}

