package ast.expr;

import ir.ArithmeticExpr;

import java.util.List;

public class ExclusiveOrExpr extends ExprBinary<ExprAnd> {
	public ExclusiveOrExpr(List<ExprAnd> _andExpr, List<ArithmeticExpr.ArithmeticOp> _binaryOp) {
		super(_andExpr, _binaryOp);
	}

	@Override
	protected ArithmeticExpr.ArithmeticOp getBinaryOp() {
		return ArithmeticExpr.ArithmeticOp.XOR;
	}
}
