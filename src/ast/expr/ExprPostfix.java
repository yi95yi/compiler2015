package ast.expr;

import ir.address.Address;

import java.util.List;

import ast.postfix.Postfix;
import type.TYPE;
import util.Pair;
import util.Trans;

public class ExprPostfix implements Expression {
	public List<Postfix> postfixList;
	public ExprPrimary primaryExpr;

	public ExprPostfix(List<Postfix> _posifixList, ExprPrimary _primaryExpr) {
		super();
		this.postfixList = _posifixList;
		this.primaryExpr = _primaryExpr;
	}

	public Pair<Address, TYPE> translateExpr(Trans trs) throws Exception {
		Pair<Address, TYPE> t = this.primaryExpr.translateExpr(trs);
		for (Postfix p : this.postfixList) {
			p.translatePostfix(trs, t);
		}
		return t;
	}

}
