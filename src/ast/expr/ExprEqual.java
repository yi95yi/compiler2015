package ast.expr;

import ir.ArithmeticExpr;

import java.util.List;

public class ExprEqual extends ExprBinary<ExprRelation> {
	public ExprEqual(List<ExprRelation> _relationExpr, List<ArithmeticExpr.ArithmeticOp> _binaryOp) {
		super(_relationExpr, _binaryOp);
	}
}
