package ast.expr;

import ir.address.Address;
import ir.address.IntegerConst;
import type.CHAR;
import type.TYPE;
import util.Pair;
import util.Trans;

public class ConstChar extends Constant {
	private final String value;

	public ConstChar(String _string) {
		value = _string;
	}

	@Override
	public Pair<Address, TYPE> translateExpr(Trans trs) throws Exception {
		if (value == null) {
			return null;
		}
		int c = (int) value.replaceAll("\\\\n", "\n").charAt(1);
		return new Pair<Address, TYPE>(new IntegerConst(c), CHAR.getInstance());
	}
}
