package ast.expr;

import ir.ArithmeticExpr;
import ir.Move;
import ir.RelationExpr;
import ir.address.Address;
import ir.address.IntegerConst;
import ir.address.Name;
import ir.address.Temp;
import type.INT;
import type.POINTER;
import type.TYPE;
import util.Pair;
import util.Trans;

//TODO
public class ExprUnary implements Expression {
	final public static int POSTFIXEXPR = 0;
	final public static int PREINC = 1;
	final public static int PREDEC = 2;
	final public static int UNARYOP = 3;
	final public static int SIZEOF = 4;
	final public static int SIZEOFNAME = 5;

	public int exprType = -1;
	public Expression expression;
	public RelationExpr.RelationOp unaryOp;

	public ExprUnary(int _exprType, Expression _expression) {
		super();
		this.exprType = _exprType;
		this.expression = _expression;
	}

	public ExprUnary(int _exprType, RelationExpr.RelationOp _unaryOp, Expression _expression) {
		this(_exprType, _expression);
		this.unaryOp = _unaryOp;
	}
	
	public Pair<Address, TYPE> translateExpr(Trans trs) throws Exception {

		int eType = this.exprType;
		if (eType == ExprUnary.POSTFIXEXPR)
			return ((ExprPostfix) this.expression).translateExpr(trs);

		if (eType == ExprUnary.PREDEC || eType == ExprUnary.PREINC) {
			Pair<Address, TYPE> pair = ((ExprUnary) this.expression).translateExpr(trs);
			ArithmeticExpr.ArithmeticOp op = eType == ExprUnary.PREDEC ? ArithmeticExpr.ArithmeticOp.SUB : ArithmeticExpr.ArithmeticOp.ADD;
			Temp tmp = trs.newTemp();
			trs.emitArithmeticOp(tmp, pair.first, new IntegerConst(1), op);
			trs.emitMove(pair.first, tmp);
			return pair;
		}

		if (eType == ExprUnary.SIZEOF || eType == ExprUnary.SIZEOFNAME) {
			TYPE type = eType == ExprUnary.SIZEOFNAME ? ((Type_Name) this.expression).translateTypeName(trs)
					: ((ExprUnary) this.expression).translateExpr(trs).second;
			Temp temp = trs.newTemp(), tt = trs.newTemp();
			trs.emit(new Move(tt, type.size));
			trs.emit(new Move(temp, tt, "    #sizeof"));
			return new Pair<Address, TYPE>(temp, INT.getInstance());
		}

		if (eType == ExprUnary.UNARYOP)
			return translateUnaryExpr(trs, this.unaryOp, (ExprCast) this.expression);
		return null;
	}

	private Pair<Address, TYPE> translateUnaryExpr(Trans trs, RelationExpr.RelationOp op, ExprCast castExpr) throws Exception {
		Pair<Address, TYPE> pair = castExpr.translateExpr(trs);
		Address ret = null;
		TYPE type = null;

		if (op == RelationExpr.RelationOp.AND) {
			
			ret = trs.newTemp();
			trs.emitGetAddress(ret, pair.first, pair.second);
		}

		if (op == RelationExpr.RelationOp.PLUS) {
			ret = pair.first;
			type = pair.second;
		}

		if (op == RelationExpr.RelationOp.MINUS || op == RelationExpr.RelationOp.NOT || op == RelationExpr.RelationOp.TILDE) {
			Temp tmp = trs.loadToTemp(pair.first);
			ret = trs.newTemp();
			trs.emit(new RelationExpr((Temp) ret, op, tmp));
			type = pair.second;
		}

		if (op == RelationExpr.RelationOp.STAR) {
			Temp tmp = null;
			if (pair.first instanceof Name)
				tmp = trs.loadToTemp(pair.first);
			if (pair.first instanceof Temp)
				tmp = (Temp) pair.first;
			if (tmp != null) {
				type = ((POINTER) pair.second).elementType;
				return new Pair<Address, TYPE>(new Name(tmp), type);
			} else
				System.err.println("WTF");
		}

		return new Pair<Address, TYPE>(ret, type);
	}

}
