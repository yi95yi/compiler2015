package ast.expr;

import ast.type.Type_Specifier;
import ir.address.Address;
import type.POINTER;
import type.TYPE;
import util.Pair;
import util.Trans;

public class Type_Name implements Expression {
	public Type_Specifier typeSpecifier = null;
	public int numOfStars;

	public Type_Name(Type_Specifier _typeSpecifier, int _numOfStars) {
		this.typeSpecifier = _typeSpecifier;
		this.numOfStars = _numOfStars;
	}
	
	public TYPE translateTypeName(Trans trs) throws Exception {
		TYPE t = this.typeSpecifier.translateType(trs);
		for (int i = 0; i < this.numOfStars; i++)
			t = new POINTER(t);
		return t;
	}

	@Override
	public Pair<Address, TYPE> translateExpr(Trans trs) throws Exception {
		return null;
	}
	
}
