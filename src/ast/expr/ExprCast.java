package ast.expr;

import ir.address.Address;
import type.TYPE;
import util.Pair;
import util.Trans;

public class ExprCast implements Expression {
	private final Type_Name typeName;
	private final Expression expression;

	public ExprCast(Type_Name _typeName, Expression _expression) {
		super();
		this.typeName = _typeName;
		this.expression = _expression;
	}
	
	public Pair<Address, TYPE> translateExpr(Trans trs) throws Exception {
		if (this.expression instanceof ExprUnary)
			return ((ExprUnary) this.expression).translateExpr(trs);

		if (this.expression instanceof ExprCast)
			return new Pair<Address, TYPE>(
					((ExprCast) this.expression).translateExpr(trs).first,
					this.typeName.translateTypeName(trs));
		return null;
	}


}
