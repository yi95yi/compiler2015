package ast.expr;

import ir.ArithmeticExpr;
import ir.Goto;
import ir.IfFalse;
import ir.LabelQuad;
import ir.address.Address;
import ir.address.IntegerConst;
import ir.address.Label;
import ir.address.Name;
import ir.address.Temp;

import java.util.List;

import type.INT;
import type.TYPE;
import util.Pair;
import util.Trans;

public class LogicalAndExpr extends ExprBinary<InclusiveOrExpr> {
	public LogicalAndExpr(List<InclusiveOrExpr> _inclusiveOrExpr,
			List<ArithmeticExpr.ArithmeticOp> _binaryOp) {
		super(_inclusiveOrExpr, _binaryOp);
	}

	public Pair<Address, TYPE> translateExpr(Trans trs) throws Exception {
		if (this.exprList.size() == 1)
			return this.exprList.get(0).translateExpr(trs);

		Label failLabel = new Label(), endLabel = new Label();
		for (InclusiveOrExpr expr : this.exprList) {
			Pair<Address, TYPE> pair = expr.translateExpr(trs);
			if (pair.first instanceof Name)
				pair.first = trs.loadToTemp(pair.first);
			trs.emit(new IfFalse(pair.first, failLabel));
		}

		Temp temp = trs.newTemp();
		trs.emitMove(temp, new IntegerConst(1));
		trs.emit(new Goto(endLabel));

		trs.emit(new LabelQuad(failLabel));
		trs.emitMove(temp, new IntegerConst(0));
		trs.emit(new LabelQuad(endLabel));
		return new Pair<Address, TYPE>(temp, INT.getInstance());
	}
}
