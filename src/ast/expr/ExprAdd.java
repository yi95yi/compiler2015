package ast.expr;

import ir.ArithmeticExpr;
import ir.address.Address;
import ir.address.Temp;

import java.util.List;

import type.INT;
import type.POINTER;
import type.TYPE;
import util.Pair;
import util.Trans;

public class ExprAdd extends ExprBinary<ExprMultiply> {
	public ExprAdd(List<ExprMultiply> _multiplyExpr, List<ArithmeticExpr.ArithmeticOp> _binaryOp) {
		super(_multiplyExpr, _binaryOp);
	}
	
	public Pair<Address, TYPE> translateExpr(Trans trs) throws Exception {
		Pair<Address, TYPE> p = this.exprList.get(0).translateExpr(trs);
		Address addr = p.first;
		TYPE type = p.second;

		for (int i = 1; i < this.exprList.size(); i++) {
			ArithmeticExpr.ArithmeticOp op = this.binaryOpList.get(i - 1);
			Pair<Address, TYPE> pair = this.exprList.get(i).translateExpr(trs);
			Address other = null;

			if (type instanceof POINTER && op == ArithmeticExpr.ArithmeticOp.ADD) {
				Temp offset = trs.newTemp();
				other = trs.emitArithmeticOp(offset, type.size, pair.first, ArithmeticExpr.ArithmeticOp.MUL);
			} else {
				other = pair.first;
				type = INT.getInstance();
			}

			Temp tmp = trs.newTemp();
			addr = trs.emitArithmeticOp(tmp, addr, other, op);
		}

		return new Pair<Address, TYPE>(addr, type);
	}
}
