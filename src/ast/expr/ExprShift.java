package ast.expr;

import ir.ArithmeticExpr;

import java.util.List;

public class ExprShift extends ExprBinary<ExprAdd> {
	public ExprShift(List<ExprAdd> _addExpr, List<ArithmeticExpr.ArithmeticOp> _binaryOp) {
		super(_addExpr, _binaryOp);
	}
}
