package ast.expr;

import ir.address.Address;

import java.util.List;

import type.TYPE;
import util.Pair;
import util.Trans;

public class Expr implements Expression {
	private final List<ExprAssign> assignExprList;

	public Expr(List<ExprAssign> _assignExprList) {
		super();
		assignExprList = _assignExprList;
	}

	public Pair<Address, TYPE> translateExpr(Trans trs) throws Exception {
		Pair<Address, TYPE> ret = null;
		for (ExprAssign e : this.assignExprList) {
			ret = e.translateExpr(trs);
		}
		return ret;
	}

}
