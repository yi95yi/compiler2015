package ast.expr;

import ir.ArithmeticExpr;

import java.util.List;

public class ExprAnd extends ExprBinary<ExprEqual> {

	public ExprAnd(List<ExprEqual> _equalExpr, List<ArithmeticExpr.ArithmeticOp> _binaryOp) {
		super(_equalExpr, _binaryOp);
	}

	@Override
	protected ArithmeticExpr.ArithmeticOp getBinaryOp() {
		return ArithmeticExpr.ArithmeticOp.AND;
	}
}

