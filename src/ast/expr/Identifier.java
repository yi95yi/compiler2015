package ast.expr;

import ir.address.Address;
import type.TYPE;
import util.*;

public class Identifier implements Expression {
	public final Symbol symbol;

	public Identifier(Symbol _symbol) {
		this.symbol = _symbol;
	}

	@Override
	public Pair<Address, TYPE> translateExpr(Trans trs) throws Exception {
		TYPE t = trs.env.getByIdenName(symbol);
		Address a = trs.env.getAddr(symbol);
		return new Pair<Address, TYPE>(a, t);
	}
}
