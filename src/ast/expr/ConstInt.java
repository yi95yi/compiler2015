package ast.expr;

import ir.address.Address;
import ir.address.IntegerConst;
import type.INT;
import type.TYPE;
import util.Pair;
import util.Trans;

public class ConstInt extends Constant {
	private final int value;

	public ConstInt(int _intConst) {
		value = _intConst;
	}

	@Override
	public Pair<Address, TYPE> translateExpr(Trans trs) throws Exception {
		return new Pair<Address, TYPE>(new IntegerConst(value),
				INT.getInstance());
	}
}
