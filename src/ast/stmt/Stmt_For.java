package ast.stmt;

import ast.expr.Expr;
import util.Trans;

public class Stmt_For extends Stmt_Iterate {
	public final Expr begin, end; // heritage condition and stmt

	public Stmt_For(Expr _begin, Expr _condition, Expr _end, Stmt _stmt) {
		super(_condition, _stmt);
		this.begin = _begin;
		this.end = _end;
	}

	@Override
	public void translateStmtBegin(Trans trs) throws Exception {
		if (begin != null) {
			begin.translateExpr(trs);
		}
	}

	@Override
	public void translateStmtEnd(Trans trs) throws Exception {
		if (end != null) {
			end.translateExpr(trs);
		}
	}
}
