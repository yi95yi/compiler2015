package ast.stmt;

import ast.expr.Expr;
import util.Trans;

public class Stmt_While extends Stmt_Iterate {
	// heritage condition and stmt
	public Stmt_While(Expr _condition, Stmt _stmt) {
		super(_condition, _stmt);
	}

	@Override
	public void translateStmtBegin(Trans trs) throws Exception {
	}

	@Override
	public void translateStmtEnd(Trans trs) throws Exception {
	}
}
