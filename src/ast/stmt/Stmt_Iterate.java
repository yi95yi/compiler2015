package ast.stmt;

import ast.expr.Expr;
import ir.Goto;
import ir.IfFalse;
import ir.LabelQuad;
import ir.address.Address;
import ir.address.Label;
import ir.address.Temp;
import type.TYPE;
import util.*;

public abstract class Stmt_Iterate implements Stmt {
	private final Expr condition;
	private final Stmt stmt;
	
	public Stmt_Iterate(Expr condition, Stmt stmt) {
		super();
		this.condition = condition;
		this.stmt = stmt;
	}

	public void translateStmt(Trans trs, TYPE type, Label funcLabel,	Label breakLabel, Label continueLabel) throws Exception
	{
		Label iterLabel = new Label();
		continueLabel = new Label();
		Label endLabel = new Label();
		this.translateStmtBegin(trs);

		trs.emit(new LabelQuad(iterLabel));
		Pair<Address, TYPE> tmp = condition == null? null:condition.translateExpr(trs);
		if(tmp!=null)
		{
			Address addr = tmp.first;
			Temp temp = trs.loadToTemp(addr);
			trs.emit(new IfFalse(temp, endLabel));
		}

		if (this.stmt != null) {
			this.stmt.translateStmt(trs, type, funcLabel, endLabel, continueLabel);
		}

		trs.emit(new LabelQuad(continueLabel));
		this.translateStmtEnd(trs);
		trs.emit(new Goto(iterLabel));

		trs.emit(new LabelQuad(endLabel));
	}
	
	public abstract void translateStmtBegin(Trans trs) throws Exception;
	
	public abstract void translateStmtEnd(Trans trs) throws Exception;

}
