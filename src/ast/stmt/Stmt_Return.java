package ast.stmt;

import ast.expr.Expr;
import ir.Leave;
import ir.Return;
import ir.address.Address;
import ir.address.Label;
import ir.address.Name;
import ir.address.Temp;
import type.RECORD;
import type.TYPE;
import util.Pair;
import util.Trans;

public class Stmt_Return extends Stmt_Jump {
	public Expr expr;

	public Stmt_Return(Expr _expr) {
		super();
		this.expr = _expr;
	}

	@Override
	public void translateStmt(Trans trs, TYPE type, Label funcLabel,
			Label breakLabel, Label continueLabel) throws Exception {
		if (expr != null) {
			Pair<Address, TYPE> pair = expr.translateExpr(trs);
			Address addr = pair.first;
			if (pair.second instanceof RECORD) {
				if (pair.first instanceof Name) {
					Name ref = (Name) pair.first;
					Temp tmp = trs.loadRefToTemp(ref);
					trs.emit(new Return(tmp));
				}
			} else {
				Temp temp = trs.loadToTemp(addr);
				trs.emit(new Return(temp));
			}
		}
		trs.emit(new Leave(funcLabel, trs.cUnit.level));
	}
}
