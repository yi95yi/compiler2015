package ast.stmt;

import ast.expr.Expr;
import ir.Goto;
import ir.IfFalse;
import ir.LabelQuad;
import ir.address.Address;
import ir.address.Label;
import ir.address.Temp;
import type.TYPE;
import util.Trans;

public class Stmt_Selection implements Stmt {
	private final Expr condition;
	private final Stmt thenStmt, elseStmt;

	public Stmt_Selection(Expr _condition, Stmt _thenStmt, Stmt _elseStmt) {
		super();
		this.condition = _condition;
		this.thenStmt = _thenStmt;
		this.elseStmt = _elseStmt;
	}

	public void translateStmt(Trans trs, TYPE type, Label funcLabel,
			Label breakLabel, Label continueLabel) throws Exception {
		Label elseLabel = new Label();
		Label endLabel = this.elseStmt == null ? elseLabel : new Label();
		Address exprAddress = this.condition == null ? null : this.condition
				.translateExpr(trs).first;

		Temp t = trs.newTemp();
		trs.emitMove(t, exprAddress);
		trs.emit(new IfFalse(t, elseLabel));
		this.thenStmt.translateStmt(trs, type, funcLabel, breakLabel,
				continueLabel);

		if (this.elseStmt != null) {
			trs.emit(new Goto(endLabel));
		}
		trs.emit(new LabelQuad(elseLabel));

		if (this.elseStmt != null) {
			this.elseStmt.translateStmt(trs, type, funcLabel, breakLabel,
					continueLabel);
			trs.emit(new LabelQuad(endLabel));
		}
	}

}
