package ast.stmt;

import ir.address.Label;
import type.TYPE;
import util.Trans;

public interface Stmt {
	public void translateStmt(Trans trs, TYPE type, Label funcLabel,
			Label breakLabel, Label continueLabel) throws Exception;

}
