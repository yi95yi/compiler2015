package ast.stmt;

import ast.expr.Expr;
import ir.address.Label;
import type.TYPE;
import util.Trans;

public class Stmt_Expr implements Stmt {
	private final Expr expr;

	public Stmt_Expr(Expr _expr) {
		super();
		this.expr = _expr;
	}

	@Override
	public void translateStmt(Trans trs, TYPE type, Label funcLabel,
			Label breakLabel, Label continueLabel) throws Exception {
		if (this.expr != null) {
			this.expr.translateExpr(trs);
		}
	}
}
