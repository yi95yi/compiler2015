package ast.stmt;

import ir.Goto;
import ir.address.Label;
import type.TYPE;
import util.Trans;

public class Stmt_Break extends Stmt_Jump {

	@Override
	public void translateStmt(Trans trs, TYPE type, Label funcLabel,
			Label breakLabel, Label continueLabel) throws Exception {
		trs.emit(new Goto(breakLabel));
	}

}
