package ast.stmt;

import ir.address.Label;

import java.util.List;

import ast.Declaration;
import util.*;
import type.TYPE;

public class Stmt_Comp implements Stmt {
	private final List<Declaration> declarationList;
	private final List<Stmt> stmtList;
	
	public Stmt_Comp(List<Declaration> _declarationList, List<Stmt> _stmtList) {
		super();
		this.declarationList = _declarationList;
		this.stmtList = _stmtList;
	}

	@Override
	public void translateStmt(Trans trs, TYPE type, Label funcLabel,
			Label breakLabel, Label continueLabel) throws Exception {
		trs.env.beginScope();
		for (Declaration d : this.declarationList) {
			d.translateDeclaration(trs);
		}
		for (Stmt stmt : this.stmtList) {
			stmt.translateStmt(trs, type, funcLabel, breakLabel, continueLabel);
		}
		trs.env.endScope();
	}
}
