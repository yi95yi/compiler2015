package ast;

import ast.decl.Declarator;
import ast.type.Type_Specifier;
import type.TYPE;
import util.Pair;
import util.Symbol;
import util.Trans;

public class PlainDeclaration {
	private final Type_Specifier typeSpecifier;
	private final Declarator declarator;

	public PlainDeclaration(Type_Specifier _typeSpecifier,
			Declarator _declarator) {
		super();
		this.typeSpecifier = _typeSpecifier;
		this.declarator = _declarator;
	}

	public Pair<TYPE, Symbol> translatePlainDeclaration(Trans trs)
			throws Exception {
		return declarator.translateDeclarator(trs,
				typeSpecifier.translateType(trs));
	}
}
