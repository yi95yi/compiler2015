package ast;

import ir.address.Address;
import ir.address.AddressList;

import java.util.List;

import ast.expr.ExprAssign;
import type.TYPE;
import util.Trans;

public class Initializer {
	private final ExprAssign assignExpr;
	private final List<Initializer> initializerList;

	public Initializer(ExprAssign _assignExpr,
			List<Initializer> _initializerList) {
		super();
		this.assignExpr = _assignExpr;
		this.initializerList = _initializerList;
	}

	public Address translateInitializer(Trans trs, TYPE type) throws Exception {
		if (this.assignExpr != null) {
			return this.assignExpr.translateExpr(trs).first;
		} else {
			AddressList addrList = new AddressList();
			for (Initializer init : this.initializerList) {
				addrList.add(translateInitializer(trs, type));
			}
			return addrList;
		}
	}
}
