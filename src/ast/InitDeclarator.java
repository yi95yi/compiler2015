package ast;

import ast.decl.Declarator;
import util.Symbol;
import type.ARRAY;
import type.STRUCT;
import type.TYPE;
import util.*;
import ir.address.Address;
import ir.address.AddressList;
import ir.address.IntegerConst;
import ir.address.Name;
import ir.address.Temp;

public class InitDeclarator {
	private final Declarator declarator;
	private final Initializer initializer;

	public InitDeclarator(Declarator _declarator, Initializer _initializer) {
		super();
		this.declarator = _declarator;
		this.initializer = _initializer;
	}
	
	public void translateInitDeclarator(Trans trs, TYPE type, boolean isTopLevel) throws Exception 
	{
		Pair<TYPE, Symbol> pair = declarator.translateDeclarator(trs, type);
		Name ref;
		if ((pair.first instanceof ARRAY) && !isTopLevel) {
			if (pair.first.size instanceof IntegerConst &&
					((IntegerConst)pair.first.size).value == trs.arSizeLmt) {
				ref = trs.setArrayAddr;
			} else {
				ref = trs.cUnit.level.getPointer();

				trs.cUnit.level.size += CommonEnv.wordSize;
				Temp res = trs.newTemp();
				Address mallocAddress = trs.env.getAddr(Symbol.symbol("malloc"));
				trs.emitCall(res, mallocAddress, pair.first.size);
				trs.emitMove(ref, res);
			}
		} else { 
			ref = trs.allocate(pair.first.size);
			if (pair.first.size instanceof IntegerConst 
					&& ((IntegerConst) pair.first.size).value == trs.arSizeLmt
					&& trs.setArrayAddr == null) {
				trs.setArrayAddr = trs.allocate(pair.first.size);
			}
		}
		trs.env.putIden(pair.first, pair.second);
		trs.env.putAddr(pair.second, ref);

		if (this.initializer == null)
			return;

		Address addr = initializer.translateInitializer(trs, pair.first);

		if (pair.first instanceof ARRAY)
			trs.initArray(ref, (AddressList) addr, (ARRAY) pair.first);
		else {
			if (pair.first instanceof STRUCT)
				trs.copyStruct(ref, addr, (STRUCT) pair.first);
			else
				trs.emitMove(ref, addr);
		}
	}


}
