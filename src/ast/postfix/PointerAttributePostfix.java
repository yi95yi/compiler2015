package ast.postfix;

import ir.ArithmeticExpr;
import ir.address.Address;
import ir.address.IntegerConst;
import ir.address.Name;
import ir.address.Temp;
import ast.expr.Identifier;
import type.POINTER;
import type.RECORD;
import type.TYPE;
import type.RECORD.RecordField;
import util.Pair;
import util.Symbol;
import util.Trans;

public class PointerAttributePostfix implements Postfix {
	private Identifier identifier;

	public PointerAttributePostfix(Identifier _identifier) {
		super();
		this.identifier = _identifier;
	}

	@Override
	public void translatePostfix(Trans trs, Pair<Address, TYPE> t)
			throws Exception {
		Symbol s = identifier.symbol;
		RECORD r = (RECORD) ((POINTER) t.second).elementType;
		RecordField field = r.getField(s);
		Name ref;
		
		if (t.first instanceof Temp) {
			ref = new Name((Temp) t.first, new IntegerConst(0));
		} else {
			Temp tmp = trs.newTemp();
			trs.emitMove(tmp, t.first);
			ref = new Name(tmp, new IntegerConst(0));
		}
		Temp y = trs.newTemp();
		trs.emitArithmeticOp(y, ((Name) ref).base, field.offset, ArithmeticExpr.ArithmeticOp.ADD);
		t.first = new Name(y, ((Name) ref).offset);
	}
}
