package ast.postfix;

import ir.ArithmeticExpr;
import ir.address.Address;
import ir.address.IntegerConst;
import ir.address.Temp;
import type.TYPE;
import util.Pair;
import util.Trans;

public abstract class SelfPostfix implements Postfix {
	@Override
	public void translatePostfix(Trans trs, Pair<Address, TYPE> t)
			throws Exception {
		ArithmeticExpr.ArithmeticOp op = this.getBinaryOp();
		Temp temp = trs.newTemp();
		Temp temp1 = trs.loadToTemp(t.first);
		trs.emitArithmeticOp(temp, t.first, new IntegerConst(1), op);
		trs.emitMove(t.first, temp);
		t.first = temp1;
	}
	
	protected abstract ArithmeticExpr.ArithmeticOp getBinaryOp();
}
