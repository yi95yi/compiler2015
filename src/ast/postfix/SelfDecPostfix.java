package ast.postfix;

import ir.ArithmeticExpr;

public class SelfDecPostfix extends SelfPostfix {

	@Override
	protected ArithmeticExpr.ArithmeticOp getBinaryOp() {
		return ArithmeticExpr.ArithmeticOp.SUB;
	}

}
