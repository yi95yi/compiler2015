package ast.postfix;

import ast.expr.Expr;
import ir.ArithmeticExpr;
import ir.address.Address;
import ir.address.Temp;
import type.ARRAY;
import type.POINTER;
import type.TYPE;
import util.Pair;
import util.Trans;

public class ArrayPostfix implements Postfix {
	private final Expr expr;

	public ArrayPostfix(Expr _expr) {
		this.expr = _expr;
	}

	@Override
	public void translatePostfix(Trans trs, Pair<Address, TYPE> t)
			throws Exception {
		if (expr == null) {
			return;
		}
		Pair<Address, TYPE> pair = expr.translateExpr(trs);

		Address offset = trs.newTemp();
		offset = trs
				.emitArithmeticOp((Temp) offset,
						((POINTER) t.second).elementType.size, pair.first,
						ArithmeticExpr.ArithmeticOp.MUL);

		if (((POINTER) t.second).elementType instanceof ARRAY) {
			Temp base = trs.newTemp();
			t.first = trs.emitArithmeticOp(base, t.first, offset, ArithmeticExpr.ArithmeticOp.ADD);
		} else {
			Temp base = t.first instanceof Temp ? (Temp) t.first : trs
					.loadToTemp(t.first);
			t.first = trs.newReference(base, offset);
		}
		t.second = ((POINTER) t.second).elementType;
	}
}
