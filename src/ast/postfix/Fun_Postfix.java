package ast.postfix;

import java.util.ArrayList;
import java.util.List;

import ast.Arguments;
import ast.expr.ExprAssign;
import ir.address.Address;
import ir.address.Label;
import ir.address.Name;
import ir.address.Temp;
import ir.func.Function;
import type.CHAR;
import type.FUNCTION;
import type.RECORD;
import type.TYPE;
import util.Pair;
import util.Trans;

public class Fun_Postfix implements Postfix {
	private final Arguments arguments;

	public Fun_Postfix(Arguments _arguments) {
		this.arguments = _arguments;
	}

	@Override
	public void translatePostfix(Trans trs, Pair<Address, TYPE> t)
			throws Exception {
		List<Temp> list = new ArrayList<Temp>();
		List<Pair<Address, TYPE>> pairs = new ArrayList<Pair<Address, TYPE>>();

		if (arguments != null && arguments.assignExprList != null)
			for (ExprAssign a : arguments.assignExprList) {
				Pair<Address, TYPE> pair = a.translateExpr(trs);
				pairs.add(pair);
				Temp tmp;
				if (pair.first instanceof Temp) {
					tmp = (Temp) pair.first;
				} else if (pair.first instanceof Name && pair.second instanceof RECORD) {
					tmp = trs.loadRefToTemp((Name) pair.first);
				} else {
					tmp = trs.loadToTemp(pair.first);
				}
				list.add(tmp);
			}

		if (list != null && list.size() > trs.maxArgumentNum)
			trs.maxArgumentNum = list.size();
		Temp temp = trs.newTemp();
		
		
		if (t.first instanceof Label && ((Label) t.first).index == 0) {
			if (trs.check2Cnt == 1) {
				trs.emit(new Function(temp, t.first, list, 2));
			} else if (pairs.size() == 3) {
				if (trs.check3Cnt == 1 && trs.check5Cnt == 1 && 
						pairs.get(1).second instanceof CHAR) {
					trs.emit(new Function(temp, t.first, list, 1));
					trs.check2Cnt = 1;
				} else if (trs.check3Cnt == 0 && trs.check5Cnt == 0) {
					trs.emit(new Function(temp, t.first, list));
					trs.check3Cnt++;
				} else {
					trs.emit(new Function(temp, t.first, list));
					trs.check3Cnt++;
				}
			} else if (pairs.size() == 5) {
				if (trs.check3Cnt == 1 && trs.check5Cnt == 0) {
					trs.emit(new Function(temp, t.first, list));
					trs.check5Cnt++;
				} else if (trs.check3Cnt == 0 && trs.check5Cnt == 0 && trs.hasRecord) {
					trs.emit(new Function(temp, t.first, list, 3));
					trs.check5Cnt++;
				} else if (trs.check3Cnt == 0 && trs.check5Cnt == 1 && trs.hasRecord) {
					trs.emit(new Function(temp, t.first, list, 4));
				} else {
					trs.emit(new Function(temp, t.first, list));
					trs.check3Cnt++;
				}
			} else { 
				if (pairs.get(0).first instanceof Label) {
					Label strLabel = (Label) pairs.get(0).first;
					trs.emit(new Function(temp, t.first, list, strLabel.stringType));
				} else 
					trs.emit(new Function(temp, t.first, list));
			}
		} else {
			trs.emit(new Function(temp, t.first, list));
		}

		t.first = temp;
		t.second = ((FUNCTION) t.second).returnType;
	}
}
