package ast.postfix;

import ir.ArithmeticExpr;

public class SelfIncPostfix extends SelfPostfix {

	@Override
	protected ArithmeticExpr.ArithmeticOp getBinaryOp() {
		return ArithmeticExpr.ArithmeticOp.ADD;
	}

}
