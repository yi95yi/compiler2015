package ast.postfix;

import ir.ArithmeticExpr;
import ir.address.Address;
import ir.address.Name;
import ir.address.Temp;
import ast.expr.Identifier;
import type.ARRAY;
import type.RECORD;
import type.TYPE;
import type.RECORD.RecordField;
import util.Pair;
import util.Symbol;
import util.Trans;

public class ValueAttributePostfix implements Postfix {
	private final Identifier identifier;

	public ValueAttributePostfix(Identifier _identifier) {
		super();
		this.identifier = _identifier;
	}

	@Override
	public void translatePostfix(Trans trs, Pair<Address, TYPE> t)
			throws Exception {
		Symbol s = identifier.symbol;
		RecordField field = ((RECORD) t.second).getField(s);

		Temp temp = trs.loadRefToTemp(t.first);
		Temp ptr = trs.newTemp();
		trs.emitArithmeticOp(ptr, temp, field.offset, ArithmeticExpr.ArithmeticOp.ADD);
		if (field.type instanceof ARRAY) {
			t.first = ptr;
		} else {
			t.first = new Name(ptr);
		}
		t.second = field.type;
	}
}
