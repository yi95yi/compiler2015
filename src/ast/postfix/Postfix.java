package ast.postfix;

import ir.address.Address;
import type.TYPE;
import util.Pair;
import util.Trans;

public interface Postfix {
	public void translatePostfix(Trans trs, Pair<Address, TYPE> t)
			throws Exception;
}
