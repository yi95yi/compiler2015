package ast.decl;

import java.util.List;

import ast.Parameters;
import ast.PlainDeclarator;
import ast.expr.Identifier;
import type.TYPE;
import util.FunctionBuilder;
import util.Pair;
import util.Symbol;
import util.Trans;

public class Fun_Declarator extends Declarator {
	private final Parameters parameters;

	public Fun_Declarator(PlainDeclarator _plainDeclarator,
			Parameters _parameters) {
		super(_plainDeclarator);
		this.parameters = _parameters;
	}

	@Override
	protected Pair<TYPE, Symbol> translateDeclarator(Trans trs, TYPE type,
			int starCount, Identifier id) throws Exception {
		if (this.parameters == null)
			return new Pair<TYPE, Symbol>(FunctionBuilder.build(type, starCount,
					null, false, false), id.symbol);
		else {
			List<Pair<TYPE, Symbol>> params = parameters.translateParameters(trs);
			return new Pair<TYPE, Symbol>(FunctionBuilder.build(type, starCount,
					params, this.parameters.isVariableParameter, false), id.symbol);
		}
	}
}
