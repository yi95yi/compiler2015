package ast.decl;

import ast.PlainDeclarator;
import ast.expr.Identifier;
import type.TYPE;
import util.Pair;
import util.Symbol;
import util.Trans;

public abstract class Declarator {
	private PlainDeclarator plainDeclarator;

	public Declarator(PlainDeclarator _plainDeclarator) {
		super();
		this.plainDeclarator = _plainDeclarator;
	}

	public Pair<TYPE, Symbol> translateDeclarator(Trans trs, TYPE type)
			throws Exception {
		int starCount = getStarCount();
		Identifier id = getId();
		
		return this.translateDeclarator(trs, type, starCount, id);
	}

	protected abstract Pair<TYPE, Symbol> translateDeclarator(Trans trs,
			TYPE type, int starCount, Identifier id) throws Exception;

	protected Identifier getId() {
		return this.plainDeclarator.identifier;
	}

	protected int getStarCount() {
		return this.plainDeclarator.numOfStars;
	}
}
