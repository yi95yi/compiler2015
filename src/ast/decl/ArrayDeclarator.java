package ast.decl;

import ir.ArithmeticExpr;
import ir.address.Address;
import ir.address.IntegerConst;
import ir.address.Name;
import ir.address.Temp;

import java.util.List;

import ast.PlainDeclarator;
import ast.expr.ExprConst;
import ast.expr.Identifier;
import util.Symbol;
import type.ARRAY;
import type.INT;
import type.POINTER;
import type.TYPE;
import util.Pair;
import util.Trans;

public class ArrayDeclarator extends Declarator {
	private final List<ExprConst> constExprList;

	public ArrayDeclarator(PlainDeclarator plainDeclarator,
			List<ExprConst> _constExprList) {
		super(plainDeclarator);
		this.constExprList = _constExprList;
	}

	@Override
	protected Pair<TYPE, Symbol> translateDeclarator(Trans trs, TYPE type,
			int starCount, Identifier id) throws Exception {
		for (int i = 0; i < starCount; i++)
			type = new POINTER(type);

		Address prevSize = type.size;

		if (prevSize == null)
			System.err.println("WTF");

		for (int i = this.constExprList.size() - 1; i >= 0; i--) {
			ExprConst constExpr = this.constExprList.get(i);
			if(constExpr == null)
				continue;
			Address cap = constExpr.translateExpr(trs).first;

			Temp size = trs.newTemp();
			Address addr = trs.emitArithmeticOp(size, cap, prevSize, ArithmeticExpr.ArithmeticOp.MUL);
			if (addr instanceof IntegerConst) {
				type = new ARRAY(type, (IntegerConst) addr);
			} else {
				Name ref = trs.allocate(INT.getInstance().size);
				trs.emitMove(ref, addr);
				type = new ARRAY(type, ref);
			}
			prevSize = ((ARRAY) type).size;
		}
	
		if (type instanceof ARRAY && ((IntegerConst) type.size).value == 4)
			((IntegerConst) type.size).value = 8;

		return new Pair<TYPE, Symbol>(type, id.symbol);
	}
}
