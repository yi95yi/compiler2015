package ast;

import java.util.*;

import ast.type.Type_Record;
import ast.type.Type_Specifier;
import util.Pair;
import util.Symbol;
import type.TYPE;
import type.VOID;
import util.*;

public class Declaration implements Node {
	public Type_Specifier typeSpecifier = null;
	public Declarators declarators = null;
	public InitDeclarators initDeclarators = null;
	public Envment env;

	public Declaration(Type_Specifier _typeSpecifier, Declarators _declarators) {
		typeSpecifier = _typeSpecifier;
		declarators = _declarators;
	}

	public Declaration(Type_Specifier _typeSpecifier,
			InitDeclarators _initDeclarators) {
		typeSpecifier = _typeSpecifier;
		initDeclarators = _initDeclarators;
	}
	
	private TYPE semantic(Type_Specifier typespecifier) { return null; }
	private void semantic(InitDeclarators initDeclarators, TYPE type) { }
	
	public void semantic(Declaration declaration) throws Exception{
		if (declaration.typeSpecifier instanceof Type_Record) {
			Type_Record t = (Type_Record) declaration.typeSpecifier;
			if (t.recordTypeList == null
					&& declaration.initDeclarators == null
					&& declaration.declarators == null)
				return;
		}
		TYPE type = semantic(declaration.typeSpecifier);
		if (type instanceof VOID) {
			System.out.print("ILLEGAL!!!");
		}
		if (declaration.declarators != null) {
			List<Pair<TYPE, Symbol>> list = new ArrayList<Pair<TYPE, Symbol>>();
			env = new Envment();
			for (Pair<TYPE, Symbol> pair : list)
				env.putIden(pair.first, pair.second);
		} else if (declaration.initDeclarators != null) {
			semantic(declaration.initDeclarators, type);
		}
	}
	
	public void translate(Trans trs) throws Exception 
	{
		trs.isTopLevel = true;
		translateDeclaration(trs);
	}
	
	public void translateDeclaration(Trans trs) throws Exception 
	{
		TYPE type = typeSpecifier.translateType(trs);
		if (declarators != null) 
		{

			List<Pair<TYPE, Symbol>> list = declarators.translateDeclarators(trs, type);

			for (Pair<TYPE, Symbol> pair : list) {
				env.putIden(pair.first, pair.second);
				env.putAddr(pair.second, trs.allocate(pair.first.size));
			}
		} else if (initDeclarators != null) {
			initDeclarators.translateInitDeclarators(trs, type);
		}
	}

}
