package ast;

import java.util.List;

import ast.expr.ExprAssign;

public class Arguments {
	public List<ExprAssign> assignExprList = null;

	public Arguments(List<ExprAssign> _assExprList) {
		super();
		this.assignExprList = _assExprList;
	}
}
