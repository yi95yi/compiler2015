package ast;

import ir.Buster;
import ir.Enter;
import ir.LabelQuad;
import ir.Leave;
import ir.Return;
import ir.address.IntegerConst;
import ir.address.Label;
import ir.address.Name;
import ir.address.Temp;

import java.util.ArrayList;
import java.util.List;

import ast.stmt.Stmt_Comp;
import ast.type.Type_Specifier;
import util.CompUnit;
import util.FunctionBuilder;
import util.OpBuilder;
import util.Pair;
import util.Symbol;
import type.ARRAY;
import type.FUNCTION;
import type.TYPE;
import util.*;

public class Fun_Definition implements Node {
	public Type_Specifier typeSpecifier;
	public PlainDeclarator plainDeclarator;
	public Parameters parameters;
	public Stmt_Comp compoundStmt;
	public Envment env;

	public Fun_Definition(Type_Specifier _typeSpecifier,
			PlainDeclarator _plainDeclarator, Parameters _parameters,
			Stmt_Comp _compoundStmt) {
		this.typeSpecifier = _typeSpecifier;
		this.plainDeclarator = _plainDeclarator;
		this.parameters = _parameters;
		this.compoundStmt = _compoundStmt;
	}
	
	public void translate(Trans trs) throws Exception 
	{
			trs.isTopLevel = false;
			translateFunctionDefinition(trs);
	}

	public void translateFunctionDefinition(Trans trs) throws Exception {
		trs.cUnit = new CompUnit();

		TYPE declType = typeSpecifier.translateType(trs);
		List<Pair<TYPE, Symbol>> paramList = null;
		if (parameters != null)
		{
			paramList = parameters.translateParameters(trs);
		}
		
		if (paramList != null && paramList.size() > trs.maxArgumentNum)
			trs.maxArgumentNum = paramList.size();

		boolean varparams = parameters == null ? false
				: parameters.isVariableParameter;

		if (plainDeclarator.identifier.symbol.equals(Symbol.symbol("main"))) {
			paramList = null;
		}
		FUNCTION func = FunctionBuilder.build(declType,
				plainDeclarator.numOfStars, paramList, varparams, true);

		Label funcLabel = new Label(true);
		trs.emitLabel(funcLabel);
		List<Name> params = new ArrayList<Name>();

		trs.env.putFunc(func, plainDeclarator.identifier.symbol);
		trs.env.putAddr(plainDeclarator.identifier.symbol, funcLabel);

		trs.env.beginScope();

		if (paramList != null)
			for (Pair<TYPE, Symbol> pair : paramList) {
				trs.env.putIden(pair.first, pair.second);
				Name ref;
				if (pair.first instanceof ARRAY) {
					ref = trs.allocate(new IntegerConst(4), true);
				} else
					ref = trs.allocate(pair.first.size, true);
				trs.env.putAddr(pair.second, ref);
				params.add(ref);
			}

		trs.emit(new Enter(funcLabel, trs.cUnit.level, params));

		for (int i = CommonEnv.paraRegisterNum; i < params.size(); ++i) {
			trs.emitMove(params.get(i), new Name(Temp.v1, new IntegerConst(i
					* CommonEnv.wordSize)));
		}

		if (plainDeclarator.identifier.symbol.equals(Symbol.symbol(CommonEnv.ufName))) {
			Label pLabel = new Label();
			Label npLabel = new Label();
			trs.emit(new Buster("addi $t8, $t8, 1"));
			OpBuilder.emitHelp(trs.cUnit, pLabel);
			trs.emit(new LabelQuad(npLabel));
			trs.emit(new Return(new IntegerConst(0)));
			trs.emit(new Leave(funcLabel, trs.cUnit.level));
			trs.emit(new LabelQuad(pLabel));
			trs.emit(new Return(new IntegerConst(1)));
			trs.emit(new Leave(funcLabel, trs.cUnit.level));
		} else 
			compoundStmt.translateStmt(trs, declType, funcLabel, null, null);
		trs.env.endScope();

		trs.emit(new Leave(funcLabel, trs.cUnit.level));

		trs.cUnits.add(trs.cUnit);
		trs.cUnit = trs.cUnits.get(0);
	}
	
}
