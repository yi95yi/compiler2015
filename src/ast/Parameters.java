package ast;

import java.util.ArrayList;
import java.util.List;

import type.TYPE;
import util.Pair;
import util.Symbol;
import util.Trans;

public class Parameters {
	public List<PlainDeclaration> plainDeclarationList = null;
	public boolean isVariableParameter;

	public Parameters(List<PlainDeclaration> _plainDeclaratorList,
			boolean _variableParameter) {
		this.plainDeclarationList = _plainDeclaratorList;
		this.isVariableParameter = _variableParameter;
	}
	
	public List<Pair<TYPE, Symbol>> translateParameters(Trans trs) throws Exception 
	{
		ArrayList<Pair<TYPE, Symbol>> params = new ArrayList<Pair<TYPE, Symbol>>();

		for (PlainDeclaration p : plainDeclarationList) {
			params.add(p.translatePlainDeclaration(trs));
		}

		return params;
	}

}
