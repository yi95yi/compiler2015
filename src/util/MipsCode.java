package util;

import ir.Quadruple;

import java.io.PrintStream;
import java.util.ArrayList;


public class MipsCode {
	
	private ArrayList<String> cmds = new ArrayList<String>();
	private String align = ".align 2";

	public void codeGen(MipsAsm assem) {
		cmds.add(assem.toString());
	}

	public void gen(AsmList assems) {
		for (AsmList p = assems; p != null; p = p.tail) {
			codeGen(p.head);
		}
	}

	public void CommGen(int ArgNum, int LSize, int size ) {
		codeGen(new MipsAsm(align));
		codeGen(new MipsAsm("zhangyiyi_whiteSpace: .asciiz \" \""));
		codeGen(new MipsAsm(align));
		codeGen(new MipsAsm("zhangyiyi_newLine: .asciiz \"\\n\""));
		codeGen(new MipsAsm(align));
		codeGen(new MipsAsm(".globl args"));
		codeGen(new MipsAsm("!args:\t.space %", (ArgNum + 1) * size));
		codeGen(new MipsAsm(align));
		codeGen(new MipsAsm(".globl stat"));
		codeGen(new MipsAsm("!stat:\t.space %", (LSize + 1) * size));
		codeGen(new MipsAsm(".text"));
		codeGen(new MipsAsm(align));
		codeGen(new MipsAsm(".globl main"));
		codeGen(new MipsAsm("!main:"));
		codeGen(new MipsAsm("la $gp, stat"));
		codeGen(new MipsAsm("la $v1, args"));
	}
	
	public void print(CompUnit cu, RegAlloc alloc) {
		ArrayList<Quadruple> quads = new ArrayList<Quadruple>(cu.getQuads());
		ArrayList<MipsAsm> assems = new ArrayList<MipsAsm>();

		for (Quadruple q : quads) {
			for (AsmList p = q.gen(); p != null; p = p.tail) {
				assems.add(p.head);
			}
		}

		for (MipsAsm assem : assems) {
			cmds.add(assem.toString());
		}
	}

	public void flush(PrintStream out) {
		for (String line : cmds) {
			out.println(line);
		}
	}
}
