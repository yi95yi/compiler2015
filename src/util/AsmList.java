package util;

public class AsmList {
	
	public MipsAsm head;
	public AsmList tail;

	public AsmList(MipsAsm h, AsmList t) {
		head = h;
		tail = t;
	}

	public static AsmList L(MipsAsm h, AsmList t) {
		return new AsmList(h, t);
	}

	public static AsmList L(MipsAsm h) {
		return new AsmList(h, null);
	}

	public static AsmList L(AsmList a, AsmList b) {
		if (a == null) return b;
		if (b == null) return a;
		return L(a.head, L(a.tail, b));
	}
}
