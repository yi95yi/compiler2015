package util;

import ir.address.Temp;

public class LiveInterval implements CommonEnv, Comparable<LiveInterval> {

	private Temp tmp = null;
	private int st = 0;
	private int ed = 0;
	public int reg = spillReg;
	public boolean isSpilled = false;

	public LiveInterval(Temp t, int pos) {
		this(t, pos, pos);
	}

	public LiveInterval(Temp t, int start, int end){
		tmp = t;
		st = start;
		ed = end;
	}

	public void insert(int pos) {
		if (pos < st)
			st = pos;
		if (pos > ed)
			ed = pos;
	}

	public Temp getTemp() {
		return tmp;
	}

	public int getStartPoint() {
		return st;
	}

	public int getEndPoint() {
		return ed + 1;
	}

	@Override
	public String toString() {
		return "{interval:[" + st + ", " + ed + "],register:" + reg + "}";
	}

	@Override
	public int compareTo(LiveInterval other) {
		return this.st - other.st;
	}
}
