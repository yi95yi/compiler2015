package util;

import ir.address.IntegerConst;
import ir.address.Name;
import ir.address.Temp;

import java.util.HashSet;

import util.CommonEnv;

public class Level implements CommonEnv {
	public int index = 0; // number of used registers in IR
	public int size = 0; // number(size) of local variables
	private HashSet<Integer> usedRegisters = new HashSet<Integer>();
	
	public int frameSize() {
		return size + (index + numOfSavedRegisters + 1) * wordSize;
	}

	public Temp newTemp() {
		return new Temp(this, index++);
	}

	public Name getPointer() {
		return new Name(Temp.sp, new IntegerConst(size));
	}

	public void useRegister(int r) {
		usedRegisters.add(r);
	}

	public AsmList saveRegisters() {
		AsmList saves = null;
		for (int i = 0; i < numOfSavedRegisters; i++) {
			int r = baseOfSavedRegisters + i;
			if (usedRegisters.contains(r)) {
				saves = new AsmList(new MipsAsm("sw $%, %($sp)\t# level save register", regName[r],
							size + (index + i) * wordSize), saves);
			}
		}
		return saves;
	}

	public AsmList loadRegisters() {
		AsmList loads = null;
		for (int i = 0; i < numOfSavedRegisters; i++) {
			int r = baseOfSavedRegisters + i;
			if (usedRegisters.contains(r)) {
				loads = new AsmList(new MipsAsm("lw $%, %($sp)\t# level load register", regName[r],
					size + (index + i) * wordSize), loads);
			}
		}
		return loads;
	}
}
