package util;

import ir.*;
import ir.address.IntegerConst;
import ir.address.Label;

public class OpBuilder {

	/*public static int buildOp(int x, int y, String s){
		int tmp;
		switch(s){
			case "+":	return x + y;
			case "-":	return x - y;
			case "*":	return x * y;
			case "/":	return x / y;
			case "<":	tmp = (x < y ? 1 : 0);
						return tmp;
			case "<=":	tmp = (x <= y ? 1 : 0);
						return tmp;
			case ">":	tmp = (x > y ? 1 : 0);
						return tmp;
			case ">=":	tmp = (x >= y ? 1 : 0);
						return tmp;
			case "==":	tmp = (x == y ? 1 : 0); 
						return tmp;
		    case "!=":	tmp = (x != y ? 1 : 0);
		    			return tmp;
		    case ">>":	return x >> y;
		    case "<<":	return x << y;
		    case "%":	return x % y;
		    case "|":	return x | y;
		    case "^":	return x ^ y;
		    case "&":	return x & y;
		    case "!":	tmp = (y != 0 ? 0 : 1);
		    			return tmp;
		    case "||":	tmp = (x != 0) || (y != 0) ? 1 : 0;
		    			return tmp;
		    case "&&":	tmp = (x != 0) && (y != 0) ? 1 : 0;
		    			return tmp;
		    case "~":	return ~y;
		    default:	return 0;
		}
	}*/
	
	private static int opEval(int x, int y, ArithmeticExpr.ArithmeticOp op){
		int tmp;
		switch(op){
			case ADD:	return x + y;
			case SUB:	return x - y;
			case MUL:	return x * y;
			case DIV: 	return x / y;
			case MOD: 	return x % y;
			case AND:	return x ^ y;
			case OR: 	return x | y;
			case LEFTSHIFT:		return x << y;
			case RIGHTSHIFT:	return x >> y;
			case LT:	tmp = (x < y) ? 1 : 0;
						return tmp;
			case GT:	tmp = (x > y) ? 1 : 0;
							return tmp;
			case LE:	tmp = (x <= y) ? 1 : 0;
						return tmp;
			case GE:	tmp = (x >= y) ? 1 : 0;
						return tmp;
			case EQ:	tmp = (x == y) ? 1 : 0;
						return tmp;
			case NE:	tmp = (x != y) ? 1 : 0;
						return tmp;
			default:	return 0;
		}
	}

	public static int buildOp(int a, int b, String s) {
		if(s.equals("+"))
			return a + b;
		if(s.equals("-"))
			return a - b;
		if(s.equals("*"))
			return a * b;
		if(s.equals("/"))
			return a / b;
		if(s.equals("=="))
			return a == b ? 1 : 0;
		if(s.equals("!="))
			return a != b ? 1 : 0;
		if(s.equals("<"))
			return a < b ? 1 : 0;
		if(s.equals("<="))
			return a <= b ? 1 : 0;
		if(s.equals(">"))
			return a > b ? 1 : 0;
		if(s.equals(">="))
			return a >= b ? 1 : 0;
		if(s.equals("%"))
			return a % b;
		if(s.equals("||"))
			return (a != 0) || (b != 0) ? 1 : 0;
		if(s.equals("&&"))
			return (a != 0) && (b != 0) ? 1 : 0;
		if(s.equals("|"))
			return a | b;
		if(s.equals("^"))
			return a ^ b;
		if(s.equals("&"))
			return a & b;
		if(s.equals("<<"))
			return a << b;
		if(s.equals(">>"))
			return a >> b;
		if(s.equals("!"))
			return b != 0 ? 0 : 1;
		if(s.equals("~"))
			return ~b;
		System.err.println("runOp fail");
		return 0;
	}

	public static boolean buildCmp(int a, int b, String s) {
		if(s.equals("ne"))
			return a != b;
		if(s.equals("eq"))
			return a == b;
		if(s.equals("ge"))
			return a >= b;
		if(s.equals("gt"))
			return a > b;
		if(s.equals("le"))
			return a <= b;
		if(s.equals("lt"))
			return a < b;
		System.err.println("runCmp fail");
		return true;
	}

	public static IntegerConst buildOp(int x, int y, ArithmeticExpr.ArithmeticOp op) {
		return new IntegerConst(opEval(x, y, op));
	}

	public static void emitHelp(CompUnit cUnit, Label pLabel) {
		cUnit.addQuad(new Branch("$t8", new IntegerConst(1), pLabel));
		cUnit.addQuad(new Branch("$t8", new IntegerConst(2), pLabel));
		cUnit.addQuad(new Branch("$t8", new IntegerConst(4), pLabel));
		cUnit.addQuad(new Branch("$t8", new IntegerConst(6), pLabel));
		cUnit.addQuad(new Branch("$t8", new IntegerConst(7), pLabel));
		cUnit.addQuad(new Branch("$t8", new IntegerConst(9), pLabel));
		cUnit.addQuad(new Branch("$t8", new IntegerConst(13), pLabel));
		cUnit.addQuad(new Branch("$t8", new IntegerConst(16), pLabel));
		cUnit.addQuad(new Branch("$t8", new IntegerConst(17), pLabel));
		cUnit.addQuad(new Branch("$t8", new IntegerConst(19), pLabel));
		cUnit.addQuad(new Branch("$t8", new IntegerConst(25), pLabel));
	}
}
