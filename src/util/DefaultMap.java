package util;

import ir.address.Temp;

public class DefaultMap implements RegAlloc {

	private static DefaultMap instance = null;

	@Override
	public String map(Temp tmp) {
		LiveInterval interval = tmp.getLiveInterval();
		return "$" + regName[interval.reg];
	}
	
	public static DefaultMap getInstance() {
		if (instance == null) {
			instance = new DefaultMap();
		}
		return instance;
	}
}
