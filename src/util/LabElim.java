package util;

import ir.*;
import ir.address.Label;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class LabElim {

	public boolean eliminate(CompUnit u) {
		List<Quadruple> quads = new ArrayList<Quadruple>(u.getQuads());

		for (int i = 0; i < quads.size()-1; i++) {
			if (quads.get(i) instanceof LabelQuad && quads.get(i+1) instanceof LabelQuad) {
				LabelQuad q = (LabelQuad) quads.get(i+1);
				Label l = ((LabelQuad) quads.get(i)).label;
				List<Quadruple> result = new LinkedList<Quadruple>();
				for (Quadruple quad : u.getQuads()) {
					if (quad != q) {
						quad.replaceLabelOf(q.label, l);
						result.add(quad);
					}
				}
				u.setQuads(result);
				return true;
			}
		}

		boolean removed = false;
		List<Quadruple> result = new LinkedList<Quadruple>();
		for (Quadruple quad : u.getQuads()) {
			if (quad instanceof LabelQuad) {
				if (isLabelUsed(quads, ((LabelQuad) quad).label)) {
					result.add(quad);
				}
				else {
					removed = true;
				}
			}
			else {
				result.add(quad);
			}
		}
		if (removed) {
			u.setQuads(result);
		}
		return removed;
	}

	private boolean isLabelUsed(List<Quadruple> quads, Label label) {
		if (label.isFunctionLabel) {
			return true;
		}
		for (Quadruple q : quads) {
			if (label.equals(q.jumpLabel())) {
				return true;
			}
		}
		return false;
	}
}
