package util;

import ir.*;
import ir.address.Address;
import ir.address.AddressList;
import ir.address.IntegerConst;
import ir.address.Label;
import ir.address.Name;
import ir.address.Temp;
import ir.func.Function;

import java.util.List;
import java.util.ArrayList;

import util.Symbol;
import type.*;
import type.RECORD.RecordField;

public class Trans implements CommonEnv {
	public Envment env;
	public boolean isTopLevel = true;
	public int topLevelSize = 0;
	public int check2Cnt = 0, check3Cnt = 0, check5Cnt = 0;
	public boolean hasRecord = false;
	public int arSizeLmt = 4000;
	public Name setArrayAddr = null;
	public CompUnit cUnit;
	public List<DFrag> dataFrags = new ArrayList<DFrag>();
	public List<CompUnit> cUnits = new ArrayList<CompUnit>();
	private final int cuSizeLimit = 10000;
	public int maxArgumentNum;

	public Trans() {
		env = new Envment();
		cUnit = new CompUnit();
		cUnits.add(cUnit);
		maxArgumentNum = 0;
		env.putAddr(Symbol.symbol("printf"), new Label(true));
		env.putAddr(Symbol.symbol("malloc"), new Label(true));
		env.putAddr(Symbol.symbol("getchar"), new Label(true));
	}

	public void emit(Quadruple q) {
		cUnit.addQuad(q);
	}

	public void emitMove(Address target, Address source) {
		if (target instanceof IntegerConst || target instanceof Label) {
			System.err.println("WTF");
			return;
		}

		if (target instanceof Temp) {
			emit(new Move(target, source));
			return;
		}

		if (!(source instanceof Temp)) {
			Temp tmp = newTemp();
			emit(new Move(tmp, source));
			emit(new Move(target, tmp));
			return;
		}

		emit(new Move(target, source));
	}

	public Address emitArithmeticOp(Temp target, Address addr1, Address addr2, ArithmeticExpr.ArithmeticOp op) {
		if (addr1 == null)
			return addr2;

		if (addr1 instanceof IntegerConst && addr2 instanceof IntegerConst) {
			return OpBuilder.buildOp(((IntegerConst) addr1).value,
					((IntegerConst) addr2).value, op);
		}

		Address left, right;
		if (!(addr1 instanceof Temp)) {
			left = newTemp();
			emitMove(left, addr1);
		} else {
			left = addr1;
		}

		if (!(addr2 instanceof Temp) && !(addr2 instanceof IntegerConst)) {
			right = newTemp();
			emitMove(right, addr2);
		} else {
			right = addr2;
		}

		emit(new ArithmeticExpr(target, left, right, op));
		return target;
	}

	public void emitGetAddress(Address left, Address right, TYPE rType) {
		if (right instanceof Name) {
			if (rType.size instanceof IntegerConst) {
				if (left instanceof Temp) {
					emitArithmeticOp((Temp) left, ((Name) right).base, 
							((Name) right).offset,
							ArithmeticExpr.ArithmeticOp.ADD);
					return;
				} else { 
					Temp tmp = newTemp();
					emitArithmeticOp(tmp, ((Name) right).base, ((Name) right).offset,
							ArithmeticExpr.ArithmeticOp.ADD);
					emitMove(left, tmp);
					return;
				}
			} else { 
				emitMove(left, right);
				return;
			}
		} else if (rType instanceof RECORD || rType instanceof ARRAY) {
			emitMove(left, right);
			return;
		}
	}

	public void emitLabel(Label label) {
		emit(new LabelQuad(label));
	}

	private void emitCall(Temp target, Address source, List<Temp> params) {
		if (!(source instanceof Temp) && !(source instanceof Label))
			source = loadToTemp(source);
		emit(new Function(target, source, params));
	}

	public void emitCall(Temp target, Address source, Address param) {
		Temp tmp = param instanceof Temp ? (Temp) param : loadToTemp(param);
		List<Temp> params = new ArrayList<Temp>();
		params.add(tmp);
		emitCall(target, source, params);
	}

	private void emitCall(Temp target, Address source, List<Temp> params, boolean isParameter) {
		if (!(source instanceof Temp) && !(source instanceof Label))
			source = loadToTemp(source);
		emit(new Function(target, source, params, isParameter));
	}

	private void emitCall(Temp target, Address source, Address param, boolean isParameter) {
		Temp tmp = param instanceof Temp ? (Temp) param : loadToTemp(param);
		List<Temp> params = new ArrayList<Temp>();
		params.add(tmp);
		emitCall(target, source, params, isParameter);
	}

	public Name newReference(Temp base, Address offset) {
		if (offset instanceof IntegerConst)
			return new Name(base, (IntegerConst) offset);

		Temp newBase = newTemp();
		Address addr = emitArithmeticOp(newBase, base, offset, ArithmeticExpr.ArithmeticOp.ADD);
		if (addr instanceof IntegerConst)
			return new Name((IntegerConst) addr);
		else 
			return new Name((Temp) addr);
	}

	public Temp newTemp() {
		return cUnit.newTemp();
	}
	
	public Temp loadToTemp(Address addr) {
		Temp tmp = newTemp();
		emit(new Move(tmp, addr));
		return tmp;
	}

	public Temp loadRefToTemp(Address addr) {
		Temp tmp = newTemp();
		if (addr instanceof Temp)
			emitMove(tmp, addr);
		if (addr instanceof Name) {
			Name ref = (Name) addr;
			emitArithmeticOp(tmp, ref.base, ref.offset, ArithmeticExpr.ArithmeticOp.ADD);
		}
		return tmp;
	}
	
	public void copyStruct(Address l, Address r, STRUCT type) {

		Temp left = loadRefToTemp(l);
		Temp right = loadRefToTemp(r);
		Temp offset = loadToTemp(new IntegerConst(0));

		Label startLabel = new Label();
		emit(new LabelQuad(startLabel));

		Temp tmp = newTemp();
		emitMove(newReference(left, offset), newReference(right, offset));
		emitArithmeticOp(offset, offset, new IntegerConst(4), ArithmeticExpr.ArithmeticOp.ADD);
		emitArithmeticOp(tmp, offset, type.size, ArithmeticExpr.ArithmeticOp.LT);
		emit(new IfTrue(tmp, startLabel));
	}

	public Name allocate(Address size) {
		Address mallocAddress = env.getAddr(Symbol.symbol("malloc"));

		if (isTopLevel) {
			if (size instanceof IntegerConst && ((IntegerConst) size).value <= 4) {
				Name ref = new Name(Temp.gp, new IntegerConst(
						topLevelSize));
				emitMove(ref, new IntegerConst(0));
				topLevelSize += ((IntegerConst) size).value;
				return ref;
			} else {

				Temp res = newTemp();
				emitCall(res, mallocAddress, size);

				Name ref;
				ref = new Name(Temp.gp, new IntegerConst(topLevelSize));
				topLevelSize += wordSize;
				emitMove(ref, res);

				return ref;
			}
		}

		Name ref = cUnit.level.getPointer();

		if (size instanceof IntegerConst && ((IntegerConst) size).value <= cuSizeLimit) {
			cUnit.level.size += ((IntegerConst) size).value;
			return ref;
		} else {
			cUnit.level.size += wordSize;

			Temp res = newTemp();
			emitCall(res, mallocAddress, size);
			emitMove(ref, res);

			return ref;
		}
	}

	public Name allocate(Address size, boolean isParameter) {
		Address mallocAddress = env.getAddr(Symbol.symbol("malloc"));
		Name ref = cUnit.level.getPointer();

		if (size instanceof IntegerConst && ((IntegerConst) size).value <= cuSizeLimit) {
			cUnit.level.size += ((IntegerConst) size).value;
			return ref;
		} else {
			cUnit.level.size += wordSize;

			Temp res = newTemp();
			emitCall(res, mallocAddress, size, isParameter);

			return ref;
		}
	}
	private void initStruct(Name ref, AddressList source, STRUCT type) {
		Temp tmp = loadRefToTemp(ref);

		for (int i = 0; i < source.list.size(); i++) {
			RecordField rf = type.fields.get(i);
			Address addr = source.list.get(i);
			if (rf.type instanceof STRUCT)
				initStruct(new Name(tmp), (AddressList) addr, (STRUCT) rf.type);
			else if (rf.type instanceof ARRAY)
				initArray(new Name(tmp), (AddressList) addr, (ARRAY) rf.type);
			else
				emitMove(new Name(tmp), addr);

			emitArithmeticOp(tmp, tmp, rf.type.size, ArithmeticExpr.ArithmeticOp.ADD);
		}
	}

	public void initArray(Name tmp, AddressList source, ARRAY type) {
		Temp ptr = loadToTemp(tmp);
		TYPE eleType = type.elementType;

		for (int i = 0; i < source.list.size(); i++) {
			Address addr = source.list.get(i);
			if (eleType instanceof STRUCT)
				initStruct(new Name(ptr), (AddressList) addr,
						(STRUCT) eleType);
			else if (eleType instanceof ARRAY)
				initArray(new Name(ptr), (AddressList) addr, (ARRAY) eleType);
			else
				emitMove(new Name(ptr), addr);

			emitArithmeticOp(ptr, ptr, eleType.size, ArithmeticExpr.ArithmeticOp.ADD);
		}
	}

	public void emit_Call() 
	{
		emitCall(null, env.getAddr(Symbol.symbol("main")),	new ArrayList<Temp>());
	}

}
