package util;

import util.CommonEnv;
import ir.address.Temp;

public interface RegAlloc extends CommonEnv {

	String map(Temp tmp);
}
