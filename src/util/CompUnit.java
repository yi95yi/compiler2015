package util;

import ir.*;
import ir.address.Temp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class CompUnit {
	public Level level = null;
	public List<Quadruple> quads = null;
	private LinkedList<BasicBlock> blocks = null;
	private ArrayList<LiveInterval> liveIntervals = null;

	public CompUnit() {
		this(new ArrayList<Quadruple>(), new Level());
	}

	public CompUnit(List<Quadruple> q, Level lev){
		quads = q;
		level = lev;
	}

	public Temp newTemp() {
		return level.newTemp();
	}

	public List<Quadruple> getQuads() {
		return quads;
	}

	public Level getLevel() {
		return level;
	}

	public void addQuad(Quadruple q) {
		quads.add(q);
	}

	public void setQuads(List<Quadruple> q) {
		if (!(quads instanceof LinkedList)) {
			quads = new LinkedList<Quadruple>(quads);
		}
		this.quads = q;
	}

	public void findBranches(Analy analyzer) {
		quads = analyzer.findBranches(quads);
	}

	public void findBasicBlocks(Analy analyzer) {
		blocks = analyzer.getBasicBlocks(quads);
	}

	public LinkedList<BasicBlock> getBasicBlocks() {
		return blocks;
	}

	public void findLiveness(Analy analyzer) {
		analyzer.findLiveness(blocks);
	}

	public void findLiveIntervals(Analy analyzer) {
		liveIntervals = new ArrayList<LiveInterval>(analyzer.findLiveIntervals(quads).values());
		Collections.sort(liveIntervals);
	}

	public ArrayList<LiveInterval> getLiveIntervals() {
		return liveIntervals;
	}
}
