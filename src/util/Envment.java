package util;

import util.Symbol;
import util.Table;
import type.CHAR;
import type.FUNCTION;
import type.INT;
import type.POINTER;
import type.RECORD;
import type.TYPE;
import type.VOID;
import ir.address.Address;

public final class Envment {
	public Table typeEnv = null;
	public Table funcEnv = null;
	public Table idenEnv = null;
	public Table addrEnv = null;

	public Envment() {
		initTypeEnv();
		initFuncEnv();
		initIdenEnv();
		initAddrEnv();
	}

	private void initAddrEnv() {
		addrEnv = new Table();

	}

	private void initIdenEnv() {
		idenEnv = new Table();
	}

	private static Symbol _symbol(String n) {
		return Symbol.symbol(n);
	}

	private void initTypeEnv() {
		typeEnv = new Table();
		typeEnv.put(_symbol("int"), INT.getInstance());
		typeEnv.put(_symbol("char"), CHAR.getInstance());
		typeEnv.put(_symbol("void"), VOID.getInstance());
	}

	private void initFuncEnv() {
		funcEnv = new Table();
		funcEnv.put(_symbol("printf"), new FUNCTION(
				new POINTER(CHAR.getInstance()), INT.getInstance(), true, true));
		funcEnv.put(_symbol("malloc"), new FUNCTION(INT.getInstance(), new POINTER(
				VOID.getInstance()), true, true));
		funcEnv.put(_symbol("scanf"), new FUNCTION(new POINTER(CHAR.getInstance()), 
					INT.getInstance(), true, true));
		funcEnv.put(_symbol("getchar"), new FUNCTION(VOID.getInstance(), 
				CHAR.getInstance(), true, true));
	}

	public void beginScope() {
		funcEnv.beginScope();
		typeEnv.beginScope();
		idenEnv.beginScope();
		addrEnv.beginScope();
	}

	public void endScope() {
		funcEnv.endScope();
		typeEnv.endScope();
		idenEnv.endScope();
		addrEnv.endScope();
	}

	public void putIden(TYPE type, Symbol name) {
		idenEnv.put(name, type);
	}

	public void putType(TYPE type, Symbol name) {
		typeEnv.put(name, type);
	}

	public void putFunc(FUNCTION f, Symbol name) {
		funcEnv.put(name, f);
	}

	public void putAddr(Symbol name, Address addr) {
		addrEnv.put(name, addr);
	}

	public TYPE getByTypedefName(Symbol name) {
		Object obj = typeEnv.get(name);
		if (obj == null)
			return null;
		return (TYPE) obj;
	}

	public RECORD getByRecordName(Symbol name) {
		Object obj = typeEnv.get(name);
		if (obj == null)
			return null;
		if (!(obj instanceof RECORD)) {
			System.err.println("warning" + name.toString() + "not a record");
			return null;
		}
		return (RECORD) obj;
	}

	public TYPE getByIdenName(Symbol name) {
		Object obj = idenEnv.get(name);
		if (obj == null)
			obj = funcEnv.get(name);
		if (obj == null)
			return null;
		return (TYPE) obj;
	}

	public Address getAddr(Symbol name) {
		Object obj = addrEnv.get(name);
		if (obj == null)
			return null;
		return (Address) obj;
	}

}
