package util;

public interface CommonEnv {
	static final String[] ifFalseCmp = { "ge", "gt", "le", "lt", "ne", "eq" };

	public static String binOpStr[] = { "+", "-", "*", "/", "%", "<<", ">>", "&",
			"^", "|", "<", "<=", ">", ">=", "==", "!=", "=", "*=", "/=", "%=",
			"+=", "-=", "<<=", ">>=", "+=", "^=", "|=" };

	public static boolean isInvertable[] = {true, false, true, false, false, false, false, true,
		true, true, false, false, false, false, true, true, false, false, false, false,
		false, false, false, false, false, false, false};

	static final String[] binOpAsm = { "add", "sub", "mul", "div", "rem", "sll",
			"srl", "and", "xor", "or", "slt", "sle", "sgt", "sge", "seq",
			"sne"};

	public static String unOpStr[] = { "&", "*", "+", "-", "~", "!" };

	static final String[] regName = { "zero", "at", "v0", "v1", "a0", "a1",
		"a2", "a3", "t0", "t1", "t2", "t3", "t4", "t5", "t6", "t7", "s0",
		"s1", "s2", "s3", "s4", "s5", "s6", "s7", "t8", "t9", "k0", "k1",
		"gp", "sp", "fp", "ra", "spill"};

	static final int wordSize = 4;
	static final int spillReg = regName.length - 1;

	static int baseOfSavedRegisters = 8; 
	static int numOfSavedRegisters = 16; 

	static final int paraRegisterBase = 4; 
	static final int paraRegisterNum = 4; 

	static final String ufName = "compare";
}
