package util;

import ir.address.Temp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.ListIterator;
import java.util.TreeSet;


public class LinearScan implements RegAlloc, Comparator<LiveInterval> {

	private TreeSet<Integer> freeRegs = new TreeSet<Integer>();
	private ArrayList<LiveInterval> active = new ArrayList<LiveInterval>();

	public LinearScan(CompUnit unit, Analy analyzer) {
		for (int i = 0; i < numOfSavedRegisters; i++) {
			freeRegs.add(baseOfSavedRegisters + i);
		}

		unit.findLiveIntervals(analyzer);

		for (LiveInterval interval : unit.getLiveIntervals()) {
			expireOldIntervals(interval);
			if (active.size() == numOfSavedRegisters) {
				spillAtInterval(interval);
			} else {
				interval.reg = getFreeRegister();
				active.add(interval);
			}
		}

		for (LiveInterval interval : unit.getLiveIntervals()) {
			if (!interval.isSpilled)
				unit.getLevel().useRegister(interval.reg);
		}
	}

	public void expireOldIntervals(LiveInterval interval) {
		Collections.sort(active, this);
		ListIterator<LiveInterval> iter = active.listIterator();
		while (iter.hasNext()) {
			LiveInterval j = iter.next();
			if (j.getEndPoint() >= interval.getStartPoint())
				return;
			putFreeRegister(j.reg);
			iter.remove();
		}
	} 

	public void spillAtInterval(LiveInterval interval) {
		LiveInterval spill = active.get(active.size() - 1);
		if (spill.getEndPoint() > interval.getEndPoint()) {
			interval.reg = spill.reg;
			spill.isSpilled = true;
			spill.reg = spillReg;
			active.remove(spill);
			active.add(interval);
		} else {
			interval.isSpilled = true;
			interval.reg = spillReg;
		}
	}

	private int getFreeRegister() {
		return freeRegs.pollFirst();
	}

	private void putFreeRegister(int reg) {
		freeRegs.add(reg);
	}

	@Override
	public String map(Temp tmp) {
		LiveInterval interval = tmp.getLiveInterval();
		return "$" + regName[interval.reg];
	}

	@Override
	public int compare(LiveInterval o1, LiveInterval o2) {
		return o1.getEndPoint() - o2.getEndPoint();
	}
}

