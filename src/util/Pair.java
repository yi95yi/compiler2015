package util;

public class Pair<Type1, Type2> {
	public Type1 first;
	public Type2 second;

	public Pair(Type1 aa, Type2 bb) {
		this.first = aa;
		this.second = bb;
	}
}
