package util;

import ir.Quadruple;
import ir.address.Temp;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Set;

public class BasicBlock {

	public static int cnt = 0;
	private int num = 0; // unique id
	private LinkedList<Quadruple> quads = new LinkedList<Quadruple>();
	private LinkedList<BasicBlock> successors = new LinkedList<BasicBlock>();
	private LinkedList<BasicBlock> predecessors = new LinkedList<BasicBlock>();

	public Set<Temp> IN = new LinkedHashSet<Temp>();
	public Set<Temp> OUT = new LinkedHashSet<Temp>();

	public BasicBlock() {
		num = ++cnt;
	}

	public String toString() {
		return "B" + num;
	}

	// Quad methods
	public void addQuad(Quadruple quad) {
		quads.add(quad);
	}

	public LinkedList<Quadruple> getQuads() {
		return quads;
	}

	public Object getFirstQuad() {
		return quads.get(0);
	}

	public Quadruple getLastQuad() {
		return quads.getLast();
	}

	// BasicBlock methods
	public void addSuccessor(BasicBlock s) {
		successors.add(s);
	}

	public LinkedList<BasicBlock> getSuccessors() {
		return successors;
	}

	public void addPredecessor(BasicBlock p) {
		predecessors.add(p);
	}

	public LinkedList<BasicBlock> getPredecessors() {
		return predecessors;
	}

	// Analysis related methods
	public Set<Temp> def() {
		Set<Temp> set = new LinkedHashSet<Temp>();
		Iterator<Quadruple> iter = quads.descendingIterator();
		while(iter.hasNext()) {
			Quadruple q = iter.next();
			set.addAll(q.def());
			set.removeAll(q.use());
		}
		return set;
	}

	public Set<Temp> use() {
		Set<Temp> set = new LinkedHashSet<Temp>();
		Iterator<Quadruple> iter = quads.descendingIterator();
		while(iter.hasNext()) {
		Quadruple q = iter.next();
		set.removeAll(q.def());
		set.addAll(q.use());
		}
		return set;
	}
}
