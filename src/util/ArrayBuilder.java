package util;

import type.ARRAY;
import type.POINTER;
import type.TYPE;

public class ArrayBuilder {

	public static TYPE build(TYPE type, int starCount, int exprCount) {
		for (int i = 0; i < starCount; i++)
			type = new POINTER(type);
		for (int i = 0; i < exprCount; i++)
			type = new ARRAY(type, 0);
		return  type;
	}

}
