package util;

import ir.*;
import ir.address.Label;
import ir.address.Temp;

import java.util.Set;
import java.util.LinkedHashSet;

public class DFrag extends Quadruple {

	Label label = null;
	String value = null;

	public DFrag(Label t, String v){
		label = t;
		value = v;
	}

	@Override
	public Set<Temp> def() {
		return new LinkedHashSet<Temp>();
	}

	@Override
	public Set<Temp> use() {
		return new LinkedHashSet<Temp>();
	}

	@Override
	public AsmList gen() {
		return L(new MipsAsm("!%:", label),
				L(new MipsAsm(".asciiz \"%\"", escapeChars(value)),
					L(new MipsAsm(".align 2"))));
	}

	private static String escapeChars(String s) {
		StringBuffer buff = new StringBuffer();
		for (int i =0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c == '\n')
				buff.append("\\n");
			else if (c == '\t')
				buff.append("\\t");
			else if (c == '\"')
				buff.append("\\\"");
			else if (c == '\r')
				buff.append("\\r");
			else
				buff.append(c);
		}
		return buff.toString();
	}
}
