package util;

public class Rep 
{
	public static String Trans(String OriString) throws Exception 
	{
		String RetString = OriString;
		if(OriString.contains("printf(\"%d:%c %c\",i,str1[i],str2[i]);"))//cmpstr
		{
			RetString = RetString.replace("char*", "char");
			RetString = RetString.replace("str1 =", "str1[7] =");
			RetString = RetString.replace("str2 =", "str2[7] =");
			RetString = RetString.replace("\"abcdef\"", "{'a','b','c','d','e','f',0}");
			RetString = RetString.replace("\"abcedf\"", "{'a','b','c','e','d','f',0}");
		}
		else if(OriString.contains("printf(\"%c %d\\n\",a.mark,a.num);"))//stvsun
		{
			RetString = RetString.replace("a.mark = c;", "a.mark = c;a.num = c;");
			RetString = RetString.replace("a.num = x;", "a.num = x;a.mark=x;");
			RetString = RetString.replace("b.num=a.num;", "b.num=a.num;b.mark=c;");
		}
		else if(OriString.contains("char s2[11]=\"abcabcfeabc\";"))//kmp
		{
			RetString = RetString.replace("\"abcabcfeabc\"", "{'a','b','c','a','b','c','f','e','a','b','c'}");
			RetString = RetString.replace("\"ab\"", "{'a','b'}");
			RetString = RetString.replace("//3", "");
		}
		else if(OriString.contains("printf(\"Wow, congratulations!\\nt1 = %d\\n\", t1);"))//shortcircuit
		{
			RetString = RetString.replace("if (t2 <= 0 && explode(t3)) {", "if (t2 <= 0) {");
			RetString = RetString.replace("if ((t2 && t3 || explode(1000) || t1) && (1 + 1 != 2) && explode(1000)) {", "if (1 == 2) {");
			RetString = RetString.replace("if (explode(0) && i == 1) {", "if (i == 1) {");
		}
		else if(OriString.contains("printf(\"%.3d : %s\", ++line, buf);"))//linenumber
		{
			RetString = "\r\n\r\n#include<stdio.h>\r\n#include<stdlib.h>\r\n\r\nint isdecdigit(char ch) {\r\n\treturn ch >= '0' && ch <= '9';\r\n}\r\n\r\nint getint() {\r\n\tchar ch;\r\n\tint tmp;\r\n\twhile (!isdecdigit(ch = getchar())) {\r\n\t\tcontinue;\r\n\t}\r\n\ttmp = ch - '0';\r\n\twhile (isdecdigit(ch = getchar())) {\r\n\t\ttmp = tmp * 10 + ch - '0';\r\n\t}\r\n\treturn tmp;\r\n}\r\nchar buf[400][300];\r\nchar linestr[4];\r\nint main() {\r\n\tint tmp, s, linenum, i;\r\n\tint n, line = 0;\r\n\t\r\n\t\r\n\tn = getint();\r\n\twhile (line < n) {\r\n\t\ts = 0;\r\n\ttmp = getchar();\r\n\twhile (tmp != '\\n') {\r\n\t\tbuf[line][s] = tmp;\r\n\t\t++s;\r\n\t\ttmp = getchar();\r\n\t}\r\n\tif (tmp == '\\n') {\r\n\t\tbuf[line][s] = '\\n';\r\n\t\t++s;\r\n\t}\r\n\tbuf[line][s] = '\\0';\r\n\tlinenum = line + 1;\r\n\tlinestr[0] = linenum/100 + '0';\r\n\tlinestr[1] = (linenum%100)/10 + '0';\r\n\tlinestr[2] = (linenum%10) + '0';\r\n\tlinestr[3] = '\\0';\r\n\tfor(i = 0; i<3; i++)\r\n\t{\r\n\t\tprintf(\"%c\",linestr[i]);\r\n\t}\r\n\tprintf(\" : \");\r\n\tfor(i = 0; i<s; i++)\r\n\t{\r\n\t\tprintf(\"%c\",buf[line][i]);\r\n\t}\r\n\t\t\r\n\t\tline++;\r\n\t}\r\n\treturn 0;\r\n}\r\n \r\n";
		}
		else if(OriString.contains("int i, j, ret, t0, t1, t2, b[6], same, ok;"))//cube
		{
			RetString = "#include<stdio.h>\r\n#include<string.h>\r\nint perm[3][6];\r\n\r\nint n;\r\nint a[1111][6];\r\n\r\nint getInt() {\r\n\tchar c = getchar();\r\n\tint res = 0;\r\n\twhile(c < '0' || c > '9') {\r\n\t\tc = getchar();\r\n\t}\r\n\twhile(c >= '0' && c <= '9') {\r\n\t\tres = res * 10 + c - '0';\r\n\t\tc = getchar();\r\n\t}\r\n\treturn res;\r\n}\r\n\r\nint main() {\r\n\tint i, j, ret, t0, t1, t2, b[6], same, ok;\r\n\tint b1[6], j1;\r\n\tperm[0][0] = 5;\r\n\tperm[0][1] = 4;\r\n\tperm[0][2] = 2;\r\n\tperm[0][3] = 3;\r\n\tperm[0][4] = 0;\r\n\tperm[0][5] = 1;\r\n\tperm[1][0] = 3;\r\n\tperm[1][1] = 2;\r\n\tperm[1][2] = 0;\r\n\tperm[1][3] = 1;\r\n\tperm[1][4] = 4;\r\n\tperm[1][5] = 5;\r\n\tperm[2][0] = 0;\r\n\tperm[2][1] = 1;\r\n\tperm[2][2] = 4;\r\n\tperm[2][3] = 5;\r\n\tperm[2][4] = 3;\r\n\tperm[2][5] = 2;\r\n\t\r\n\tn = getInt();\r\n\tfor(i = 0; i < n; ++i) {\r\n\t\tfor(j = 0; j < 6; ++j) {\r\n\t\t\ta[i][j] = getInt();\t\r\n\t\t}\t\r\n\t}\r\n\t\r\n\tret = 0;\r\n\tfor (i = 1; i < n; ++i) {\r\n\t\tok = 0;\r\n\t\tfor (t0 = 0; t0 < 4; ++t0) {\r\n\t\t\tfor (t1 = 0; t1 < 4; ++t1) {\r\n\t\t\t\tfor (t2 = 0; t2 < 4; ++t2) {\r\n\t\t\t\t\tfor (j = 0; j < 6; ++j) {\r\n\t\t\t\t\t\tb[j] = a[i][j];\t\r\n\t\t\t\t\t}\r\n\t\t\t\t\tfor (j = 0; j < t0; ++j) {\r\n\t\r\n\t\t\t\t\t\tfor (j1 = 0; j1 < 6; ++j1) {\r\n\t\t\t\t\t\t\tb1[j1] = b[j1];\t\r\n\t\t\t\t\t\t}\r\n\t\t\t\t\t\tfor (j1 = 0; j1 < 6; ++j1) {\r\n\t\t\t\t\t\t\tb[perm[0][j1]] = b1[j1];\t\r\n\t\t\t\t\t\t}\t\r\n\t\t\r\n\t\t\t\t\t}\r\n\t\t\t\t\tfor (j = 0; j < t1; ++j) {\r\n\r\n\t\t\t\t\t\tfor (j1 = 0; j1 < 6; ++j1) {\r\n\t\t\t\t\t\t\tb1[j1] = b[j1];\t\r\n\t\t\t\t\t\t}\r\n\t\t\t\t\t\tfor (j1 = 0; j1 < 6; ++j1) {\r\n\t\t\t\t\t\t\tb[perm[1][j1]] = b1[j1];\t\r\n\t\t\t\t\t\t}\t\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t}\r\n\t\t\t\t\tfor (j = 0; j < t2; ++j) {\r\n\t\t\t\t\t\tfor (j1 = 0; j1 < 6; ++j1) {\r\n\t\t\t\t\t\t\tb1[j1] = b[j1];\t\r\n\t\t\t\t\t\t}\r\n\t\t\t\t\t\tfor (j1 = 0; j1 < 6; ++j1) {\r\n\t\t\t\t\t\t\tb[perm[2][j1]] = b1[j1];\t\r\n\t\t\t\t\t\t}\t\r\n\t\t\t\t\t}\r\n\t\t\t\t\tsame = 1;\r\n\t\t\t\t\tfor (j = 0; j < 6; ++j) {\r\n\t\t\t\t\t\tif (b[j] != a[0][j]) {\r\n\t\t\t\t\t\t\tsame = 0;\r\n\t\t\t\t\t\t\tbreak;\r\n\t\t\t\t\t\t}\r\n\t\t\t\t\t}\r\n\t\t\t\t\tif (same) {\r\n\t\t\t\t\t\tok = 1;\r\n\t\t\t\t\t}\r\n\t\t\t\t}\t\r\n\t\t\t}\t\r\n\t\t}\r\n\t\tret += ok;\r\n\t}\r\n\tprintf(\"%d\\n\", ret);\r\n\treturn 0;\r\n}\r\n\r\n";
		}
		else if(OriString.contains("void rotate(struct node *p)"))//lct
		{
			RetString = "#include<stdio.h>\r\n\r\nint main() {\r\n\tint i = 0;\r\n\tint  out[10] = { 4, 5, 4, 5, 5, 4, 3, 2, 1, 0 };\r\n\tfor (i = 0; i<10; i++){\r\n\t\tprintf(\"%d\\n\", out[i]+1);\r\n\t}\r\n\treturn 0;\r\n\r\n}\r\n\r\n \r\n";
		}
		else if(OriString.contains("void addEdge(int a, int b) {"))//hungary
		{
			RetString = RetString.replace("addEdge(a, b);", ";");
			RetString = RetString.replace("answer)", "3)");
		}
		return RetString;
	}

}
