package util;

import ir.address.Temp;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class MipsAsm implements CommonEnv {
	
	public String format;
	public Object[] paras;

	public MipsAsm(String f, Object... p) {
		format = f;
		paras = p;
	}

	public Set<Temp> def() {
		Set<Temp> set = new LinkedHashSet<Temp>();
		for (int i = 0, j = 0; i < format.length(); i++) {
			char c = format.charAt(i);
			if (c == '@' || c == '%') {
				if (c == '@' && paras[j] instanceof Temp) {
					set.add((Temp) paras[j]);
				}
				j++;
			}
		}
		return set;
	}
	
	public Set<Temp> use() {
		Set<Temp> set = new LinkedHashSet<Temp>();
		for (int i = 0, j = 0; i < format.length(); i++) {
			char c = format.charAt(i);
			if (c == '@' || c == '%') {
				if (c == '%' && paras[j] instanceof Temp) {
					set.add((Temp) paras[j]);
				}
				j++;
			}
		}
		return set;
	}

	public String toString() {
		return toString(DefaultMap.getInstance());
	}

	public String toString(RegAlloc allocator) {
		return doSpill(allocator);
	}

	private String doNormal(RegAlloc allocator) {
		StringBuffer buff = new StringBuffer();

		if (format.charAt(0) == '!') {
			format = format.substring(1);
		} else {
			buff.append('\t');
		}

		for (int i = 0, j = 0; i < format.length(); i++) {
			char c = format.charAt(i);
			if (c == '@' || c == '%') {
				if (paras[j] instanceof Temp) {
					buff.append(allocator.map((Temp) paras[j]));
				} else {
					buff.append(paras[j]);
				}
				j++;
			} else {
				buff.append(c);
			}
		}
		return buff.toString();
	}

	private String doSpill(RegAlloc allocator) {
		StringBuffer before = new StringBuffer();
		StringBuffer after = new StringBuffer();

		TreeSet<Integer> freeRegs = new TreeSet<Integer>();
		freeRegs.add(26); // $k0
		freeRegs.add(27); // $k1

		for (Temp tmp : use()) {
			if (tmp.getLiveInterval().reg == spillReg) {
				int r = freeRegs.pollFirst();
				tmp.getLiveInterval().reg = r;
				before.append("\t\t\tlw $" + regName[r] + ", "
						+ (wordSize * tmp.index + tmp.level.size) 
						+ "($sp)\t#load for spilling\n");
			}
		}
		
		freeRegs.add(26); // $k0

		for (Temp tmp : def()) {
			if (tmp.getLiveInterval().reg == spillReg) {
				int r = freeRegs.pollFirst();
				tmp.getLiveInterval().reg = r;
			}
		}

		String nomal = doNormal(allocator);

		for (Temp tmp : def()) {
			if (tmp.getLiveInterval().isSpilled
					&& tmp.getLiveInterval().reg != spillReg) {
					int r = tmp.getLiveInterval().reg;
					tmp.getLiveInterval().reg = spillReg;
					after.append("\n\t\t\tsw $" + regName[r] + ", "
							+ (wordSize * tmp.index + tmp.level.size)
							+ "($sp)\t# write back for spillling");
			}
		}

		for (Temp tmp : use()) {
			if (tmp.getLiveInterval().isSpilled
					&& tmp.getLiveInterval().reg != spillReg) {
				tmp.getLiveInterval().reg = spillReg;
			}
		}

		return before + nomal + after;
	}
}
