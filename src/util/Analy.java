package util;

import ir.*;
import ir.address.Temp;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Analy implements CommonEnv {

	public LinkedList<BasicBlock> getBasicBlocks(List<Quadruple> quads) {
		identifyLeaders(quads);
		return buildBasicBlocks(quads);
	}

	private void identifyLeaders(List<Quadruple> quads) {
		quads = new ArrayList<Quadruple>(quads);

		for (int i = 0; i < quads.size(); i++)
			quads.get(i).clearAll();

		for (int i = 0; i < quads.size() - 1; i++) {
			Quadruple q = quads.get(i);
			if (!(q instanceof Goto || q instanceof Leave)) {
				q.addSuccessor(quads.get(i + 1));
			}
		}

		quads.get(0).setLeader();
		if (quads.size() > 1 && quads.get(1) instanceof Enter) {
			quads.get(2).setLeader();
		}

		for (Quadruple q : quads) {
			if (q instanceof Leave) {
				q.setLeader();
			}
		}

		for (int i = 0; i < quads.size() - 1; i++) {
			Quadruple q = quads.get(i);

			if (q.isJump()) {
				Quadruple t = q.jumpTargetIn(quads);
				t.setLeader();
				q.addSuccessor(t);
				quads.get(i + 1).setLeader();
			}
			
			if (q instanceof Leave)
				quads.get(i + 1).setLeader();
		}
	}

	private LinkedList<BasicBlock> buildBasicBlocks(List<Quadruple> quads) {
		LinkedList<BasicBlock> blocks = new LinkedList<BasicBlock>();
		BasicBlock current = null;

		for (Quadruple q : quads) {
			if (q.isLeader()) {
				if (current != null)
					blocks.add(current);
				current = new BasicBlock();
			}
			current.addQuad(q);
		}

		if (current != null)
			blocks.add(current);

		for (BasicBlock block : blocks) {
			Quadruple last = block.getLastQuad();
			for(Quadruple succ : last.getSuccessors()) {
				addFlowEdge(block, findBlockByQuad(blocks, succ));
			}	
		}

		return blocks;
	}

	private void addFlowEdge(BasicBlock from ,BasicBlock to) {
		from.addSuccessor(to);
		to.addPredecessor(from);
	}

	private BasicBlock findBlockByQuad(List<BasicBlock> blocks, Quadruple quad) {
		for (BasicBlock block : blocks) {
			if (block.getFirstQuad().equals(quad)) {
				return block;
			}
		}
		return null;
	}

	public void findLiveness(LinkedList<BasicBlock> blocks) {
		LinkedList<BasicBlock> blks = new LinkedList<BasicBlock>();
		Iterator<BasicBlock> iter = blocks.descendingIterator();

		while(iter.hasNext()) {
			blks.add(iter.next());
		}

		// clear. May calculate several times 
		for (BasicBlock b : blks) {
			b.IN.clear();
			b.OUT.clear();
			for (Quadruple q : b.getQuads()) {
				q.IN.clear();
				q.OUT.clear();
			}
		}

		boolean changed = true;
		while(changed) {
			changed = false;
			for (BasicBlock block : blks) {
				block.OUT.clear();
				for (BasicBlock succ : block.getSuccessors()) {
					block.OUT.addAll(succ.IN);
				}

				Set<Temp> preIN = new LinkedHashSet<Temp>(block.IN);

				block.IN.clear();
				block.IN.addAll(block.OUT);
				block.IN.removeAll(block.def());
				block.IN.addAll(block.use());

				if (!preIN.equals(block.IN))
					changed = true;
			}
		}

		for (BasicBlock block : blks) {
			findLiveness(block.getQuads(), block.OUT);
		}
	}

	private void findLiveness(LinkedList<Quadruple> quads, Set<Temp> OUT) {
		Iterator<Quadruple> iter = quads.descendingIterator();
		Quadruple last = null;
		while (iter.hasNext()) {
			Quadruple q = iter.next();
			if (last == null) {
				q.OUT = OUT;
			} else {
				q.OUT = last.IN;
			}
			q.IN.addAll(q.OUT);
			q.IN.removeAll(q.def());
			q.IN.addAll(q.use());
			last = q;
		}
	}

	public LinkedHashMap<Temp, LiveInterval> findLiveIntervals(List<Quadruple> quads) {
		for (Quadruple q : quads) {
			for (Temp t : q.IN) {
				t.clearLiveInterval();
			}
			for (Temp t : q.OUT) {
				t.clearLiveInterval();
			}
			for (Temp t : q.use()) {
				t.clearLiveInterval();
			}
			for (Temp t : q.def()) {
				t.clearLiveInterval();
			}
		}

		LinkedHashMap<Temp, LiveInterval> intervals = new LinkedHashMap<Temp, LiveInterval>();
		int qnum = 0;

		for (Quadruple q : quads) {
			qnum ++;
			for (Temp t : q.IN) {
				t.expandInterval(qnum);
				intervals.put(t, t.getLiveInterval());
			}
			for (Temp t : q.OUT) {
				t.expandInterval(qnum);
				intervals.put(t, t.getLiveInterval());
			}
			for (Temp t : q.use()) {
				t.expandInterval(qnum);
				intervals.put(t, t.getLiveInterval());
			}
			for (Temp t : q.def()) {
				t.expandInterval(qnum);
				intervals.put(t, t.getLiveInterval());
			}
		}
		
		return intervals;
	}
	
	public List<Quadruple> findBranches(List<Quadruple> quads) {
		List<Quadruple> q = new ArrayList<Quadruple>(quads);
		
		for (int i = 0; i < q.size(); i++) {
			if (q.get(i) instanceof IfFalse) {
				IfFalse f = (IfFalse) q.get(i);
				if (q.get(i - 1) instanceof ArithmeticExpr) {
					ArithmeticExpr b = (ArithmeticExpr) q.get(i - 1);
					int ord1 = b.binaryOp.ordinal();
					int ord_less = ArithmeticExpr.ArithmeticOp.LT.ordinal();
					if (f.addr.equals(b.target) && b.isCmp()) {
						q.set(i, new Branch(ifFalseCmp[ord1 - ord_less], b.left, b.right, f.label));
						q.remove(i - 1);
					}
				}
			}
		}

		return q;
	}

}
