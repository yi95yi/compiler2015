package ir;

import ir.address.Label;
import util.*;

public class LabelQuad extends Quadruple {

	public Label label;

	public LabelQuad() {
		this(new Label());
	}

	public LabelQuad(Label l) {
		label = l;
	}

	public String toString() {
		return label + ":";
	}

	@Override
	public AsmList gen() {
		return L(new MipsAsm("!%:", label));
	}
}
