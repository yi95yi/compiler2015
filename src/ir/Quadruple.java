package ir;

import ir.address.IntegerConst;
import ir.address.Label;
import ir.address.Temp;

import java.util.List;
import java.util.LinkedList;
import java.util.Set;
import java.util.LinkedHashSet;

import util.*;

public abstract class Quadruple implements CommonEnv {
	private boolean leader = false;
	private List<Quadruple> succ = new LinkedList<Quadruple>();

	public Set<Temp> IN = new LinkedHashSet<Temp>();
	public Set<Temp> OUT = new LinkedHashSet<Temp>();

	public String cmt = "";

	public void clearAll() {
		leader = false;
		succ.clear();
		IN.clear();
		OUT.clear();
	}

	public boolean isLeader() {
		return leader;
	}

	public void setLeader() {
		leader = true;
	}

	public boolean isJump() {
		return false;
	}

	public Label jumpLabel() {
		return null;
	}

	public Quadruple jumpTargetIn(List<Quadruple> quads) {
		return null;
	}

	protected Quadruple findTargetIn(List<Quadruple> quads, Label target) {
		for (Quadruple quad : quads) 
			if (quad instanceof LabelQuad && ((LabelQuad) quad).label.equals(target))
				return quad;
		return null;
	}

	public Set<Temp> def() {
		return new LinkedHashSet<Temp>();
	}

	public Set<Temp> use() {
		return new LinkedHashSet<Temp>();
	}

	public void addSuccessor(Quadruple quad) {
		succ.add(quad);
	}

	public List<Quadruple> getSuccessors() {
		return succ;
	}

	public abstract AsmList gen();

	protected AsmList L(MipsAsm h, AsmList t) {
		return AsmList.L(h, t);
	}

	protected AsmList L(MipsAsm h) {
		return AsmList.L(h);
	}

	protected AsmList L(AsmList a, AsmList b) {
		return AsmList.L(a, b);
	}

	public Quadruple commentWith(String s) {
		cmt = s;
		return this;
	}

	// for copy propagation
	public void replaceUseOf(Temp old, Temp t) {
	}

	public void replaceUseOf(Temp old, IntegerConst t){
	}

	public void replaceLabelOf(Label old, Label l) {
	}

	public Quadruple promotion() {
		return this;
	}
}

