package ir.address;


public class Label implements Address {
	public static int total = 0;
	public int index;
	public boolean isFunctionLabel = false;
	public int stringType = 0;
	
	public Label() {
		index = total++;
	}

	public Label(boolean bool) {
		index = total++;
		isFunctionLabel = bool;
	}

	@Override
	public String toString() {
		return "Tag" + index;
	}
}
