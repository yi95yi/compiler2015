package ir.address;

public class StringAddressConst implements Address {
	public String value;

	public StringAddressConst(String value) {
		this.value = value;
	}

	@Override
	public StringAddressConst clone() {
		return new StringAddressConst(value);
	}

	@Override
	public String toString() {
		return value;
	}
}
