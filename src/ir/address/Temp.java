package ir.address;

import util.Level;
import util.LiveInterval;

public class Temp implements Address {
	public static int total = 0;
	public static Temp sp = new Temp(null, 0);
	public static Temp gp = new Temp(null, 0);
	public static Temp v1 = new Temp(null, 0);

	public int num; 
	public int index; 
	public Level level;
	private LiveInterval interval = null;

	public Temp(Level lev, int idx) {
		this.num = total++;
		index = idx;
		level = lev;
	}

	public Object getName() {
		if (this.equals(sp))
			return "$sp";
		if (this.equals(gp))
			return "$gp";
		if (this.equals(v1)) 
			return "$v1";
		return this;
	}

	@Override
	public String toString() {
		if (this.equals(sp))
			return "sp";
		if (this.equals(gp))
			return "gp";
		if (this.equals(v1))
			return "v1";
		return "T" + num;
	}

	@Override
	public int hashCode() {
		return num;
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof Temp) 
			return num == ((Temp) other).num;
		return false;
	}

	public void expandInterval(int qnum) {
		if (interval == null)
			interval = new LiveInterval(this, qnum);
		interval.insert(qnum);
	}

	public LiveInterval getLiveInterval() {
		return interval;
	}

	public void clearLiveInterval() {
		interval = null;
	}
}
