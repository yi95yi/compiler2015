package ir.address;

import java.util.List;
import java.util.ArrayList;

public class AddressList implements Address {

	public List<Address> list = new ArrayList<Address>();

	public AddressList() {
	}

	public AddressList(List<Address> l) {
		list = l;
	}
	public void add(Address addr) {
		list.add(addr);
	}

	public Address getFirst() {
		Address result = null;
		if (list.size() > 0) {
			Address temp = list.get(0);
			if (temp instanceof AddressList) {
				result = ((AddressList) temp).getFirst();
			} else {
				result = temp;
			}
		}
		
		return result;
	}
}
