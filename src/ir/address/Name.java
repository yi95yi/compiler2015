package ir.address;


public class Name implements Address {
	public Temp base;
	public IntegerConst offset;
	public boolean isChar;

	public Name(Temp b) {
		this(b, new IntegerConst(0));
	}

	public Name(IntegerConst i) {
		this(null, i);
	}

	public Name(Temp b, IntegerConst o){
		base = b;
		offset = o;
	}

	public Name(Temp b, IntegerConst o, boolean bl){
		base = b;
		offset = o;
		isChar = bl;
	}

	@Override
	public String toString() {
		return base.toString() + "[" + offset.toString() + "]";
	}
}
