package ir.address;


public class IntegerConst implements Address {
	public int value;

	public IntegerConst(int v) {
		value = v;
	}

	@Override
	public IntegerConst clone() {
		return new IntegerConst(value);
	}

	@Override
	public String toString() {
		return String.valueOf(value);
	}
}
