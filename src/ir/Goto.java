package ir;

import ir.address.Label;
import util.*;

public class Goto extends AbstractJump {
	public Goto(Label label) {
		super(label);
	}

	@Override
	public String toString() {
		return "Goto " + label;
	}

	@Override
	public AsmList gen() {
		return L(new MipsAsm("j %", label));
	}
}
