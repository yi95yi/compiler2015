package ir;

import ir.address.Address;
import ir.address.IntegerConst;
import ir.address.Label;
import ir.address.Name;
import ir.address.Temp;

import java.util.Set;
import java.util.LinkedHashSet;

import util.*;

public class Move extends Quadruple {

	public Address target = null;
	public Address source = null;

	public Move(Address tar, Address src){
		target = tar;
		source = src;
	}

	public Move(Temp tar, Temp src, String c){
		target = tar;
		source = src;
		cmt = c;
	}

	public String toString() {
		return target + " = " + source + cmt;
	}

	@Override
	public Set<Temp> def() {
		Set<Temp> set = new LinkedHashSet<Temp>();
		if (target instanceof Temp)
			set.add((Temp) target);
		return set;
	}

	@Override
	public Set<Temp> use() {
		Set<Temp> set = new LinkedHashSet<Temp>();
		if (source instanceof Temp)
			set.add((Temp) source);
		if (source instanceof Name)
			set.add(((Name) source).base);
		if (target instanceof Name)
			set.add(((Name) target).base);
		return set;
	}

	@Override
	public AsmList gen() {
		Object t = target instanceof Temp ? ((Temp) target).getName() : target;
		Object s = source instanceof Temp ? ((Temp) source).getName() : source;
		
		if (target instanceof Temp) {
			if (source instanceof Temp)
				return L(new MipsAsm("move @, %" + cmt, t, s));
			if (source instanceof Label)
				return L(new MipsAsm("la @, %" + cmt, t, s));
			if (source instanceof IntegerConst)
				return L(new MipsAsm("li @, %" + cmt, t, s));
			if (source instanceof Name) {
				s = ((Name) source).base.getName();
				return L(new MipsAsm("lw @, %(%)" + cmt, t,
							((Name) source).offset, s));
			}
		}
		
		if (target instanceof Name) {
			if (source instanceof Temp) {
				t = ((Name) target).base.getName();
				return L(new MipsAsm("sw %, %(%)" + cmt, s,
						((Name) target).offset, t));
			}
		}
		return null;
	}

	@Override
	public void replaceUseOf(Temp old, Temp t) {
		if (source.equals(old)) {
			source = t;
		} else if (source instanceof Name
				&& ((Name) source).base.equals(old)) {
			((Name) source).base = t;
		} else if (target instanceof Name
				&& ((Name) target).base.equals(old)) {
			((Name) target).base = t;
		}
	}

	@Override
	public void replaceUseOf(Temp old, IntegerConst t) {
		if (target instanceof Temp && source.equals(old)) {
			source = t;
		}
	}
}
