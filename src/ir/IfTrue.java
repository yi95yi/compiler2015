package ir;

import ir.address.Address;
import ir.address.IntegerConst;
import ir.address.Label;
import ir.address.Temp;
import util.*;

public class IfTrue extends AbstractIf {
	public IfTrue(Address addr, Label label) {
		super(addr, label);
	}

	@Override
	public String toString() {
		return "IfTrue " + addr + " Goto " + label;
	}

	@Override
	public AsmList gen() {
		Object ad = addr;
		if (Temp.sp.equals(ad))
			ad = "$sp";
		if (Temp.gp.equals(ad))
			ad = "$gp";
		if (Temp.v1.equals(ad))
			ad = "$v1";
		return L(new MipsAsm("bnez %, %", ad, label));
	}

	@Override
	public Quadruple promotion() {
		if (addr instanceof IntegerConst) {
			if (((IntegerConst) addr).value != 0) 
				return new Goto(label);
			else 
				return null;
		}
		return this;
	}
}
