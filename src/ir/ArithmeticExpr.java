package ir;

import ir.address.Address;
import ir.address.IntegerConst;
import ir.address.Temp;

import java.util.Set;
import java.util.LinkedHashSet;

import util.AsmList;
import util.CommonEnv;
import util.MipsAsm;
import util.OpBuilder;

public class ArithmeticExpr extends Quadruple implements CommonEnv {

	public Address left;
	public Address right;
	public final Temp target;
	public final ArithmeticExpr.ArithmeticOp binaryOp;

	public ArithmeticExpr(Temp target, Address left, Address right, ArithmeticOp binaryOp) {
		super();
		this.left = left;
		this.right = right;
		this.target = target;
		this.binaryOp = binaryOp;
	}

	public String toString() {
		return target + " = " + left + " " + binOpStr[binaryOp.ordinal()] + " " + right;
	}

	public boolean isCmp() {
		return (binaryOp.ordinal() >= ArithmeticExpr.ArithmeticOp.LT.ordinal() && binaryOp.ordinal() <= ArithmeticExpr.ArithmeticOp.NE.ordinal());
	}

	@Override
	public Set<Temp> def() {
		Set<Temp> set= new LinkedHashSet<Temp>();
		set.add(target);
		return set;
	}

	@Override
	public Set<Temp> use() {
		Set<Temp> set = new LinkedHashSet<Temp>();
		if (left instanceof Temp)
			set.add((Temp) left);
		if (right instanceof Temp)
			set.add((Temp) right);
		return set;
	}

	@Override
	public AsmList gen() {
		Object l = left instanceof Temp ? ((Temp) left).getName() : left;
		Object r = right instanceof Temp ? ((Temp) right).getName() : right;

		return L(new MipsAsm("% @, %, %"
					+ (cmt.length() > 0 ? (" # " + cmt) : ""),
					binOpAsm[binaryOp.ordinal()], target, l, r));
	}

	@Override
	public void replaceUseOf(Temp old, Temp t)  {
		if (left.equals(old)) {
			System.err.println("replace " + binOpStr[binaryOp.ordinal()]);
			left = t;
		}
		if ((isInvertable[binaryOp.ordinal()] || right instanceof IntegerConst)&& right.equals(t)) {
			System.err.println("replace " + binOpStr[binaryOp.ordinal()]);
			right = t;
		}
	}

	@Override
	public Quadruple promotion() {
		if (left instanceof IntegerConst && right instanceof IntegerConst) {
			return new Move(target, new IntegerConst(OpBuilder.buildOp(
							((IntegerConst) left).value, ((IntegerConst) right).value, binaryOp).value));
		}
		if (left instanceof IntegerConst && ((IntegerConst) left).value == 0) {
			if (binaryOp == ArithmeticExpr.ArithmeticOp.ADD)
				return new Move(target, right);
			if (binaryOp == ArithmeticExpr.ArithmeticOp.SUB)
				return new RelationExpr(target, RelationExpr.RelationOp.MINUS, (Temp) right);
			if (binaryOp == ArithmeticExpr.ArithmeticOp.MUL || binaryOp == ArithmeticExpr.ArithmeticOp.DIV)
				return new Move(target, new IntegerConst(0));
		}
		if (right instanceof IntegerConst && ((IntegerConst) right).value == 0) {
			if (binaryOp == ArithmeticExpr.ArithmeticOp.ADD)
				return new Move(target, left);
			if (binaryOp == ArithmeticExpr.ArithmeticOp.SUB)
				return new Move(target, left);
			if (binaryOp == ArithmeticExpr.ArithmeticOp.MUL)
				return new Move(target, new IntegerConst(0));
		}
		return this;
	}
	
	public static enum ArithmeticOp {
		ADD, SUB, MUL, DIV, MOD,
		LEFTSHIFT, RIGHTSHIFT, AND, XOR, OR,
		LT, LE, GT, GE, EQ, NE, ASSIGN,
		ADDASSIGN,SUBASSIGN, MULASSIGN, DIVASSIGN, MODASSIGN,
		 LSHIFTASSIGN, RSHIFTASSIGN, ANDASSIGN, XORASSIGN, ORASSIGN
	}
}
