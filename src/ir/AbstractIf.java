package ir;

import ir.address.Address;
import ir.address.IntegerConst;
import ir.address.Label;
import ir.address.Temp;

import java.util.LinkedHashSet;
import java.util.Set;

public abstract class AbstractIf extends AbstractJump {
	public Address addr = null;

	public AbstractIf(Address addr, Label label) {
		super(label);
		this.addr = addr;
	}

	@Override
	public Set<Temp> use() {
		Set<Temp> set = new LinkedHashSet<Temp>();
		if (addr instanceof Temp) {
			set.add((Temp) addr);
		}
		return set;
	}

	@Override
	public void replaceUseOf(Temp old, Temp t) {
		if (addr.equals(old)) {
			addr = t;
		}
	}

	@Override
	public void replaceUseOf(Temp old, IntegerConst t) {
		if (addr.equals(old)) {
			addr = t;
		}
	}
}
