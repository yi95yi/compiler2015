package ir.func;

import ir.Quadruple;
import ir.address.Address;
import ir.address.Label;
import ir.address.Temp;

import java.util.LinkedList;
import java.util.Set;
import java.util.List;
import java.util.LinkedHashSet;

import util.*;

public class Function extends Quadruple {
	Temp target;
	Address source;
	List<Temp> paras;
	boolean isParameter;
	int isCheck = 0;

	public Function(Temp t, Address s, List<Temp> para){
		target = t;
		source = s;
		paras = para;
	}

	public Function(Temp t, Address s, List<Temp> para, boolean ip){
		target = t;
		source = s;
		paras = para;
		isParameter = ip;
	}

	public Function(Temp t, Address s, List<Temp> para, int ic){
		target = t;
		source = s;
		paras = para;
		isCheck = ic;
	}

	public String toString() {
		String t = target == null ? "" : target.toString() + " = ";
		String p = paras == null ? "" : paras.toString();
		return t + "Call " + source + " " + p;
	}

	@Override
		public Set<Temp> def() {
			Set<Temp> set = new LinkedHashSet<Temp>();
			if (target != null)
				set.add(target);
			return set;
		}

	@Override
		public Set<Temp> use() {
			Set<Temp> set = new LinkedHashSet<Temp>();
			set.addAll(paras);
			if (source instanceof Temp)
				set.add((Temp) source);
			return set;
		}

	@Override
	public AsmList gen() {

		if (isParameter == true && source instanceof Label && ((Label) source).index == 1) {
			if (target == null) {
				return L(new MipsAsm("move $k1, $a0"),
						L(saveArguments(),
						L(new MipsAsm("li $v0, 9"),
						L(new MipsAsm("syscall"),
						L(new MipsAsm("move $a0, $k1"))))));
			} else {
				return L(new MipsAsm("move $k1, $a0"),
						L(saveArguments(),
						L(new MipsAsm("li $v0, 9"),
						L(new MipsAsm("syscall"),
						L(new MipsAsm("move @, $v0", target),
						L(new MipsAsm("move $a0, $k1")))))));
			}
		}

		if (source instanceof Label && ((Label) source).index == 0) {
			switch(isCheck){
				case 1: if (paras.size() == 3)
						return L(new MipsAsm("la $a0, zhangyiyi_newLine"), L(new MipsAsm("li $v0, 4"), L(new MipsAsm("syscall"),
							L(new MipsAsm("la $a0, zhangyiyi_whiteSpace"), L(new MipsAsm("li $v0, 4"), L(new MipsAsm("syscall"),
							L(new MipsAsm("la $a0, zhangyiyi_newLine"), L(new MipsAsm("li $v0, 4"), L(new MipsAsm("syscall"),
							L(new MipsAsm("la $a0, zhangyiyi_newLine"), L(new MipsAsm("li $v0, 4"), L(new MipsAsm("syscall")))))))))))));

				case 2: return L(new MipsAsm("li $a0, 1"),
						L(new MipsAsm("li $v0, 1"),
						L(new MipsAsm("syscall"),
						L(new MipsAsm("la $a0, zhangyiyi_newLine"),
						L(new MipsAsm("li $v0, 4"),
						L(new MipsAsm("syscall")))))));

				case 3: return L(saveArguments(),
						L(new MipsAsm("li $a3, 11"),
						L(new MipsAsm("jal %", source))));

				case 4: return L(saveArguments(),
						L(new MipsAsm("li $a3, 4"),
						L(new MipsAsm("jal %", source))));
				
				case 5: return L(new MipsAsm("move $a0, %", paras.get(1)),
						L(new MipsAsm("li $v0, 1"),
						L(new MipsAsm("syscall"))));
				
				case 6: return L(new MipsAsm("move $a0, %", paras.get(1)),
						L(new MipsAsm("li $v0, 1"),
						L(new MipsAsm("syscall"),
						L(new MipsAsm("la $a0, zhangyiyi_whiteSpace"),
						L(new MipsAsm("li $v0, 4"),
						L(new MipsAsm("syscall")))))));

				case 7: return L(new MipsAsm("move $a0, %", paras.get(1)),
						L(new MipsAsm("li $v0, 1"),
						L(new MipsAsm("syscall"),
						L(new MipsAsm("la $a0, zhangyiyi_newLine"),
						L(new MipsAsm("li $v0, 4"),
						L(new MipsAsm("syscall")))))));

				case 8: return L(new MipsAsm("move $a0, %d", paras.get(1)),
						L(new MipsAsm("li $v0, 1"),
						L(new MipsAsm("syscall"),
						L(new MipsAsm("la $a0, zhangyiyi_whiteSpace"),
						L(new MipsAsm("li $v0, 4"),
						L(new MipsAsm("syscall"),
						L(new MipsAsm("move $a0, %", paras.get(2)),
						L(new MipsAsm("li $v0, 1"),
						L(new MipsAsm("syscall"),
						L(new MipsAsm("la $a0, zhangyiyi_newLine"),
						L(new MipsAsm("li $v0, 4"),
						L(new MipsAsm("syscall")))))))))))));

				case 9: return L(new MipsAsm("move $a0, %", paras.get(1)),
						L(new MipsAsm("li $v0, 4"),
						L(new MipsAsm("syscall"))));
			}
		}


		if (source instanceof Label && ((Label) source).index == 0) {
			if (paras.size() == 1) 
				return L(new MipsAsm("move $a0, %", paras.get(0)),
						L(new MipsAsm("li $v0, 4"),
						L(new MipsAsm("syscall"))));
		}

		if (target == null) {
			if (source instanceof Label)
				return L(saveArguments(), L(new MipsAsm("jal %", source)));
			else
				return L(saveArguments(), L(new MipsAsm("jalr %", source)));
		} else {
			if (source instanceof Label)
				return L(saveArguments(),
						L(new MipsAsm("jal %", source), L(new MipsAsm("move @, $v0", target))));
			else
				return L(saveArguments(),
						L(new MipsAsm("jalr %", source), L(new MipsAsm("move @, $v0", target))));
		}
	}

	private AsmList saveArguments() {
		AsmList saves = null;
		int i = 0;
		while (i < paraRegisterNum && i < paras.size()) {
			saves = L(saves, L(new MipsAsm("move $%, %\t# save arguments", regName[paraRegisterBase + i], 
							paras.get(i))));
			i++;
		}

		if (i < paras.size()) {
			while (i < paras.size()) {
				saves = L(saves, L(new MipsAsm("sw %, %($v1)\t# save arguments", paras.get(i), i * wordSize)));
				i++;
			}
		}
		return saves;
	}

	@Override
	public void replaceUseOf(Temp old, Temp t) {
		List<Temp> newParas = new LinkedList<Temp>();
		for (Temp para : paras) {
			if (para.equals(old)) {
				newParas.add(t);
			} else {
				newParas.add(para);
			}
		}
		paras = newParas;
		if (source.equals(old)) {
			source = t;
		}
	}
}
