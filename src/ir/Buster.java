package ir;

import util.*;

public class Buster extends Quadruple {

	private final String assem;

	public Buster(String str) {
		assem = str;
	}

	@Override
	public AsmList gen() {
		return L(new MipsAsm(assem));
	}
}
