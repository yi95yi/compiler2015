package ir;

import ir.address.Label;

import java.util.List;

public abstract class AbstractJump extends Quadruple {
	public Label label = null; 	
	
	public AbstractJump(Label label) {
		super();
		this.label = label;
	}

	@Override
	public boolean isJump() {
		return true;
	}

	@Override
	public Label jumpLabel() {
		return label;
	}

	@Override
	public Quadruple jumpTargetIn(List<Quadruple> quads) {
		return findTargetIn(quads, label);
	}
	
	@Override
	public void replaceLabelOf(Label old, Label l) {
		if (label.equals(old)) {
			label = l;
		}
	}
}
