package ir;

import ir.address.Address;
import ir.address.IntegerConst;
import ir.address.Label;
import ir.address.Temp;

import java.util.Set;
import java.util.LinkedHashSet;

import util.*;

public class Branch extends AbstractJump {

	private String cmp;
	private Address left;
	private Address right;
	private String strLeft;

	public Branch(String str, Address l, Address r, Label t){
		super(t);
		cmp = str;
		left = l;
		right = r;
	}

	public Branch(String str, Address r, Label t){
		super(t);
		strLeft = str;
		right = r;
	}

	public String toString() {
		if (right instanceof IntegerConst && ((IntegerConst) right).value == 0)
			return "b" + cmp + "z" + left + "goto" + label;
		return "b" + cmp + " " + left + " " + right + "goto" + label;
	}

	@Override
	public Set<Temp> use() {
		Set<Temp> set = new LinkedHashSet<Temp>();
		if (left instanceof Temp)
			set.add((Temp) left);
		if (right instanceof Temp)
			set.add((Temp) right);
		return set;
	}

	@Override
	public AsmList gen() {
		if (strLeft != null)
			return L(new MipsAsm("beq %, %, %", strLeft, right, label));
		if (right instanceof IntegerConst && ((IntegerConst) right).value == 0)
			return L(new MipsAsm("b%z %, %", cmp, left, label));
		return L(new MipsAsm("b% %, %, %", cmp, left, right, label));
	}

	@Override
	public void replaceUseOf(Temp old, Temp t) {
		if (left.equals(t)) {
			left = t;
		}
		
		if (right.equals(t)) {
			right = t;
		}
	}

	@Override
	public void replaceUseOf(Temp old, IntegerConst t) {
		if (left.equals(old)) {
			left = t;
		}

		if (right.equals(old)) {
			right = t;
		}
	}

	@Override
	public Quadruple promotion() {
		if (left instanceof IntegerConst && right instanceof IntegerConst) {
			if (OpBuilder.buildCmp(((IntegerConst) left).value, ((IntegerConst) right).value,
						cmp))
				return new Goto(label);
			else
				return null;
		}
		return this;
	}

}
