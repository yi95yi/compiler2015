package ir.var;

public abstract class AbstractVariable implements Variable {
	private final String name;
	private final int size;
	
	public AbstractVariable() {
		this(null, 0);
	}
	
	public AbstractVariable(String name, int size) {
		super();
		this.name = name;
		this.size = size;
	}

	public String getName() {
		return name;
	}

	public int getSize() {
		return size;
	}
}
