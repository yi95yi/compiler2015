package ir.var;

public class ArrayVariable extends AbstractVariable {

	public ArrayVariable() {
		super();
	}

	public ArrayVariable(String name, int size) {
		super(name, size);
	}

}
