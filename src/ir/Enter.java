package ir;

import ir.address.Label;
import ir.address.Name;
import ir.address.Temp;

import java.util.Set;
import java.util.List;
import java.util.LinkedHashSet;

import util.*;

public class Enter extends Quadruple {

	Label label = null;
	Level level = null;
	List<Name> paras = null;

	public Enter(Label t, Level lev, List<Name> para){
		label = t;
		level = lev;
		paras = para;
	}

	public String toString() {
		return "Enter " + label;
	}

	@Override
	public Set<Temp> def() {
		return new LinkedHashSet<Temp>();
	}

	private AsmList loadArguments() {
		AsmList loads = null;
		int i = 0;
		while(i < paraRegisterNum && i < paras.size()) {
			Object x = paras.get(i).base.getName();
			loads = L(loads, L(new MipsAsm("sw $%, %(%)\t# load argument",
							regName[paraRegisterBase + i],
							paras.get(i).offset, x)));
			i++;
		}
		return loads;
	}

	@Override
	public AsmList gen() {
		int levelSize = level.frameSize();
		return L(new MipsAsm("addi $sp, $sp, -%\t# enter", levelSize),
				L(new MipsAsm("sw $ra, %($sp)", levelSize - wordSize),
					L(level.saveRegisters(), loadArguments())));
	}
}

