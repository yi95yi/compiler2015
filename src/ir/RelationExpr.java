package ir;

import ir.address.Address;
import ir.address.IntegerConst;
import ir.address.Temp;

import java.util.LinkedHashSet;
import java.util.Set;

import util.*;

public class RelationExpr extends Quadruple {

	public Temp target;
	public Address left;
	public RelationExpr.RelationOp op;

	public RelationExpr(Temp t, RelationExpr.RelationOp up, 	Temp l){
		target = t;
		op = up;
		left = l;
	}

	public String toString() {
		return target + " = " + unOpStr[op.ordinal()] + " " + left;
	}

	@Override
	public Set<Temp> def() {
		Set<Temp> set = new LinkedHashSet<Temp>();
		set.add(target);
		return set;
	}

	@Override
	public Set<Temp> use() {
		Set<Temp> set = new LinkedHashSet<Temp>();
		if (left instanceof Temp)
			set.add((Temp) left);
		return set;
	}

	@Override
	public AsmList gen() {
		Object l = left instanceof Temp ? ((Temp) left).getName() : left;
		Object t = target instanceof Temp ? ((Temp) target).getName() : target;
		
		if (op == RelationExpr.RelationOp.NOT)
			return L(new MipsAsm("seq @, %, $zero", t, l));
		String cmd = op == RelationExpr.RelationOp.MINUS ? "neg" : "not"; 
		return L(new MipsAsm("% @, %", cmd, t, l));
	}

	@Override
	public void replaceUseOf(Temp old, Temp t) {
		if (left.equals(old)) {
			left = t;
		}
	}

	@Override
	public void replaceUseOf(Temp old, IntegerConst t) {
		if (left.equals(old)) {
			left = t;
		}
	}

	@Override
	public Quadruple promotion() {
		if (left instanceof IntegerConst) {
			return new Move(target, new IntegerConst(
						OpBuilder.buildOp(0, ((IntegerConst) left).value, unOpStr[op.ordinal()])));
		}
		return this;
	}
	
	public static enum RelationOp {
		AND,STAR,PLUS,MINUS,TILDE,NOT
	}
}
