package ir;

import ir.address.Address;
import ir.address.IntegerConst;
import ir.address.Label;
import ir.address.Name;
import ir.address.Temp;

import java.util.LinkedHashSet;
import java.util.Set;

import util.*;

public class Return extends Quadruple {

	public Address addr = null;

	public Return(Address ad) {
		this.addr = ad;
	}

	public String toString() {
		return "return " + addr;
	}

	@Override
	public Set<Temp> use() {
		Set<Temp> set = new LinkedHashSet<Temp>();
		if (addr instanceof Temp)
			set.add((Temp) addr);
		return set;
	}
	
	@Override
	public AsmList gen() {
		if (addr instanceof IntegerConst)
			return L(new MipsAsm("li $v0, %", addr));
		if (addr instanceof Label)
			return L(new MipsAsm("la $v0, %", addr));
		Object ad = addr instanceof Temp ? ((Temp) addr).getName() : addr;
		return L(new MipsAsm("move $v0, %", ad));
	}

	@Override
	public void replaceUseOf(Temp old, Temp t) {
		if (addr.equals(old)) {
			addr = t;
		} else if (addr instanceof Name
				&& ((Name) addr).base.equals(old)) {
			((Name) addr).base = t;		
		}
	}

	@Override
	public void replaceUseOf(Temp old, IntegerConst t) {
		if (addr.equals(old)) {
			addr = t;
		}
	}
}
