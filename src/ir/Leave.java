package ir;

import ir.address.Label;
import util.*;

public class Leave extends Quadruple {

	Label label = null;
	Level level = null;

	public Leave(Label t, Level lev){
		label = t;
		level = lev;
	}

	public String toString() {
		return "Leave " + label;
	}

	@Override
	public AsmList gen() {
		int levelSize = level.frameSize();
		return L(level.loadRegisters(),
				L(new MipsAsm("lw $ra, %($sp)", levelSize - wordSize),
					L(new MipsAsm("addiu $sp, $sp, %", levelSize),
						L(new MipsAsm("jr $ra")))));
	}
}
