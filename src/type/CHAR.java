package type;

import ir.address.IntegerConst;

public final class CHAR extends TYPE {

	private static int charSize = 4;
	private static CHAR instance = null;

	public static synchronized CHAR getInstance() {
		if (instance == null) {
			instance = new CHAR();
		}
		return instance;
	}

	private CHAR() {
		size = new IntegerConst(CHAR.charSize);
	}

	@Override
	public boolean equals(Object other) {
		return other instanceof CHAR;
	}
}
