package type;

import ir.address.Address;

import java.util.List;
import java.util.ArrayList;

import util.Symbol;

public class RECORD extends TYPE {

	public Symbol name;
	public List<RecordField> fields;

	public RECORD(Symbol _name) {
		fields = new ArrayList<RecordField>();
		this.name = _name;
	}

	public static final class RecordField {
		public TYPE type;
		public Symbol name;
		public Address offset;

		public RecordField(TYPE _type, Symbol _name, Address _offset) {
			this.type = _type;
			this.name = _name;
			this.offset = _offset;
		}
	}

	public boolean hasField(Symbol name) {
		for (RecordField f : fields)
			if (name.equals(f.name))
				return true;
		return false;
	}

	public void addField(TYPE type, Symbol name, Address offset) {
		fields.add(new RecordField(type, name, offset));
	}

	public RecordField getField(Symbol name) {
		for (RecordField f : fields)
			if (name.equals(f.name))
				return f;
		return null;
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof RECORD)
			return name.equals(((RECORD) other).name);
		return false;
	}
}
