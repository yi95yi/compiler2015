package type;

import util.Symbol;
import ir.address.Address;

public abstract class TYPE {
	public static final RECORD NULL = new RECORD(Symbol.symbol("(null)"));
	public Address size;

	public boolean isNull() {
		return this == NULL;
	}
}
