package type;

import ir.address.IntegerConst;

public class FUNCTION extends TYPE {

	public TYPE argumentType;
	public TYPE returnType;
	public boolean isDefined, isVariableArgument;

	public FUNCTION(TYPE _argumentType, TYPE _returnType, boolean _isDefined,
			boolean _variableArgument) {
		this.argumentType = _argumentType;
		this.returnType = _returnType;
		this.isDefined = _isDefined;
		this.isVariableArgument = _variableArgument;
		size = new IntegerConst(0);
	}

	public TYPE getFinalReturnType() {
		if (returnType instanceof FUNCTION)
			return ((FUNCTION) returnType).getFinalReturnType();
		return returnType;
	}
}
