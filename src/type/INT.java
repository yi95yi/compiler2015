package type;

import ir.address.IntegerConst;

public final class INT extends TYPE {

	private static int intSize = 4;
	private static INT instance = null;

	public static synchronized INT getInstance() {
		if (instance == null)
			instance = new INT();
		return instance;
	}

	private INT() {
		size = new IntegerConst(INT.intSize);
	}

	@Override
	public boolean equals(Object other) {
		return other instanceof INT;
	}
}
