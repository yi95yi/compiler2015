package type;

import ir.address.IntegerConst;

public class VOID extends TYPE {

	private static int voidSize = 0;
	private static VOID instance = null;

	private VOID() {
		size = new IntegerConst(VOID.voidSize);
	}

	public static VOID getInstance() {
		if (instance == null)
			instance = new VOID();
		return instance;
	}

	@Override
	public boolean equals(Object other) {
		return other == instance;
	}
}
