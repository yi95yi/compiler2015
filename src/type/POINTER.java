package type;

import ir.address.IntegerConst;

public class POINTER extends TYPE {
	public TYPE elementType;
	private static int pointerSize = 4;

	public POINTER(TYPE _type) {
		this.elementType = _type;
		size = new IntegerConst(pointerSize);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null)
			return false;
		if (other instanceof POINTER)
			return this.elementType.equals(((POINTER) other).elementType);
		return false;
	}
}
