package type;

import ir.address.Address;
import ir.address.IntegerConst;

public final class ARRAY extends POINTER {

	// $size is where the size of array is stored
	public ARRAY(TYPE _type, Address _size) {
		super(_type);
		this.size = _size;
	}

	public ARRAY(TYPE _type, int _size) {
		this(_type, new IntegerConst(_size));
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof ARRAY)
			return elementType.equals(((ARRAY) other).elementType);
		return false;
	}
}
