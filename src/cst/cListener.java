// Generated from C.g4 by ANTLR 4.5

  package cst;
  
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link cParser}.
 */
public interface cListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link cParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(cParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(cParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#declaration}.
	 * @param ctx the parse tree
	 */
	void enterDeclaration(cParser.DeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#declaration}.
	 * @param ctx the parse tree
	 */
	void exitDeclaration(cParser.DeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#function_definition}.
	 * @param ctx the parse tree
	 */
	void enterFunction_definition(cParser.Function_definitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#function_definition}.
	 * @param ctx the parse tree
	 */
	void exitFunction_definition(cParser.Function_definitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#parameters}.
	 * @param ctx the parse tree
	 */
	void enterParameters(cParser.ParametersContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#parameters}.
	 * @param ctx the parse tree
	 */
	void exitParameters(cParser.ParametersContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#declarators}.
	 * @param ctx the parse tree
	 */
	void enterDeclarators(cParser.DeclaratorsContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#declarators}.
	 * @param ctx the parse tree
	 */
	void exitDeclarators(cParser.DeclaratorsContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#init_declarators}.
	 * @param ctx the parse tree
	 */
	void enterInit_declarators(cParser.Init_declaratorsContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#init_declarators}.
	 * @param ctx the parse tree
	 */
	void exitInit_declarators(cParser.Init_declaratorsContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#init_declarator}.
	 * @param ctx the parse tree
	 */
	void enterInit_declarator(cParser.Init_declaratorContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#init_declarator}.
	 * @param ctx the parse tree
	 */
	void exitInit_declarator(cParser.Init_declaratorContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#initializer}.
	 * @param ctx the parse tree
	 */
	void enterInitializer(cParser.InitializerContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#initializer}.
	 * @param ctx the parse tree
	 */
	void exitInitializer(cParser.InitializerContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#type_specifier}.
	 * @param ctx the parse tree
	 */
	void enterType_specifier(cParser.Type_specifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#type_specifier}.
	 * @param ctx the parse tree
	 */
	void exitType_specifier(cParser.Type_specifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#struct_or_union}.
	 * @param ctx the parse tree
	 */
	void enterStruct_or_union(cParser.Struct_or_unionContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#struct_or_union}.
	 * @param ctx the parse tree
	 */
	void exitStruct_or_union(cParser.Struct_or_unionContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#plain_declaration}.
	 * @param ctx the parse tree
	 */
	void enterPlain_declaration(cParser.Plain_declarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#plain_declaration}.
	 * @param ctx the parse tree
	 */
	void exitPlain_declaration(cParser.Plain_declarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#declarator}.
	 * @param ctx the parse tree
	 */
	void enterDeclarator(cParser.DeclaratorContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#declarator}.
	 * @param ctx the parse tree
	 */
	void exitDeclarator(cParser.DeclaratorContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#plain_declarator}.
	 * @param ctx the parse tree
	 */
	void enterPlain_declarator(cParser.Plain_declaratorContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#plain_declarator}.
	 * @param ctx the parse tree
	 */
	void exitPlain_declarator(cParser.Plain_declaratorContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(cParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(cParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#expression_statement}.
	 * @param ctx the parse tree
	 */
	void enterExpression_statement(cParser.Expression_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#expression_statement}.
	 * @param ctx the parse tree
	 */
	void exitExpression_statement(cParser.Expression_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#compound_statement}.
	 * @param ctx the parse tree
	 */
	void enterCompound_statement(cParser.Compound_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#compound_statement}.
	 * @param ctx the parse tree
	 */
	void exitCompound_statement(cParser.Compound_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#selection_statement}.
	 * @param ctx the parse tree
	 */
	void enterSelection_statement(cParser.Selection_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#selection_statement}.
	 * @param ctx the parse tree
	 */
	void exitSelection_statement(cParser.Selection_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#iteration_statement}.
	 * @param ctx the parse tree
	 */
	void enterIteration_statement(cParser.Iteration_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#iteration_statement}.
	 * @param ctx the parse tree
	 */
	void exitIteration_statement(cParser.Iteration_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#jump_statement}.
	 * @param ctx the parse tree
	 */
	void enterJump_statement(cParser.Jump_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#jump_statement}.
	 * @param ctx the parse tree
	 */
	void exitJump_statement(cParser.Jump_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(cParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(cParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#assignment_expression}.
	 * @param ctx the parse tree
	 */
	void enterAssignment_expression(cParser.Assignment_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#assignment_expression}.
	 * @param ctx the parse tree
	 */
	void exitAssignment_expression(cParser.Assignment_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#assignment_operator}.
	 * @param ctx the parse tree
	 */
	void enterAssignment_operator(cParser.Assignment_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#assignment_operator}.
	 * @param ctx the parse tree
	 */
	void exitAssignment_operator(cParser.Assignment_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#constant_expression}.
	 * @param ctx the parse tree
	 */
	void enterConstant_expression(cParser.Constant_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#constant_expression}.
	 * @param ctx the parse tree
	 */
	void exitConstant_expression(cParser.Constant_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#logical_or_expression}.
	 * @param ctx the parse tree
	 */
	void enterLogical_or_expression(cParser.Logical_or_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#logical_or_expression}.
	 * @param ctx the parse tree
	 */
	void exitLogical_or_expression(cParser.Logical_or_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#logical_and_expression}.
	 * @param ctx the parse tree
	 */
	void enterLogical_and_expression(cParser.Logical_and_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#logical_and_expression}.
	 * @param ctx the parse tree
	 */
	void exitLogical_and_expression(cParser.Logical_and_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#inclusive_or_expression}.
	 * @param ctx the parse tree
	 */
	void enterInclusive_or_expression(cParser.Inclusive_or_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#inclusive_or_expression}.
	 * @param ctx the parse tree
	 */
	void exitInclusive_or_expression(cParser.Inclusive_or_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#exclusive_or_expression}.
	 * @param ctx the parse tree
	 */
	void enterExclusive_or_expression(cParser.Exclusive_or_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#exclusive_or_expression}.
	 * @param ctx the parse tree
	 */
	void exitExclusive_or_expression(cParser.Exclusive_or_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#and_expression}.
	 * @param ctx the parse tree
	 */
	void enterAnd_expression(cParser.And_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#and_expression}.
	 * @param ctx the parse tree
	 */
	void exitAnd_expression(cParser.And_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#equality_expression}.
	 * @param ctx the parse tree
	 */
	void enterEquality_expression(cParser.Equality_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#equality_expression}.
	 * @param ctx the parse tree
	 */
	void exitEquality_expression(cParser.Equality_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#equality_operator}.
	 * @param ctx the parse tree
	 */
	void enterEquality_operator(cParser.Equality_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#equality_operator}.
	 * @param ctx the parse tree
	 */
	void exitEquality_operator(cParser.Equality_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#relational_expression}.
	 * @param ctx the parse tree
	 */
	void enterRelational_expression(cParser.Relational_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#relational_expression}.
	 * @param ctx the parse tree
	 */
	void exitRelational_expression(cParser.Relational_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#relational_operator}.
	 * @param ctx the parse tree
	 */
	void enterRelational_operator(cParser.Relational_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#relational_operator}.
	 * @param ctx the parse tree
	 */
	void exitRelational_operator(cParser.Relational_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#shift_expression}.
	 * @param ctx the parse tree
	 */
	void enterShift_expression(cParser.Shift_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#shift_expression}.
	 * @param ctx the parse tree
	 */
	void exitShift_expression(cParser.Shift_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#shift_operator}.
	 * @param ctx the parse tree
	 */
	void enterShift_operator(cParser.Shift_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#shift_operator}.
	 * @param ctx the parse tree
	 */
	void exitShift_operator(cParser.Shift_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#additive_expression}.
	 * @param ctx the parse tree
	 */
	void enterAdditive_expression(cParser.Additive_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#additive_expression}.
	 * @param ctx the parse tree
	 */
	void exitAdditive_expression(cParser.Additive_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#additive_operator}.
	 * @param ctx the parse tree
	 */
	void enterAdditive_operator(cParser.Additive_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#additive_operator}.
	 * @param ctx the parse tree
	 */
	void exitAdditive_operator(cParser.Additive_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#multiplicative_expression}.
	 * @param ctx the parse tree
	 */
	void enterMultiplicative_expression(cParser.Multiplicative_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#multiplicative_expression}.
	 * @param ctx the parse tree
	 */
	void exitMultiplicative_expression(cParser.Multiplicative_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#multiplicative_operator}.
	 * @param ctx the parse tree
	 */
	void enterMultiplicative_operator(cParser.Multiplicative_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#multiplicative_operator}.
	 * @param ctx the parse tree
	 */
	void exitMultiplicative_operator(cParser.Multiplicative_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#cast_expression}.
	 * @param ctx the parse tree
	 */
	void enterCast_expression(cParser.Cast_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#cast_expression}.
	 * @param ctx the parse tree
	 */
	void exitCast_expression(cParser.Cast_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#type_name}.
	 * @param ctx the parse tree
	 */
	void enterType_name(cParser.Type_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#type_name}.
	 * @param ctx the parse tree
	 */
	void exitType_name(cParser.Type_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#unary_expression}.
	 * @param ctx the parse tree
	 */
	void enterUnary_expression(cParser.Unary_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#unary_expression}.
	 * @param ctx the parse tree
	 */
	void exitUnary_expression(cParser.Unary_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#unary_operator}.
	 * @param ctx the parse tree
	 */
	void enterUnary_operator(cParser.Unary_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#unary_operator}.
	 * @param ctx the parse tree
	 */
	void exitUnary_operator(cParser.Unary_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#postfix_expression}.
	 * @param ctx the parse tree
	 */
	void enterPostfix_expression(cParser.Postfix_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#postfix_expression}.
	 * @param ctx the parse tree
	 */
	void exitPostfix_expression(cParser.Postfix_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#postfix}.
	 * @param ctx the parse tree
	 */
	void enterPostfix(cParser.PostfixContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#postfix}.
	 * @param ctx the parse tree
	 */
	void exitPostfix(cParser.PostfixContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#arguments}.
	 * @param ctx the parse tree
	 */
	void enterArguments(cParser.ArgumentsContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#arguments}.
	 * @param ctx the parse tree
	 */
	void exitArguments(cParser.ArgumentsContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#primary_expression}.
	 * @param ctx the parse tree
	 */
	void enterPrimary_expression(cParser.Primary_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#primary_expression}.
	 * @param ctx the parse tree
	 */
	void exitPrimary_expression(cParser.Primary_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterConstant(cParser.ConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitConstant(cParser.ConstantContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#identifier}.
	 * @param ctx the parse tree
	 */
	void enterIdentifier(cParser.IdentifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#identifier}.
	 * @param ctx the parse tree
	 */
	void exitIdentifier(cParser.IdentifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#typedef_name}.
	 * @param ctx the parse tree
	 */
	void enterTypedef_name(cParser.Typedef_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#typedef_name}.
	 * @param ctx the parse tree
	 */
	void exitTypedef_name(cParser.Typedef_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#integer_constant}.
	 * @param ctx the parse tree
	 */
	void enterInteger_constant(cParser.Integer_constantContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#integer_constant}.
	 * @param ctx the parse tree
	 */
	void exitInteger_constant(cParser.Integer_constantContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#character_constant}.
	 * @param ctx the parse tree
	 */
	void enterCharacter_constant(cParser.Character_constantContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#character_constant}.
	 * @param ctx the parse tree
	 */
	void exitCharacter_constant(cParser.Character_constantContext ctx);
	/**
	 * Enter a parse tree produced by {@link cParser#stringExpr}.
	 * @param ctx the parse tree
	 */
	void enterStringExpr(cParser.StringExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link cParser#stringExpr}.
	 * @param ctx the parse tree
	 */
	void exitStringExpr(cParser.StringExprContext ctx);
}