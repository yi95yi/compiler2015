grammar C;

@header {
  package cc2015.syntactic;
  import cc2015.ast.*;
  import cc2015.symbol.*;
  import cc2015.util.Pair;
  import static cc2015.symbol.Symbol.symbol;
}


//PARSER RULES =================================================


program returns [Program ret]
locals[List<Node> list=new ArrayList<Node>()]
@after {$ret = new Program($list);}
	: (dclr = declaration {$list.add($dclr.ret);}
	| fdef = function_definition {$list.add($fdef.ret);})+
	;


declaration returns [Declaration ret]
	: 'typedef' tspc = type_specifier dclrs = declarators ';'
	  {$ret = new Declaration($tspc.ret, $dclrs.ret);}
	| {InitDeclarators initDclr = null;}
	  tspec = type_specifier
	  (idclrs = init_declarators {initDclr = $idclrs.ret;})? ';'
	  {$ret = new Declaration($tspec.ret, initDclr);}
	;


function_definition returns [FunctionDefinition ret]
	: {Parameters paras = null;}
	  tsepc = type_specifier pdclr = plain_declarator '('
	  (p = parameters {paras = $p.ret;})? ')'
	  cstmt = compound_statement
	  {$ret = new FunctionDefinition($tsepc.ret, $pdclr.ret, paras, $cstmt.ret);}
	;


parameters returns [Parameters ret]
locals[List<PlainDeclaration> list = new ArrayList<PlainDeclaration>(),
		boolean isVar = false]
@after{$ret = new Parameters($list, $isVar);}
	: pdclr = plain_declaration {$list.add($pdclr.ret);}
	  (',' pdclr = plain_declaration {$list.add($pdclr.ret);})*
	  (',' '...'{$isVar = true;})?
	;


declarators returns [Declarators ret]
	: {List<Declarator> dclr = new ArrayList<Declarator>();}
	  dc = declarator {dclr.add($dc.ret);}
	  (',' dc = declarator {dclr.add($dc.ret);} )*
	  {$ret = new Declarators(dclr);}
	;


init_declarators returns [InitDeclarators ret]
	: {List<InitDeclarator> idclr = new ArrayList<InitDeclarator>();}
	  id = init_declarator {idclr.add($id.ret);}
	  (',' id = init_declarator {idclr.add($id.ret);})*
	  {$ret = new InitDeclarators(idclr);}
	;


init_declarator returns [InitDeclarator ret]
	: {Initializer init = null;}
	  dclr = declarator ('=' in = initializer {init = $in.ret;})?
	  {$ret = new InitDeclarator($dclr.ret, init);}
	;


initializer returns [Initializer ret]
	: aexpr = assignment_expression
	{$ret = new Initializer($aexpr.ret, null);}
	| {List<Initializer> init = new ArrayList<Initializer>();}
	  '{' in = initializer {init.add($in.ret);}
	  (',' in = initializer {init.add($in.ret);})* '}'
	  {$ret = new Initializer(null, init);}
	;


type_specifier returns [TypeSpecifier ret]
	: 'void' {$ret = new VoidType();}
	| 'char' {$ret = new CharType();}
	| 'int'  {$ret = new IntType();}
	| typedef_name {$ret = new NameType($typedef_name.ret);}
	| {
	    List< Pair<TypeSpecifier, Declarators> > pairs
		= new ArrayList< Pair<TypeSpecifier, Declarators> >();
	    Identifier id = null;
	  }
	  sou = struct_or_union (i = identifier {id = $i.ret;})?
	  '{' (tspec = type_specifier dclrs = declarators ';'
	      {pairs.add(new Pair<TypeSpecifier, Declarators>($tspec.ret,
				$dclrs.ret));} )+ '}'
	  {$ret = new RecordType($sou.ret, id, pairs);}
	| sou = struct_or_union id = identifier
	  {$ret = new RecordType($sou.ret, $id.ret, null);}
	;


struct_or_union returns [StructUnion ret]
	: 'struct'  {$ret = StructUnion.STRUCT;}
	| 'union'   {$ret = StructUnion.UNION;}
	;


plain_declaration returns [PlainDeclaration ret]
	: tspec = type_specifier dclr = declarator
	  {$ret = new PlainDeclaration($tspec.ret, $dclr.ret);}
	;


declarator returns [Declarator ret]
	: {Parameters paras = null;}
	  pdclr = plain_declarator '(' (pa = parameters {paras = $pa.ret;} )? ')'
	  {$ret = new FunctionDeclarator($pdclr.ret, paras);}
	| {List<ConstExpr> expr = new ArrayList<ConstExpr>();}
	  pdclr = plain_declarator ('[' cexpr = constant_expression ']'
    {expr.add($cexpr.ret);})*
	  {$ret = new ArrayDeclarator($pdclr.ret, expr);}
	;


plain_declarator returns[PlainDeclarator ret]
locals[int cnt = 0]
	: ('*' {$cnt++;})* id = identifier
	  {$ret = new PlainDeclarator($cnt,$id.ret);}
	;


statement returns[Stmt ret]
	: expression_statement {$ret = $expression_statement.ret;}
	| compound_statement {$ret = $compound_statement.ret;}
	| selection_statement {$ret = $selection_statement.ret;}
	| iteration_statement {$ret = $iteration_statement.ret;}
	| jump_statement {$ret = $jump_statement.ret;}
	;


expression_statement returns[ExprStmt ret]
	: {Expr expr = null;}
	  (e = expression {expr = $e.ret;})? ';'
	  {$ret = new ExprStmt(expr);}
	;


compound_statement returns[CompoundStmt ret]
locals [List<Declaration> dList = new ArrayList<Declaration>(),
		List<Stmt> sList= new ArrayList<Stmt>()]
@after {$ret = new CompoundStmt($dList, $sList);}
	: '{' (dclr = declaration {$dList.add($dclr.ret);})*
	  (stmt = statement {$sList.add($stmt.ret);})* '}'
	;


selection_statement returns[SelectionStmt ret]
	: {Stmt stmt = null;}
	  'if' '(' expr = expression ')' stmt1 = statement
	  ('else' stmt2 = statement {stmt = $stmt2.ret;})?
	  {$ret = new SelectionStmt($expr.ret, $stmt1.ret, stmt);}
	;


iteration_statement returns [IterateStmt ret]
	: 'while' '(' expr = expression ')' stmt = statement
   	  {$ret = new WhileStmt($expr.ret, $stmt.ret);}
	| {Expr expr1 = null, expr2 = null, expr3 = null;}
	  'for' '(' (e1 = expression {expr1 = $e1.ret;})? ';'
	    (e2 = expression {expr2 = $e2.ret;})? ';'
	    (e3 = expression {expr3 = $e3.ret;})? ')' stmt = statement
	  {$ret = new ForStmt(expr1, expr2, expr3, $stmt.ret);}
	;


jump_statement returns[JumpStmt ret]
	: 'continue' ';' {$ret = new ContinueStmt();}
	| 'break' ';' {$ret = new BreakStmt();}
	| {Expr expr = null;}
	  'return' (expression {expr = $expression.ret;})? ';'
	  {$ret = new ReturnStmt(expr);}
	;


expression returns[Expr ret]
locals[List<AssignExpr> list = new ArrayList<AssignExpr>()]
@after{$ret = new Expr($list);}
	: aexpr = assignment_expression {$list.add($aexpr.ret);}
	  (',' aexpr = assignment_expression {$list.add($aexpr.ret);})*
	;


assignment_expression returns[AssignExpr ret]
	: l = logical_or_expression {$ret = new AssignExpr($l.ret, null, null, null);}
	| u = unary_expression o = assignment_operator e = assignment_expression
	  {$ret = new AssignExpr(null,$u.ret, $o.ret, $e.ret);}
	;


assignment_operator returns[BinaryOp ret]
	: '=' {$ret = BinaryOp.ASSIGN;}
	| '*=' {$ret = BinaryOp.MULASSIGN;}
	| '/=' {$ret = BinaryOp.DIVASSIGN;}
	| '%=' {$ret = BinaryOp.MODASSIGN;}
	| '+=' {$ret = BinaryOp.ADDASSIGN;}
	| '-=' {$ret = BinaryOp.SUBASSIGN;}
	| '<<=' {$ret = BinaryOp.LSHIFTASSIGN;}
	| '>>=' {$ret = BinaryOp.RSHIFTASSIGN;}
	| '&=' {$ret = BinaryOp.ANDASSIGN;}
	| '^=' {$ret = BinaryOp.XORASSIGN;}
	| '|=' {$ret = BinaryOp.ORASSIGN;}
	;


constant_expression returns[ConstExpr ret]
	: logical_or_expression {$ret = new ConstExpr($logical_or_expression.ret);}
	;


logical_or_expression returns[LogicalOrExpr ret]
locals[List<LogicalAndExpr> expr = new ArrayList<LogicalAndExpr>()]
@after {$ret = new LogicalOrExpr($expr, null);}
	: l = logical_and_expression {$expr.add($l.ret);}
	  ('||' l = logical_and_expression {$expr.add($l.ret);})*
	;


logical_and_expression returns [LogicalAndExpr ret]
locals[List<InclusiveOrExpr> expr = new ArrayList<InclusiveOrExpr>()]
@after {$ret = new LogicalAndExpr($expr, null);}
	: i = inclusive_or_expression {$expr.add($i.ret);}
	  ('&&' i = inclusive_or_expression {$expr.add($i.ret);})*
	;


inclusive_or_expression returns [InclusiveOrExpr ret]
locals[List<ExclusiveOrExpr> expr = new ArrayList<ExclusiveOrExpr>()]
@after {$ret = new InclusiveOrExpr($expr, null);}
	: e = exclusive_or_expression  {$expr.add($e.ret);}
	  ('|' e = exclusive_or_expression {$expr.add($e.ret);})*
	;


exclusive_or_expression returns [ExclusiveOrExpr ret]
locals[List<AndExpr> expr = new ArrayList<AndExpr>()]
@after {$ret = new ExclusiveOrExpr($expr, null);}
	: a = and_expression {$expr.add($a.ret);}
	  ('^' a = and_expression {$expr.add($a.ret);})*
	;


and_expression returns [AndExpr ret]
locals[List<EqualExpr> expr = new ArrayList<EqualExpr>()]
@after {$ret = new AndExpr($expr, null);}
	: e = equality_expression {$expr.add($e.ret);}
	  ('&' e = equality_expression {$expr.add($e.ret);})*
	;


equality_expression returns [EqualExpr ret]
locals[List<RelationExpr> expr = new ArrayList<RelationExpr>(),
	   List<BinaryOp> binOp = new ArrayList<BinaryOp>()]
@after {$ret = new EqualExpr($expr, $binOp);}
	: r = relational_expression {$expr.add($r.ret);}
	  (e = equality_operator {$binOp.add($e.ret);}
	  r = relational_expression {$expr.add($r.ret);})*
	;


equality_operator returns [BinaryOp ret]
	: '==' {$ret = BinaryOp.EQ;}
	| '!=' {$ret = BinaryOp.NEQ;}
	;


relational_expression returns [RelationExpr ret]
locals[List<ShiftExpr> expr = new ArrayList<ShiftExpr>(),
	   List<BinaryOp> binOp = new ArrayList<BinaryOp>()]
@after {$ret = new RelationExpr($expr, $binOp);}
	: s = shift_expression {$expr.add($s.ret);}
	  (r = relational_operator {$binOp.add($r.ret);}
	  s = shift_expression {$expr.add($s.ret);})*
	;


relational_operator returns [BinaryOp ret]
	: '<' {$ret = BinaryOp.LESS;}
	| '>' {$ret = BinaryOp.GREATER;}
	| '<=' {$ret = BinaryOp.LEQ;}
	| '>=' {$ret = BinaryOp.GEQ;}
	;


shift_expression returns [ShiftExpr ret]
locals[List<AddExpr> expr = new ArrayList<AddExpr>(),
	   List<BinaryOp> binOp = new ArrayList<BinaryOp>()]
@after {$ret = new ShiftExpr($expr, $binOp);}
	: a = additive_expression {$expr.add($a.ret);}
	  (s = shift_operator {$binOp.add($s.ret);}
	  a = additive_expression {$expr.add($a.ret);})*
	;


shift_operator returns [BinaryOp ret]
	: '<<' {$ret = BinaryOp.LEFTSHIFT;}
	| '>>' {$ret = BinaryOp.RIGHTSHIFT;}
	;


additive_expression returns [AddExpr ret]
locals[List<MultiplyExpr> expr = new ArrayList<MultiplyExpr>(),
	   List<BinaryOp> binOp = new ArrayList<BinaryOp>()]
@after {$ret = new AddExpr($expr, $binOp);}
	: m = multiplicative_expression {$expr.add($m.ret);}
	  (a = additive_operator {$binOp.add($a.ret);}
	  m = multiplicative_expression {$expr.add($m.ret);})*
	;


additive_operator returns [BinaryOp ret]
	: '+' {$ret = BinaryOp.ADD;}
	| '-' {$ret = BinaryOp.SUB;}
	;


multiplicative_expression returns [MultiplyExpr ret]
locals[List<CastExpr> expr = new ArrayList<CastExpr>(),
	   List<BinaryOp> binOp = new ArrayList<BinaryOp>()]
@after {$ret = new MultiplyExpr($expr, $binOp);}
	: c = cast_expression {$expr.add($c.ret);}
	  (m = multiplicative_operator {$binOp.add($m.ret);}
	  c = cast_expression {$expr.add($c.ret);})*
	;


multiplicative_operator returns [BinaryOp ret]
	: '*' {$ret = BinaryOp.MUL;}
	| '/' {$ret = BinaryOp.DIV;}
	| '%' {$ret = BinaryOp.MOD;}
	;


cast_expression returns [CastExpr ret]
	: u = unary_expression {$ret = new CastExpr(null, $u.ret);}
	| '(' t = type_name ')' c = cast_expression
	  {$ret = new CastExpr($t.ret, $c.ret);}
	;


type_name returns [TypeName ret]
	: {int cnt = 0;}
	  t = type_specifier ('*' {cnt++;})*
	  {$ret = new TypeName($t.ret, cnt);}
	;


unary_expression returns [UnaryExpr ret]
	: p = postfix_expression {$ret=new UnaryExpr(UnaryExpr.POSTFIXEXPR, $p.ret);}
	| '++' u = unary_expression {$ret = new UnaryExpr(UnaryExpr.PREINC, $u.ret);}
	| '--' u = unary_expression {$ret = new UnaryExpr(UnaryExpr.PREDEC, $u.ret);}
	| o = unary_operator c=cast_expression
	  {$ret = new UnaryExpr(UnaryExpr.UNARYOP, $o.ret, $c.ret);}
	| 'sizeof' u = unary_expression
    {$ret = new UnaryExpr(UnaryExpr.SIZEOF, $u.ret);}
	| 'sizeof' '(' t = type_name ')'
    {$ret = new UnaryExpr(UnaryExpr.SIZEOFNAME, $t.ret);}
	;


unary_operator  returns [UnaryOp ret]
	: '&' {$ret = UnaryOp.AND;}
	| '*' {$ret = UnaryOp.STAR;}
	| '+' {$ret = UnaryOp.PLUS;}
	| '-' {$ret = UnaryOp.MINUS;}
	| '~' {$ret = UnaryOp.TILDE;}
	| '!' {$ret = UnaryOp.NOT;}
	;


postfix_expression returns [PostfixExpr ret]
locals[ArrayList<Postfix> list = new ArrayList<Postfix>(), PrimaryExpr expr]
@after{$ret=new PostfixExpr($list, $expr);}
	: p = primary_expression{$expr = $p.ret;}
	  (postfix {$list.add($postfix.ret);})*
	;


postfix returns [Postfix ret]
	: '[' expression ']' {$ret = new ArrayPostfix($expression.ret);}
	|  {Arguments argu = null;}
	  '(' (arguments {argu = $arguments.ret;})? ')'
	  {$ret = new FunctionPostfix(argu);}
	| '.' identifier {$ret = new ValueAttributePostfix($identifier.ret);}
	| '->' identifier {$ret = new PointerAttributePostfix($identifier.ret);}
	| '++' {$ret = new SelfIncPostfix();}
	| '--' {$ret = new SelfDecPostfix();}
	;


arguments returns [Arguments ret]
locals [List<AssignExpr> list=new ArrayList<AssignExpr>()]
@after {$ret = new Arguments($list);}
	: a = assignment_expression{$list.add($a.ret);}
	  (',' e = assignment_expression {$list.add($e.ret);})*
	;


primary_expression returns [PrimaryExpr ret]
	: identifier {$ret = new PrimaryExpr($identifier.ret);}
	| constant {$ret = new PrimaryExpr($constant.ret);}
	| stringExpr {$ret = new PrimaryExpr($stringExpr.ret);}
	| '(' expression ')' {$ret = new PrimaryExpr($expression.ret);}
	;


constant returns [Constant ret]
	: i = integer_constant {$ret = $i.ret;}
	| c = character_constant {$ret = $c.ret;}
	;


identifier returns [Identifier ret]
	: id = Identifier {$ret = new Identifier(symbol($id.text));}
	;


typedef_name returns [TypedefName ret]
	: id=Identifier {$ret = new TypedefName(symbol($id.text));}
	;


integer_constant returns[IntConst ret]
  : Hex {$ret = new IntConst(Integer.parseInt($Hex.getText().substring(2), 16));}
  | Dec {$ret = new IntConst($Dec.int);}
  | Oct {$ret = new IntConst(Integer.parseInt($Oct.getText(), 8));}
  ;

character_constant returns[CharConst ret]
  : c=CHARACTERLITERAL {$ret=new CharConst(new StringBuilder($c.text).toString());}
  ;

stringExpr returns[StringExpr ret]
  : s=STRINGLITERAL {$ret=new StringExpr(new StringBuilder($s.text).toString());}
  ;


// LEXER =====================================================

Whitespace : [ \r\t\n]+ -> channel(HIDDEN);

fragment EOL : '\r' | '\n' | ('\r''\n') ;

Multi_comment : '/*' .*? '*/' -> channel(HIDDEN);
Single_comment : '//' ~[\r\n]* (EOL) -> channel(HIDDEN);
Preprocessing : '#' ~[\r\n]* (EOL) -> channel(HIDDEN);

Hex : '0' ('x'|'X') HexDigit+ ;
Dec : '0' | ([1-9] Digit*)  ;
Oct : '0' OctDigit+  ;

fragment Digit : [0-9];
fragment HexDigit : (Digit | [a-f] | [A-F]) ;
fragment OctDigit : [0-7] ;
fragment Letter: '$' | [A-Z] | '_' |  [a-z];

CHARACTERLITERAL : '\'' ( ('\\' ('0'|'b'|'t'|'n'|'f'|'r'|'\"'|'\''|'\\')) 
	| ~('\''|'\\') ) '\'' 
	| '\'\\' OctDigit OctDigit OctDigit '\''
	| '\'\\' ('0x'|'0X') HexDigit HexDigit'\'';
STRINGLITERAL : '"' ( ('\\' ('0'|'b'|'t'|'n'|'f'|'r'|'\"'|'\''|'\\')) | ~('\\'|'"') )* '"' ;

Identifier : Letter (Letter | Digit)*;
