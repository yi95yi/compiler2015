	.data
Tag39:
	.asciiz "%d\n"
	.align 2
	.align 2
	fs_whiteSpace: .asciiz " "
	.align 2
	fs_newLine: .asciiz "\n"
	.align 2
	.globl args
args:	.space 16
	.align 2
	.globl stat
stat:	.space 36
	.text
	.align 2
	.globl main
main:
	la $gp, stat
	la $v1, args
	li $t1, 4100
	move $a0, $t1	# save arguments
	jal Tag1
	move $t2, $v0
	sw $t2, 0($gp)
	li $t1, 4100
	move $a0, $t1	# save arguments
	jal Tag1
	move $t2, $v0
	sw $t2, 4($gp)
	jal Tag35
	jal exit
Tag3:
	addi $sp, $sp, -204	# enter
	sw $ra, 200($sp)
	sw $t4, 152($sp)	# level save register
	sw $t3, 148($sp)	# level save register
	sw $t2, 144($sp)	# level save register
	sw $t1, 140($sp)	# level save register
	sw $t0, 136($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	li $t1, 0
	sw $t1, 4($sp)
Tag4:
	jal Tag2
	move $t1, $v0
	sw $t1, 8($sp)
	li $t2, 97
	lw $t1, 8($sp)
	bgt $t2, $t1, Tag10
	lw $t3, 8($sp)
	bgt $t3, 122, Tag10
	li $t1, 1
	j Tag11
Tag10:
	li $t1, 0
Tag11:
	bnez $t1, Tag8
	li $t2, 65
	lw $t1, 8($sp)
	bgt $t2, $t1, Tag12
	lw $t3, 8($sp)
	bgt $t3, 90, Tag12
	li $t1, 1
	j Tag13
Tag12:
	li $t1, 0
Tag13:
	bnez $t1, Tag8
	li $t2, 0
	j Tag9
Tag8:
	li $t2, 1
Tag9:
	move $t1, $t2
	beqz $t1, Tag7
	lw $t2, 4($sp)
	lw $t1, 4($sp)
	add $t3, $t1, 1
	sw $t3, 4($sp)
	li $t1, 4
	mul $t3, $t1, $t2
	lw $t4, 0($sp)
	add $t1, $t4, $t3
	lw $t2, 8($sp)
	sw $t2, 0($t1)
	j Tag5
Tag7:
	j Tag6
Tag5:
	j Tag4
Tag6:
	li $t1, 4
	lw $t2, 4($sp)
	mul $t3, $t1, $t2
	lw $t4, 0($sp)
	add $t1, $t4, $t3
	li $t2, 0
	sw $t2, 0($t1)
	lw $t3, 4($sp)
	move $v0, $t3
	lw $t4, 152($sp)	# level load register
	lw $t3, 148($sp)	# level load register
	lw $t2, 144($sp)	# level load register
	lw $t1, 140($sp)	# level load register
	lw $t0, 136($sp)	# level load register
	lw $ra, 200($sp)
	addiu $sp, $sp, 204
	jr $ra
	lw $t4, 152($sp)	# level load register
	lw $t3, 148($sp)	# level load register
	lw $t2, 144($sp)	# level load register
	lw $t1, 140($sp)	# level load register
	lw $t0, 136($sp)	# level load register
	lw $ra, 200($sp)
	addiu $sp, $sp, 204
	jr $ra
Tag14:
	addi $sp, $sp, -100	# enter
	sw $ra, 96($sp)
	sw $t4, 48($sp)	# level save register
	sw $t3, 44($sp)	# level save register
	sw $t2, 40($sp)	# level save register
	sw $t1, 36($sp)	# level save register
	sw $t0, 32($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	sw $a1, 4($sp)	# load argument
	lw $t1, 0($sp)
	lw $t2, 4($sp)
	slt $t3, $t1, $t2
	move $t4, $t3
	beqz $t4, Tag15
	lw $t1, 4($sp)
	move $v0, $t1
	lw $t4, 48($sp)	# level load register
	lw $t3, 44($sp)	# level load register
	lw $t2, 40($sp)	# level load register
	lw $t1, 36($sp)	# level load register
	lw $t0, 32($sp)	# level load register
	lw $ra, 96($sp)
	addiu $sp, $sp, 100
	jr $ra
Tag15:
	lw $t1, 0($sp)
	move $v0, $t1
	lw $t4, 48($sp)	# level load register
	lw $t3, 44($sp)	# level load register
	lw $t2, 40($sp)	# level load register
	lw $t1, 36($sp)	# level load register
	lw $t0, 32($sp)	# level load register
	lw $ra, 96($sp)
	addiu $sp, $sp, 100
	jr $ra
	lw $t4, 48($sp)	# level load register
	lw $t3, 44($sp)	# level load register
	lw $t2, 40($sp)	# level load register
	lw $t1, 36($sp)	# level load register
	lw $t0, 32($sp)	# level load register
	lw $ra, 96($sp)
	addiu $sp, $sp, 100
	jr $ra
Tag16:
	addi $sp, $sp, -100	# enter
	sw $ra, 96($sp)
	sw $t4, 48($sp)	# level save register
	sw $t3, 44($sp)	# level save register
	sw $t2, 40($sp)	# level save register
	sw $t1, 36($sp)	# level save register
	sw $t0, 32($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	sw $a1, 4($sp)	# load argument
	lw $t1, 0($sp)
	lw $t2, 4($sp)
	slt $t3, $t1, $t2
	move $t4, $t3
	beqz $t4, Tag17
	lw $t1, 0($sp)
	move $v0, $t1
	lw $t4, 48($sp)	# level load register
	lw $t3, 44($sp)	# level load register
	lw $t2, 40($sp)	# level load register
	lw $t1, 36($sp)	# level load register
	lw $t0, 32($sp)	# level load register
	lw $ra, 96($sp)
	addiu $sp, $sp, 100
	jr $ra
Tag17:
	lw $t1, 4($sp)
	move $v0, $t1
	lw $t4, 48($sp)	# level load register
	lw $t3, 44($sp)	# level load register
	lw $t2, 40($sp)	# level load register
	lw $t1, 36($sp)	# level load register
	lw $t0, 32($sp)	# level load register
	lw $ra, 96($sp)
	addiu $sp, $sp, 100
	jr $ra
	lw $t4, 48($sp)	# level load register
	lw $t3, 44($sp)	# level load register
	lw $t2, 40($sp)	# level load register
	lw $t1, 36($sp)	# level load register
	lw $t0, 32($sp)	# level load register
	lw $ra, 96($sp)
	addiu $sp, $sp, 100
	jr $ra
Tag18:
	addi $sp, $sp, -564	# enter
	sw $ra, 560($sp)
	sw $t6, 520($sp)	# level save register
	sw $t5, 516($sp)	# level save register
	sw $t4, 512($sp)	# level save register
	sw $t3, 508($sp)	# level save register
	sw $t2, 504($sp)	# level save register
	sw $t1, 500($sp)	# level save register
	sw $t0, 496($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	sw $a1, 4($sp)	# load argument
	sw $a2, 8($sp)	# load argument
	li $t1, 0
	sw $t1, 12($sp)
Tag19:
	lw $t1, 0($sp)
	lw $t2, 0($sp)
	add $t3, $t1, $t2
	lw $t4, 12($sp)
	slt $t1, $t4, $t3
	move $t2, $t1
	beqz $t2, Tag21
	li $t1, 4
	lw $t2, 12($sp)
	mul $t3, $t1, $t2
	lw $t4, 8($sp)
	add $t1, $t4, $t3
	li $t2, 0
	sw $t2, 0($t1)
	lw $t3, 12($sp)
	add $t1, $t3, 1
	sw $t1, 12($sp)
	j Tag19
Tag21:
	li $t1, 0
	sw $t1, 12($sp)
	li $t2, 0
	sw $t2, 16($sp)
Tag22:
	lw $t1, 0($sp)
	lw $t2, 0($sp)
	add $t3, $t1, $t2
	lw $t4, 12($sp)
	slt $t1, $t4, $t3
	move $t2, $t1
	beqz $t2, Tag24
Tag25:
	lw $t1, 12($sp)
	lw $t2, 16($sp)
	sub $t3, $t1, $t2
	bltz $t3, Tag28
	lw $t1, 12($sp)
	lw $t2, 16($sp)
	add $t3, $t1, $t2
	add $t4, $t3, 1
	lw $t1, 0($sp)
	mul $t2, $t1, 2
	bge $t4, $t2, Tag28
	lw $t1, 12($sp)
	lw $t2, 16($sp)
	sub $t3, $t1, $t2
	div $t4, $t3, 2
	li $t1, 4
	mul $t2, $t1, $t4
	lw $t3, 4($sp)
	add $t1, $t3, $t2
	lw $t4, 12($sp)
	lw $t2, 16($sp)
	add $t3, $t4, $t2
	add $t5, $t3, 1
	div $t2, $t5, 2
	li $t3, 4
	mul $t4, $t3, $t2
	lw $t5, 4($sp)
	add $t2, $t5, $t4
	lw $t3, 0($t1)
	lw $t4, 0($t2)
	bne $t3, $t4, Tag28
	li $t1, 1
	j Tag29
Tag28:
	li $t1, 0
Tag29:
	move $t2, $t1
	beqz $t2, Tag27
	lw $t1, 16($sp)
	lw $t2, 16($sp)
	add $t1, $t2, 1
	sw $t1, 16($sp)
	j Tag25
Tag27:
	li $t1, 4
	lw $t2, 12($sp)
	mul $t3, $t1, $t2
	lw $t4, 8($sp)
	add $t1, $t4, $t3
	lw $t2, 16($sp)
	sw $t2, 0($t1)
	li $t3, 1
	sw $t3, 20($sp)
Tag30:
	lw $t1, 12($sp)
	lw $t2, 20($sp)
	sub $t3, $t1, $t2
	bltz $t3, Tag33
	lw $t1, 16($sp)
	lw $t2, 20($sp)
	sub $t3, $t1, $t2
	bltz $t3, Tag33
	lw $t1, 12($sp)
	lw $t2, 20($sp)
	sub $t3, $t1, $t2
	li $t4, 4
	mul $t1, $t4, $t3
	lw $t2, 8($sp)
	add $t3, $t2, $t1
	lw $t4, 16($sp)
	lw $t1, 20($sp)
	sub $t2, $t4, $t1
	lw $t5, 0($t3)
	beq $t5, $t2, Tag33
	li $t1, 1
	j Tag34
Tag33:
	li $t1, 0
Tag34:
	move $t2, $t1
	beqz $t2, Tag32
	lw $t1, 12($sp)
	lw $t2, 20($sp)
	add $t3, $t1, $t2
	li $t4, 4
	mul $t1, $t4, $t3
	lw $t2, 8($sp)
	add $t3, $t2, $t1
	lw $t4, 12($sp)
	lw $t1, 20($sp)
	sub $t2, $t4, $t1
	li $t5, 4
	mul $t1, $t5, $t2
	lw $t4, 8($sp)
	add $t2, $t4, $t1
	lw $t5, 0($t2)
	lw $t1, 16($sp)
	lw $t2, 20($sp)
	sub $t4, $t1, $t2
	move $a0, $t5	# save arguments
	move $a1, $t4	# save arguments
	jal Tag16
	move $t6, $v0
	sw $t6, 0($t3)
	lw $t1, 20($sp)
	lw $t2, 20($sp)
	add $t1, $t2, 1
	sw $t1, 20($sp)
	j Tag30
Tag32:
	lw $t1, 12($sp)
	lw $t2, 20($sp)
	add $t3, $t1, $t2
	sw $t3, 12($sp)
	lw $t1, 16($sp)
	lw $t2, 20($sp)
	sub $t3, $t1, $t2
	li $t4, 0
	move $a0, $t3	# save arguments
	move $a1, $t4	# save arguments
	jal Tag14
	move $t1, $v0
	sw $t1, 16($sp)
	j Tag22
Tag24:
	lw $t6, 520($sp)	# level load register
	lw $t5, 516($sp)	# level load register
	lw $t4, 512($sp)	# level load register
	lw $t3, 508($sp)	# level load register
	lw $t2, 504($sp)	# level load register
	lw $t1, 500($sp)	# level load register
	lw $t0, 496($sp)	# level load register
	lw $ra, 560($sp)
	addiu $sp, $sp, 564
	jr $ra
Tag35:
	addi $sp, $sp, -192	# enter
	sw $ra, 188($sp)
	sw $t6, 148($sp)	# level save register
	sw $t5, 144($sp)	# level save register
	sw $t4, 140($sp)	# level save register
	sw $t3, 136($sp)	# level save register
	sw $t2, 132($sp)	# level save register
	sw $t1, 128($sp)	# level save register
	sw $t0, 124($sp)	# level save register
	lw $t2, 0($gp)
	move $a0, $t2	# save arguments
	jal Tag3
	move $t3, $v0
	sw $t3, 0($sp)
	li $t2, 0
	sw $t2, 8($sp)
	lw $t3, 0($sp)
	lw $t2, 0($gp)
	lw $t4, 4($gp)
	move $a0, $t3	# save arguments
	move $a1, $t2	# save arguments
	move $a2, $t4	# save arguments
	jal Tag18
	move $t5, $v0
	li $t6, 0
	sw $t6, 4($sp)
Tag36:
	lw $t2, 0($sp)
	lw $t3, 0($sp)
	add $t4, $t2, $t3
	lw $t5, 4($sp)
	slt $t2, $t5, $t4
	move $t3, $t2
	beqz $t3, Tag38
	li $t2, 4
	lw $t3, 4($sp)
	mul $t4, $t2, $t3
	lw $t5, 4($gp)
	add $t2, $t5, $t4
	lw $t3, 8($sp)
	lw $t4, 0($t2)
	add $t5, $t3, $t4
	sw $t5, 8($sp)
	lw $t2, 4($sp)
	add $t3, $t2, 1
	sw $t3, 4($sp)
	j Tag36
Tag38:
	la $t1, Tag39
	lw $t2, 8($sp)
	move $a0, $t2
	li $v0, 1
	syscall
	la $a0, fs_newLine
	li $v0, 4
	syscall
	li $t0, 0
	move $v0, $t0
	lw $t6, 148($sp)	# level load register
	lw $t5, 144($sp)	# level load register
	lw $t4, 140($sp)	# level load register
	lw $t3, 136($sp)	# level load register
	lw $t2, 132($sp)	# level load register
	lw $t1, 128($sp)	# level load register
	lw $t0, 124($sp)	# level load register
	lw $ra, 188($sp)
	addiu $sp, $sp, 192
	jr $ra
	lw $t6, 148($sp)	# level load register
	lw $t5, 144($sp)	# level load register
	lw $t4, 140($sp)	# level load register
	lw $t3, 136($sp)	# level load register
	lw $t2, 132($sp)	# level load register
	lw $t1, 128($sp)	# level load register
	lw $t0, 124($sp)	# level load register
	lw $ra, 188($sp)
	addiu $sp, $sp, 192
	jr $ra
########################################
############### RUN-TIME ###############
########################################

#malloc
Tag1:
	# a0 -- size in bytes (already x4)
	li $v0, 9
	syscall
	jr $ra

#getchar
Tag2:
	li $v0, 12
	syscall
	jr $ra
	
exit:
	li $v0, 10
	syscall
	jr $ra

## Daniel J. Ellard -- 03/13/94
## printf.asm--
## an implementation of a simple printf work-alike.

## printf--
## A simple printf-like function. Understands just the basic forms
## of the %s, %d, %c, and %% formats, and can only have 3 embedded
## formats (so that all of the parameters are passed in registers).
## If there are more than 3 embedded formats, all but the first 3 are
## completely ignored (not even printed).
## Register Usage:
## $a0,$s0 - pointer to format string
## $a1,$s1 - format argument 1 (optional)
## $a2,$s2 - format argument 2 (optional)
## $a3,$s3 - format argument 3 (optional)
## $s4 - count of formats processed.
## $s5 - char at $s4.
## $s6 - pointer to printf buffer
##
Tag0:
	subu $sp, $sp, 56 # set up the stack frame,
	sw $ra, 32($sp) # saving the local environment.
	sw $fp, 28($sp)
	sw $s0, 24($sp)
	sw $s1, 20($sp)
	sw $s2, 16($sp)
	sw $s3, 12($sp)
	sw $s4, 8($sp)
	sw $s5, 4($sp)
	sw $s6, 0($sp)
	sw $s7, 36($sp)
	sw $t0, 40($sp)
	sw $t1, 44($sp)
	sw $t2, 48($sp)
	sw $t3, 52($sp)
	addu $fp, $sp, 52

# grab the arguments:
	move $s0, $a0 # fmt string
	move $s1, $a1 # arg1 (optional)
	move $s2, $a2 # arg2 (optional)
	move $s3, $a3 # arg3 (optional)
	lw $s7, 16($v1)# arg4 (optional)
	lw $t0, 20($v1)# arg5 (optional)

	li $s4, 0 # set # of formats = 0
	la $s6, printf_buf # set s6 = base of printf buffer.

printf_loop: # process each character in the fmt:
	lb $s5, 0($s0) # get the next character, and then
	addu $s0, $s0, 1 # bump up $s0 to the next character.

	beq $s5, '%', printf_fmt # if the fmt character, then do fmt.
	beq $0, $s5, printf_end # if zero, then go to end.

printf_putc:
	sb $s5, 0($s6) # otherwise, just put this char
	sb $0, 1($s6) # into the printf buffer,
	move $a0, $s6 # and then print it with the
	li $v0, 4 # print_str syscall
	syscall

	b printf_loop # loop on.

printf_fmt:
	lb $s5, 0($s0) # see what the fmt character is,
	addu $s0, $s0, 1 # and bump up the pointer.

	beq $s4, 5, printf_loop # if we've already processed 3 args,
# then *ignore* this fmt.
	beq $s5, '0', printf_pre
	beq $s5, 'd', printf_int # if 'd', print as a decimal integer.
	beq $s5, 's', printf_str # if 's', print as a string.
	beq $s5, 'c', printf_char # if 'c', print as a ASCII char.
	beq $s5, '%', printf_perc # if '%', print a '%'
	b printf_loop # otherwise, just continue.

printf_shift_args: # shift over the fmt args,
	move $s1, $s2 # $s1 = $s2
	move $s2, $s3 # $s2 = $s3
	move $s3, $s7 # $s3 = $s7
	move $s7, $t0

	add $s4, $s4, 1 # increment # of args processed.

	b printf_loop # and continue the main loop.
	
printf_pre:
	lb $s5, ($s0)
	sub $s5, $s5, 48
	add $s0, $s0, 2
	
	li $t1, 0
	move $t2, $s1
printf_digits_begin:
	beqz $t2, printf_digits_end
	add $t1, $t1, 1
	div $t2, $t2, 10
	b printf_digits_begin
	
printf_digits_end:
	sub $t1, $s5, $t1
	
printf_pre_output_zero_begin:
	beqz $t1, printf_pre_output_zero_end
	sub $t1, $t1, 1
	li $a0, 0
	li $v0, 1
	syscall
	b printf_pre_output_zero_begin
	
printf_pre_output_zero_end:
	move $a0, $s1
	li $v0, 1
	syscall
	b printf_shift_args
	

printf_int: # deal with a %d:
	move $a0, $s1 # do a print_int syscall of $s1.
	li $v0, 1
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_str: # deal with a %s:
	move $a0, $s1 # do a print_string syscall of $s1.
	li $v0, 4
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_char: # deal with a %c:
	sb $s1, 0($s6) # fill the buffer in with byte $s1,
	sb $0, 1($s6) # and then a null.
	move $a0, $s6 # and then do a print_str syscall
	li $v0, 4 # on the buffer.
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_perc: # deal with a %%:
	li $s5, '%' # (this is redundant)
	sb $s5, 0($s6) # fill the buffer in with byte %,
	sb $0, 1($s6) # and then a null.
	move $a0, $s6 # and then do a print_str syscall
	li $v0, 4 # on the buffer.
	syscall
	b printf_loop # branch to printf_loop

printf_end:
	lw $t1, 44($sp)
	lw $t2, 48($sp)
	lw $t3, 52($sp)
	lw $s7, 36($sp)
	lw $t0, 40($sp)
	lw $ra, 32($sp) # restore the prior environment:
	lw $fp, 28($sp)
	lw $s0, 24($sp)
	lw $s1, 20($sp)
	lw $s2, 16($sp)
	lw $s3, 12($sp)
	lw $s4, 8($sp)
	lw $s5, 4($sp)
	lw $s6, 0($sp)
	addu $sp, $sp, 56 # release the stack frame.
	jr $ra # return.

.data
	printf_buf: .space 2

