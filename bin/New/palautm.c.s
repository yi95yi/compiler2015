	.data
Tag41:
	.asciiz "%c"
	.align 2
Tag42:
	.asciiz "%c"
	.align 2
Tag43:
	.asciiz "\n"
	.align 2
	.align 2
	fs_whiteSpace: .asciiz " "
	.align 2
	fs_newLine: .asciiz "\n"
	.align 2
	.globl args
args:	.space 16
	.align 2
	.globl stat
stat:	.space 164
	.text
	.align 2
	.globl main
main:
	la $gp, stat
	la $v1, args
	li $t1, 2000
	move $a0, $t1	# save arguments
	jal Tag1
	move $t2, $v0
	sw $t2, 0($gp)
	li $t1, 2000
	move $a0, $t1	# save arguments
	jal Tag1
	move $t2, $v0
	sw $t2, 4($gp)
	li $t1, 0
	sw $t1, 8($gp)
	li $t2, 0
	sw $t2, 12($gp)
	li $t1, 0
	sw $t1, 16($gp)
	li $t2, 0
	sw $t2, 20($gp)
	li $t1, 4000
	move $a0, $t1	# save arguments
	jal Tag1
	move $t2, $v0
	sw $t2, 24($gp)
	li $t1, 4000
	move $a0, $t1	# save arguments
	jal Tag1
	move $t2, $v0
	sw $t2, 28($gp)
	li $t1, 2000
	move $a0, $t1	# save arguments
	jal Tag1
	move $t2, $v0
	sw $t2, 32($gp)
	li $t1, 2000
	move $a0, $t1	# save arguments
	jal Tag1
	move $t2, $v0
	sw $t2, 36($gp)
	jal Tag30
	jal exit
Tag3:
	addi $sp, $sp, -360	# enter
	sw $ra, 356($sp)
	sw $t6, 316($sp)	# level save register
	sw $t5, 312($sp)	# level save register
	sw $t4, 308($sp)	# level save register
	sw $t3, 304($sp)	# level save register
	sw $t2, 300($sp)	# level save register
	sw $t1, 296($sp)	# level save register
	sw $t0, 292($sp)	# level save register
	li $t2, 0
	sw $t2, 0($sp)
Tag4:
	lw $t2, 0($sp)
	lw $t3, 8($gp)
	sle $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag6
	li $t2, 0
	sw $t2, 4($sp)
Tag7:
	lw $t2, 4($sp)
	sle $t3, $t2, 1
	move $t4, $t3
	beqz $t4, Tag9
	li $t2, 8
	lw $t3, 0($sp)
	mul $t4, $t2, $t3
	lw $t5, 24($gp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 4($sp)
	mul $t5, $t3, $t4
	add $t6, $t2, $t5
	li $t3, 0
	sw $t3, 0($t6)
	lw $t2, 4($sp)
	lw $t3, 4($sp)
	add $t2, $t3, 1
	sw $t2, 4($sp)
	j Tag7
Tag9:
	li $t2, 4
	lw $t3, 8($gp)
	mul $t4, $t2, $t3
	lw $t5, 36($gp)
	add $t2, $t5, $t4
	li $t3, 2
	neg $t4, $t3
	sw $t4, 0($t2)
	li $t3, 4
	lw $t2, 8($gp)
	mul $t4, $t3, $t2
	lw $t5, 32($gp)
	add $t2, $t5, $t4
	li $t3, 0
	sw $t3, 0($t2)
	lw $t4, 0($sp)
	lw $t2, 0($sp)
	add $t3, $t2, 1
	sw $t3, 0($sp)
	j Tag4
Tag6:
	li $t0, 0
	sw $t0, 8($gp)
	lw $t2, 8($gp)
	add $t0, $t2, 1
	sw $t0, 8($gp)
	lw $t2, 8($gp)
	sw $t2, 12($gp)
	li $t0, 4
	lw $t2, 12($gp)
	mul $t3, $t0, $t2
	lw $t4, 36($gp)
	add $t0, $t4, $t3
	li $t2, 1
	neg $t3, $t2
	sw $t3, 0($t0)
	li $t2, 4
	lw $t0, 12($gp)
	mul $t3, $t2, $t0
	lw $t4, 32($gp)
	add $t0, $t4, $t3
	lw $t2, 12($gp)
	sw $t2, 0($t0)
	lw $t3, 8($gp)
	add $t0, $t3, 1
	sw $t0, 8($gp)
	lw $t2, 8($gp)
	sw $t2, 16($gp)
	li $t0, 4
	lw $t2, 16($gp)
	mul $t3, $t0, $t2
	lw $t4, 36($gp)
	add $t0, $t4, $t3
	li $t2, 0
	sw $t2, 0($t0)
	li $t3, 4
	lw $t0, 16($gp)
	mul $t2, $t3, $t0
	lw $t4, 32($gp)
	add $t0, $t4, $t2
	lw $t3, 12($gp)
	sw $t3, 0($t0)
	lw $t2, 12($gp)
	sw $t2, 20($gp)
	lw $t6, 316($sp)	# level load register
	lw $t5, 312($sp)	# level load register
	lw $t4, 308($sp)	# level load register
	lw $t3, 304($sp)	# level load register
	lw $t2, 300($sp)	# level load register
	lw $t1, 296($sp)	# level load register
	lw $t0, 292($sp)	# level load register
	lw $ra, 356($sp)
	addiu $sp, $sp, 360
	jr $ra
Tag10:
	addi $sp, $sp, -152	# enter
	sw $ra, 148($sp)
	sw $t6, 108($sp)	# level save register
	sw $t5, 104($sp)	# level save register
	sw $t4, 100($sp)	# level save register
	sw $t3, 96($sp)	# level save register
	sw $t2, 92($sp)	# level save register
	sw $t1, 88($sp)	# level save register
	sw $t0, 84($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	sw $a1, 4($sp)	# load argument
	sw $a2, 8($sp)	# load argument
	lw $t2, 4($sp)
	sub $t3, $t2, 1
	li $t4, 4
	lw $t2, 0($sp)
	mul $t5, $t4, $t2
	lw $t6, 36($gp)
	add $t2, $t6, $t5
	lw $t4, 0($t2)
	sub $t5, $t3, $t4
	li $t2, 4
	mul $t3, $t2, $t5
	lw $t4, 0($gp)
	add $t2, $t4, $t3
	lw $t1, 0($t2)
	sub $t3, $t1, 97
	lw $t2, 8($sp)
	seq $t1, $t3, $t2
	move $t0, $t1
	move $v0, $t0
	lw $t6, 108($sp)	# level load register
	lw $t5, 104($sp)	# level load register
	lw $t4, 100($sp)	# level load register
	lw $t3, 96($sp)	# level load register
	lw $t2, 92($sp)	# level load register
	lw $t1, 88($sp)	# level load register
	lw $t0, 84($sp)	# level load register
	lw $ra, 148($sp)
	addiu $sp, $sp, 152
	jr $ra
	lw $t6, 108($sp)	# level load register
	lw $t5, 104($sp)	# level load register
	lw $t4, 100($sp)	# level load register
	lw $t3, 96($sp)	# level load register
	lw $t2, 92($sp)	# level load register
	lw $t1, 88($sp)	# level load register
	lw $t0, 84($sp)	# level load register
	lw $ra, 148($sp)
	addiu $sp, $sp, 152
	jr $ra
Tag11:
	addi $sp, $sp, -644	# enter
	sw $ra, 640($sp)
	sw $t6, 600($sp)	# level save register
	sw $t5, 596($sp)	# level save register
	sw $t4, 592($sp)	# level save register
	sw $t3, 588($sp)	# level save register
	sw $t2, 584($sp)	# level save register
	sw $t1, 580($sp)	# level save register
	sw $t0, 576($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	sw $a1, 4($sp)	# load argument
Tag12:
	lw $t2, 20($gp)
	lw $t3, 4($sp)
	lw $t4, 0($sp)
	move $a0, $t2	# save arguments
	move $a1, $t3	# save arguments
	move $a2, $t4	# save arguments
	jal Tag10
	move $t5, $v0
	move $t6, $t5
	seq $t2, $t6, $zero
	move $t3, $t2
	beqz $t3, Tag14
	li $t2, 4
	lw $t3, 20($gp)
	mul $t4, $t2, $t3
	lw $t5, 32($gp)
	add $t2, $t5, $t4
	lw $t3, 0($t2)
	sw $t3, 20($gp)
	j Tag12
Tag14:
	li $t2, 8
	lw $t3, 20($gp)
	mul $t4, $t2, $t3
	lw $t5, 24($gp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 0($sp)
	mul $t5, $t3, $t4
	add $t6, $t2, $t5
	lw $t3, 0($t6)
	beqz $t3, Tag15
	li $t2, 8
	lw $t3, 20($gp)
	mul $t4, $t2, $t3
	lw $t5, 24($gp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 0($sp)
	mul $t5, $t3, $t4
	add $t6, $t2, $t5
	lw $t3, 0($t6)
	sw $t3, 20($gp)
	j Tag16
Tag15:
	lw $t2, 20($gp)
	sw $t2, 8($sp)
	lw $t3, 8($gp)
	add $t2, $t3, 1
	sw $t2, 8($gp)
	li $t3, 8
	lw $t2, 8($sp)
	mul $t4, $t3, $t2
	lw $t5, 24($gp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 0($sp)
	mul $t5, $t3, $t4
	add $t6, $t2, $t5
	lw $t3, 8($gp)
	sw $t3, 0($t6)
	li $t2, 4
	lw $t3, 8($gp)
	mul $t4, $t2, $t3
	lw $t5, 36($gp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 8($sp)
	mul $t5, $t3, $t4
	lw $t6, 36($gp)
	add $t3, $t6, $t5
	lw $t4, 0($t3)
	add $t5, $t4, 2
	sw $t5, 0($t2)
	lw $t3, 8($sp)
	lw $t2, 12($gp)
	seq $t4, $t3, $t2
	move $t5, $t4
	beqz $t5, Tag17
	li $t2, 4
	lw $t3, 8($gp)
	mul $t4, $t2, $t3
	lw $t5, 32($gp)
	add $t2, $t5, $t4
	lw $t3, 16($gp)
	sw $t3, 0($t2)
	j Tag27
Tag17:
	li $t2, 4
	lw $t3, 8($sp)
	mul $t4, $t2, $t3
	lw $t5, 32($gp)
	add $t2, $t5, $t4
	lw $t3, 0($t2)
	sw $t3, 8($sp)
Tag19:
	lw $t2, 8($sp)
	lw $t3, 12($gp)
	beq $t2, $t3, Tag22
	li $t4, 8
	lw $t2, 8($sp)
	mul $t3, $t4, $t2
	lw $t5, 24($gp)
	add $t2, $t5, $t3
	li $t4, 4
	lw $t3, 0($sp)
	mul $t5, $t4, $t3
	add $t6, $t2, $t5
	lw $t3, 0($t6)
	seq $t2, $t3, $zero
	bnez $t2, Tag24
	lw $t3, 8($sp)
	lw $t2, 4($sp)
	lw $t4, 0($sp)
	move $a0, $t3	# save arguments
	move $a1, $t2	# save arguments
	move $a2, $t4	# save arguments
	jal Tag10
	move $t5, $v0
	move $t6, $t5
	seq $t2, $t6, $zero
	bnez $t2, Tag24
	li $t3, 0
	j Tag25
Tag24:
	li $t3, 1
Tag25:
	beqz $t3, Tag22
	li $t2, 1
	j Tag23
Tag22:
	li $t2, 0
Tag23:
	move $t3, $t2
	beqz $t3, Tag21
	li $t2, 4
	lw $t3, 8($sp)
	mul $t4, $t2, $t3
	lw $t5, 32($gp)
	add $t2, $t5, $t4
	lw $t3, 0($t2)
	sw $t3, 8($sp)
	j Tag19
Tag21:
	li $t2, 8
	lw $t3, 8($sp)
	mul $t4, $t2, $t3
	lw $t5, 24($gp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 0($sp)
	mul $t5, $t3, $t4
	add $t6, $t2, $t5
	lw $t3, 0($t6)
	beqz $t3, Tag28
	lw $t2, 8($sp)
	lw $t3, 4($sp)
	lw $t4, 0($sp)
	move $a0, $t2	# save arguments
	move $a1, $t3	# save arguments
	move $a2, $t4	# save arguments
	jal Tag10
	move $t5, $v0
	beqz $t5, Tag28
	li $t2, 1
	j Tag29
Tag28:
	li $t2, 0
Tag29:
	move $t3, $t2
	beqz $t3, Tag26
	li $t2, 4
	lw $t3, 8($gp)
	mul $t4, $t2, $t3
	lw $t5, 32($gp)
	add $t2, $t5, $t4
	li $t3, 8
	lw $t4, 8($sp)
	mul $t5, $t3, $t4
	lw $t6, 24($gp)
	add $t3, $t6, $t5
	li $t4, 4
	lw $t5, 0($sp)
	mul $t6, $t4, $t5
	add $t1, $t3, $t6
	lw $t4, 0($t1)
	sw $t4, 0($t2)
	j Tag27
Tag26:
	li $t1, 4
	lw $t2, 8($gp)
	mul $t3, $t1, $t2
	lw $t4, 32($gp)
	add $t1, $t4, $t3
	lw $t2, 12($gp)
	sw $t2, 0($t1)
Tag27:
	lw $t1, 8($gp)
	sw $t1, 20($gp)
Tag16:
	lw $t6, 600($sp)	# level load register
	lw $t5, 596($sp)	# level load register
	lw $t4, 592($sp)	# level load register
	lw $t3, 588($sp)	# level load register
	lw $t2, 584($sp)	# level load register
	lw $t1, 580($sp)	# level load register
	lw $t0, 576($sp)	# level load register
	lw $ra, 640($sp)
	addiu $sp, $sp, 644
	jr $ra
Tag30:
	addi $sp, $sp, -396	# enter
	sw $ra, 392($sp)
	sw $t5, 348($sp)	# level save register
	sw $t4, 344($sp)	# level save register
	sw $t3, 340($sp)	# level save register
	sw $t2, 336($sp)	# level save register
	sw $t1, 332($sp)	# level save register
	sw $t0, 328($sp)	# level save register
	li $t2, 1
	sw $t2, 4($sp)
	li $t3, 4
	lw $t2, 4($sp)
	mul $t4, $t3, $t2
	lw $t5, 0($gp)
	add $t2, $t5, $t4
	jal Tag2
	move $t3, $v0
	sw $t3, 0($t2)
Tag31:
	li $t2, 4
	lw $t3, 4($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($gp)
	add $t2, $t5, $t4
	lw $t3, 0($t2)
	seq $t4, $t3, 97
	bnez $t4, Tag34
	li $t2, 4
	lw $t3, 4($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($gp)
	add $t2, $t5, $t4
	lw $t3, 0($t2)
	seq $t4, $t3, 98
	bnez $t4, Tag34
	li $t2, 0
	j Tag35
Tag34:
	li $t2, 1
Tag35:
	move $t3, $t2
	beqz $t3, Tag33
	lw $t2, 4($sp)
	add $t3, $t2, 1
	sw $t3, 4($sp)
	li $t2, 4
	lw $t3, 4($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($gp)
	add $t2, $t5, $t4
	jal Tag2
	move $t3, $v0
	sw $t3, 0($t2)
	j Tag31
Tag33:
	li $t2, 4
	lw $t3, 4($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($gp)
	add $t2, $t5, $t4
	li $t3, 92
	sw $t3, 0($t2)
	jal Tag3
	move $t4, $v0
	lw $t2, 4($sp)
	lw $t3, 4($sp)
	sub $t2, $t3, 1
	sw $t2, 4($sp)
	li $t3, 1
	sw $t3, 0($sp)
Tag36:
	lw $t2, 0($sp)
	lw $t3, 4($sp)
	sle $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag38
	lw $t2, 8($gp)
	sw $t2, 8($sp)
	li $t3, 4
	lw $t2, 0($sp)
	mul $t4, $t3, $t2
	lw $t5, 0($gp)
	add $t2, $t5, $t4
	lw $t3, 0($t2)
	sub $t4, $t3, 97
	lw $t2, 0($sp)
	move $a0, $t4	# save arguments
	move $a1, $t2	# save arguments
	jal Tag11
	move $t3, $v0
	lw $t5, 8($sp)
	lw $t2, 8($gp)
	sne $t3, $t5, $t2
	move $t4, $t3
	beqz $t4, Tag39
	la $t2, Tag41
	li $t3, 49
	move $a0, $t2	# save arguments
	move $a1, $t3	# save arguments
	jal Tag0
	move $t4, $v0
	j Tag40
Tag39:
	la $t2, Tag42
	li $t3, 48
	move $a0, $t2	# save arguments
	move $a1, $t3	# save arguments
	jal Tag0
	move $t4, $v0
Tag40:
	lw $t2, 0($sp)
	lw $t3, 0($sp)
	add $t2, $t3, 1
	sw $t2, 0($sp)
	j Tag36
Tag38:
	lw $t2, 4($sp)
	add $t3, $t2, 1
	li $t1, 4
	mul $t2, $t1, $t3
	lw $t4, 4($gp)
	add $t1, $t4, $t2
	li $t0, 92
	sw $t0, 0($t1)
	la $t2, Tag43
	move $a0, $t2
	li $v0, 4
	syscall
	li $t1, 0
	move $v0, $t1
	lw $t5, 348($sp)	# level load register
	lw $t4, 344($sp)	# level load register
	lw $t3, 340($sp)	# level load register
	lw $t2, 336($sp)	# level load register
	lw $t1, 332($sp)	# level load register
	lw $t0, 328($sp)	# level load register
	lw $ra, 392($sp)
	addiu $sp, $sp, 396
	jr $ra
	lw $t5, 348($sp)	# level load register
	lw $t4, 344($sp)	# level load register
	lw $t3, 340($sp)	# level load register
	lw $t2, 336($sp)	# level load register
	lw $t1, 332($sp)	# level load register
	lw $t0, 328($sp)	# level load register
	lw $ra, 392($sp)
	addiu $sp, $sp, 396
	jr $ra
########################################
############### RUN-TIME ###############
########################################

#malloc
Tag1:
	# a0 -- size in bytes (already x4)
	li $v0, 9
	syscall
	jr $ra

#getchar
Tag2:
	li $v0, 12
	syscall
	jr $ra
	
exit:
	li $v0, 10
	syscall
	jr $ra

## Daniel J. Ellard -- 03/13/94
## printf.asm--
## an implementation of a simple printf work-alike.

## printf--
## A simple printf-like function. Understands just the basic forms
## of the %s, %d, %c, and %% formats, and can only have 3 embedded
## formats (so that all of the parameters are passed in registers).
## If there are more than 3 embedded formats, all but the first 3 are
## completely ignored (not even printed).
## Register Usage:
## $a0,$s0 - pointer to format string
## $a1,$s1 - format argument 1 (optional)
## $a2,$s2 - format argument 2 (optional)
## $a3,$s3 - format argument 3 (optional)
## $s4 - count of formats processed.
## $s5 - char at $s4.
## $s6 - pointer to printf buffer
##
Tag0:
	subu $sp, $sp, 56 # set up the stack frame,
	sw $ra, 32($sp) # saving the local environment.
	sw $fp, 28($sp)
	sw $s0, 24($sp)
	sw $s1, 20($sp)
	sw $s2, 16($sp)
	sw $s3, 12($sp)
	sw $s4, 8($sp)
	sw $s5, 4($sp)
	sw $s6, 0($sp)
	sw $s7, 36($sp)
	sw $t0, 40($sp)
	sw $t1, 44($sp)
	sw $t2, 48($sp)
	sw $t3, 52($sp)
	addu $fp, $sp, 52

# grab the arguments:
	move $s0, $a0 # fmt string
	move $s1, $a1 # arg1 (optional)
	move $s2, $a2 # arg2 (optional)
	move $s3, $a3 # arg3 (optional)
	lw $s7, 16($v1)# arg4 (optional)
	lw $t0, 20($v1)# arg5 (optional)

	li $s4, 0 # set # of formats = 0
	la $s6, printf_buf # set s6 = base of printf buffer.

printf_loop: # process each character in the fmt:
	lb $s5, 0($s0) # get the next character, and then
	addu $s0, $s0, 1 # bump up $s0 to the next character.

	beq $s5, '%', printf_fmt # if the fmt character, then do fmt.
	beq $0, $s5, printf_end # if zero, then go to end.

printf_putc:
	sb $s5, 0($s6) # otherwise, just put this char
	sb $0, 1($s6) # into the printf buffer,
	move $a0, $s6 # and then print it with the
	li $v0, 4 # print_str syscall
	syscall

	b printf_loop # loop on.

printf_fmt:
	lb $s5, 0($s0) # see what the fmt character is,
	addu $s0, $s0, 1 # and bump up the pointer.

	beq $s4, 5, printf_loop # if we've already processed 3 args,
# then *ignore* this fmt.
	beq $s5, '0', printf_pre
	beq $s5, 'd', printf_int # if 'd', print as a decimal integer.
	beq $s5, 's', printf_str # if 's', print as a string.
	beq $s5, 'c', printf_char # if 'c', print as a ASCII char.
	beq $s5, '%', printf_perc # if '%', print a '%'
	b printf_loop # otherwise, just continue.

printf_shift_args: # shift over the fmt args,
	move $s1, $s2 # $s1 = $s2
	move $s2, $s3 # $s2 = $s3
	move $s3, $s7 # $s3 = $s7
	move $s7, $t0

	add $s4, $s4, 1 # increment # of args processed.

	b printf_loop # and continue the main loop.
	
printf_pre:
	lb $s5, ($s0)
	sub $s5, $s5, 48
	add $s0, $s0, 2
	
	li $t1, 0
	move $t2, $s1
printf_digits_begin:
	beqz $t2, printf_digits_end
	add $t1, $t1, 1
	div $t2, $t2, 10
	b printf_digits_begin
	
printf_digits_end:
	sub $t1, $s5, $t1
	
printf_pre_output_zero_begin:
	beqz $t1, printf_pre_output_zero_end
	sub $t1, $t1, 1
	li $a0, 0
	li $v0, 1
	syscall
	b printf_pre_output_zero_begin
	
printf_pre_output_zero_end:
	move $a0, $s1
	li $v0, 1
	syscall
	b printf_shift_args
	

printf_int: # deal with a %d:
	move $a0, $s1 # do a print_int syscall of $s1.
	li $v0, 1
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_str: # deal with a %s:
	move $a0, $s1 # do a print_string syscall of $s1.
	li $v0, 4
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_char: # deal with a %c:
	sb $s1, 0($s6) # fill the buffer in with byte $s1,
	sb $0, 1($s6) # and then a null.
	move $a0, $s6 # and then do a print_str syscall
	li $v0, 4 # on the buffer.
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_perc: # deal with a %%:
	li $s5, '%' # (this is redundant)
	sb $s5, 0($s6) # fill the buffer in with byte %,
	sb $0, 1($s6) # and then a null.
	move $a0, $s6 # and then do a print_str syscall
	li $v0, 4 # on the buffer.
	syscall
	b printf_loop # branch to printf_loop

printf_end:
	lw $t1, 44($sp)
	lw $t2, 48($sp)
	lw $t3, 52($sp)
	lw $s7, 36($sp)
	lw $t0, 40($sp)
	lw $ra, 32($sp) # restore the prior environment:
	lw $fp, 28($sp)
	lw $s0, 24($sp)
	lw $s1, 20($sp)
	lw $s2, 16($sp)
	lw $s3, 12($sp)
	lw $s4, 8($sp)
	lw $s5, 4($sp)
	lw $s6, 0($sp)
	addu $sp, $sp, 56 # release the stack frame.
	jr $ra # return.

.data
	printf_buf: .space 2

