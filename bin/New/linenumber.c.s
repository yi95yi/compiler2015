	.data
Tag24:
	.asciiz "%c"
	.align 2
Tag25:
	.asciiz " : "
	.align 2
Tag29:
	.asciiz "%c"
	.align 2
	.align 2
	fs_whiteSpace: .asciiz " "
	.align 2
	fs_newLine: .asciiz "\n"
	.align 2
	.globl args
args:	.space 12
	.align 2
	.globl stat
stat:	.space 36
	.text
	.align 2
	.globl main
main:
	la $gp, stat
	la $v1, args
	li $t1, 480000
	move $a0, $t1	# save arguments
	jal Tag1
	move $t2, $v0
	sw $t2, 0($gp)
	li $t1, 16
	move $a0, $t1	# save arguments
	jal Tag1
	move $t2, $v0
	sw $t2, 4($gp)
	jal Tag13
	jal exit
Tag3:
	addi $sp, $sp, -96	# enter
	sw $ra, 92($sp)
	sw $t2, 36($sp)	# level save register
	sw $t1, 32($sp)	# level save register
	sw $t0, 28($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	lw $t1, 0($sp)
	blt $t1, 48, Tag4
	lw $t2, 0($sp)
	bgt $t2, 57, Tag4
	li $t0, 1
	j Tag5
Tag4:
	li $t0, 0
Tag5:
	move $t1, $t0
	move $v0, $t1
	lw $t2, 36($sp)	# level load register
	lw $t1, 32($sp)	# level load register
	lw $t0, 28($sp)	# level load register
	lw $ra, 92($sp)
	addiu $sp, $sp, 96
	jr $ra
	lw $t2, 36($sp)	# level load register
	lw $t1, 32($sp)	# level load register
	lw $t0, 28($sp)	# level load register
	lw $ra, 92($sp)
	addiu $sp, $sp, 96
	jr $ra
Tag6:
	addi $sp, $sp, -148	# enter
	sw $ra, 144($sp)
	sw $t4, 96($sp)	# level save register
	sw $t3, 92($sp)	# level save register
	sw $t2, 88($sp)	# level save register
	sw $t1, 84($sp)	# level save register
	sw $t0, 80($sp)	# level save register
Tag7:
	jal Tag2
	move $t1, $v0
	sw $t1, 0($sp)
	lw $t2, 0($sp)
	move $a0, $t2	# save arguments
	jal Tag3
	move $t1, $v0
	move $t3, $t1
	seq $t2, $t3, $zero
	move $t1, $t2
	beqz $t1, Tag9
	j Tag8
Tag8:
	j Tag7
Tag9:
	lw $t1, 0($sp)
	sub $t2, $t1, 48
	sw $t2, 4($sp)
Tag10:
	jal Tag2
	move $t1, $v0
	sw $t1, 0($sp)
	lw $t2, 0($sp)
	move $a0, $t2	# save arguments
	jal Tag3
	move $t1, $v0
	move $t3, $t1
	beqz $t3, Tag12
	lw $t1, 4($sp)
	mul $t2, $t1, 10
	lw $t3, 0($sp)
	add $t1, $t2, $t3
	sub $t4, $t1, 48
	sw $t4, 4($sp)
	j Tag10
Tag12:
	lw $t1, 4($sp)
	move $v0, $t1
	lw $t4, 96($sp)	# level load register
	lw $t3, 92($sp)	# level load register
	lw $t2, 88($sp)	# level load register
	lw $t1, 84($sp)	# level load register
	lw $t0, 80($sp)	# level load register
	lw $ra, 144($sp)
	addiu $sp, $sp, 148
	jr $ra
	lw $t4, 96($sp)	# level load register
	lw $t3, 92($sp)	# level load register
	lw $t2, 88($sp)	# level load register
	lw $t1, 84($sp)	# level load register
	lw $t0, 80($sp)	# level load register
	lw $ra, 144($sp)
	addiu $sp, $sp, 148
	jr $ra
Tag13:
	addi $sp, $sp, -536	# enter
	sw $ra, 532($sp)
	sw $t7, 496($sp)	# level save register
	sw $t6, 492($sp)	# level save register
	sw $t5, 488($sp)	# level save register
	sw $t4, 484($sp)	# level save register
	sw $t3, 480($sp)	# level save register
	sw $t2, 476($sp)	# level save register
	sw $t1, 472($sp)	# level save register
	sw $t0, 468($sp)	# level save register
	li $t2, 0
	sw $t2, 20($sp)
	jal Tag6
	move $t3, $v0
	sw $t3, 16($sp)
Tag14:
	lw $t2, 20($sp)
	lw $t3, 16($sp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag16
	li $t2, 0
	sw $t2, 4($sp)
	jal Tag2
	move $t3, $v0
	sw $t3, 0($sp)
Tag17:
	lw $t2, 0($sp)
	sne $t3, $t2, 10
	move $t4, $t3
	beqz $t4, Tag19
	li $t2, 1200
	lw $t3, 20($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($gp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 4($sp)
	mul $t5, $t3, $t4
	add $t6, $t2, $t5
	lw $t3, 0($sp)
	sw $t3, 0($t6)
	lw $t2, 4($sp)
	add $t3, $t2, 1
	sw $t3, 4($sp)
	jal Tag2
	move $t2, $v0
	sw $t2, 0($sp)
	j Tag17
Tag19:
	lw $t2, 0($sp)
	seq $t3, $t2, 10
	move $t4, $t3
	beqz $t4, Tag20
	li $t2, 1200
	lw $t3, 20($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($gp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 4($sp)
	mul $t5, $t3, $t4
	add $t6, $t2, $t5
	li $t3, 10
	sw $t3, 0($t6)
	lw $t2, 4($sp)
	add $t3, $t2, 1
	sw $t3, 4($sp)
Tag20:
	li $t2, 1200
	lw $t3, 20($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($gp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 4($sp)
	mul $t5, $t3, $t4
	add $t6, $t2, $t5
	li $t3, 92
	sw $t3, 0($t6)
	lw $t2, 20($sp)
	add $t3, $t2, 1
	sw $t3, 8($sp)
	lw $t2, 4($gp)
	lw $t3, 8($sp)
	div $t4, $t3, 100
	add $t5, $t4, 48
	sw $t5, 0($t2)
	lw $t3, 4($gp)
	lw $t2, 8($sp)
	rem $t4, $t2, 100
	div $t5, $t4, 10
	add $t2, $t5, 48
	sw $t2, 4($t3)
	lw $t4, 4($gp)
	lw $t2, 8($sp)
	rem $t3, $t2, 10
	add $t5, $t3, 48
	sw $t5, 8($t4)
	lw $t2, 4($gp)
	li $t3, 92
	sw $t3, 12($t2)
	li $t4, 0
	sw $t4, 12($sp)
Tag21:
	lw $t2, 12($sp)
	slt $t3, $t2, 3
	move $t4, $t3
	beqz $t4, Tag23
	la $t2, Tag24
	li $t3, 4
	lw $t4, 12($sp)
	mul $t5, $t3, $t4
	lw $t6, 4($gp)
	add $t3, $t6, $t5
	lw $t4, 0($t3)
	move $a0, $t2	# save arguments
	move $a1, $t4	# save arguments
	jal Tag0
	move $t5, $v0
	lw $t3, 12($sp)
	lw $t2, 12($sp)
	add $t3, $t2, 1
	sw $t3, 12($sp)
	j Tag21
Tag23:
	la $t2, Tag25
	move $a0, $t2
	li $v0, 4
	syscall
	li $t4, 0
	sw $t4, 12($sp)
Tag26:
	lw $t2, 12($sp)
	lw $t3, 4($sp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag28
	la $t2, Tag29
	li $t3, 1200
	lw $t4, 20($sp)
	mul $t5, $t3, $t4
	lw $t6, 0($gp)
	add $t3, $t6, $t5
	li $t4, 4
	lw $t5, 12($sp)
	mul $t6, $t4, $t5
	add $t7, $t3, $t6
	lw $t4, 0($t7)
	move $a0, $t2	# save arguments
	move $a1, $t4	# save arguments
	jal Tag0
	move $t3, $v0
	lw $t5, 12($sp)
	lw $t2, 12($sp)
	add $t3, $t2, 1
	sw $t3, 12($sp)
	j Tag26
Tag28:
	lw $t2, 20($sp)
	lw $t3, 20($sp)
	add $t2, $t3, 1
	sw $t2, 20($sp)
	j Tag14
Tag16:
	li $t0, 0
	move $v0, $t0
	lw $t7, 496($sp)	# level load register
	lw $t6, 492($sp)	# level load register
	lw $t5, 488($sp)	# level load register
	lw $t4, 484($sp)	# level load register
	lw $t3, 480($sp)	# level load register
	lw $t2, 476($sp)	# level load register
	lw $t1, 472($sp)	# level load register
	lw $t0, 468($sp)	# level load register
	lw $ra, 532($sp)
	addiu $sp, $sp, 536
	jr $ra
	lw $t7, 496($sp)	# level load register
	lw $t6, 492($sp)	# level load register
	lw $t5, 488($sp)	# level load register
	lw $t4, 484($sp)	# level load register
	lw $t3, 480($sp)	# level load register
	lw $t2, 476($sp)	# level load register
	lw $t1, 472($sp)	# level load register
	lw $t0, 468($sp)	# level load register
	lw $ra, 532($sp)
	addiu $sp, $sp, 536
	jr $ra
########################################
############### RUN-TIME ###############
########################################

#malloc
Tag1:
	# a0 -- size in bytes (already x4)
	li $v0, 9
	syscall
	jr $ra

#getchar
Tag2:
	li $v0, 12
	syscall
	jr $ra
	
exit:
	li $v0, 10
	syscall
	jr $ra

## Daniel J. Ellard -- 03/13/94
## printf.asm--
## an implementation of a simple printf work-alike.

## printf--
## A simple printf-like function. Understands just the basic forms
## of the %s, %d, %c, and %% formats, and can only have 3 embedded
## formats (so that all of the parameters are passed in registers).
## If there are more than 3 embedded formats, all but the first 3 are
## completely ignored (not even printed).
## Register Usage:
## $a0,$s0 - pointer to format string
## $a1,$s1 - format argument 1 (optional)
## $a2,$s2 - format argument 2 (optional)
## $a3,$s3 - format argument 3 (optional)
## $s4 - count of formats processed.
## $s5 - char at $s4.
## $s6 - pointer to printf buffer
##
Tag0:
	subu $sp, $sp, 56 # set up the stack frame,
	sw $ra, 32($sp) # saving the local environment.
	sw $fp, 28($sp)
	sw $s0, 24($sp)
	sw $s1, 20($sp)
	sw $s2, 16($sp)
	sw $s3, 12($sp)
	sw $s4, 8($sp)
	sw $s5, 4($sp)
	sw $s6, 0($sp)
	sw $s7, 36($sp)
	sw $t0, 40($sp)
	sw $t1, 44($sp)
	sw $t2, 48($sp)
	sw $t3, 52($sp)
	addu $fp, $sp, 52

# grab the arguments:
	move $s0, $a0 # fmt string
	move $s1, $a1 # arg1 (optional)
	move $s2, $a2 # arg2 (optional)
	move $s3, $a3 # arg3 (optional)
	lw $s7, 16($v1)# arg4 (optional)
	lw $t0, 20($v1)# arg5 (optional)

	li $s4, 0 # set # of formats = 0
	la $s6, printf_buf # set s6 = base of printf buffer.

printf_loop: # process each character in the fmt:
	lb $s5, 0($s0) # get the next character, and then
	addu $s0, $s0, 1 # bump up $s0 to the next character.

	beq $s5, '%', printf_fmt # if the fmt character, then do fmt.
	beq $0, $s5, printf_end # if zero, then go to end.

printf_putc:
	sb $s5, 0($s6) # otherwise, just put this char
	sb $0, 1($s6) # into the printf buffer,
	move $a0, $s6 # and then print it with the
	li $v0, 4 # print_str syscall
	syscall

	b printf_loop # loop on.

printf_fmt:
	lb $s5, 0($s0) # see what the fmt character is,
	addu $s0, $s0, 1 # and bump up the pointer.

	beq $s4, 5, printf_loop # if we've already processed 3 args,
# then *ignore* this fmt.
	beq $s5, '0', printf_pre
	beq $s5, 'd', printf_int # if 'd', print as a decimal integer.
	beq $s5, 's', printf_str # if 's', print as a string.
	beq $s5, 'c', printf_char # if 'c', print as a ASCII char.
	beq $s5, '%', printf_perc # if '%', print a '%'
	b printf_loop # otherwise, just continue.

printf_shift_args: # shift over the fmt args,
	move $s1, $s2 # $s1 = $s2
	move $s2, $s3 # $s2 = $s3
	move $s3, $s7 # $s3 = $s7
	move $s7, $t0

	add $s4, $s4, 1 # increment # of args processed.

	b printf_loop # and continue the main loop.
	
printf_pre:
	lb $s5, ($s0)
	sub $s5, $s5, 48
	add $s0, $s0, 2
	
	li $t1, 0
	move $t2, $s1
printf_digits_begin:
	beqz $t2, printf_digits_end
	add $t1, $t1, 1
	div $t2, $t2, 10
	b printf_digits_begin
	
printf_digits_end:
	sub $t1, $s5, $t1
	
printf_pre_output_zero_begin:
	beqz $t1, printf_pre_output_zero_end
	sub $t1, $t1, 1
	li $a0, 0
	li $v0, 1
	syscall
	b printf_pre_output_zero_begin
	
printf_pre_output_zero_end:
	move $a0, $s1
	li $v0, 1
	syscall
	b printf_shift_args
	

printf_int: # deal with a %d:
	move $a0, $s1 # do a print_int syscall of $s1.
	li $v0, 1
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_str: # deal with a %s:
	move $a0, $s1 # do a print_string syscall of $s1.
	li $v0, 4
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_char: # deal with a %c:
	sb $s1, 0($s6) # fill the buffer in with byte $s1,
	sb $0, 1($s6) # and then a null.
	move $a0, $s6 # and then do a print_str syscall
	li $v0, 4 # on the buffer.
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_perc: # deal with a %%:
	li $s5, '%' # (this is redundant)
	sb $s5, 0($s6) # fill the buffer in with byte %,
	sb $0, 1($s6) # and then a null.
	move $a0, $s6 # and then do a print_str syscall
	li $v0, 4 # on the buffer.
	syscall
	b printf_loop # branch to printf_loop

printf_end:
	lw $t1, 44($sp)
	lw $t2, 48($sp)
	lw $t3, 52($sp)
	lw $s7, 36($sp)
	lw $t0, 40($sp)
	lw $ra, 32($sp) # restore the prior environment:
	lw $fp, 28($sp)
	lw $s0, 24($sp)
	lw $s1, 20($sp)
	lw $s2, 16($sp)
	lw $s3, 12($sp)
	lw $s4, 8($sp)
	lw $s5, 4($sp)
	lw $s6, 0($sp)
	addu $sp, $sp, 56 # release the stack frame.
	jr $ra # return.

.data
	printf_buf: .space 2

