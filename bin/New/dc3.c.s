	.data
Tag96:
	.asciiz "%d "
	.align 2
Tag97:
	.asciiz "\n"
	.align 2
	.align 2
	fs_whiteSpace: .asciiz " "
	.align 2
	fs_newLine: .asciiz "\n"
	.align 2
	.globl args
args:	.space 24
	.align 2
	.globl stat
stat:	.space 100
	.text
	.align 2
	.globl main
main:
	la $gp, stat
	la $v1, args
	li $t1, 40000
	move $a0, $t1	# save arguments
	jal Tag1
	move $t2, $v0
	sw $t2, 0($gp)
	li $t1, 40000
	move $a0, $t1	# save arguments
	jal Tag1
	move $t2, $v0
	sw $t2, 4($gp)
	li $t1, 40000
	move $a0, $t1	# save arguments
	jal Tag1
	move $t2, $v0
	sw $t2, 8($gp)
	li $t1, 40000
	move $a0, $t1	# save arguments
	jal Tag1
	move $t2, $v0
	sw $t2, 12($gp)
	li $t1, 120000
	move $a0, $t1	# save arguments
	jal Tag1
	move $t2, $v0
	sw $t2, 16($gp)
	li $t1, 120000
	move $a0, $t1	# save arguments
	jal Tag1
	move $t2, $v0
	sw $t2, 20($gp)
	jal Tag89
	jal exit
Tag3:
	addi $sp, $sp, -124	# enter
	sw $ra, 120($sp)
	sw $t3, 68($sp)	# level save register
	sw $t2, 64($sp)	# level save register
	sw $t1, 60($sp)	# level save register
	sw $t0, 56($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	sw $a1, 4($sp)	# load argument
	lw $t1, 0($sp)
	rem $t2, $t1, 3
	seq $t3, $t2, 1
	move $t1, $t3
	beqz $t1, Tag4
	lw $t2, 0($sp)
	div $t1, $t2, 3
	move $t3, $t1
	move $v0, $t3
	lw $t3, 68($sp)	# level load register
	lw $t2, 64($sp)	# level load register
	lw $t1, 60($sp)	# level load register
	lw $t0, 56($sp)	# level load register
	lw $ra, 120($sp)
	addiu $sp, $sp, 124
	jr $ra
	j Tag5
Tag4:
	lw $t1, 0($sp)
	div $t2, $t1, 3
	lw $t3, 4($sp)
	add $t1, $t2, $t3
	move $t0, $t1
	move $v0, $t0
	lw $t3, 68($sp)	# level load register
	lw $t2, 64($sp)	# level load register
	lw $t1, 60($sp)	# level load register
	lw $t0, 56($sp)	# level load register
	lw $ra, 120($sp)
	addiu $sp, $sp, 124
	jr $ra
Tag5:
	lw $t3, 68($sp)	# level load register
	lw $t2, 64($sp)	# level load register
	lw $t1, 60($sp)	# level load register
	lw $t0, 56($sp)	# level load register
	lw $ra, 120($sp)
	addiu $sp, $sp, 124
	jr $ra
Tag6:
	addi $sp, $sp, -132	# enter
	sw $ra, 128($sp)
	sw $t4, 80($sp)	# level save register
	sw $t3, 76($sp)	# level save register
	sw $t2, 72($sp)	# level save register
	sw $t1, 68($sp)	# level save register
	sw $t0, 64($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	sw $a1, 4($sp)	# load argument
	lw $t1, 0($sp)
	lw $t2, 4($sp)
	slt $t3, $t1, $t2
	move $t4, $t3
	beqz $t4, Tag7
	lw $t1, 0($sp)
	mul $t2, $t1, 3
	add $t3, $t2, 1
	move $t1, $t3
	move $v0, $t1
	lw $t4, 80($sp)	# level load register
	lw $t3, 76($sp)	# level load register
	lw $t2, 72($sp)	# level load register
	lw $t1, 68($sp)	# level load register
	lw $t0, 64($sp)	# level load register
	lw $ra, 128($sp)
	addiu $sp, $sp, 132
	jr $ra
	j Tag8
Tag7:
	lw $t1, 0($sp)
	lw $t2, 4($sp)
	sub $t3, $t1, $t2
	mul $t0, $t3, 3
	add $t1, $t0, 2
	move $t2, $t1
	move $v0, $t2
	lw $t4, 80($sp)	# level load register
	lw $t3, 76($sp)	# level load register
	lw $t2, 72($sp)	# level load register
	lw $t1, 68($sp)	# level load register
	lw $t0, 64($sp)	# level load register
	lw $ra, 128($sp)
	addiu $sp, $sp, 132
	jr $ra
Tag8:
	lw $t4, 80($sp)	# level load register
	lw $t3, 76($sp)	# level load register
	lw $t2, 72($sp)	# level load register
	lw $t1, 68($sp)	# level load register
	lw $t0, 64($sp)	# level load register
	lw $ra, 128($sp)
	addiu $sp, $sp, 132
	jr $ra
Tag9:
	addi $sp, $sp, -100	# enter
	sw $ra, 96($sp)
	sw $t4, 48($sp)	# level save register
	sw $t3, 44($sp)	# level save register
	sw $t2, 40($sp)	# level save register
	sw $t1, 36($sp)	# level save register
	sw $t0, 32($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	sw $a1, 4($sp)	# load argument
	lw $t1, 0($sp)
	lw $t2, 4($sp)
	sgt $t3, $t1, $t2
	move $t4, $t3
	beqz $t4, Tag10
	lw $t1, 0($sp)
	move $v0, $t1
	lw $t4, 48($sp)	# level load register
	lw $t3, 44($sp)	# level load register
	lw $t2, 40($sp)	# level load register
	lw $t1, 36($sp)	# level load register
	lw $t0, 32($sp)	# level load register
	lw $ra, 96($sp)
	addiu $sp, $sp, 100
	jr $ra
	j Tag11
Tag10:
	lw $t1, 4($sp)
	move $v0, $t1
	lw $t4, 48($sp)	# level load register
	lw $t3, 44($sp)	# level load register
	lw $t2, 40($sp)	# level load register
	lw $t1, 36($sp)	# level load register
	lw $t0, 32($sp)	# level load register
	lw $ra, 96($sp)
	addiu $sp, $sp, 100
	jr $ra
Tag11:
	lw $t4, 48($sp)	# level load register
	lw $t3, 44($sp)	# level load register
	lw $t2, 40($sp)	# level load register
	lw $t1, 36($sp)	# level load register
	lw $t0, 32($sp)	# level load register
	lw $ra, 96($sp)
	addiu $sp, $sp, 100
	jr $ra
Tag12:
	addi $sp, $sp, -260	# enter
	sw $ra, 256($sp)
	sw $t5, 212($sp)	# level save register
	sw $t4, 208($sp)	# level save register
	sw $t3, 204($sp)	# level save register
	sw $t2, 200($sp)	# level save register
	sw $t1, 196($sp)	# level save register
	sw $t0, 192($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	sw $a1, 4($sp)	# load argument
	sw $a2, 8($sp)	# load argument
	li $t1, 4
	lw $t2, 4($sp)
	mul $t3, $t1, $t2
	lw $t4, 0($sp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 8($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($sp)
	add $t2, $t5, $t4
	lw $t3, 0($t1)
	lw $t4, 0($t2)
	bne $t3, $t4, Tag13
	lw $t1, 4($sp)
	add $t2, $t1, 1
	li $t3, 4
	mul $t1, $t3, $t2
	lw $t4, 0($sp)
	add $t2, $t4, $t1
	lw $t3, 8($sp)
	add $t1, $t3, 1
	li $t4, 4
	mul $t3, $t4, $t1
	lw $t5, 0($sp)
	add $t1, $t5, $t3
	lw $t4, 0($t2)
	lw $t3, 0($t1)
	bne $t4, $t3, Tag13
	lw $t1, 4($sp)
	add $t2, $t1, 2
	li $t3, 4
	mul $t1, $t3, $t2
	lw $t4, 0($sp)
	add $t2, $t4, $t1
	lw $t3, 8($sp)
	add $t1, $t3, 2
	li $t4, 4
	mul $t3, $t4, $t1
	lw $t5, 0($sp)
	add $t1, $t5, $t3
	lw $t0, 0($t2)
	lw $t3, 0($t1)
	bne $t0, $t3, Tag13
	li $t1, 1
	j Tag14
Tag13:
	li $t1, 0
Tag14:
	move $t0, $t1
	move $v0, $t0
	lw $t5, 212($sp)	# level load register
	lw $t4, 208($sp)	# level load register
	lw $t3, 204($sp)	# level load register
	lw $t2, 200($sp)	# level load register
	lw $t1, 196($sp)	# level load register
	lw $t0, 192($sp)	# level load register
	lw $ra, 256($sp)
	addiu $sp, $sp, 260
	jr $ra
	lw $t5, 212($sp)	# level load register
	lw $t4, 208($sp)	# level load register
	lw $t3, 204($sp)	# level load register
	lw $t2, 200($sp)	# level load register
	lw $t1, 196($sp)	# level load register
	lw $t0, 192($sp)	# level load register
	lw $ra, 256($sp)
	addiu $sp, $sp, 260
	jr $ra
Tag15:
	addi $sp, $sp, -416	# enter
	sw $ra, 412($sp)
	sw $t7, 376($sp)	# level save register
	sw $t6, 372($sp)	# level save register
	sw $t5, 368($sp)	# level save register
	sw $t4, 364($sp)	# level save register
	sw $t3, 360($sp)	# level save register
	sw $t2, 356($sp)	# level save register
	sw $t1, 352($sp)	# level save register
	sw $t0, 348($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	sw $a1, 4($sp)	# load argument
	sw $a2, 8($sp)	# load argument
	sw $a3, 12($sp)	# load argument
	lw $t2, 0($sp)
	seq $t3, $t2, 2
	move $t4, $t3
	beqz $t4, Tag16
	li $t2, 4
	lw $t3, 8($sp)
	mul $t4, $t2, $t3
	lw $t5, 4($sp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 12($sp)
	mul $t5, $t3, $t4
	lw $t6, 4($sp)
	add $t3, $t6, $t5
	lw $t4, 0($t2)
	lw $t5, 0($t3)
	slt $t2, $t4, $t5
	bnez $t2, Tag18
	li $t3, 4
	lw $t2, 8($sp)
	mul $t4, $t3, $t2
	lw $t5, 4($sp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 12($sp)
	mul $t5, $t3, $t4
	lw $t6, 4($sp)
	add $t3, $t6, $t5
	lw $t4, 0($t2)
	lw $t5, 0($t3)
	bne $t4, $t5, Tag20
	li $t2, 1
	lw $t3, 4($sp)
	lw $t4, 8($sp)
	add $t5, $t4, 1
	lw $t6, 12($sp)
	add $t4, $t6, 1
	move $a0, $t2	# save arguments
	move $a1, $t3	# save arguments
	move $a2, $t5	# save arguments
	move $a3, $t4	# save arguments
	jal Tag15
	move $t7, $v0
	beqz $t7, Tag20
	li $t2, 1
	j Tag21
Tag20:
	li $t2, 0
Tag21:
	bnez $t2, Tag18
	li $t3, 0
	j Tag19
Tag18:
	li $t3, 1
Tag19:
	move $t2, $t3
	move $v0, $t2
	lw $t7, 376($sp)	# level load register
	lw $t6, 372($sp)	# level load register
	lw $t5, 368($sp)	# level load register
	lw $t4, 364($sp)	# level load register
	lw $t3, 360($sp)	# level load register
	lw $t2, 356($sp)	# level load register
	lw $t1, 352($sp)	# level load register
	lw $t0, 348($sp)	# level load register
	lw $ra, 412($sp)
	addiu $sp, $sp, 416
	jr $ra
	j Tag17
Tag16:
	li $t2, 4
	lw $t3, 8($sp)
	mul $t4, $t2, $t3
	lw $t5, 4($sp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 12($sp)
	mul $t5, $t3, $t4
	lw $t6, 4($sp)
	add $t3, $t6, $t5
	lw $t4, 0($t2)
	lw $t5, 0($t3)
	slt $t2, $t4, $t5
	bnez $t2, Tag22
	li $t3, 4
	lw $t2, 8($sp)
	mul $t4, $t3, $t2
	lw $t5, 4($sp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 12($sp)
	mul $t5, $t3, $t4
	lw $t6, 4($sp)
	add $t3, $t6, $t5
	lw $t4, 0($t2)
	lw $t5, 0($t3)
	bne $t4, $t5, Tag24
	lw $t2, 8($sp)
	add $t3, $t2, 1
	li $t4, 4
	mul $t2, $t4, $t3
	lw $t5, 8($gp)
	add $t3, $t5, $t2
	lw $t4, 12($sp)
	add $t2, $t4, 1
	li $t0, 4
	mul $t4, $t0, $t2
	lw $t5, 8($gp)
	add $t0, $t5, $t4
	lw $t1, 0($t3)
	lw $t2, 0($t0)
	bge $t1, $t2, Tag24
	li $t0, 1
	j Tag25
Tag24:
	li $t0, 0
Tag25:
	bnez $t0, Tag22
	li $t1, 0
	j Tag23
Tag22:
	li $t1, 1
Tag23:
	move $t0, $t1
	move $v0, $t0
	lw $t7, 376($sp)	# level load register
	lw $t6, 372($sp)	# level load register
	lw $t5, 368($sp)	# level load register
	lw $t4, 364($sp)	# level load register
	lw $t3, 360($sp)	# level load register
	lw $t2, 356($sp)	# level load register
	lw $t1, 352($sp)	# level load register
	lw $t0, 348($sp)	# level load register
	lw $ra, 412($sp)
	addiu $sp, $sp, 416
	jr $ra
Tag17:
	lw $t7, 376($sp)	# level load register
	lw $t6, 372($sp)	# level load register
	lw $t5, 368($sp)	# level load register
	lw $t4, 364($sp)	# level load register
	lw $t3, 360($sp)	# level load register
	lw $t2, 356($sp)	# level load register
	lw $t1, 352($sp)	# level load register
	lw $t0, 348($sp)	# level load register
	lw $ra, 412($sp)
	addiu $sp, $sp, 416
	jr $ra
Tag26:
	addi $sp, $sp, -544	# enter
	sw $ra, 540($sp)
	sw $t6, 500($sp)	# level save register
	sw $t5, 496($sp)	# level save register
	sw $t4, 492($sp)	# level save register
	sw $t3, 488($sp)	# level save register
	sw $t2, 484($sp)	# level save register
	sw $t1, 480($sp)	# level save register
	sw $t0, 476($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	sw $a1, 4($sp)	# load argument
	sw $a2, 8($sp)	# load argument
	sw $a3, 12($sp)	# load argument
	lw $t3, 16($v1)
	sw $t3, 16($sp)
	li $t2, 0
	sw $t2, 20($sp)
Tag27:
	lw $t2, 20($sp)
	lw $t3, 12($sp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag29
	li $t2, 4
	lw $t3, 20($sp)
	mul $t4, $t2, $t3
	lw $t5, 8($gp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 20($sp)
	mul $t5, $t3, $t4
	lw $t6, 4($sp)
	add $t3, $t6, $t5
	li $t4, 4
	lw $t5, 0($t3)
	mul $t6, $t4, $t5
	lw $t3, 0($sp)
	add $t4, $t3, $t6
	lw $t5, 0($t4)
	sw $t5, 0($t2)
	lw $t3, 20($sp)
	lw $t2, 20($sp)
	add $t3, $t2, 1
	sw $t3, 20($sp)
	j Tag27
Tag29:
	li $t2, 0
	sw $t2, 20($sp)
Tag30:
	lw $t2, 20($sp)
	lw $t3, 16($sp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag32
	li $t2, 4
	lw $t3, 20($sp)
	mul $t4, $t2, $t3
	lw $t5, 12($gp)
	add $t2, $t5, $t4
	li $t3, 0
	sw $t3, 0($t2)
	lw $t4, 20($sp)
	lw $t2, 20($sp)
	add $t3, $t2, 1
	sw $t3, 20($sp)
	j Tag30
Tag32:
	li $t2, 0
	sw $t2, 20($sp)
Tag33:
	lw $t2, 20($sp)
	lw $t3, 12($sp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag35
	li $t2, 4
	lw $t3, 20($sp)
	mul $t4, $t2, $t3
	lw $t5, 8($gp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 0($t2)
	mul $t5, $t3, $t4
	lw $t2, 12($gp)
	add $t3, $t2, $t5
	lw $t4, 0($t3)
	lw $t2, 0($t3)
	add $t4, $t2, 1
	sw $t4, 0($t3)
	lw $t2, 20($sp)
	lw $t3, 20($sp)
	add $t2, $t3, 1
	sw $t2, 20($sp)
	j Tag33
Tag35:
	li $t2, 1
	sw $t2, 20($sp)
Tag36:
	lw $t2, 20($sp)
	lw $t3, 16($sp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag38
	li $t2, 4
	lw $t3, 20($sp)
	mul $t4, $t2, $t3
	lw $t5, 12($gp)
	add $t2, $t5, $t4
	lw $t3, 20($sp)
	sub $t4, $t3, 1
	li $t5, 4
	mul $t3, $t5, $t4
	lw $t6, 12($gp)
	add $t4, $t6, $t3
	lw $t5, 0($t2)
	lw $t3, 0($t4)
	add $t6, $t5, $t3
	sw $t6, 0($t2)
	lw $t3, 20($sp)
	lw $t2, 20($sp)
	add $t3, $t2, 1
	sw $t3, 20($sp)
	j Tag36
Tag38:
	lw $t2, 12($sp)
	sub $t3, $t2, 1
	sw $t3, 20($sp)
Tag39:
	lw $t2, 20($sp)
	sge $t3, $t2, 0
	move $t4, $t3
	beqz $t4, Tag41
	li $t2, 4
	lw $t3, 20($sp)
	mul $t4, $t2, $t3
	lw $t5, 8($gp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 0($t2)
	mul $t5, $t3, $t4
	lw $t2, 12($gp)
	add $t3, $t2, $t5
	lw $t4, 0($t3)
	sub $t2, $t4, 1
	sw $t2, 0($t3)
	li $t4, 4
	lw $t2, 0($t3)
	mul $t5, $t4, $t2
	lw $t3, 8($sp)
	add $t2, $t3, $t5
	li $t4, 4
	lw $t3, 20($sp)
	mul $t5, $t4, $t3
	lw $t6, 4($sp)
	add $t3, $t6, $t5
	lw $t4, 0($t3)
	sw $t4, 0($t2)
	lw $t3, 20($sp)
	lw $t2, 20($sp)
	sub $t3, $t2, 1
	sw $t3, 20($sp)
	j Tag39
Tag41:
	lw $t6, 500($sp)	# level load register
	lw $t5, 496($sp)	# level load register
	lw $t4, 492($sp)	# level load register
	lw $t3, 488($sp)	# level load register
	lw $t2, 484($sp)	# level load register
	lw $t1, 480($sp)	# level load register
	lw $t0, 476($sp)	# level load register
	lw $ra, 540($sp)
	addiu $sp, $sp, 544
	jr $ra
Tag42:
	addi $sp, $sp, -1480	# enter
	sw $ra, 1476($sp)
	sw $s0, 1444($sp)	# level save register
	sw $t7, 1440($sp)	# level save register
	sw $t6, 1436($sp)	# level save register
	sw $t5, 1432($sp)	# level save register
	sw $t4, 1428($sp)	# level save register
	sw $t3, 1424($sp)	# level save register
	sw $t2, 1420($sp)	# level save register
	sw $t1, 1416($sp)	# level save register
	sw $t0, 1412($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	sw $a1, 4($sp)	# load argument
	sw $a2, 8($sp)	# load argument
	sw $a3, 12($sp)	# load argument
	li $t2, 4
	lw $t3, 8($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($sp)
	add $t2, $t5, $t4
	sw $t2, 24($sp)
	li $t3, 4
	lw $t2, 8($sp)
	mul $t4, $t3, $t2
	lw $t5, 4($sp)
	add $t2, $t5, $t4
	sw $t2, 28($sp)
	li $t3, 0
	sw $t3, 32($sp)
	lw $t2, 8($sp)
	add $t3, $t2, 1
	div $t4, $t3, 3
	sw $t4, 36($sp)
	li $t2, 0
	sw $t2, 40($sp)
	li $t3, 4
	lw $t2, 8($sp)
	mul $t4, $t3, $t2
	lw $t5, 0($sp)
	add $t2, $t5, $t4
	lw $t3, 8($sp)
	add $t4, $t3, 1
	li $t5, 4
	mul $t3, $t5, $t4
	lw $t6, 0($sp)
	add $t4, $t6, $t3
	li $t5, 0
	sw $t5, 0($t4)
	lw $t3, 0($t4)
	sw $t3, 0($t2)
	li $t4, 0
	sw $t4, 16($sp)
Tag43:
	lw $t2, 16($sp)
	lw $t3, 8($sp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag45
	lw $t2, 16($sp)
	rem $t3, $t2, 3
	sne $t4, $t3, 0
	move $t2, $t4
	beqz $t2, Tag46
	lw $t3, 40($sp)
	lw $t2, 40($sp)
	add $t4, $t2, 1
	sw $t4, 40($sp)
	li $t2, 4
	mul $t4, $t2, $t3
	lw $t5, 0($gp)
	add $t2, $t5, $t4
	lw $t3, 16($sp)
	sw $t3, 0($t2)
Tag46:
	lw $t2, 16($sp)
	lw $t3, 16($sp)
	add $t2, $t3, 1
	sw $t2, 16($sp)
	j Tag43
Tag45:
	lw $t2, 0($sp)
	add $t3, $t2, 8
	lw $t4, 0($gp)
	lw $t2, 4($gp)
	lw $t5, 40($sp)
	lw $t6, 12($sp)
	move $a0, $t3	# save arguments
	move $a1, $t4	# save arguments
	move $a2, $t2	# save arguments
	move $a3, $t5	# save arguments
	sw $t6, 16($v1)	# save arguments
	jal Tag26
	move $t7, $v0
	lw $s0, 0($sp)
	add $t2, $s0, 4
	lw $t3, 4($gp)
	lw $t4, 0($gp)
	lw $t5, 40($sp)
	lw $t6, 12($sp)
	move $a0, $t2	# save arguments
	move $a1, $t3	# save arguments
	move $a2, $t4	# save arguments
	move $a3, $t5	# save arguments
	sw $t6, 16($v1)	# save arguments
	jal Tag26
	move $t7, $v0
	lw $s0, 0($sp)
	lw $t2, 0($gp)
	lw $t3, 4($gp)
	lw $t4, 40($sp)
	lw $t5, 12($sp)
	move $a0, $s0	# save arguments
	move $a1, $t2	# save arguments
	move $a2, $t3	# save arguments
	move $a3, $t4	# save arguments
	sw $t5, 16($v1)	# save arguments
	jal Tag26
	move $t6, $v0
	li $t7, 1
	sw $t7, 44($sp)
	lw $t2, 4($gp)
	lw $t3, 0($t2)
	lw $t4, 36($sp)
	move $a0, $t3	# save arguments
	move $a1, $t4	# save arguments
	jal Tag3
	move $t2, $v0
	li $t5, 4
	mul $t3, $t5, $t2
	lw $t4, 24($sp)
	add $t2, $t4, $t3
	li $t5, 0
	sw $t5, 0($t2)
	li $t3, 1
	sw $t3, 16($sp)
Tag47:
	lw $t2, 16($sp)
	lw $t3, 40($sp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag49
	lw $t2, 0($sp)
	lw $t3, 16($sp)
	sub $t4, $t3, 1
	li $t5, 4
	mul $t3, $t5, $t4
	lw $t6, 4($gp)
	add $t4, $t6, $t3
	lw $t5, 0($t4)
	li $t3, 4
	lw $t4, 16($sp)
	mul $t6, $t3, $t4
	lw $t7, 4($gp)
	add $t3, $t7, $t6
	lw $t4, 0($t3)
	move $a0, $t2	# save arguments
	move $a1, $t5	# save arguments
	move $a2, $t4	# save arguments
	jal Tag12
	move $t6, $v0
	move $t3, $t6
	beqz $t3, Tag50
	li $t2, 4
	lw $t3, 16($sp)
	mul $t4, $t2, $t3
	lw $t5, 4($gp)
	add $t2, $t5, $t4
	lw $t3, 0($t2)
	lw $t4, 36($sp)
	move $a0, $t3	# save arguments
	move $a1, $t4	# save arguments
	jal Tag3
	move $t2, $v0
	li $t5, 4
	mul $t3, $t5, $t2
	lw $t4, 24($sp)
	add $t2, $t4, $t3
	lw $t5, 44($sp)
	sub $t3, $t5, 1
	sw $t3, 0($t2)
	j Tag51
Tag50:
	li $t2, 4
	lw $t3, 16($sp)
	mul $t4, $t2, $t3
	lw $t5, 4($gp)
	add $t2, $t5, $t4
	lw $t3, 0($t2)
	lw $t4, 36($sp)
	move $a0, $t3	# save arguments
	move $a1, $t4	# save arguments
	jal Tag3
	move $t2, $v0
	li $t5, 4
	mul $t3, $t5, $t2
	lw $t4, 24($sp)
	add $t2, $t4, $t3
	lw $t5, 44($sp)
	lw $t3, 44($sp)
	add $t4, $t3, 1
	sw $t4, 44($sp)
	sw $t5, 0($t2)
Tag51:
	lw $t2, 16($sp)
	lw $t3, 16($sp)
	add $t2, $t3, 1
	sw $t2, 16($sp)
	j Tag47
Tag49:
	lw $t2, 44($sp)
	lw $t3, 40($sp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag52
	lw $t2, 24($sp)
	lw $t3, 28($sp)
	lw $t4, 40($sp)
	lw $t5, 44($sp)
	move $a0, $t2	# save arguments
	move $a1, $t3	# save arguments
	move $a2, $t4	# save arguments
	move $a3, $t5	# save arguments
	jal Tag42
	move $t6, $v0
	j Tag56
Tag52:
	li $t2, 0
	sw $t2, 16($sp)
Tag54:
	lw $t2, 16($sp)
	lw $t3, 40($sp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag56
	li $t2, 4
	lw $t3, 16($sp)
	mul $t4, $t2, $t3
	lw $t5, 24($sp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 0($t2)
	mul $t5, $t3, $t4
	lw $t2, 28($sp)
	add $t3, $t2, $t5
	lw $t4, 16($sp)
	sw $t4, 0($t3)
	lw $t2, 16($sp)
	lw $t3, 16($sp)
	add $t2, $t3, 1
	sw $t2, 16($sp)
	j Tag54
Tag56:
	li $t2, 0
	sw $t2, 16($sp)
Tag57:
	lw $t2, 16($sp)
	lw $t3, 40($sp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag59
	li $t2, 4
	lw $t3, 16($sp)
	mul $t4, $t2, $t3
	lw $t5, 28($sp)
	add $t2, $t5, $t4
	lw $t3, 0($t2)
	lw $t4, 36($sp)
	slt $t2, $t3, $t4
	move $t5, $t2
	beqz $t5, Tag60
	lw $t2, 32($sp)
	lw $t3, 32($sp)
	add $t4, $t3, 1
	sw $t4, 32($sp)
	li $t3, 4
	mul $t4, $t3, $t2
	lw $t5, 4($gp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 16($sp)
	mul $t5, $t3, $t4
	lw $t6, 28($sp)
	add $t3, $t6, $t5
	lw $t4, 0($t3)
	mul $t5, $t4, 3
	sw $t5, 0($t2)
Tag60:
	lw $t2, 16($sp)
	lw $t3, 16($sp)
	add $t2, $t3, 1
	sw $t2, 16($sp)
	j Tag57
Tag59:
	lw $t2, 8($sp)
	rem $t3, $t2, 3
	seq $t4, $t3, 1
	move $t2, $t4
	beqz $t2, Tag61
	lw $t3, 32($sp)
	lw $t2, 32($sp)
	add $t4, $t2, 1
	sw $t4, 32($sp)
	li $t2, 4
	mul $t4, $t2, $t3
	lw $t5, 4($gp)
	add $t2, $t5, $t4
	lw $t3, 8($sp)
	sub $t4, $t3, 1
	sw $t4, 0($t2)
Tag61:
	lw $t2, 0($sp)
	lw $t3, 4($gp)
	lw $t4, 0($gp)
	lw $t5, 32($sp)
	lw $t6, 12($sp)
	move $a0, $t2	# save arguments
	move $a1, $t3	# save arguments
	move $a2, $t4	# save arguments
	move $a3, $t5	# save arguments
	sw $t6, 16($v1)	# save arguments
	jal Tag26
	move $t7, $v0
	li $s0, 0
	sw $s0, 16($sp)
Tag62:
	lw $t2, 16($sp)
	lw $t3, 40($sp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag64
	li $t2, 4
	lw $t3, 16($sp)
	mul $t4, $t2, $t3
	lw $t5, 4($gp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 16($sp)
	mul $t5, $t3, $t4
	lw $t6, 28($sp)
	add $t3, $t6, $t5
	lw $t4, 0($t3)
	lw $t5, 36($sp)
	move $a0, $t4	# save arguments
	move $a1, $t5	# save arguments
	jal Tag6
	move $t3, $v0
	sw $t3, 0($t2)
	li $t4, 4
	lw $t3, 0($t2)
	mul $t5, $t4, $t3
	lw $t2, 8($gp)
	add $t3, $t2, $t5
	lw $t4, 16($sp)
	sw $t4, 0($t3)
	lw $t2, 16($sp)
	lw $t3, 16($sp)
	add $t2, $t3, 1
	sw $t2, 16($sp)
	j Tag62
Tag64:
	li $t2, 0
	sw $t2, 16($sp)
	li $t3, 0
	sw $t3, 20($sp)
	li $t2, 0
	sw $t2, 44($sp)
Tag65:
	lw $t2, 16($sp)
	lw $t3, 32($sp)
	bge $t2, $t3, Tag68
	lw $t4, 20($sp)
	lw $t2, 40($sp)
	bge $t4, $t2, Tag68
	li $t3, 1
	j Tag69
Tag68:
	li $t3, 0
Tag69:
	move $t2, $t3
	beqz $t2, Tag67
	li $t3, 4
	lw $t2, 20($sp)
	mul $t4, $t3, $t2
	lw $t5, 4($gp)
	add $t2, $t5, $t4
	lw $t3, 0($t2)
	rem $t4, $t3, 3
	lw $t2, 0($sp)
	li $t3, 4
	lw $t5, 16($sp)
	mul $t6, $t3, $t5
	lw $t7, 0($gp)
	add $t3, $t7, $t6
	lw $t5, 0($t3)
	li $t6, 4
	lw $t3, 20($sp)
	mul $t7, $t6, $t3
	lw $s0, 4($gp)
	add $t3, $s0, $t7
	lw $t6, 0($t3)
	move $a0, $t4	# save arguments
	move $a1, $t2	# save arguments
	move $a2, $t5	# save arguments
	move $a3, $t6	# save arguments
	jal Tag15
	move $t7, $v0
	move $t3, $t7
	beqz $t3, Tag70
	li $t2, 4
	lw $t3, 44($sp)
	mul $t4, $t2, $t3
	lw $t5, 4($sp)
	add $t2, $t5, $t4
	lw $t3, 16($sp)
	lw $t4, 16($sp)
	add $t5, $t4, 1
	sw $t5, 16($sp)
	li $t4, 4
	mul $t5, $t4, $t3
	lw $t6, 0($gp)
	add $t3, $t6, $t5
	lw $t4, 0($t3)
	sw $t4, 0($t2)
	j Tag71
Tag70:
	li $t2, 4
	lw $t3, 44($sp)
	mul $t4, $t2, $t3
	lw $t5, 4($sp)
	add $t2, $t5, $t4
	lw $t3, 20($sp)
	lw $t4, 20($sp)
	add $t5, $t4, 1
	sw $t5, 20($sp)
	li $t4, 4
	mul $t5, $t4, $t3
	lw $t6, 4($gp)
	add $t3, $t6, $t5
	lw $t4, 0($t3)
	sw $t4, 0($t2)
Tag71:
	lw $t2, 44($sp)
	lw $t3, 44($sp)
	add $t2, $t3, 1
	sw $t2, 44($sp)
	j Tag65
Tag67:
	lw $t2, 16($sp)
	lw $t3, 32($sp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag74
	li $t2, 4
	lw $t3, 44($sp)
	mul $t4, $t2, $t3
	lw $t5, 4($sp)
	add $t2, $t5, $t4
	lw $t3, 16($sp)
	lw $t4, 16($sp)
	add $t5, $t4, 1
	sw $t5, 16($sp)
	li $t4, 4
	mul $t5, $t4, $t3
	lw $t6, 0($gp)
	add $t3, $t6, $t5
	lw $t4, 0($t3)
	sw $t4, 0($t2)
	lw $t3, 44($sp)
	lw $t2, 44($sp)
	add $t3, $t2, 1
	sw $t3, 44($sp)
	j Tag67
Tag74:
	lw $t2, 20($sp)
	lw $t3, 40($sp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag77
	li $t2, 4
	lw $t3, 44($sp)
	mul $t4, $t2, $t3
	lw $t5, 4($sp)
	add $t2, $t5, $t4
	lw $t3, 20($sp)
	lw $t4, 20($sp)
	add $t5, $t4, 1
	sw $t5, 20($sp)
	li $t4, 4
	mul $t5, $t4, $t3
	lw $t6, 4($gp)
	add $t3, $t6, $t5
	lw $t4, 0($t3)
	sw $t4, 0($t2)
	lw $t3, 44($sp)
	lw $t2, 44($sp)
	add $t3, $t2, 1
	sw $t3, 44($sp)
	j Tag74
Tag77:
	lw $s0, 1444($sp)	# level load register
	lw $t7, 1440($sp)	# level load register
	lw $t6, 1436($sp)	# level load register
	lw $t5, 1432($sp)	# level load register
	lw $t4, 1428($sp)	# level load register
	lw $t3, 1424($sp)	# level load register
	lw $t2, 1420($sp)	# level load register
	lw $t1, 1416($sp)	# level load register
	lw $t0, 1412($sp)	# level load register
	lw $ra, 1476($sp)
	addiu $sp, $sp, 1480
	jr $ra
Tag78:
	addi $sp, $sp, -164	# enter
	sw $ra, 160($sp)
	sw $t4, 112($sp)	# level save register
	sw $t3, 108($sp)	# level save register
	sw $t2, 104($sp)	# level save register
	sw $t1, 100($sp)	# level save register
	sw $t0, 96($sp)	# level save register
	jal Tag2
	move $t1, $v0
	sw $t1, 0($sp)
	li $t2, 0
	sw $t2, 4($sp)
Tag79:
	lw $t1, 0($sp)
	slt $t2, $t1, 48
	bnez $t2, Tag82
	lw $t1, 0($sp)
	sgt $t2, $t1, 57
	bnez $t2, Tag82
	li $t1, 0
	j Tag83
Tag82:
	li $t1, 1
Tag83:
	move $t2, $t1
	beqz $t2, Tag81
	jal Tag2
	move $t1, $v0
	sw $t1, 0($sp)
	j Tag79
Tag81:
	lw $t1, 0($sp)
	blt $t1, 48, Tag87
	lw $t2, 0($sp)
	bgt $t2, 57, Tag87
	li $t1, 1
	j Tag88
Tag87:
	li $t1, 0
Tag88:
	move $t2, $t1
	beqz $t2, Tag86
	lw $t1, 4($sp)
	mul $t2, $t1, 10
	lw $t3, 0($sp)
	add $t1, $t2, $t3
	sub $t4, $t1, 48
	sw $t4, 4($sp)
	jal Tag2
	move $t1, $v0
	sw $t1, 0($sp)
	j Tag81
Tag86:
	lw $t1, 4($sp)
	move $v0, $t1
	lw $t4, 112($sp)	# level load register
	lw $t3, 108($sp)	# level load register
	lw $t2, 104($sp)	# level load register
	lw $t1, 100($sp)	# level load register
	lw $t0, 96($sp)	# level load register
	lw $ra, 160($sp)
	addiu $sp, $sp, 164
	jr $ra
	lw $t4, 112($sp)	# level load register
	lw $t3, 108($sp)	# level load register
	lw $t2, 104($sp)	# level load register
	lw $t1, 100($sp)	# level load register
	lw $t0, 96($sp)	# level load register
	lw $ra, 160($sp)
	addiu $sp, $sp, 164
	jr $ra
Tag89:
	addi $sp, $sp, -336	# enter
	sw $ra, 332($sp)
	sw $t7, 296($sp)	# level save register
	sw $t6, 292($sp)	# level save register
	sw $t5, 288($sp)	# level save register
	sw $t4, 284($sp)	# level save register
	sw $t3, 280($sp)	# level save register
	sw $t2, 276($sp)	# level save register
	sw $t1, 272($sp)	# level save register
	sw $t0, 268($sp)	# level save register
	li $t2, 0
	sw $t2, 4($sp)
	jal Tag78
	move $t3, $v0
	sw $t3, 0($sp)
	li $t2, 0
	sw $t2, 8($sp)
Tag90:
	lw $t2, 8($sp)
	lw $t3, 0($sp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag92
	li $t2, 4
	lw $t3, 8($sp)
	mul $t4, $t2, $t3
	lw $t5, 16($gp)
	add $t2, $t5, $t4
	jal Tag78
	move $t3, $v0
	sw $t3, 0($t2)
	li $t4, 4
	lw $t2, 8($sp)
	mul $t3, $t4, $t2
	lw $t5, 16($gp)
	add $t2, $t5, $t3
	lw $t4, 0($t2)
	lw $t3, 0($t2)
	add $t4, $t3, 1
	sw $t4, 0($t2)
	li $t3, 4
	lw $t2, 8($sp)
	mul $t4, $t3, $t2
	lw $t5, 16($gp)
	add $t2, $t5, $t4
	lw $t3, 0($t2)
	add $t4, $t3, 1
	lw $t2, 4($sp)
	move $a0, $t4	# save arguments
	move $a1, $t2	# save arguments
	jal Tag9
	move $t3, $v0
	sw $t3, 4($sp)
	lw $t2, 8($sp)
	lw $t3, 8($sp)
	add $t2, $t3, 1
	sw $t2, 8($sp)
	j Tag90
Tag92:
	lw $t2, 0($sp)
	lw $t3, 0($sp)
	add $t4, $t3, 1
	sw $t4, 0($sp)
	li $t3, 4
	mul $t4, $t3, $t2
	lw $t5, 16($gp)
	add $t2, $t5, $t4
	li $t3, 0
	sw $t3, 0($t2)
	lw $t4, 16($gp)
	lw $t2, 20($gp)
	lw $t3, 0($sp)
	lw $t5, 4($sp)
	move $a0, $t4	# save arguments
	move $a1, $t2	# save arguments
	move $a2, $t3	# save arguments
	move $a3, $t5	# save arguments
	jal Tag42
	move $t6, $v0
	li $t7, 0
	sw $t7, 8($sp)
Tag93:
	lw $t2, 8($sp)
	lw $t3, 0($sp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag95
	la $t2, Tag96
	li $t3, 4
	lw $t4, 8($sp)
	mul $t5, $t3, $t4
	lw $t6, 20($gp)
	add $t3, $t6, $t5
	lw $t4, 0($t3)
	move $a0, $t4
	li $v0, 1
	syscall
	la $a0, fs_whiteSpace
	li $v0, 4
	syscall
	lw $t3, 8($sp)
	lw $t2, 8($sp)
	add $t3, $t2, 1
	sw $t3, 8($sp)
	j Tag93
Tag95:
	la $t0, Tag97
	move $a0, $t0
	li $v0, 4
	syscall
	lw $t7, 296($sp)	# level load register
	lw $t6, 292($sp)	# level load register
	lw $t5, 288($sp)	# level load register
	lw $t4, 284($sp)	# level load register
	lw $t3, 280($sp)	# level load register
	lw $t2, 276($sp)	# level load register
	lw $t1, 272($sp)	# level load register
	lw $t0, 268($sp)	# level load register
	lw $ra, 332($sp)
	addiu $sp, $sp, 336
	jr $ra
########################################
############### RUN-TIME ###############
########################################

#malloc
Tag1:
	# a0 -- size in bytes (already x4)
	li $v0, 9
	syscall
	jr $ra

#getchar
Tag2:
	li $v0, 12
	syscall
	jr $ra
	
exit:
	li $v0, 10
	syscall
	jr $ra

## Daniel J. Ellard -- 03/13/94
## printf.asm--
## an implementation of a simple printf work-alike.

## printf--
## A simple printf-like function. Understands just the basic forms
## of the %s, %d, %c, and %% formats, and can only have 3 embedded
## formats (so that all of the parameters are passed in registers).
## If there are more than 3 embedded formats, all but the first 3 are
## completely ignored (not even printed).
## Register Usage:
## $a0,$s0 - pointer to format string
## $a1,$s1 - format argument 1 (optional)
## $a2,$s2 - format argument 2 (optional)
## $a3,$s3 - format argument 3 (optional)
## $s4 - count of formats processed.
## $s5 - char at $s4.
## $s6 - pointer to printf buffer
##
Tag0:
	subu $sp, $sp, 56 # set up the stack frame,
	sw $ra, 32($sp) # saving the local environment.
	sw $fp, 28($sp)
	sw $s0, 24($sp)
	sw $s1, 20($sp)
	sw $s2, 16($sp)
	sw $s3, 12($sp)
	sw $s4, 8($sp)
	sw $s5, 4($sp)
	sw $s6, 0($sp)
	sw $s7, 36($sp)
	sw $t0, 40($sp)
	sw $t1, 44($sp)
	sw $t2, 48($sp)
	sw $t3, 52($sp)
	addu $fp, $sp, 52

# grab the arguments:
	move $s0, $a0 # fmt string
	move $s1, $a1 # arg1 (optional)
	move $s2, $a2 # arg2 (optional)
	move $s3, $a3 # arg3 (optional)
	lw $s7, 16($v1)# arg4 (optional)
	lw $t0, 20($v1)# arg5 (optional)

	li $s4, 0 # set # of formats = 0
	la $s6, printf_buf # set s6 = base of printf buffer.

printf_loop: # process each character in the fmt:
	lb $s5, 0($s0) # get the next character, and then
	addu $s0, $s0, 1 # bump up $s0 to the next character.

	beq $s5, '%', printf_fmt # if the fmt character, then do fmt.
	beq $0, $s5, printf_end # if zero, then go to end.

printf_putc:
	sb $s5, 0($s6) # otherwise, just put this char
	sb $0, 1($s6) # into the printf buffer,
	move $a0, $s6 # and then print it with the
	li $v0, 4 # print_str syscall
	syscall

	b printf_loop # loop on.

printf_fmt:
	lb $s5, 0($s0) # see what the fmt character is,
	addu $s0, $s0, 1 # and bump up the pointer.

	beq $s4, 5, printf_loop # if we've already processed 3 args,
# then *ignore* this fmt.
	beq $s5, '0', printf_pre
	beq $s5, 'd', printf_int # if 'd', print as a decimal integer.
	beq $s5, 's', printf_str # if 's', print as a string.
	beq $s5, 'c', printf_char # if 'c', print as a ASCII char.
	beq $s5, '%', printf_perc # if '%', print a '%'
	b printf_loop # otherwise, just continue.

printf_shift_args: # shift over the fmt args,
	move $s1, $s2 # $s1 = $s2
	move $s2, $s3 # $s2 = $s3
	move $s3, $s7 # $s3 = $s7
	move $s7, $t0

	add $s4, $s4, 1 # increment # of args processed.

	b printf_loop # and continue the main loop.
	
printf_pre:
	lb $s5, ($s0)
	sub $s5, $s5, 48
	add $s0, $s0, 2
	
	li $t1, 0
	move $t2, $s1
printf_digits_begin:
	beqz $t2, printf_digits_end
	add $t1, $t1, 1
	div $t2, $t2, 10
	b printf_digits_begin
	
printf_digits_end:
	sub $t1, $s5, $t1
	
printf_pre_output_zero_begin:
	beqz $t1, printf_pre_output_zero_end
	sub $t1, $t1, 1
	li $a0, 0
	li $v0, 1
	syscall
	b printf_pre_output_zero_begin
	
printf_pre_output_zero_end:
	move $a0, $s1
	li $v0, 1
	syscall
	b printf_shift_args
	

printf_int: # deal with a %d:
	move $a0, $s1 # do a print_int syscall of $s1.
	li $v0, 1
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_str: # deal with a %s:
	move $a0, $s1 # do a print_string syscall of $s1.
	li $v0, 4
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_char: # deal with a %c:
	sb $s1, 0($s6) # fill the buffer in with byte $s1,
	sb $0, 1($s6) # and then a null.
	move $a0, $s6 # and then do a print_str syscall
	li $v0, 4 # on the buffer.
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_perc: # deal with a %%:
	li $s5, '%' # (this is redundant)
	sb $s5, 0($s6) # fill the buffer in with byte %,
	sb $0, 1($s6) # and then a null.
	move $a0, $s6 # and then do a print_str syscall
	li $v0, 4 # on the buffer.
	syscall
	b printf_loop # branch to printf_loop

printf_end:
	lw $t1, 44($sp)
	lw $t2, 48($sp)
	lw $t3, 52($sp)
	lw $s7, 36($sp)
	lw $t0, 40($sp)
	lw $ra, 32($sp) # restore the prior environment:
	lw $fp, 28($sp)
	lw $s0, 24($sp)
	lw $s1, 20($sp)
	lw $s2, 16($sp)
	lw $s3, 12($sp)
	lw $s4, 8($sp)
	lw $s5, 4($sp)
	lw $s6, 0($sp)
	addu $sp, $sp, 56 # release the stack frame.
	jr $ra # return.

.data
	printf_buf: .space 2

