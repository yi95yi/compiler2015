	.data
Tag62:
	.asciiz "%d %d\n"
	.align 2
Tag66:
	.asciiz "%d:"
	.align 2
Tag70:
	.asciiz " %d"
	.align 2
Tag71:
	.asciiz "\n"
	.align 2
	.align 2
	fs_whiteSpace: .asciiz " "
	.align 2
	fs_newLine: .asciiz "\n"
	.align 2
	.globl args
args:	.space 16
	.align 2
	.globl stat
stat:	.space 180
	.text
	.align 2
	.globl main
main:
	la $gp, stat
	la $v1, args
	li $t1, 0
	sw $t1, 0($gp)
	li $t2, 0
	sw $t2, 4($gp)
	li $t1, 0
	sw $t1, 8($gp)
	li $t2, 0
	sw $t2, 12($gp)
	li $t1, 0
	sw $t1, 16($gp)
	li $t2, 0
	sw $t2, 20($gp)
	li $t1, 0
	sw $t1, 24($gp)
	li $t2, 0
	sw $t2, 28($gp)
	li $t1, 0
	sw $t1, 32($gp)
	li $t2, 0
	sw $t2, 36($gp)
	li $t1, 0
	sw $t1, 40($gp)
	jal Tag28
	jal exit
Tag3:
	addi $sp, $sp, -376	# enter
	sw $ra, 372($sp)
	sw $t6, 332($sp)	# level save register
	sw $t5, 328($sp)	# level save register
	sw $t4, 324($sp)	# level save register
	sw $t3, 320($sp)	# level save register
	sw $t2, 316($sp)	# level save register
	sw $t1, 312($sp)	# level save register
	sw $t0, 308($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	li $t2, 4
	lw $t3, 0($sp)
	mul $t4, $t2, $t3
	lw $t5, 32($gp)
	add $t2, $t5, $t4
	lw $t3, 0($t2)
	sw $t3, 4($sp)
Tag4:
	lw $t2, 4($sp)
	beqz $t2, Tag6
	li $t3, 8
	move $t2, $t3    #sizeof
	move $a0, $t2	# save arguments
	jal Tag1
	move $t4, $v0
	sw $t4, 8($sp)
	lw $t2, 4($sp)
	add $t3, $t2, 0
	li $t4, 4
	lw $t2, 0($t3)
	mul $t5, $t4, $t2
	lw $t3, 20($gp)
	add $t2, $t3, $t5
	li $t4, 1
	neg $t3, $t4
	lw $t5, 0($t2)
	seq $t4, $t5, $t3
	move $t2, $t4
	beqz $t2, Tag7
	lw $t3, 4($sp)
	add $t2, $t3, 0
	li $t4, 4
	lw $t3, 0($t2)
	mul $t5, $t4, $t3
	lw $t2, 20($gp)
	add $t3, $t2, $t5
	lw $t4, 0($sp)
	sw $t4, 0($t3)
	lw $t2, 4($sp)
	add $t3, $t2, 0
	lw $t4, 0($t3)
	move $a0, $t4	# save arguments
	jal Tag3
	move $t2, $v0
Tag7:
	lw $t2, 8($sp)
	add $t3, $t2, 0
	lw $t4, 0($sp)
	sw $t4, 0($t3)
	lw $t2, 8($sp)
	add $t3, $t2, 4
	lw $t4, 4($sp)
	add $t2, $t4, 0
	li $t5, 4
	lw $t4, 0($t2)
	mul $t6, $t5, $t4
	lw $t2, 36($gp)
	add $t4, $t2, $t6
	lw $t5, 0($t4)
	sw $t5, 0($t3)
	lw $t2, 4($sp)
	add $t3, $t2, 0
	li $t4, 4
	lw $t2, 0($t3)
	mul $t5, $t4, $t2
	lw $t3, 36($gp)
	add $t2, $t3, $t5
	lw $t4, 8($sp)
	sw $t4, 0($t2)
	lw $t3, 4($sp)
	add $t2, $t3, 4
	lw $t4, 0($t2)
	sw $t4, 4($sp)
	j Tag4
Tag6:
	lw $t2, 8($gp)
	add $t3, $t2, 1
	sw $t3, 8($gp)
	lw $t2, 0($gp)
	lw $t3, 8($gp)
	sub $t4, $t2, $t3
	li $t5, 4
	mul $t2, $t5, $t4
	lw $t3, 24($gp)
	add $t4, $t3, $t2
	lw $t5, 0($sp)
	sw $t5, 0($t4)
	li $t2, 4
	lw $t3, 0($sp)
	mul $t4, $t2, $t3
	lw $t0, 28($gp)
	add $t2, $t0, $t4
	lw $t3, 8($gp)
	sw $t3, 0($t2)
	lw $t6, 332($sp)	# level load register
	lw $t5, 328($sp)	# level load register
	lw $t4, 324($sp)	# level load register
	lw $t3, 320($sp)	# level load register
	lw $t2, 316($sp)	# level load register
	lw $t1, 312($sp)	# level load register
	lw $t0, 308($sp)	# level load register
	lw $ra, 372($sp)
	addiu $sp, $sp, 376
	jr $ra
Tag8:
	addi $sp, $sp, -200	# enter
	sw $ra, 196($sp)
	sw $t6, 156($sp)	# level save register
	sw $t5, 152($sp)	# level save register
	sw $t4, 148($sp)	# level save register
	sw $t3, 144($sp)	# level save register
	sw $t2, 140($sp)	# level save register
	sw $t1, 136($sp)	# level save register
	sw $t0, 132($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	sw $a1, 4($sp)	# load argument
Tag9:
	lw $t2, 0($sp)
	lw $t3, 4($sp)
	sne $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag11
	li $t2, 4
	lw $t3, 0($sp)
	mul $t4, $t2, $t3
	lw $t5, 28($gp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 4($sp)
	mul $t5, $t3, $t4
	lw $t6, 28($gp)
	add $t3, $t6, $t5
	lw $t4, 0($t2)
	lw $t5, 0($t3)
	slt $t2, $t4, $t5
	move $t3, $t2
	beqz $t3, Tag12
	li $t2, 4
	lw $t3, 0($sp)
	mul $t4, $t2, $t3
	lw $t5, 12($gp)
	add $t2, $t5, $t4
	lw $t3, 0($t2)
	sw $t3, 0($sp)
	j Tag13
Tag12:
	li $t2, 4
	lw $t3, 4($sp)
	mul $t4, $t2, $t3
	lw $t5, 12($gp)
	add $t2, $t5, $t4
	lw $t3, 0($t2)
	sw $t3, 4($sp)
Tag13:
	j Tag9
Tag11:
	lw $t1, 0($sp)
	move $v0, $t1
	lw $t6, 156($sp)	# level load register
	lw $t5, 152($sp)	# level load register
	lw $t4, 148($sp)	# level load register
	lw $t3, 144($sp)	# level load register
	lw $t2, 140($sp)	# level load register
	lw $t1, 136($sp)	# level load register
	lw $t0, 132($sp)	# level load register
	lw $ra, 196($sp)
	addiu $sp, $sp, 200
	jr $ra
	lw $t6, 156($sp)	# level load register
	lw $t5, 152($sp)	# level load register
	lw $t4, 148($sp)	# level load register
	lw $t3, 144($sp)	# level load register
	lw $t2, 140($sp)	# level load register
	lw $t1, 136($sp)	# level load register
	lw $t0, 132($sp)	# level load register
	lw $ra, 196($sp)
	addiu $sp, $sp, 200
	jr $ra
Tag14:
	addi $sp, $sp, -204	# enter
	sw $ra, 200($sp)
	sw $t4, 152($sp)	# level save register
	sw $t3, 148($sp)	# level save register
	sw $t2, 144($sp)	# level save register
	sw $t1, 140($sp)	# level save register
	sw $t0, 136($sp)	# level save register
	jal Tag2
	move $t1, $v0
	sw $t1, 0($sp)
	li $t2, 0
	sw $t2, 4($sp)
Tag15:
	lw $t1, 0($sp)
	slt $t2, $t1, 48
	bnez $t2, Tag20
	lw $t1, 0($sp)
	sgt $t2, $t1, 57
	bnez $t2, Tag20
	li $t1, 0
	j Tag21
Tag20:
	li $t1, 1
Tag21:
	beqz $t1, Tag18
	lw $t2, 0($sp)
	beq $t2, 45, Tag18
	li $t1, 1
	j Tag19
Tag18:
	li $t1, 0
Tag19:
	move $t2, $t1
	beqz $t2, Tag17
	jal Tag2
	move $t1, $v0
	sw $t1, 0($sp)
	j Tag15
Tag17:
	lw $t1, 0($sp)
	seq $t2, $t1, 45
	move $t3, $t2
	beqz $t3, Tag22
	jal Tag14
	move $t1, $v0
	move $t2, $t1
	neg $t3, $t2
	move $t1, $t3
	move $v0, $t1
	lw $t4, 152($sp)	# level load register
	lw $t3, 148($sp)	# level load register
	lw $t2, 144($sp)	# level load register
	lw $t1, 140($sp)	# level load register
	lw $t0, 136($sp)	# level load register
	lw $ra, 200($sp)
	addiu $sp, $sp, 204
	jr $ra
Tag22:
	lw $t1, 0($sp)
	blt $t1, 48, Tag26
	lw $t2, 0($sp)
	bgt $t2, 57, Tag26
	li $t1, 1
	j Tag27
Tag26:
	li $t1, 0
Tag27:
	move $t2, $t1
	beqz $t2, Tag25
	lw $t1, 4($sp)
	mul $t2, $t1, 10
	lw $t3, 0($sp)
	add $t1, $t2, $t3
	sub $t4, $t1, 48
	sw $t4, 4($sp)
	jal Tag2
	move $t1, $v0
	sw $t1, 0($sp)
	j Tag22
Tag25:
	lw $t1, 4($sp)
	move $v0, $t1
	lw $t4, 152($sp)	# level load register
	lw $t3, 148($sp)	# level load register
	lw $t2, 144($sp)	# level load register
	lw $t1, 140($sp)	# level load register
	lw $t0, 136($sp)	# level load register
	lw $ra, 200($sp)
	addiu $sp, $sp, 204
	jr $ra
	lw $t4, 152($sp)	# level load register
	lw $t3, 148($sp)	# level load register
	lw $t2, 144($sp)	# level load register
	lw $t1, 140($sp)	# level load register
	lw $t0, 136($sp)	# level load register
	lw $ra, 200($sp)
	addiu $sp, $sp, 204
	jr $ra
Tag28:
	addi $sp, $sp, -1344	# enter
	sw $ra, 1340($sp)
	sw $t7, 1304($sp)	# level save register
	sw $t6, 1300($sp)	# level save register
	sw $t5, 1296($sp)	# level save register
	sw $t4, 1292($sp)	# level save register
	sw $t3, 1288($sp)	# level save register
	sw $t2, 1284($sp)	# level save register
	sw $t1, 1280($sp)	# level save register
	sw $t0, 1276($sp)	# level save register
	li $t2, 1
	sw $t2, 0($sp)
	jal Tag14
	move $t3, $v0
	sw $t3, 0($gp)
	jal Tag14
	move $t2, $v0
	sw $t2, 4($gp)
	li $t3, 4
	move $t2, $t3    #sizeof
	lw $t4, 0($gp)
	mul $t3, $t4, $t2
	move $a0, $t3	# save arguments
	jal Tag1
	move $t5, $v0
	sw $t5, 12($gp)
	li $t2, 4
	move $t3, $t2    #sizeof
	lw $t4, 0($gp)
	mul $t2, $t4, $t3
	move $a0, $t2	# save arguments
	jal Tag1
	move $t5, $v0
	sw $t5, 20($gp)
	li $t2, 4
	move $t3, $t2    #sizeof
	lw $t4, 0($gp)
	mul $t2, $t4, $t3
	move $a0, $t2	# save arguments
	jal Tag1
	move $t5, $v0
	sw $t5, 24($gp)
	li $t2, 4
	move $t3, $t2    #sizeof
	lw $t4, 0($gp)
	mul $t2, $t4, $t3
	move $a0, $t2	# save arguments
	jal Tag1
	move $t5, $v0
	sw $t5, 28($gp)
	li $t2, 4
	move $t3, $t2    #sizeof
	lw $t4, 0($gp)
	mul $t2, $t4, $t3
	move $a0, $t2	# save arguments
	jal Tag1
	move $t5, $v0
	sw $t5, 32($gp)
	li $t2, 4
	move $t3, $t2    #sizeof
	lw $t4, 0($gp)
	mul $t2, $t4, $t3
	move $a0, $t2	# save arguments
	jal Tag1
	move $t5, $v0
	sw $t5, 36($gp)
	li $t2, 4
	move $t3, $t2    #sizeof
	lw $t4, 0($gp)
	mul $t2, $t4, $t3
	move $a0, $t2	# save arguments
	jal Tag1
	move $t5, $v0
	sw $t5, 40($gp)
	li $t2, 0
	sw $t2, 4($sp)
Tag29:
	lw $t2, 4($sp)
	lw $t3, 0($gp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag31
	li $t2, 4
	lw $t3, 4($sp)
	mul $t4, $t2, $t3
	lw $t5, 20($gp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 4($sp)
	mul $t5, $t3, $t4
	lw $t6, 12($gp)
	add $t3, $t6, $t5
	li $t4, 1
	neg $t5, $t4
	sw $t5, 0($t3)
	lw $t4, 0($t3)
	sw $t4, 0($t2)
	li $t3, 4
	lw $t2, 4($sp)
	mul $t4, $t3, $t2
	lw $t5, 36($gp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 4($sp)
	mul $t5, $t3, $t4
	lw $t6, 32($gp)
	add $t3, $t6, $t5
	li $t4, 0
	sw $t4, 0($t3)
	lw $t5, 0($t3)
	sw $t5, 0($t2)
	lw $t3, 4($sp)
	add $t2, $t3, 1
	sw $t2, 4($sp)
	j Tag29
Tag31:
	li $t2, 0
	sw $t2, 4($sp)
Tag32:
	lw $t2, 4($sp)
	lw $t3, 4($gp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag34
	jal Tag14
	move $t2, $v0
	sw $t2, 8($sp)
	jal Tag14
	move $t3, $v0
	sw $t3, 12($sp)
	li $t2, 8
	move $t3, $t2    #sizeof
	move $a0, $t3	# save arguments
	jal Tag1
	move $t4, $v0
	sw $t4, 16($sp)
	lw $t2, 16($sp)
	add $t3, $t2, 0
	lw $t4, 12($sp)
	sw $t4, 0($t3)
	lw $t2, 16($sp)
	add $t3, $t2, 4
	li $t4, 4
	lw $t2, 8($sp)
	mul $t5, $t4, $t2
	lw $t6, 32($gp)
	add $t2, $t6, $t5
	lw $t4, 0($t2)
	sw $t4, 0($t3)
	li $t2, 4
	lw $t3, 8($sp)
	mul $t4, $t2, $t3
	lw $t5, 32($gp)
	add $t2, $t5, $t4
	lw $t3, 16($sp)
	sw $t3, 0($t2)
	lw $t4, 4($sp)
	add $t2, $t4, 1
	sw $t2, 4($sp)
	j Tag32
Tag34:
	jal Tag14
	move $t2, $v0
	sw $t2, 16($gp)
	li $t3, 4
	lw $t2, 16($gp)
	mul $t4, $t3, $t2
	lw $t5, 12($gp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 16($gp)
	mul $t5, $t3, $t4
	lw $t6, 20($gp)
	add $t3, $t6, $t5
	lw $t4, 16($gp)
	sw $t4, 0($t3)
	lw $t5, 0($t3)
	sw $t5, 0($t2)
	lw $t3, 16($gp)
	move $a0, $t3	# save arguments
	jal Tag3
	move $t2, $v0
Tag35:
	lw $t2, 0($sp)
	beqz $t2, Tag37
	li $t3, 0
	sw $t3, 0($sp)
	li $t2, 1
	sw $t2, 4($sp)
Tag38:
	lw $t2, 4($sp)
	lw $t3, 0($gp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag40
	li $t2, 4
	lw $t3, 4($sp)
	mul $t4, $t2, $t3
	lw $t5, 24($gp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 0($t2)
	mul $t5, $t3, $t4
	lw $t2, 36($gp)
	add $t3, $t2, $t5
	lw $t4, 0($t3)
	sw $t4, 24($sp)
	li $t2, 4
	lw $t3, 4($sp)
	mul $t4, $t2, $t3
	lw $t5, 24($gp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 0($t2)
	mul $t5, $t3, $t4
	lw $t2, 20($gp)
	add $t3, $t2, $t5
	lw $t4, 0($t3)
	sw $t4, 20($sp)
Tag41:
	lw $t2, 24($sp)
	beqz $t2, Tag43
	lw $t3, 24($sp)
	add $t2, $t3, 0
	li $t4, 4
	lw $t3, 0($t2)
	mul $t5, $t4, $t3
	lw $t2, 12($gp)
	add $t3, $t2, $t5
	li $t4, 1
	neg $t2, $t4
	lw $t5, 0($t3)
	sne $t4, $t5, $t2
	move $t3, $t4
	beqz $t3, Tag44
	lw $t2, 20($sp)
	lw $t3, 24($sp)
	add $t4, $t3, 0
	lw $t5, 0($t4)
	move $a0, $t2	# save arguments
	move $a1, $t5	# save arguments
	jal Tag8
	move $t3, $v0
	sw $t3, 20($sp)
Tag44:
	lw $t2, 24($sp)
	add $t3, $t2, 4
	lw $t4, 0($t3)
	sw $t4, 24($sp)
	j Tag41
Tag43:
	li $t2, 4
	lw $t3, 4($sp)
	mul $t4, $t2, $t3
	lw $t5, 24($gp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 0($t2)
	mul $t5, $t3, $t4
	lw $t2, 12($gp)
	add $t3, $t2, $t5
	lw $t4, 20($sp)
	lw $t2, 0($t3)
	sne $t5, $t4, $t2
	move $t3, $t5
	beqz $t3, Tag45
	li $t2, 4
	lw $t3, 4($sp)
	mul $t4, $t2, $t3
	lw $t5, 24($gp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 0($t2)
	mul $t5, $t3, $t4
	lw $t2, 12($gp)
	add $t3, $t2, $t5
	lw $t4, 20($sp)
	sw $t4, 0($t3)
	li $t2, 1
	sw $t2, 0($sp)
Tag45:
	lw $t2, 4($sp)
	add $t3, $t2, 1
	sw $t3, 4($sp)
	j Tag38
Tag40:
	j Tag35
Tag37:
	li $t2, 0
	sw $t2, 4($sp)
Tag46:
	lw $t2, 4($sp)
	lw $t3, 0($gp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag48
	li $t2, 0
	sw $t2, 28($sp)
	li $t3, 4
	lw $t2, 4($sp)
	mul $t4, $t3, $t2
	lw $t5, 36($gp)
	add $t2, $t5, $t4
	lw $t3, 0($t2)
	sw $t3, 32($sp)
Tag49:
	lw $t2, 32($sp)
	beqz $t2, Tag51
	lw $t3, 28($sp)
	add $t2, $t3, 1
	sw $t2, 28($sp)
	lw $t3, 32($sp)
	add $t2, $t3, 4
	lw $t4, 0($t2)
	sw $t4, 32($sp)
	j Tag49
Tag51:
	lw $t2, 28($sp)
	sge $t3, $t2, 2
	move $t4, $t3
	beqz $t4, Tag55
	li $t2, 4
	lw $t3, 4($sp)
	mul $t4, $t2, $t3
	lw $t5, 36($gp)
	add $t2, $t5, $t4
	lw $t3, 0($t2)
	sw $t3, 32($sp)
Tag53:
	lw $t2, 32($sp)
	beqz $t2, Tag55
	lw $t3, 32($sp)
	add $t2, $t3, 0
	lw $t4, 0($t2)
	sw $t4, 36($sp)
Tag56:
	li $t2, 4
	lw $t3, 4($sp)
	mul $t4, $t2, $t3
	lw $t5, 12($gp)
	add $t2, $t5, $t4
	lw $t3, 36($sp)
	lw $t4, 0($t2)
	sne $t5, $t3, $t4
	move $t2, $t5
	beqz $t2, Tag58
	li $t3, 8
	move $t2, $t3    #sizeof
	move $a0, $t2	# save arguments
	jal Tag1
	move $t4, $v0
	sw $t4, 40($sp)
	lw $t2, 40($sp)
	add $t3, $t2, 0
	lw $t4, 4($sp)
	sw $t4, 0($t3)
	lw $t2, 40($sp)
	add $t3, $t2, 4
	li $t4, 4
	lw $t2, 36($sp)
	mul $t5, $t4, $t2
	lw $t6, 40($gp)
	add $t2, $t6, $t5
	lw $t4, 0($t2)
	sw $t4, 0($t3)
	li $t2, 4
	lw $t3, 36($sp)
	mul $t4, $t2, $t3
	lw $t5, 40($gp)
	add $t2, $t5, $t4
	lw $t3, 40($sp)
	sw $t3, 0($t2)
	li $t4, 4
	lw $t2, 36($sp)
	mul $t3, $t4, $t2
	lw $t5, 12($gp)
	add $t2, $t5, $t3
	lw $t4, 0($t2)
	sw $t4, 36($sp)
	j Tag56
Tag58:
	lw $t2, 32($sp)
	add $t3, $t2, 4
	lw $t4, 0($t3)
	sw $t4, 32($sp)
	j Tag53
Tag55:
	lw $t2, 4($sp)
	add $t3, $t2, 1
	sw $t3, 4($sp)
	j Tag46
Tag48:
	li $t2, 0
	sw $t2, 4($sp)
Tag59:
	lw $t2, 4($sp)
	lw $t3, 0($gp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag61
	la $t2, Tag62
	lw $t3, 4($sp)
	li $t4, 4
	lw $t5, 4($sp)
	mul $t6, $t4, $t5
	lw $t7, 12($gp)
	add $t4, $t7, $t6
	lw $t5, 0($t4)
	move $a0, $t2	# save arguments
	move $a1, $t3	# save arguments
	move $a2, $t5	# save arguments
	jal Tag0
	move $t6, $v0
	lw $t4, 4($sp)
	add $t2, $t4, 1
	sw $t2, 4($sp)
	j Tag59
Tag61:
	li $t2, 0
	sw $t2, 4($sp)
Tag63:
	lw $t2, 4($sp)
	lw $t3, 0($gp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag65
	li $t2, 4
	lw $t3, 4($sp)
	mul $t4, $t2, $t3
	lw $t5, 40($gp)
	add $t2, $t5, $t4
	lw $t3, 0($t2)
	sw $t3, 44($sp)
	la $t2, Tag66
	lw $t3, 4($sp)
	move $a0, $t2	# save arguments
	move $a1, $t3	# save arguments
	jal Tag0
	move $t4, $v0
Tag67:
	lw $t2, 44($sp)
	beqz $t2, Tag69
	la $t3, Tag70
	lw $t2, 44($sp)
	add $t4, $t2, 0
	lw $t5, 0($t4)
	move $a0, $t3	# save arguments
	move $a1, $t5	# save arguments
	jal Tag0
	move $t2, $v0
	lw $t4, 44($sp)
	add $t2, $t4, 4
	lw $t3, 0($t2)
	sw $t3, 44($sp)
	j Tag67
Tag69:
	la $t2, Tag71
	move $a0, $t2
	li $v0, 4
	syscall
	lw $t4, 4($sp)
	add $t2, $t4, 1
	sw $t2, 4($sp)
	j Tag63
Tag65:
	li $t0, 0
	move $v0, $t0
	lw $t7, 1304($sp)	# level load register
	lw $t6, 1300($sp)	# level load register
	lw $t5, 1296($sp)	# level load register
	lw $t4, 1292($sp)	# level load register
	lw $t3, 1288($sp)	# level load register
	lw $t2, 1284($sp)	# level load register
	lw $t1, 1280($sp)	# level load register
	lw $t0, 1276($sp)	# level load register
	lw $ra, 1340($sp)
	addiu $sp, $sp, 1344
	jr $ra
	lw $t7, 1304($sp)	# level load register
	lw $t6, 1300($sp)	# level load register
	lw $t5, 1296($sp)	# level load register
	lw $t4, 1292($sp)	# level load register
	lw $t3, 1288($sp)	# level load register
	lw $t2, 1284($sp)	# level load register
	lw $t1, 1280($sp)	# level load register
	lw $t0, 1276($sp)	# level load register
	lw $ra, 1340($sp)
	addiu $sp, $sp, 1344
	jr $ra
########################################
############### RUN-TIME ###############
########################################

#malloc
Tag1:
	# a0 -- size in bytes (already x4)
	li $v0, 9
	syscall
	jr $ra

#getchar
Tag2:
	li $v0, 12
	syscall
	jr $ra
	
exit:
	li $v0, 10
	syscall
	jr $ra

## Daniel J. Ellard -- 03/13/94
## printf.asm--
## an implementation of a simple printf work-alike.

## printf--
## A simple printf-like function. Understands just the basic forms
## of the %s, %d, %c, and %% formats, and can only have 3 embedded
## formats (so that all of the parameters are passed in registers).
## If there are more than 3 embedded formats, all but the first 3 are
## completely ignored (not even printed).
## Register Usage:
## $a0,$s0 - pointer to format string
## $a1,$s1 - format argument 1 (optional)
## $a2,$s2 - format argument 2 (optional)
## $a3,$s3 - format argument 3 (optional)
## $s4 - count of formats processed.
## $s5 - char at $s4.
## $s6 - pointer to printf buffer
##
Tag0:
	subu $sp, $sp, 56 # set up the stack frame,
	sw $ra, 32($sp) # saving the local environment.
	sw $fp, 28($sp)
	sw $s0, 24($sp)
	sw $s1, 20($sp)
	sw $s2, 16($sp)
	sw $s3, 12($sp)
	sw $s4, 8($sp)
	sw $s5, 4($sp)
	sw $s6, 0($sp)
	sw $s7, 36($sp)
	sw $t0, 40($sp)
	sw $t1, 44($sp)
	sw $t2, 48($sp)
	sw $t3, 52($sp)
	addu $fp, $sp, 52

# grab the arguments:
	move $s0, $a0 # fmt string
	move $s1, $a1 # arg1 (optional)
	move $s2, $a2 # arg2 (optional)
	move $s3, $a3 # arg3 (optional)
	lw $s7, 16($v1)# arg4 (optional)
	lw $t0, 20($v1)# arg5 (optional)

	li $s4, 0 # set # of formats = 0
	la $s6, printf_buf # set s6 = base of printf buffer.

printf_loop: # process each character in the fmt:
	lb $s5, 0($s0) # get the next character, and then
	addu $s0, $s0, 1 # bump up $s0 to the next character.

	beq $s5, '%', printf_fmt # if the fmt character, then do fmt.
	beq $0, $s5, printf_end # if zero, then go to end.

printf_putc:
	sb $s5, 0($s6) # otherwise, just put this char
	sb $0, 1($s6) # into the printf buffer,
	move $a0, $s6 # and then print it with the
	li $v0, 4 # print_str syscall
	syscall

	b printf_loop # loop on.

printf_fmt:
	lb $s5, 0($s0) # see what the fmt character is,
	addu $s0, $s0, 1 # and bump up the pointer.

	beq $s4, 5, printf_loop # if we've already processed 3 args,
# then *ignore* this fmt.
	beq $s5, '0', printf_pre
	beq $s5, 'd', printf_int # if 'd', print as a decimal integer.
	beq $s5, 's', printf_str # if 's', print as a string.
	beq $s5, 'c', printf_char # if 'c', print as a ASCII char.
	beq $s5, '%', printf_perc # if '%', print a '%'
	b printf_loop # otherwise, just continue.

printf_shift_args: # shift over the fmt args,
	move $s1, $s2 # $s1 = $s2
	move $s2, $s3 # $s2 = $s3
	move $s3, $s7 # $s3 = $s7
	move $s7, $t0

	add $s4, $s4, 1 # increment # of args processed.

	b printf_loop # and continue the main loop.
	
printf_pre:
	lb $s5, ($s0)
	sub $s5, $s5, 48
	add $s0, $s0, 2
	
	li $t1, 0
	move $t2, $s1
printf_digits_begin:
	beqz $t2, printf_digits_end
	add $t1, $t1, 1
	div $t2, $t2, 10
	b printf_digits_begin
	
printf_digits_end:
	sub $t1, $s5, $t1
	
printf_pre_output_zero_begin:
	beqz $t1, printf_pre_output_zero_end
	sub $t1, $t1, 1
	li $a0, 0
	li $v0, 1
	syscall
	b printf_pre_output_zero_begin
	
printf_pre_output_zero_end:
	move $a0, $s1
	li $v0, 1
	syscall
	b printf_shift_args
	

printf_int: # deal with a %d:
	move $a0, $s1 # do a print_int syscall of $s1.
	li $v0, 1
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_str: # deal with a %s:
	move $a0, $s1 # do a print_string syscall of $s1.
	li $v0, 4
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_char: # deal with a %c:
	sb $s1, 0($s6) # fill the buffer in with byte $s1,
	sb $0, 1($s6) # and then a null.
	move $a0, $s6 # and then do a print_str syscall
	li $v0, 4 # on the buffer.
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_perc: # deal with a %%:
	li $s5, '%' # (this is redundant)
	sb $s5, 0($s6) # fill the buffer in with byte %,
	sb $0, 1($s6) # and then a null.
	move $a0, $s6 # and then do a print_str syscall
	li $v0, 4 # on the buffer.
	syscall
	b printf_loop # branch to printf_loop

printf_end:
	lw $t1, 44($sp)
	lw $t2, 48($sp)
	lw $t3, 52($sp)
	lw $s7, 36($sp)
	lw $t0, 40($sp)
	lw $ra, 32($sp) # restore the prior environment:
	lw $fp, 28($sp)
	lw $s0, 24($sp)
	lw $s1, 20($sp)
	lw $s2, 16($sp)
	lw $s3, 12($sp)
	lw $s4, 8($sp)
	lw $s5, 4($sp)
	lw $s6, 0($sp)
	addu $sp, $sp, 56 # release the stack frame.
	jr $ra # return.

.data
	printf_buf: .space 2

