	.data
Tag10:
	.asciiz "%d "
	.align 2
Tag11:
	.asciiz "\n"
	.align 2
	.align 2
	fs_whiteSpace: .asciiz " "
	.align 2
	fs_newLine: .asciiz "\n"
	.align 2
	.globl args
args:	.space 40
	.align 2
	.globl stat
stat:	.space 52
	.text
	.align 2
	.globl main
main:
	la $gp, stat
	la $v1, args
	li $t1, 0
	sw $t1, 0($gp)
	li $t2, 0
	sw $t2, 4($gp)
	li $t1, 0
	sw $t1, 8($gp)
	jal Tag26
	jal exit
Tag3:
	addi $sp, $sp, -212	# enter
	sw $ra, 208($sp)
	sw $t6, 168($sp)	# level save register
	sw $t5, 164($sp)	# level save register
	sw $t4, 160($sp)	# level save register
	sw $t3, 156($sp)	# level save register
	sw $t2, 152($sp)	# level save register
	sw $t1, 148($sp)	# level save register
	sw $t0, 144($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	sw $a1, 4($sp)	# load argument
	li $t1, 0
	sw $t1, 8($sp)
Tag4:
	lw $t1, 8($sp)
	lw $t2, 0($sp)
	slt $t3, $t1, $t2
	move $t4, $t3
	beqz $t4, Tag6
	li $t1, 0
	sw $t1, 12($sp)
Tag7:
	lw $t1, 12($sp)
	lw $t2, 0($sp)
	slt $t3, $t1, $t2
	move $t4, $t3
	beqz $t4, Tag9
	la $t1, Tag10
	li $t2, 4
	lw $t3, 8($sp)
	mul $t4, $t2, $t3
	lw $t5, 4($sp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 12($sp)
	mul $t5, $t3, $t4
	lw $t6, 0($t2)
	add $t3, $t6, $t5
	lw $t2, 0($t3)
	move $a0, $t2
	li $v0, 1
	syscall
	la $a0, fs_whiteSpace
	li $v0, 4
	syscall
	lw $t3, 12($sp)
	lw $t1, 12($sp)
	add $t2, $t1, 1
	sw $t2, 12($sp)
	j Tag7
Tag9:
	la $t1, Tag11
	move $a0, $t1
	li $v0, 4
	syscall
	lw $t3, 8($sp)
	lw $t1, 8($sp)
	add $t2, $t1, 1
	sw $t2, 8($sp)
	j Tag4
Tag6:
	li $t0, 0
	move $v0, $t0
	lw $t6, 168($sp)	# level load register
	lw $t5, 164($sp)	# level load register
	lw $t4, 160($sp)	# level load register
	lw $t3, 156($sp)	# level load register
	lw $t2, 152($sp)	# level load register
	lw $t1, 148($sp)	# level load register
	lw $t0, 144($sp)	# level load register
	lw $ra, 208($sp)
	addiu $sp, $sp, 212
	jr $ra
	lw $t6, 168($sp)	# level load register
	lw $t5, 164($sp)	# level load register
	lw $t4, 160($sp)	# level load register
	lw $t3, 156($sp)	# level load register
	lw $t2, 152($sp)	# level load register
	lw $t1, 148($sp)	# level load register
	lw $t0, 144($sp)	# level load register
	lw $ra, 208($sp)
	addiu $sp, $sp, 212
	jr $ra
Tag12:
	addi $sp, $sp, -156	# enter
	sw $ra, 152($sp)
	sw $t3, 100($sp)	# level save register
	sw $t2, 96($sp)	# level save register
	sw $t1, 92($sp)	# level save register
	sw $t0, 88($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	sw $a1, 4($sp)	# load argument
	sw $a2, 8($sp)	# load argument
	sw $a3, 12($sp)	# load argument
	lw $t2, 16($v1)
	sw $t2, 16($sp)
	lw $t3, 20($v1)
	sw $t3, 20($sp)
	lw $t1, 0($sp)
	lw $t2, 8($sp)
	blt $t1, $t2, Tag13
	lw $t3, 0($sp)
	lw $t1, 16($sp)
	bge $t3, $t1, Tag13
	lw $t2, 4($sp)
	lw $t1, 12($sp)
	blt $t2, $t1, Tag13
	lw $t3, 4($sp)
	lw $t1, 20($sp)
	bge $t3, $t1, Tag13
	li $t0, 1
	j Tag14
Tag13:
	li $t0, 0
Tag14:
	move $t1, $t0
	move $v0, $t1
	lw $t3, 100($sp)	# level load register
	lw $t2, 96($sp)	# level load register
	lw $t1, 92($sp)	# level load register
	lw $t0, 88($sp)	# level load register
	lw $ra, 152($sp)
	addiu $sp, $sp, 156
	jr $ra
	lw $t3, 100($sp)	# level load register
	lw $t2, 96($sp)	# level load register
	lw $t1, 92($sp)	# level load register
	lw $t0, 88($sp)	# level load register
	lw $ra, 152($sp)
	addiu $sp, $sp, 156
	jr $ra
Tag15:
	addi $sp, $sp, -576	# enter
	sw $ra, 572($sp)
	sw $s2, 548($sp)	# level save register
	sw $s1, 544($sp)	# level save register
	sw $s0, 540($sp)	# level save register
	sw $t7, 536($sp)	# level save register
	sw $t6, 532($sp)	# level save register
	sw $t5, 528($sp)	# level save register
	sw $t4, 524($sp)	# level save register
	sw $t3, 520($sp)	# level save register
	sw $t2, 516($sp)	# level save register
	sw $t1, 512($sp)	# level save register
	sw $t0, 508($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	sw $a1, 4($sp)	# load argument
	sw $a2, 8($sp)	# load argument
	sw $a3, 12($sp)	# load argument
	lw $t2, 16($v1)
	sw $t2, 16($sp)
	lw $t3, 20($v1)
	sw $t3, 20($sp)
	lw $t2, 24($v1)
	sw $t2, 24($sp)
	lw $t3, 28($v1)
	sw $t3, 28($sp)
	lw $t2, 32($v1)
	sw $t2, 32($sp)
	lw $t1, 4($sp)
	seq $t2, $t1, 0
	move $t3, $t2
	beqz $t3, Tag16
	li $t1, 4
	lw $t2, 16($sp)
	mul $t3, $t1, $t2
	lw $t4, 0($sp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 20($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($t1)
	add $t2, $t5, $t4
	lw $t1, 24($sp)
	sw $t1, 0($t2)
	j Tag20
Tag16:
	lw $t1, 4($sp)
	sub $t2, $t1, 1
	li $t3, 4
	mul $t1, $t3, $t2
	lw $t4, 28($sp)
	add $t2, $t4, $t1
	lw $t3, 0($t2)
	sw $t3, 36($sp)
	lw $t1, 32($sp)
	sw $t1, 40($sp)
	li $t2, 0
	sw $t2, 56($sp)
	lw $t1, 56($sp)
	sw $t1, 52($sp)
	lw $t2, 52($sp)
	sw $t2, 48($sp)
	lw $t1, 48($sp)
	sw $t1, 44($sp)
	lw $t2, 32($sp)
	lw $t1, 32($sp)
	add $t2, $t1, 1
	sw $t2, 32($sp)
	li $t1, 0
	sw $t1, 60($sp)
Tag18:
	lw $t1, 60($sp)
	sle $t2, $t1, 1
	move $t3, $t2
	beqz $t3, Tag20
	li $t1, 0
	sw $t1, 64($sp)
Tag21:
	lw $t1, 64($sp)
	sle $t2, $t1, 1
	move $t3, $t2
	beqz $t3, Tag23
	lw $t1, 60($sp)
	lw $t2, 36($sp)
	mul $t3, $t1, $t2
	lw $t4, 8($sp)
	add $t1, $t4, $t3
	sw $t1, 44($sp)
	lw $t2, 64($sp)
	lw $t1, 36($sp)
	mul $t3, $t2, $t1
	lw $t4, 12($sp)
	add $t1, $t4, $t3
	sw $t1, 48($sp)
	lw $t2, 60($sp)
	add $t1, $t2, 1
	lw $t3, 36($sp)
	mul $t2, $t1, $t3
	lw $t4, 8($sp)
	add $t1, $t4, $t2
	sw $t1, 52($sp)
	lw $t2, 64($sp)
	add $t1, $t2, 1
	lw $t3, 36($sp)
	mul $t2, $t1, $t3
	lw $t4, 12($sp)
	add $t1, $t4, $t2
	sw $t1, 56($sp)
	lw $t2, 16($sp)
	lw $t1, 20($sp)
	lw $t3, 44($sp)
	lw $t4, 48($sp)
	lw $t5, 52($sp)
	lw $t6, 56($sp)
	move $a0, $t2	# save arguments
	move $a1, $t1	# save arguments
	move $a2, $t3	# save arguments
	move $a3, $t4	# save arguments
	sw $t5, 16($v1)	# save arguments
	sw $t6, 20($v1)	# save arguments
	jal Tag12
	move $t7, $v0
	move $s0, $t7
	beqz $s0, Tag24
	lw $t1, 0($sp)
	lw $t2, 4($sp)
	sub $t3, $t2, 1
	lw $t4, 44($sp)
	lw $t2, 48($sp)
	lw $t5, 16($sp)
	lw $t6, 20($sp)
	lw $t7, 24($sp)
	lw $s0, 28($sp)
	lw $s1, 32($sp)
	move $a0, $t1	# save arguments
	move $a1, $t3	# save arguments
	move $a2, $t4	# save arguments
	move $a3, $t2	# save arguments
	sw $t5, 16($v1)	# save arguments
	sw $t6, 20($v1)	# save arguments
	sw $t7, 24($v1)	# save arguments
	sw $s0, 28($v1)	# save arguments
	sw $s1, 32($v1)	# save arguments
	jal Tag15
	move $s2, $v0
	sw $s2, 32($sp)
	j Tag25
Tag24:
	lw $t1, 0($sp)
	lw $t2, 4($sp)
	sub $t3, $t2, 1
	lw $t4, 44($sp)
	lw $t2, 48($sp)
	lw $t5, 8($sp)
	lw $t6, 36($sp)
	add $t7, $t5, $t6
	sub $s0, $t7, 1
	lw $t5, 60($sp)
	add $t6, $s0, $t5
	lw $t7, 12($sp)
	lw $t5, 36($sp)
	add $s0, $t7, $t5
	sub $s1, $s0, 1
	lw $t5, 64($sp)
	add $t7, $s1, $t5
	lw $s0, 40($sp)
	lw $t5, 28($sp)
	lw $s1, 32($sp)
	move $a0, $t1	# save arguments
	move $a1, $t3	# save arguments
	move $a2, $t4	# save arguments
	move $a3, $t2	# save arguments
	sw $t6, 16($v1)	# save arguments
	sw $t7, 20($v1)	# save arguments
	sw $s0, 24($v1)	# save arguments
	sw $t5, 28($v1)	# save arguments
	sw $s1, 32($v1)	# save arguments
	jal Tag15
	move $s2, $v0
	sw $s2, 32($sp)
Tag25:
	lw $t1, 64($sp)
	lw $t2, 64($sp)
	add $t1, $t2, 1
	sw $t1, 64($sp)
	j Tag21
Tag23:
	lw $t1, 60($sp)
	add $t2, $t1, 1
	sw $t2, 60($sp)
	j Tag18
Tag20:
	lw $t1, 32($sp)
	move $v0, $t1
	lw $s2, 548($sp)	# level load register
	lw $s1, 544($sp)	# level load register
	lw $s0, 540($sp)	# level load register
	lw $t7, 536($sp)	# level load register
	lw $t6, 532($sp)	# level load register
	lw $t5, 528($sp)	# level load register
	lw $t4, 524($sp)	# level load register
	lw $t3, 520($sp)	# level load register
	lw $t2, 516($sp)	# level load register
	lw $t1, 512($sp)	# level load register
	lw $t0, 508($sp)	# level load register
	lw $ra, 572($sp)
	addiu $sp, $sp, 576
	jr $ra
	lw $s2, 548($sp)	# level load register
	lw $s1, 544($sp)	# level load register
	lw $s0, 540($sp)	# level load register
	lw $t7, 536($sp)	# level load register
	lw $t6, 532($sp)	# level load register
	lw $t5, 528($sp)	# level load register
	lw $t4, 524($sp)	# level load register
	lw $t3, 520($sp)	# level load register
	lw $t2, 516($sp)	# level load register
	lw $t1, 512($sp)	# level load register
	lw $t0, 508($sp)	# level load register
	lw $ra, 572($sp)
	addiu $sp, $sp, 576
	jr $ra
Tag26:
	addi $sp, $sp, -476	# enter
	sw $ra, 472($sp)
	sw $s4, 456($sp)	# level save register
	sw $s3, 452($sp)	# level save register
	sw $s2, 448($sp)	# level save register
	sw $s1, 444($sp)	# level save register
	sw $s0, 440($sp)	# level save register
	sw $t7, 436($sp)	# level save register
	sw $t6, 432($sp)	# level save register
	sw $t5, 428($sp)	# level save register
	sw $t4, 424($sp)	# level save register
	sw $t3, 420($sp)	# level save register
	sw $t2, 416($sp)	# level save register
	sw $t1, 412($sp)	# level save register
	sw $t0, 408($sp)	# level save register
	jal Tag2
	move $t2, $v0
	sub $t3, $t2, 48
	sw $t3, 0($gp)
	lw $t2, 0($gp)
	add $t3, $t2, 1
	li $t4, 4
	move $t2, $t4    #sizeof
	mul $t5, $t3, $t2
	move $a0, $t5	# save arguments
	jal Tag1
	move $t4, $v0
	sw $t4, 0($sp)
	lw $t2, 0($sp)
	li $t3, 1
	sw $t3, 0($t2)
	li $t4, 1
	sw $t4, 4($gp)
Tag27:
	lw $t2, 4($gp)
	lw $t3, 0($gp)
	sle $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag29
	li $t2, 4
	lw $t3, 4($gp)
	mul $t4, $t2, $t3
	lw $t5, 0($sp)
	add $t2, $t5, $t4
	lw $t3, 4($gp)
	sub $t4, $t3, 1
	li $t5, 4
	mul $t3, $t5, $t4
	lw $t6, 0($sp)
	add $t4, $t6, $t3
	lw $t5, 0($t4)
	mul $t3, $t5, 2
	sw $t3, 0($t2)
	lw $t4, 4($gp)
	lw $t2, 4($gp)
	add $t3, $t2, 1
	sw $t3, 4($gp)
	j Tag27
Tag29:
	li $t2, 4
	lw $t3, 0($gp)
	mul $t4, $t2, $t3
	lw $t5, 0($sp)
	add $t2, $t5, $t4
	li $t3, 4
	move $t4, $t3    #sizeof
	lw $t5, 0($t2)
	mul $t3, $t5, $t4
	move $a0, $t3	# save arguments
	jal Tag1
	move $t2, $v0
	sw $t2, 8($gp)
	li $t3, 0
	sw $t3, 4($gp)
Tag30:
	li $t2, 4
	lw $t3, 0($gp)
	mul $t4, $t2, $t3
	lw $t5, 0($sp)
	add $t2, $t5, $t4
	lw $t3, 4($gp)
	lw $t4, 0($t2)
	slt $t5, $t3, $t4
	move $t2, $t5
	beqz $t2, Tag32
	li $t3, 4
	lw $t2, 4($gp)
	mul $t4, $t3, $t2
	lw $t5, 8($gp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 0($gp)
	mul $t5, $t3, $t4
	lw $t6, 0($sp)
	add $t3, $t6, $t5
	li $t4, 4
	move $t5, $t4    #sizeof
	lw $t6, 0($t3)
	mul $t4, $t6, $t5
	move $a0, $t4	# save arguments
	jal Tag1
	move $t3, $v0
	sw $t3, 0($t2)
	lw $t4, 4($gp)
	lw $t2, 4($gp)
	add $t3, $t2, 1
	sw $t3, 4($gp)
	j Tag30
Tag32:
	lw $t2, 8($gp)
	lw $t3, 0($gp)
	li $t4, 0
	li $t5, 0
	li $t6, 4
	lw $t7, 0($gp)
	mul $s0, $t6, $t7
	lw $s1, 0($sp)
	add $t6, $s1, $s0
	lw $t7, 0($t6)
	sub $s0, $t7, 1
	li $t6, 4
	lw $t7, 0($gp)
	mul $s1, $t6, $t7
	lw $s2, 0($sp)
	add $t6, $s2, $s1
	lw $t7, 0($t6)
	sub $s1, $t7, 1
	li $t6, 0
	lw $t7, 0($sp)
	li $s2, 1
	move $a0, $t2	# save arguments
	move $a1, $t3	# save arguments
	move $a2, $t4	# save arguments
	move $a3, $t5	# save arguments
	sw $s0, 16($v1)	# save arguments
	sw $s1, 20($v1)	# save arguments
	sw $t6, 24($v1)	# save arguments
	sw $t7, 28($v1)	# save arguments
	sw $s2, 32($v1)	# save arguments
	jal Tag15
	move $s3, $v0
	li $s4, 4
	lw $t2, 0($gp)
	mul $t3, $s4, $t2
	lw $t4, 0($sp)
	add $t2, $t4, $t3
	lw $t1, 0($t2)
	lw $t3, 8($gp)
	move $a0, $t1	# save arguments
	move $a1, $t3	# save arguments
	jal Tag3
	move $t2, $v0
	li $t0, 0
	move $v0, $t0
	lw $s4, 456($sp)	# level load register
	lw $s3, 452($sp)	# level load register
	lw $s2, 448($sp)	# level load register
	lw $s1, 444($sp)	# level load register
	lw $s0, 440($sp)	# level load register
	lw $t7, 436($sp)	# level load register
	lw $t6, 432($sp)	# level load register
	lw $t5, 428($sp)	# level load register
	lw $t4, 424($sp)	# level load register
	lw $t3, 420($sp)	# level load register
	lw $t2, 416($sp)	# level load register
	lw $t1, 412($sp)	# level load register
	lw $t0, 408($sp)	# level load register
	lw $ra, 472($sp)
	addiu $sp, $sp, 476
	jr $ra
	lw $s4, 456($sp)	# level load register
	lw $s3, 452($sp)	# level load register
	lw $s2, 448($sp)	# level load register
	lw $s1, 444($sp)	# level load register
	lw $s0, 440($sp)	# level load register
	lw $t7, 436($sp)	# level load register
	lw $t6, 432($sp)	# level load register
	lw $t5, 428($sp)	# level load register
	lw $t4, 424($sp)	# level load register
	lw $t3, 420($sp)	# level load register
	lw $t2, 416($sp)	# level load register
	lw $t1, 412($sp)	# level load register
	lw $t0, 408($sp)	# level load register
	lw $ra, 472($sp)
	addiu $sp, $sp, 476
	jr $ra
########################################
############### RUN-TIME ###############
########################################

#malloc
Tag1:
	# a0 -- size in bytes (already x4)
	li $v0, 9
	syscall
	jr $ra

#getchar
Tag2:
	li $v0, 12
	syscall
	jr $ra
	
exit:
	li $v0, 10
	syscall
	jr $ra

## Daniel J. Ellard -- 03/13/94
## printf.asm--
## an implementation of a simple printf work-alike.

## printf--
## A simple printf-like function. Understands just the basic forms
## of the %s, %d, %c, and %% formats, and can only have 3 embedded
## formats (so that all of the parameters are passed in registers).
## If there are more than 3 embedded formats, all but the first 3 are
## completely ignored (not even printed).
## Register Usage:
## $a0,$s0 - pointer to format string
## $a1,$s1 - format argument 1 (optional)
## $a2,$s2 - format argument 2 (optional)
## $a3,$s3 - format argument 3 (optional)
## $s4 - count of formats processed.
## $s5 - char at $s4.
## $s6 - pointer to printf buffer
##
Tag0:
	subu $sp, $sp, 56 # set up the stack frame,
	sw $ra, 32($sp) # saving the local environment.
	sw $fp, 28($sp)
	sw $s0, 24($sp)
	sw $s1, 20($sp)
	sw $s2, 16($sp)
	sw $s3, 12($sp)
	sw $s4, 8($sp)
	sw $s5, 4($sp)
	sw $s6, 0($sp)
	sw $s7, 36($sp)
	sw $t0, 40($sp)
	sw $t1, 44($sp)
	sw $t2, 48($sp)
	sw $t3, 52($sp)
	addu $fp, $sp, 52

# grab the arguments:
	move $s0, $a0 # fmt string
	move $s1, $a1 # arg1 (optional)
	move $s2, $a2 # arg2 (optional)
	move $s3, $a3 # arg3 (optional)
	lw $s7, 16($v1)# arg4 (optional)
	lw $t0, 20($v1)# arg5 (optional)

	li $s4, 0 # set # of formats = 0
	la $s6, printf_buf # set s6 = base of printf buffer.

printf_loop: # process each character in the fmt:
	lb $s5, 0($s0) # get the next character, and then
	addu $s0, $s0, 1 # bump up $s0 to the next character.

	beq $s5, '%', printf_fmt # if the fmt character, then do fmt.
	beq $0, $s5, printf_end # if zero, then go to end.

printf_putc:
	sb $s5, 0($s6) # otherwise, just put this char
	sb $0, 1($s6) # into the printf buffer,
	move $a0, $s6 # and then print it with the
	li $v0, 4 # print_str syscall
	syscall

	b printf_loop # loop on.

printf_fmt:
	lb $s5, 0($s0) # see what the fmt character is,
	addu $s0, $s0, 1 # and bump up the pointer.

	beq $s4, 5, printf_loop # if we've already processed 3 args,
# then *ignore* this fmt.
	beq $s5, '0', printf_pre
	beq $s5, 'd', printf_int # if 'd', print as a decimal integer.
	beq $s5, 's', printf_str # if 's', print as a string.
	beq $s5, 'c', printf_char # if 'c', print as a ASCII char.
	beq $s5, '%', printf_perc # if '%', print a '%'
	b printf_loop # otherwise, just continue.

printf_shift_args: # shift over the fmt args,
	move $s1, $s2 # $s1 = $s2
	move $s2, $s3 # $s2 = $s3
	move $s3, $s7 # $s3 = $s7
	move $s7, $t0

	add $s4, $s4, 1 # increment # of args processed.

	b printf_loop # and continue the main loop.
	
printf_pre:
	lb $s5, ($s0)
	sub $s5, $s5, 48
	add $s0, $s0, 2
	
	li $t1, 0
	move $t2, $s1
printf_digits_begin:
	beqz $t2, printf_digits_end
	add $t1, $t1, 1
	div $t2, $t2, 10
	b printf_digits_begin
	
printf_digits_end:
	sub $t1, $s5, $t1
	
printf_pre_output_zero_begin:
	beqz $t1, printf_pre_output_zero_end
	sub $t1, $t1, 1
	li $a0, 0
	li $v0, 1
	syscall
	b printf_pre_output_zero_begin
	
printf_pre_output_zero_end:
	move $a0, $s1
	li $v0, 1
	syscall
	b printf_shift_args
	

printf_int: # deal with a %d:
	move $a0, $s1 # do a print_int syscall of $s1.
	li $v0, 1
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_str: # deal with a %s:
	move $a0, $s1 # do a print_string syscall of $s1.
	li $v0, 4
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_char: # deal with a %c:
	sb $s1, 0($s6) # fill the buffer in with byte $s1,
	sb $0, 1($s6) # and then a null.
	move $a0, $s6 # and then do a print_str syscall
	li $v0, 4 # on the buffer.
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_perc: # deal with a %%:
	li $s5, '%' # (this is redundant)
	sb $s5, 0($s6) # fill the buffer in with byte %,
	sb $0, 1($s6) # and then a null.
	move $a0, $s6 # and then do a print_str syscall
	li $v0, 4 # on the buffer.
	syscall
	b printf_loop # branch to printf_loop

printf_end:
	lw $t1, 44($sp)
	lw $t2, 48($sp)
	lw $t3, 52($sp)
	lw $s7, 36($sp)
	lw $t0, 40($sp)
	lw $ra, 32($sp) # restore the prior environment:
	lw $fp, 28($sp)
	lw $s0, 24($sp)
	lw $s1, 20($sp)
	lw $s2, 16($sp)
	lw $s3, 12($sp)
	lw $s4, 8($sp)
	lw $s5, 4($sp)
	lw $s6, 0($sp)
	addu $sp, $sp, 56 # release the stack frame.
	jr $ra # return.

.data
	printf_buf: .space 2

