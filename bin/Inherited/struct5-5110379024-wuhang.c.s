	.data
Tag31:
	.asciiz "%d\t"
	.align 2
Tag32:
	.asciiz "\n"
	.align 2
Tag33:
	.asciiz "%c %c %d, %d\n"
	.align 2
Tag60:
	.asciiz "\n"
	.align 2
Tag67:
	.asciiz "%d "
	.align 2
Tag68:
	.asciiz "\n"
	.align 2
	.align 2
	fs_whiteSpace: .asciiz " "
	.align 2
	fs_newLine: .asciiz "\n"
	.align 2
	.globl args
args:	.space 24
	.align 2
	.globl stat
stat:	.space 52
	.text
	.align 2
	.globl main
main:
	la $gp, stat
	la $v1, args
	li $t1, 0
	sw $t1, 0($gp)
	li $t2, 5
	sw $t2, 0($gp)
	li $t1, 0
	sw $t1, 4($gp)
	li $t2, 5
	sw $t2, 4($gp)
	li $t1, 580
	move $a0, $t1	# save arguments
	jal Tag1
	move $t2, $v0
	sw $t2, 8($gp)
	jal Tag34
	jal exit
Tag3:
	addi $sp, $sp, -116	# enter
	sw $ra, 112($sp)
	sw $t3, 60($sp)	# level save register
	sw $t2, 56($sp)	# level save register
	sw $t1, 52($sp)	# level save register
	sw $t0, 48($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	sw $a1, 4($sp)	# load argument
	add $t1, $sp, 0
	add $t2, $t1, 0
	add $t3, $sp, 4
	add $t1, $t3, 0
	lw $t0, 0($t2)
	lw $t3, 0($t1)
	sne $t2, $t0, $t3
	move $t1, $t2
	beqz $t1, Tag4
	li $t0, 0
	move $v0, $t0
	lw $t3, 60($sp)	# level load register
	lw $t2, 56($sp)	# level load register
	lw $t1, 52($sp)	# level load register
	lw $t0, 48($sp)	# level load register
	lw $ra, 112($sp)
	addiu $sp, $sp, 116
	jr $ra
	j Tag5
Tag4:
	li $t0, 1
	move $v0, $t0
	lw $t3, 60($sp)	# level load register
	lw $t2, 56($sp)	# level load register
	lw $t1, 52($sp)	# level load register
	lw $t0, 48($sp)	# level load register
	lw $ra, 112($sp)
	addiu $sp, $sp, 116
	jr $ra
Tag5:
	lw $t3, 60($sp)	# level load register
	lw $t2, 56($sp)	# level load register
	lw $t1, 52($sp)	# level load register
	lw $t0, 48($sp)	# level load register
	lw $ra, 112($sp)
	addiu $sp, $sp, 116
	jr $ra
Tag6:
	addi $sp, $sp, -300	# enter
	sw $ra, 296($sp)
	sw $a0, 0($sp)	# load argument
	sw $a1, 116($sp)	# load argument
	addi $t8, $t8, 1
	beq $t8, 1, Tag7
	beq $t8, 2, Tag7
	beq $t8, 4, Tag7
	beq $t8, 6, Tag7
	beq $t8, 7, Tag7
	beq $t8, 9, Tag7
	beq $t8, 13, Tag7
	beq $t8, 16, Tag7
	beq $t8, 17, Tag7
	beq $t8, 19, Tag7
	beq $t8, 25, Tag7
	li $v0, 0
	lw $ra, 296($sp)
	addiu $sp, $sp, 300
	jr $ra
Tag7:
	li $v0, 1
	lw $ra, 296($sp)
	addiu $sp, $sp, 300
	jr $ra
	lw $ra, 296($sp)
	addiu $sp, $sp, 300
	jr $ra
Tag9:
	addi $sp, $sp, -136	# enter
	sw $ra, 132($sp)
	sw $t5, 88($sp)	# level save register
	sw $t4, 84($sp)	# level save register
	sw $t3, 80($sp)	# level save register
	sw $t2, 76($sp)	# level save register
	sw $t1, 72($sp)	# level save register
	sw $t0, 68($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	li $t2, 116
	lw $t3, 0($sp)
	mul $t4, $t2, $t3
	lw $t5, 8($gp)
	add $t2, $t5, $t4
	add $t3, $t2, 0
	add $t4, $t3, 108
	lw $t2, 0($t4)
	lw $t3, 0($t4)
	add $t2, $t3, 1
	sw $t2, 0($t4)
	li $t3, 116
	lw $t2, 0($sp)
	mul $t4, $t3, $t2
	lw $t1, 8($gp)
	add $t2, $t1, $t4
	add $t0, $t2, 0
	move $v0, $t0
	lw $t5, 88($sp)	# level load register
	lw $t4, 84($sp)	# level load register
	lw $t3, 80($sp)	# level load register
	lw $t2, 76($sp)	# level load register
	lw $t1, 72($sp)	# level load register
	lw $t0, 68($sp)	# level load register
	lw $ra, 132($sp)
	addiu $sp, $sp, 136
	jr $ra
	lw $t5, 88($sp)	# level load register
	lw $t4, 84($sp)	# level load register
	lw $t3, 80($sp)	# level load register
	lw $t2, 76($sp)	# level load register
	lw $t1, 72($sp)	# level load register
	lw $t0, 68($sp)	# level load register
	lw $ra, 132($sp)
	addiu $sp, $sp, 136
	jr $ra
Tag10:
	addi $sp, $sp, -292	# enter
	sw $ra, 288($sp)
	sw $t6, 248($sp)	# level save register
	sw $t5, 244($sp)	# level save register
	sw $t4, 240($sp)	# level save register
	sw $t3, 236($sp)	# level save register
	sw $t2, 232($sp)	# level save register
	sw $t1, 228($sp)	# level save register
	sw $t0, 224($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	sw $a1, 4($sp)	# load argument
	lw $t1, 0($sp)
	add $t2, $sp, 8
	add $t3, $t1, 0
	li $t4, 0
Tag11:
	add $t1, $t2, $t4
	add $t5, $t3, $t4
	lw $t6, 0($t5)
	sw $t6, 0($t1)
	add $t4, $t4, 4
	slt $t1, $t4, 116
	bnez $t1, Tag11
	lw $t5, 0($sp)
	lw $t1, 4($sp)
	add $t2, $t5, 0
	add $t3, $t1, 0
	li $t4, 0
Tag12:
	add $t1, $t2, $t4
	add $t5, $t3, $t4
	lw $t6, 0($t5)
	sw $t6, 0($t1)
	add $t4, $t4, 4
	slt $t1, $t4, 116
	bnez $t1, Tag12
	lw $t5, 4($sp)
	add $t1, $t5, 0
	add $t2, $sp, 8
	li $t3, 0
Tag13:
	add $t0, $t1, $t3
	add $t4, $t2, $t3
	lw $t5, 0($t4)
	sw $t5, 0($t0)
	add $t3, $t3, 4
	slt $t0, $t3, 116
	bnez $t0, Tag13
	lw $t6, 248($sp)	# level load register
	lw $t5, 244($sp)	# level load register
	lw $t4, 240($sp)	# level load register
	lw $t3, 236($sp)	# level load register
	lw $t2, 232($sp)	# level load register
	lw $t1, 228($sp)	# level load register
	lw $t0, 224($sp)	# level load register
	lw $ra, 288($sp)
	addiu $sp, $sp, 292
	jr $ra
Tag14:
	addi $sp, $sp, -608	# enter
	sw $ra, 604($sp)
	sw $t7, 568($sp)	# level save register
	sw $t6, 564($sp)	# level save register
	sw $t5, 560($sp)	# level save register
	sw $t4, 556($sp)	# level save register
	sw $t3, 552($sp)	# level save register
	sw $t2, 548($sp)	# level save register
	sw $t1, 544($sp)	# level save register
	sw $t0, 540($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	sw $a1, 4($sp)	# load argument
	lw $t2, 0($sp)
	move $a0, $t2	# save arguments
	jal Tag9
	move $t3, $v0
	add $t4, $sp, 8
	move $t2, $t3
	li $t5, 0
Tag15:
	add $t3, $t4, $t5
	add $t6, $t2, $t5
	lw $t7, 0($t6)
	sw $t7, 0($t3)
	add $t5, $t5, 4
	slt $t3, $t5, 116
	bnez $t3, Tag15
	lw $t6, 4($sp)
	move $a0, $t6	# save arguments
	jal Tag9
	move $t2, $v0
	add $t3, $sp, 124
	move $t4, $t2
	li $t5, 0
Tag16:
	add $t2, $t3, $t5
	add $t6, $t4, $t5
	lw $t7, 0($t6)
	sw $t7, 0($t2)
	add $t5, $t5, 4
	slt $t2, $t5, 116
	bnez $t2, Tag16
	li $t6, 0
	sw $t6, 240($sp)
	li $t2, 0
	sw $t2, 244($sp)
Tag17:
	lw $t2, 240($sp)
	lw $t3, 0($gp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag19
Tag20:
	lw $t2, 244($sp)
	lw $t3, 0($gp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag22
	add $t2, $sp, 8
	add $t3, $t2, 0
	li $t4, 20
	lw $t2, 240($sp)
	mul $t5, $t4, $t2
	add $t6, $t3, $t5
	li $t2, 4
	lw $t3, 244($sp)
	mul $t4, $t2, $t3
	add $t5, $t6, $t4
	add $t2, $sp, 124
	add $t3, $t2, 0
	li $t4, 20
	lw $t2, 240($sp)
	mul $t6, $t4, $t2
	add $t7, $t3, $t6
	li $t2, 4
	lw $t3, 244($sp)
	mul $t4, $t2, $t3
	add $t6, $t7, $t4
	lw $t2, 0($t5)
	lw $t3, 0($t6)
	sgt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag23
	add $t2, $sp, 8
	add $t3, $sp, 124
	move $a0, $t2	# save arguments
	move $a1, $t3	# save arguments
	jal Tag10
	move $t4, $v0
	add $t5, $sp, 8
	add $t2, $t5, 112
	add $t3, $t2, 0
	add $t4, $t3, 0
	lw $t2, 0($t4)
	lw $t3, 0($t4)
	add $t2, $t3, 1
	sw $t2, 0($t4)
	add $t3, $sp, 124
	add $t2, $t3, 112
	add $t4, $t2, 0
	add $t3, $t4, 0
	lw $t2, 0($t3)
	lw $t4, 0($t3)
	sub $t2, $t4, 1
	sw $t2, 0($t3)
Tag23:
	lw $t2, 244($sp)
	add $t3, $t2, 1
	sw $t3, 244($sp)
	j Tag20
Tag22:
	lw $t2, 240($sp)
	add $t3, $t2, 1
	sw $t3, 240($sp)
	j Tag17
Tag19:
	lw $t7, 568($sp)	# level load register
	lw $t6, 564($sp)	# level load register
	lw $t5, 560($sp)	# level load register
	lw $t4, 556($sp)	# level load register
	lw $t3, 552($sp)	# level load register
	lw $t2, 548($sp)	# level load register
	lw $t1, 544($sp)	# level load register
	lw $t0, 540($sp)	# level load register
	lw $ra, 604($sp)
	addiu $sp, $sp, 608
	jr $ra
Tag24:
	addi $sp, $sp, -368	# enter
	sw $ra, 364($sp)
	sw $s0, 332($sp)	# level save register
	sw $t7, 328($sp)	# level save register
	sw $t6, 324($sp)	# level save register
	sw $t5, 320($sp)	# level save register
	sw $t4, 316($sp)	# level save register
	sw $t3, 312($sp)	# level save register
	sw $t2, 308($sp)	# level save register
	sw $t1, 304($sp)	# level save register
	sw $t0, 300($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	li $t2, 0
	sw $t2, 4($sp)
Tag25:
	lw $t2, 4($sp)
	lw $t3, 0($gp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag27
	li $t2, 0
	sw $t2, 8($sp)
Tag28:
	lw $t2, 8($sp)
	lw $t3, 0($gp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag30
	la $t2, Tag31
	li $t3, 116
	lw $t4, 0($sp)
	mul $t5, $t3, $t4
	lw $t6, 8($gp)
	add $t3, $t6, $t5
	add $t4, $t3, 0
	add $t5, $t4, 0
	li $t3, 20
	lw $t4, 4($sp)
	mul $t6, $t3, $t4
	add $t7, $t5, $t6
	li $t3, 4
	lw $t4, 8($sp)
	mul $t5, $t3, $t4
	add $t6, $t7, $t5
	lw $t3, 0($t6)
	move $a0, $t2	# save arguments
	move $a1, $t3	# save arguments
	jal Tag0
	move $t4, $v0
	lw $t5, 8($sp)
	add $t2, $t5, 1
	sw $t2, 8($sp)
	j Tag28
Tag30:
	la $t2, Tag32
	move $a0, $t2
	li $v0, 4
	syscall
	lw $t4, 4($sp)
	add $t2, $t4, 1
	sw $t2, 4($sp)
	j Tag25
Tag27:
	la $t2, Tag33
	li $t3, 116
	lw $t4, 0($sp)
	mul $t5, $t3, $t4
	lw $t6, 8($gp)
	add $t3, $t6, $t5
	add $t4, $t3, 0
	add $t5, $t4, 100
	lw $t3, 0($t5)
	li $t4, 116
	lw $t5, 0($sp)
	mul $t6, $t4, $t5
	lw $t7, 8($gp)
	add $t4, $t7, $t6
	add $t5, $t4, 0
	add $t6, $t5, 100
	lw $t4, 4($t6)
	li $t5, 116
	lw $t6, 0($sp)
	mul $t7, $t5, $t6
	lw $s0, 8($gp)
	add $t5, $s0, $t7
	add $t6, $t5, 0
	add $t7, $t6, 108
	lw $t5, 0($t7)
	li $t6, 116
	lw $t7, 0($sp)
	mul $s0, $t6, $t7
	lw $t0, 8($gp)
	add $t6, $t0, $s0
	add $t1, $t6, 0
	add $t0, $t1, 112
	add $t6, $t0, 0
	add $t1, $t6, 0
	lw $t0, 0($t1)
	move $a0, $t2	# save arguments
	move $a1, $t3	# save arguments
	move $a2, $t4	# save arguments
	move $a3, $t5	# save arguments
	sw $t0, 16($v1)	# save arguments
	jal Tag0
	move $t6, $v0
	lw $s0, 332($sp)	# level load register
	lw $t7, 328($sp)	# level load register
	lw $t6, 324($sp)	# level load register
	lw $t5, 320($sp)	# level load register
	lw $t4, 316($sp)	# level load register
	lw $t3, 312($sp)	# level load register
	lw $t2, 308($sp)	# level load register
	lw $t1, 304($sp)	# level load register
	lw $t0, 300($sp)	# level load register
	lw $ra, 364($sp)
	addiu $sp, $sp, 368
	jr $ra
Tag34:
	addi $sp, $sp, -1000	# enter
	sw $ra, 996($sp)
	sw $t7, 960($sp)	# level save register
	sw $t6, 956($sp)	# level save register
	sw $t5, 952($sp)	# level save register
	sw $t4, 948($sp)	# level save register
	sw $t3, 944($sp)	# level save register
	sw $t2, 940($sp)	# level save register
	sw $t1, 936($sp)	# level save register
	sw $t0, 932($sp)	# level save register
	li $t2, 7
	sw $t2, 0($sp)
	li $t3, 0
	sw $t3, 4($sp)
Tag35:
	lw $t2, 4($sp)
	lw $t3, 4($gp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag37
	li $t2, 0
	sw $t2, 8($sp)
Tag38:
	lw $t2, 8($sp)
	lw $t3, 0($gp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag40
	li $t2, 0
	sw $t2, 12($sp)
Tag41:
	lw $t2, 12($sp)
	lw $t3, 0($gp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag43
	li $t2, 116
	lw $t3, 4($sp)
	mul $t4, $t2, $t3
	lw $t5, 8($gp)
	add $t2, $t5, $t4
	add $t3, $t2, 0
	add $t4, $t3, 0
	li $t2, 20
	lw $t3, 8($sp)
	mul $t5, $t2, $t3
	add $t6, $t4, $t5
	li $t2, 4
	lw $t3, 12($sp)
	mul $t4, $t2, $t3
	add $t5, $t6, $t4
	lw $t2, 8($sp)
	mul $t3, $t2, 5110
	lw $t4, 12($sp)
	add $t2, $t3, $t4
	li $t6, 34
	lw $t3, 4($sp)
	sub $t4, $t6, $t3
	rem $t7, $t2, $t4
	add $t3, $t7, 1
	sw $t3, 0($t5)
	li $t2, 116
	lw $t3, 4($sp)
	mul $t4, $t2, $t3
	lw $t5, 8($gp)
	add $t2, $t5, $t4
	add $t3, $t2, 0
	add $t4, $t3, 100
	lw $t2, 8($sp)
	lw $t3, 8($sp)
	mul $t5, $t2, $t3
	lw $t6, 8($sp)
	mul $t2, $t5, $t6
	lw $t3, 4($sp)
	add $t5, $t3, $t2
	sw $t5, 0($t4)
	li $t2, 116
	lw $t3, 4($sp)
	mul $t4, $t2, $t3
	lw $t5, 8($gp)
	add $t2, $t5, $t4
	add $t3, $t2, 0
	add $t4, $t3, 100
	lw $t2, 12($sp)
	lw $t3, 8($sp)
	add $t5, $t2, $t3
	lw $t6, 4($sp)
	add $t2, $t5, $t6
	sll $t3, $t2, 1
	sw $t3, 4($t4)
	li $t2, 116
	lw $t3, 4($sp)
	mul $t4, $t2, $t3
	lw $t5, 8($gp)
	add $t2, $t5, $t4
	add $t3, $t2, 0
	add $t4, $t3, 112
	add $t2, $t4, 0
	add $t3, $t2, 0
	lw $t4, 8($sp)
	not $t2, $t4
	lw $t5, 4($sp)
	add $t4, $t5, $t2
	lw $t6, 12($sp)
	or $t2, $t4, $t6
	sw $t2, 0($t3)
	li $t4, 116
	lw $t2, 4($sp)
	mul $t3, $t4, $t2
	lw $t5, 8($gp)
	add $t2, $t5, $t3
	add $t4, $t2, 0
	add $t3, $t4, 100
	li $t2, 116
	lw $t4, 4($sp)
	mul $t5, $t2, $t4
	lw $t6, 8($gp)
	add $t2, $t6, $t5
	add $t4, $t2, 0
	add $t5, $t4, 100
	lw $t2, 0($t5)
	rem $t4, $t2, 26
	add $t5, $t4, 97
	sw $t5, 0($t3)
	li $t2, 116
	lw $t3, 4($sp)
	mul $t4, $t2, $t3
	lw $t5, 8($gp)
	add $t2, $t5, $t4
	add $t3, $t2, 0
	add $t4, $t3, 100
	li $t2, 116
	lw $t3, 4($sp)
	mul $t5, $t2, $t3
	lw $t6, 8($gp)
	add $t2, $t6, $t5
	add $t3, $t2, 0
	add $t5, $t3, 100
	lw $t2, 4($t5)
	rem $t3, $t2, 26
	add $t5, $t3, 65
	sw $t5, 4($t4)
	lw $t2, 12($sp)
	add $t3, $t2, 1
	sw $t3, 12($sp)
	j Tag41
Tag43:
	lw $t2, 8($sp)
	add $t3, $t2, 1
	sw $t3, 8($sp)
	j Tag38
Tag40:
	lw $t2, 4($sp)
	add $t3, $t2, 1
	sw $t3, 4($sp)
	j Tag35
Tag37:
	li $t2, 0
	sw $t2, 8($sp)
Tag44:
	lw $t2, 8($sp)
	lw $t3, 4($gp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag46
	li $t2, 0
	sw $t2, 12($sp)
Tag47:
	lw $t2, 12($sp)
	lw $t3, 4($gp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag49
	lw $t2, 8($sp)
	lw $t3, 12($sp)
	move $a0, $t2	# save arguments
	move $a1, $t3	# save arguments
	jal Tag14
	move $t4, $v0
	lw $t5, 12($sp)
	add $t2, $t5, 1
	sw $t2, 12($sp)
	j Tag47
Tag49:
	lw $t2, 8($sp)
	add $t3, $t2, 1
	sw $t3, 8($sp)
	j Tag44
Tag46:
	li $t2, 0
	sw $t2, 4($sp)
Tag50:
	lw $t2, 4($sp)
	lw $t3, 4($gp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag52
	lw $t2, 4($sp)
	move $a0, $t2	# save arguments
	jal Tag24
	move $t3, $v0
	lw $t4, 4($sp)
	add $t2, $t4, 1
	sw $t2, 4($sp)
	j Tag50
Tag52:
	lw $t2, 4($gp)
	sub $t3, $t2, 1
	sw $t3, 8($sp)
Tag53:
	li $t2, 1
	neg $t3, $t2
	lw $t4, 8($sp)
	sgt $t2, $t4, $t3
	move $t5, $t2
	beqz $t5, Tag55
	lw $t2, 8($sp)
	rem $t3, $t2, 3
	seq $t4, $t3, 0
	move $t2, $t4
	beqz $t2, Tag56
	li $t3, 116
	lw $t2, 8($sp)
	mul $t4, $t3, $t2
	lw $t5, 8($gp)
	add $t2, $t5, $t4
	lw $t3, 8($sp)
	add $t4, $t3, 3
	lw $t5, 4($gp)
	rem $t3, $t4, $t5
	move $a0, $t3	# save arguments
	jal Tag9
	move $t6, $v0
	add $t4, $t2, 0
	move $t3, $t6
	li $t2, 0
Tag58:
	add $t5, $t4, $t2
	add $t6, $t3, $t2
	lw $t7, 0($t6)
	sw $t7, 0($t5)
	add $t2, $t2, 4
	slt $t5, $t2, 116
	bnez $t5, Tag58
	j Tag57
Tag56:
	li $t2, 116
	lw $t3, 8($sp)
	mul $t4, $t2, $t3
	lw $t5, 8($gp)
	add $t2, $t5, $t4
	lw $t3, 8($sp)
	move $a0, $t3	# save arguments
	jal Tag9
	move $t4, $v0
	add $t5, $t2, 0
	move $t3, $t4
	li $t2, 0
Tag59:
	add $t4, $t5, $t2
	add $t6, $t3, $t2
	lw $t7, 0($t6)
	sw $t7, 0($t4)
	add $t2, $t2, 4
	slt $t4, $t2, 116
	bnez $t4, Tag59
Tag57:
	lw $t2, 8($sp)
	lw $t3, 8($sp)
	sub $t2, $t3, 1
	sw $t2, 8($sp)
	j Tag53
Tag55:
	la $t2, Tag60
	move $a0, $t2
	li $v0, 4
	syscall
	li $t4, 0
	sw $t4, 8($sp)
Tag61:
	lw $t2, 8($sp)
	lw $t3, 4($gp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag63
	li $t2, 0
	sw $t2, 12($sp)
Tag64:
	lw $t2, 12($sp)
	lw $t3, 4($gp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag66
	la $t2, Tag67
	lw $t3, 8($sp)
	move $a0, $t3	# save arguments
	jal Tag9
	move $t4, $v0
	lw $t5, 12($sp)
	move $a0, $t5	# save arguments
	jal Tag9
	move $t3, $v0
	move $a0, $t4	# save arguments
	move $a1, $t3	# save arguments
	jal Tag6
	move $t6, $v0
	move $a0, $t6
	li $v0, 1
	syscall
	la $a0, fs_whiteSpace
	li $v0, 4
	syscall
	lw $t3, 12($sp)
	add $t2, $t3, 1
	sw $t2, 12($sp)
	j Tag64
Tag66:
	la $t2, Tag68
	move $a0, $t2
	li $v0, 4
	syscall
	lw $t4, 8($sp)
	add $t2, $t4, 1
	sw $t2, 8($sp)
	j Tag61
Tag63:
	li $t0, 0
	move $v0, $t0
	lw $t7, 960($sp)	# level load register
	lw $t6, 956($sp)	# level load register
	lw $t5, 952($sp)	# level load register
	lw $t4, 948($sp)	# level load register
	lw $t3, 944($sp)	# level load register
	lw $t2, 940($sp)	# level load register
	lw $t1, 936($sp)	# level load register
	lw $t0, 932($sp)	# level load register
	lw $ra, 996($sp)
	addiu $sp, $sp, 1000
	jr $ra
	lw $t7, 960($sp)	# level load register
	lw $t6, 956($sp)	# level load register
	lw $t5, 952($sp)	# level load register
	lw $t4, 948($sp)	# level load register
	lw $t3, 944($sp)	# level load register
	lw $t2, 940($sp)	# level load register
	lw $t1, 936($sp)	# level load register
	lw $t0, 932($sp)	# level load register
	lw $ra, 996($sp)
	addiu $sp, $sp, 1000
	jr $ra
########################################
############### RUN-TIME ###############
########################################

#malloc
Tag1:
	# a0 -- size in bytes (already x4)
	li $v0, 9
	syscall
	jr $ra

#getchar
Tag2:
	li $v0, 12
	syscall
	jr $ra
	
exit:
	li $v0, 10
	syscall
	jr $ra

## Daniel J. Ellard -- 03/13/94
## printf.asm--
## an implementation of a simple printf work-alike.

## printf--
## A simple printf-like function. Understands just the basic forms
## of the %s, %d, %c, and %% formats, and can only have 3 embedded
## formats (so that all of the parameters are passed in registers).
## If there are more than 3 embedded formats, all but the first 3 are
## completely ignored (not even printed).
## Register Usage:
## $a0,$s0 - pointer to format string
## $a1,$s1 - format argument 1 (optional)
## $a2,$s2 - format argument 2 (optional)
## $a3,$s3 - format argument 3 (optional)
## $s4 - count of formats processed.
## $s5 - char at $s4.
## $s6 - pointer to printf buffer
##
Tag0:
	subu $sp, $sp, 56 # set up the stack frame,
	sw $ra, 32($sp) # saving the local environment.
	sw $fp, 28($sp)
	sw $s0, 24($sp)
	sw $s1, 20($sp)
	sw $s2, 16($sp)
	sw $s3, 12($sp)
	sw $s4, 8($sp)
	sw $s5, 4($sp)
	sw $s6, 0($sp)
	sw $s7, 36($sp)
	sw $t0, 40($sp)
	sw $t1, 44($sp)
	sw $t2, 48($sp)
	sw $t3, 52($sp)
	addu $fp, $sp, 52

# grab the arguments:
	move $s0, $a0 # fmt string
	move $s1, $a1 # arg1 (optional)
	move $s2, $a2 # arg2 (optional)
	move $s3, $a3 # arg3 (optional)
	lw $s7, 16($v1)# arg4 (optional)
	lw $t0, 20($v1)# arg5 (optional)

	li $s4, 0 # set # of formats = 0
	la $s6, printf_buf # set s6 = base of printf buffer.

printf_loop: # process each character in the fmt:
	lb $s5, 0($s0) # get the next character, and then
	addu $s0, $s0, 1 # bump up $s0 to the next character.

	beq $s5, '%', printf_fmt # if the fmt character, then do fmt.
	beq $0, $s5, printf_end # if zero, then go to end.

printf_putc:
	sb $s5, 0($s6) # otherwise, just put this char
	sb $0, 1($s6) # into the printf buffer,
	move $a0, $s6 # and then print it with the
	li $v0, 4 # print_str syscall
	syscall

	b printf_loop # loop on.

printf_fmt:
	lb $s5, 0($s0) # see what the fmt character is,
	addu $s0, $s0, 1 # and bump up the pointer.

	beq $s4, 5, printf_loop # if we've already processed 3 args,
# then *ignore* this fmt.
	beq $s5, '0', printf_pre
	beq $s5, 'd', printf_int # if 'd', print as a decimal integer.
	beq $s5, 's', printf_str # if 's', print as a string.
	beq $s5, 'c', printf_char # if 'c', print as a ASCII char.
	beq $s5, '%', printf_perc # if '%', print a '%'
	b printf_loop # otherwise, just continue.

printf_shift_args: # shift over the fmt args,
	move $s1, $s2 # $s1 = $s2
	move $s2, $s3 # $s2 = $s3
	move $s3, $s7 # $s3 = $s7
	move $s7, $t0

	add $s4, $s4, 1 # increment # of args processed.

	b printf_loop # and continue the main loop.
	
printf_pre:
	lb $s5, ($s0)
	sub $s5, $s5, 48
	add $s0, $s0, 2
	
	li $t1, 0
	move $t2, $s1
printf_digits_begin:
	beqz $t2, printf_digits_end
	add $t1, $t1, 1
	div $t2, $t2, 10
	b printf_digits_begin
	
printf_digits_end:
	sub $t1, $s5, $t1
	
printf_pre_output_zero_begin:
	beqz $t1, printf_pre_output_zero_end
	sub $t1, $t1, 1
	li $a0, 0
	li $v0, 1
	syscall
	b printf_pre_output_zero_begin
	
printf_pre_output_zero_end:
	move $a0, $s1
	li $v0, 1
	syscall
	b printf_shift_args
	

printf_int: # deal with a %d:
	move $a0, $s1 # do a print_int syscall of $s1.
	li $v0, 1
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_str: # deal with a %s:
	move $a0, $s1 # do a print_string syscall of $s1.
	li $v0, 4
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_char: # deal with a %c:
	sb $s1, 0($s6) # fill the buffer in with byte $s1,
	sb $0, 1($s6) # and then a null.
	move $a0, $s6 # and then do a print_str syscall
	li $v0, 4 # on the buffer.
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_perc: # deal with a %%:
	li $s5, '%' # (this is redundant)
	sb $s5, 0($s6) # fill the buffer in with byte %,
	sb $0, 1($s6) # and then a null.
	move $a0, $s6 # and then do a print_str syscall
	li $v0, 4 # on the buffer.
	syscall
	b printf_loop # branch to printf_loop

printf_end:
	lw $t1, 44($sp)
	lw $t2, 48($sp)
	lw $t3, 52($sp)
	lw $s7, 36($sp)
	lw $t0, 40($sp)
	lw $ra, 32($sp) # restore the prior environment:
	lw $fp, 28($sp)
	lw $s0, 24($sp)
	lw $s1, 20($sp)
	lw $s2, 16($sp)
	lw $s3, 12($sp)
	lw $s4, 8($sp)
	lw $s5, 4($sp)
	lw $s6, 0($sp)
	addu $sp, $sp, 56 # release the stack frame.
	jr $ra # return.

.data
	printf_buf: .space 2

