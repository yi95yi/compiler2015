	.data
Tag73:
	.asciiz "%d\n"
	.align 2
Tag74:
	.asciiz "no solution!\n"
	.align 2
	.align 2
	fs_whiteSpace: .asciiz " "
	.align 2
	fs_newLine: .asciiz "\n"
	.align 2
	.globl args
args:	.space 12
	.align 2
	.globl stat
stat:	.space 4
	.text
	.align 2
	.globl main
main:
	la $gp, stat
	la $v1, args
	jal Tag6
	jal exit
Tag3:
	addi $sp, $sp, -104	# enter
	sw $ra, 100($sp)
	sw $t3, 48($sp)	# level save register
	sw $t2, 44($sp)	# level save register
	sw $t1, 40($sp)	# level save register
	sw $t0, 36($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	sw $a1, 4($sp)	# load argument
	lw $t1, 0($sp)
	lw $t2, 4($sp)
	bge $t1, $t2, Tag4
	lw $t3, 0($sp)
	bltz $t3, Tag4
	li $t0, 1
	j Tag5
Tag4:
	li $t0, 0
Tag5:
	move $t1, $t0
	move $v0, $t1
	lw $t3, 48($sp)	# level load register
	lw $t2, 44($sp)	# level load register
	lw $t1, 40($sp)	# level load register
	lw $t0, 36($sp)	# level load register
	lw $ra, 100($sp)
	addiu $sp, $sp, 104
	jr $ra
	lw $t3, 48($sp)	# level load register
	lw $t2, 44($sp)	# level load register
	lw $t1, 40($sp)	# level load register
	lw $t0, 36($sp)	# level load register
	lw $ra, 100($sp)
	addiu $sp, $sp, 104
	jr $ra
Tag6:
	addi $sp, $sp, -3168	# enter
	sw $ra, 3164($sp)
	sw $t5, 3120($sp)	# level save register
	sw $t4, 3116($sp)	# level save register
	sw $t3, 3112($sp)	# level save register
	sw $t2, 3108($sp)	# level save register
	sw $t1, 3104($sp)	# level save register
	sw $t0, 3100($sp)	# level save register
	jal Tag2
	move $t1, $v0
	sub $t2, $t1, 48
	li $t3, 100
	mul $t1, $t3, $t2
	jal Tag2
	move $t4, $v0
	sub $t2, $t4, 48
	li $t3, 10
	mul $t4, $t3, $t2
	add $t5, $t1, $t4
	jal Tag2
	move $t2, $v0
	add $t1, $t5, $t2
	sub $t3, $t1, 48
	sw $t3, 0($sp)
	li $t1, 0
	sw $t1, 12($sp)
	lw $t2, 12($sp)
	sw $t2, 8($sp)
	lw $t1, 8($sp)
	sw $t1, 24($sp)
	lw $t2, 24($sp)
	sw $t2, 4($sp)
	lw $t1, 0($sp)
	sub $t2, $t1, 1
	sw $t2, 20($sp)
	lw $t1, 20($sp)
	sw $t1, 16($sp)
	li $t2, 0
	sw $t2, 40($sp)
	lw $t1, 40($sp)
	sw $t1, 36($sp)
	li $t2, 0
	sw $t2, 28($sp)
	lw $t1, 28($sp)
	sw $t1, 32($sp)
	lw $t2, 0($sp)
	lw $t1, 0($sp)
	mul $t3, $t2, $t1
	li $t4, 4
	move $t1, $t4    #sizeof
	mul $t2, $t3, $t1
	move $a0, $t2	# save arguments
	jal Tag1
	move $t4, $v0
	sw $t4, 44($sp)
	li $t1, 0
	sw $t1, 56($sp)
Tag7:
	lw $t1, 0($sp)
	lw $t2, 0($sp)
	mul $t3, $t1, $t2
	lw $t4, 56($sp)
	slt $t1, $t4, $t3
	move $t2, $t1
	beqz $t2, Tag9
	li $t1, 4
	lw $t2, 56($sp)
	mul $t3, $t1, $t2
	lw $t4, 44($sp)
	add $t1, $t4, $t3
	li $t2, 0
	sw $t2, 0($t1)
	lw $t3, 56($sp)
	add $t1, $t3, 1
	sw $t1, 56($sp)
	j Tag7
Tag9:
	lw $t1, 0($sp)
	lw $t2, 0($sp)
	mul $t3, $t1, $t2
	li $t4, 4
	move $t1, $t4    #sizeof
	mul $t2, $t3, $t1
	move $a0, $t2	# save arguments
	jal Tag1
	move $t4, $v0
	sw $t4, 48($sp)
	li $t1, 0
	sw $t1, 56($sp)
Tag10:
	lw $t1, 0($sp)
	lw $t2, 0($sp)
	mul $t3, $t1, $t2
	lw $t4, 56($sp)
	slt $t1, $t4, $t3
	move $t2, $t1
	beqz $t2, Tag12
	li $t1, 4
	lw $t2, 56($sp)
	mul $t3, $t1, $t2
	lw $t4, 48($sp)
	add $t1, $t4, $t3
	li $t2, 0
	sw $t2, 0($t1)
	lw $t3, 56($sp)
	add $t1, $t3, 1
	sw $t1, 56($sp)
	j Tag10
Tag12:
	li $t1, 4
	move $t2, $t1    #sizeof
	lw $t3, 0($sp)
	mul $t1, $t3, $t2
	move $a0, $t1	# save arguments
	jal Tag1
	move $t4, $v0
	sw $t4, 52($sp)
	li $t1, 0
	sw $t1, 56($sp)
Tag13:
	lw $t1, 56($sp)
	lw $t2, 0($sp)
	slt $t3, $t1, $t2
	move $t4, $t3
	beqz $t4, Tag15
	li $t1, 4
	lw $t2, 56($sp)
	mul $t3, $t1, $t2
	lw $t4, 52($sp)
	add $t1, $t4, $t3
	li $t2, 4
	move $t3, $t2    #sizeof
	lw $t4, 0($sp)
	mul $t2, $t4, $t3
	move $a0, $t2	# save arguments
	jal Tag1
	move $t5, $v0
	sw $t5, 0($t1)
	li $t2, 0
	sw $t2, 60($sp)
Tag16:
	lw $t1, 60($sp)
	lw $t2, 0($sp)
	slt $t3, $t1, $t2
	move $t4, $t3
	beqz $t4, Tag18
	li $t1, 4
	lw $t2, 56($sp)
	mul $t3, $t1, $t2
	lw $t4, 52($sp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 60($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($t1)
	add $t2, $t5, $t4
	li $t1, 1
	neg $t3, $t1
	sw $t3, 0($t2)
	lw $t1, 60($sp)
	add $t2, $t1, 1
	sw $t2, 60($sp)
	j Tag16
Tag18:
	lw $t1, 56($sp)
	add $t2, $t1, 1
	sw $t2, 56($sp)
	j Tag13
Tag15:
	lw $t1, 44($sp)
	lw $t2, 8($sp)
	sw $t2, 0($t1)
	lw $t3, 48($sp)
	lw $t1, 12($sp)
	sw $t1, 0($t3)
	li $t2, 4
	lw $t1, 8($sp)
	mul $t3, $t2, $t1
	lw $t4, 52($sp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 12($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($t1)
	add $t2, $t5, $t4
	lw $t1, 0($t2)
	seq $t3, $t1, 0
Tag19:
	lw $t1, 4($sp)
	lw $t2, 24($sp)
	sle $t3, $t1, $t2
	move $t4, $t3
	beqz $t4, Tag21
	li $t1, 4
	lw $t2, 4($sp)
	mul $t3, $t1, $t2
	lw $t4, 44($sp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 0($t1)
	mul $t4, $t2, $t3
	lw $t1, 52($sp)
	add $t2, $t1, $t4
	li $t3, 4
	lw $t1, 4($sp)
	mul $t4, $t3, $t1
	lw $t5, 48($sp)
	add $t1, $t5, $t4
	li $t3, 4
	lw $t4, 0($t1)
	mul $t5, $t3, $t4
	lw $t1, 0($t2)
	add $t3, $t1, $t5
	lw $t2, 0($t3)
	sw $t2, 32($sp)
	li $t1, 4
	lw $t2, 4($sp)
	mul $t3, $t1, $t2
	lw $t4, 44($sp)
	add $t1, $t4, $t3
	lw $t2, 0($t1)
	sub $t3, $t2, 1
	sw $t3, 36($sp)
	li $t1, 4
	lw $t2, 4($sp)
	mul $t3, $t1, $t2
	lw $t4, 48($sp)
	add $t1, $t4, $t3
	lw $t2, 0($t1)
	sub $t3, $t2, 2
	sw $t3, 40($sp)
	lw $t1, 36($sp)
	lw $t2, 0($sp)
	move $a0, $t1	# save arguments
	move $a1, $t2	# save arguments
	jal Tag3
	move $t3, $v0
	bne $t3, 1, Tag23
	lw $t1, 40($sp)
	lw $t2, 0($sp)
	move $a0, $t1	# save arguments
	move $a1, $t2	# save arguments
	jal Tag3
	move $t3, $v0
	bne $t3, 1, Tag23
	li $t1, 4
	lw $t2, 36($sp)
	mul $t3, $t1, $t2
	lw $t4, 52($sp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 40($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($t1)
	add $t2, $t5, $t4
	li $t1, 1
	neg $t3, $t1
	lw $t4, 0($t2)
	bne $t4, $t3, Tag23
	li $t1, 1
	j Tag24
Tag23:
	li $t1, 0
Tag24:
	move $t2, $t1
	beqz $t2, Tag25
	lw $t1, 24($sp)
	add $t2, $t1, 1
	sw $t2, 24($sp)
	li $t1, 4
	lw $t2, 24($sp)
	mul $t3, $t1, $t2
	lw $t4, 44($sp)
	add $t1, $t4, $t3
	lw $t2, 36($sp)
	sw $t2, 0($t1)
	li $t3, 4
	lw $t1, 24($sp)
	mul $t2, $t3, $t1
	lw $t4, 48($sp)
	add $t1, $t4, $t2
	lw $t3, 40($sp)
	sw $t3, 0($t1)
	li $t2, 4
	lw $t1, 36($sp)
	mul $t3, $t2, $t1
	lw $t4, 52($sp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 40($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($t1)
	add $t2, $t5, $t4
	lw $t1, 32($sp)
	add $t3, $t1, 1
	sw $t3, 0($t2)
	lw $t1, 36($sp)
	lw $t2, 16($sp)
	bne $t1, $t2, Tag26
	lw $t3, 40($sp)
	lw $t1, 20($sp)
	bne $t3, $t1, Tag26
	li $t2, 1
	j Tag27
Tag26:
	li $t2, 0
Tag27:
	move $t1, $t2
	beqz $t1, Tag25
	li $t2, 1
	sw $t2, 28($sp)
Tag25:
	li $t1, 4
	lw $t2, 4($sp)
	mul $t3, $t1, $t2
	lw $t4, 44($sp)
	add $t1, $t4, $t3
	lw $t2, 0($t1)
	sub $t3, $t2, 1
	sw $t3, 36($sp)
	li $t1, 4
	lw $t2, 4($sp)
	mul $t3, $t1, $t2
	lw $t4, 48($sp)
	add $t1, $t4, $t3
	lw $t2, 0($t1)
	add $t3, $t2, 2
	sw $t3, 40($sp)
	lw $t1, 36($sp)
	lw $t2, 0($sp)
	move $a0, $t1	# save arguments
	move $a1, $t2	# save arguments
	jal Tag3
	move $t3, $v0
	bne $t3, 1, Tag29
	lw $t1, 40($sp)
	lw $t2, 0($sp)
	move $a0, $t1	# save arguments
	move $a1, $t2	# save arguments
	jal Tag3
	move $t3, $v0
	bne $t3, 1, Tag29
	li $t1, 4
	lw $t2, 36($sp)
	mul $t3, $t1, $t2
	lw $t4, 52($sp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 40($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($t1)
	add $t2, $t5, $t4
	li $t1, 1
	neg $t3, $t1
	lw $t4, 0($t2)
	bne $t4, $t3, Tag29
	li $t1, 1
	j Tag30
Tag29:
	li $t1, 0
Tag30:
	move $t2, $t1
	beqz $t2, Tag31
	lw $t1, 24($sp)
	add $t2, $t1, 1
	sw $t2, 24($sp)
	li $t1, 4
	lw $t2, 24($sp)
	mul $t3, $t1, $t2
	lw $t4, 44($sp)
	add $t1, $t4, $t3
	lw $t2, 36($sp)
	sw $t2, 0($t1)
	li $t3, 4
	lw $t1, 24($sp)
	mul $t2, $t3, $t1
	lw $t4, 48($sp)
	add $t1, $t4, $t2
	lw $t3, 40($sp)
	sw $t3, 0($t1)
	li $t2, 4
	lw $t1, 36($sp)
	mul $t3, $t2, $t1
	lw $t4, 52($sp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 40($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($t1)
	add $t2, $t5, $t4
	lw $t1, 32($sp)
	add $t3, $t1, 1
	sw $t3, 0($t2)
	lw $t1, 36($sp)
	lw $t2, 16($sp)
	bne $t1, $t2, Tag32
	lw $t3, 40($sp)
	lw $t1, 20($sp)
	bne $t3, $t1, Tag32
	li $t2, 1
	j Tag33
Tag32:
	li $t2, 0
Tag33:
	move $t1, $t2
	beqz $t1, Tag31
	li $t2, 1
	sw $t2, 28($sp)
Tag31:
	li $t1, 4
	lw $t2, 4($sp)
	mul $t3, $t1, $t2
	lw $t4, 44($sp)
	add $t1, $t4, $t3
	lw $t2, 0($t1)
	add $t3, $t2, 1
	sw $t3, 36($sp)
	li $t1, 4
	lw $t2, 4($sp)
	mul $t3, $t1, $t2
	lw $t4, 48($sp)
	add $t1, $t4, $t3
	lw $t2, 0($t1)
	sub $t3, $t2, 2
	sw $t3, 40($sp)
	lw $t1, 36($sp)
	lw $t2, 0($sp)
	move $a0, $t1	# save arguments
	move $a1, $t2	# save arguments
	jal Tag3
	move $t3, $v0
	bne $t3, 1, Tag35
	lw $t1, 40($sp)
	lw $t2, 0($sp)
	move $a0, $t1	# save arguments
	move $a1, $t2	# save arguments
	jal Tag3
	move $t3, $v0
	bne $t3, 1, Tag35
	li $t1, 4
	lw $t2, 36($sp)
	mul $t3, $t1, $t2
	lw $t4, 52($sp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 40($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($t1)
	add $t2, $t5, $t4
	li $t1, 1
	neg $t3, $t1
	lw $t4, 0($t2)
	bne $t4, $t3, Tag35
	li $t1, 1
	j Tag36
Tag35:
	li $t1, 0
Tag36:
	move $t2, $t1
	beqz $t2, Tag37
	lw $t1, 24($sp)
	add $t2, $t1, 1
	sw $t2, 24($sp)
	li $t1, 4
	lw $t2, 24($sp)
	mul $t3, $t1, $t2
	lw $t4, 44($sp)
	add $t1, $t4, $t3
	lw $t2, 36($sp)
	sw $t2, 0($t1)
	li $t3, 4
	lw $t1, 24($sp)
	mul $t2, $t3, $t1
	lw $t4, 48($sp)
	add $t1, $t4, $t2
	lw $t3, 40($sp)
	sw $t3, 0($t1)
	li $t2, 4
	lw $t1, 36($sp)
	mul $t3, $t2, $t1
	lw $t4, 52($sp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 40($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($t1)
	add $t2, $t5, $t4
	lw $t1, 32($sp)
	add $t3, $t1, 1
	sw $t3, 0($t2)
	lw $t1, 36($sp)
	lw $t2, 16($sp)
	bne $t1, $t2, Tag38
	lw $t3, 40($sp)
	lw $t1, 20($sp)
	bne $t3, $t1, Tag38
	li $t2, 1
	j Tag39
Tag38:
	li $t2, 0
Tag39:
	move $t1, $t2
	beqz $t1, Tag37
	li $t2, 1
	sw $t2, 28($sp)
Tag37:
	li $t1, 4
	lw $t2, 4($sp)
	mul $t3, $t1, $t2
	lw $t4, 44($sp)
	add $t1, $t4, $t3
	lw $t2, 0($t1)
	add $t3, $t2, 1
	sw $t3, 36($sp)
	li $t1, 4
	lw $t2, 4($sp)
	mul $t3, $t1, $t2
	lw $t4, 48($sp)
	add $t1, $t4, $t3
	lw $t2, 0($t1)
	add $t3, $t2, 2
	sw $t3, 40($sp)
	lw $t1, 36($sp)
	lw $t2, 0($sp)
	move $a0, $t1	# save arguments
	move $a1, $t2	# save arguments
	jal Tag3
	move $t3, $v0
	bne $t3, 1, Tag41
	lw $t1, 40($sp)
	lw $t2, 0($sp)
	move $a0, $t1	# save arguments
	move $a1, $t2	# save arguments
	jal Tag3
	move $t3, $v0
	bne $t3, 1, Tag41
	li $t1, 4
	lw $t2, 36($sp)
	mul $t3, $t1, $t2
	lw $t4, 52($sp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 40($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($t1)
	add $t2, $t5, $t4
	li $t1, 1
	neg $t3, $t1
	lw $t4, 0($t2)
	bne $t4, $t3, Tag41
	li $t1, 1
	j Tag42
Tag41:
	li $t1, 0
Tag42:
	move $t2, $t1
	beqz $t2, Tag43
	lw $t1, 24($sp)
	add $t2, $t1, 1
	sw $t2, 24($sp)
	li $t1, 4
	lw $t2, 24($sp)
	mul $t3, $t1, $t2
	lw $t4, 44($sp)
	add $t1, $t4, $t3
	lw $t2, 36($sp)
	sw $t2, 0($t1)
	li $t3, 4
	lw $t1, 24($sp)
	mul $t2, $t3, $t1
	lw $t4, 48($sp)
	add $t1, $t4, $t2
	lw $t3, 40($sp)
	sw $t3, 0($t1)
	li $t2, 4
	lw $t1, 36($sp)
	mul $t3, $t2, $t1
	lw $t4, 52($sp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 40($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($t1)
	add $t2, $t5, $t4
	lw $t1, 32($sp)
	add $t3, $t1, 1
	sw $t3, 0($t2)
	lw $t1, 36($sp)
	lw $t2, 16($sp)
	bne $t1, $t2, Tag44
	lw $t3, 40($sp)
	lw $t1, 20($sp)
	bne $t3, $t1, Tag44
	li $t2, 1
	j Tag45
Tag44:
	li $t2, 0
Tag45:
	move $t1, $t2
	beqz $t1, Tag43
	li $t2, 1
	sw $t2, 28($sp)
Tag43:
	li $t1, 4
	lw $t2, 4($sp)
	mul $t3, $t1, $t2
	lw $t4, 44($sp)
	add $t1, $t4, $t3
	lw $t2, 0($t1)
	sub $t3, $t2, 2
	sw $t3, 36($sp)
	li $t1, 4
	lw $t2, 4($sp)
	mul $t3, $t1, $t2
	lw $t4, 48($sp)
	add $t1, $t4, $t3
	lw $t2, 0($t1)
	sub $t3, $t2, 1
	sw $t3, 40($sp)
	lw $t1, 36($sp)
	lw $t2, 0($sp)
	move $a0, $t1	# save arguments
	move $a1, $t2	# save arguments
	jal Tag3
	move $t3, $v0
	bne $t3, 1, Tag47
	lw $t1, 40($sp)
	lw $t2, 0($sp)
	move $a0, $t1	# save arguments
	move $a1, $t2	# save arguments
	jal Tag3
	move $t3, $v0
	bne $t3, 1, Tag47
	li $t1, 4
	lw $t2, 36($sp)
	mul $t3, $t1, $t2
	lw $t4, 52($sp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 40($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($t1)
	add $t2, $t5, $t4
	li $t1, 1
	neg $t3, $t1
	lw $t4, 0($t2)
	bne $t4, $t3, Tag47
	li $t1, 1
	j Tag48
Tag47:
	li $t1, 0
Tag48:
	move $t2, $t1
	beqz $t2, Tag49
	lw $t1, 24($sp)
	add $t2, $t1, 1
	sw $t2, 24($sp)
	li $t1, 4
	lw $t2, 24($sp)
	mul $t3, $t1, $t2
	lw $t4, 44($sp)
	add $t1, $t4, $t3
	lw $t2, 36($sp)
	sw $t2, 0($t1)
	li $t3, 4
	lw $t1, 24($sp)
	mul $t2, $t3, $t1
	lw $t4, 48($sp)
	add $t1, $t4, $t2
	lw $t3, 40($sp)
	sw $t3, 0($t1)
	li $t2, 4
	lw $t1, 36($sp)
	mul $t3, $t2, $t1
	lw $t4, 52($sp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 40($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($t1)
	add $t2, $t5, $t4
	lw $t1, 32($sp)
	add $t3, $t1, 1
	sw $t3, 0($t2)
	lw $t1, 36($sp)
	lw $t2, 16($sp)
	bne $t1, $t2, Tag50
	lw $t3, 40($sp)
	lw $t1, 20($sp)
	bne $t3, $t1, Tag50
	li $t2, 1
	j Tag51
Tag50:
	li $t2, 0
Tag51:
	move $t1, $t2
	beqz $t1, Tag49
	li $t2, 1
	sw $t2, 28($sp)
Tag49:
	li $t1, 4
	lw $t2, 4($sp)
	mul $t3, $t1, $t2
	lw $t4, 44($sp)
	add $t1, $t4, $t3
	lw $t2, 0($t1)
	sub $t3, $t2, 2
	sw $t3, 36($sp)
	li $t1, 4
	lw $t2, 4($sp)
	mul $t3, $t1, $t2
	lw $t4, 48($sp)
	add $t1, $t4, $t3
	lw $t2, 0($t1)
	add $t3, $t2, 1
	sw $t3, 40($sp)
	lw $t1, 36($sp)
	lw $t2, 0($sp)
	move $a0, $t1	# save arguments
	move $a1, $t2	# save arguments
	jal Tag3
	move $t3, $v0
	bne $t3, 1, Tag53
	lw $t1, 40($sp)
	lw $t2, 0($sp)
	move $a0, $t1	# save arguments
	move $a1, $t2	# save arguments
	jal Tag3
	move $t3, $v0
	bne $t3, 1, Tag53
	li $t1, 4
	lw $t2, 36($sp)
	mul $t3, $t1, $t2
	lw $t4, 52($sp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 40($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($t1)
	add $t2, $t5, $t4
	li $t1, 1
	neg $t3, $t1
	lw $t4, 0($t2)
	bne $t4, $t3, Tag53
	li $t1, 1
	j Tag54
Tag53:
	li $t1, 0
Tag54:
	move $t2, $t1
	beqz $t2, Tag55
	lw $t1, 24($sp)
	add $t2, $t1, 1
	sw $t2, 24($sp)
	li $t1, 4
	lw $t2, 24($sp)
	mul $t3, $t1, $t2
	lw $t4, 44($sp)
	add $t1, $t4, $t3
	lw $t2, 36($sp)
	sw $t2, 0($t1)
	li $t3, 4
	lw $t1, 24($sp)
	mul $t2, $t3, $t1
	lw $t4, 48($sp)
	add $t1, $t4, $t2
	lw $t3, 40($sp)
	sw $t3, 0($t1)
	li $t2, 4
	lw $t1, 36($sp)
	mul $t3, $t2, $t1
	lw $t4, 52($sp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 40($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($t1)
	add $t2, $t5, $t4
	lw $t1, 32($sp)
	add $t3, $t1, 1
	sw $t3, 0($t2)
	lw $t1, 36($sp)
	lw $t2, 16($sp)
	bne $t1, $t2, Tag56
	lw $t3, 40($sp)
	lw $t1, 20($sp)
	bne $t3, $t1, Tag56
	li $t2, 1
	j Tag57
Tag56:
	li $t2, 0
Tag57:
	move $t1, $t2
	beqz $t1, Tag55
	li $t2, 1
	sw $t2, 28($sp)
Tag55:
	li $t1, 4
	lw $t2, 4($sp)
	mul $t3, $t1, $t2
	lw $t4, 44($sp)
	add $t1, $t4, $t3
	lw $t2, 0($t1)
	add $t3, $t2, 2
	sw $t3, 36($sp)
	li $t1, 4
	lw $t2, 4($sp)
	mul $t3, $t1, $t2
	lw $t4, 48($sp)
	add $t1, $t4, $t3
	lw $t2, 0($t1)
	sub $t3, $t2, 1
	sw $t3, 40($sp)
	lw $t1, 36($sp)
	lw $t2, 0($sp)
	move $a0, $t1	# save arguments
	move $a1, $t2	# save arguments
	jal Tag3
	move $t3, $v0
	bne $t3, 1, Tag59
	lw $t1, 40($sp)
	lw $t2, 0($sp)
	move $a0, $t1	# save arguments
	move $a1, $t2	# save arguments
	jal Tag3
	move $t3, $v0
	bne $t3, 1, Tag59
	li $t1, 4
	lw $t2, 36($sp)
	mul $t3, $t1, $t2
	lw $t4, 52($sp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 40($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($t1)
	add $t2, $t5, $t4
	li $t1, 1
	neg $t3, $t1
	lw $t4, 0($t2)
	bne $t4, $t3, Tag59
	li $t1, 1
	j Tag60
Tag59:
	li $t1, 0
Tag60:
	move $t2, $t1
	beqz $t2, Tag61
	lw $t1, 24($sp)
	add $t2, $t1, 1
	sw $t2, 24($sp)
	li $t1, 4
	lw $t2, 24($sp)
	mul $t3, $t1, $t2
	lw $t4, 44($sp)
	add $t1, $t4, $t3
	lw $t2, 36($sp)
	sw $t2, 0($t1)
	li $t3, 4
	lw $t1, 24($sp)
	mul $t2, $t3, $t1
	lw $t4, 48($sp)
	add $t1, $t4, $t2
	lw $t3, 40($sp)
	sw $t3, 0($t1)
	li $t2, 4
	lw $t1, 36($sp)
	mul $t3, $t2, $t1
	lw $t4, 52($sp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 40($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($t1)
	add $t2, $t5, $t4
	lw $t1, 32($sp)
	add $t3, $t1, 1
	sw $t3, 0($t2)
	lw $t1, 36($sp)
	lw $t2, 16($sp)
	bne $t1, $t2, Tag62
	lw $t3, 40($sp)
	lw $t1, 20($sp)
	bne $t3, $t1, Tag62
	li $t2, 1
	j Tag63
Tag62:
	li $t2, 0
Tag63:
	move $t1, $t2
	beqz $t1, Tag61
	li $t2, 1
	sw $t2, 28($sp)
Tag61:
	li $t1, 4
	lw $t2, 4($sp)
	mul $t3, $t1, $t2
	lw $t4, 44($sp)
	add $t1, $t4, $t3
	lw $t2, 0($t1)
	add $t3, $t2, 2
	sw $t3, 36($sp)
	li $t1, 4
	lw $t2, 4($sp)
	mul $t3, $t1, $t2
	lw $t4, 48($sp)
	add $t1, $t4, $t3
	lw $t2, 0($t1)
	add $t3, $t2, 1
	sw $t3, 40($sp)
	lw $t1, 36($sp)
	lw $t2, 0($sp)
	move $a0, $t1	# save arguments
	move $a1, $t2	# save arguments
	jal Tag3
	move $t3, $v0
	bne $t3, 1, Tag65
	lw $t1, 40($sp)
	lw $t2, 0($sp)
	move $a0, $t1	# save arguments
	move $a1, $t2	# save arguments
	jal Tag3
	move $t3, $v0
	bne $t3, 1, Tag65
	li $t1, 4
	lw $t2, 36($sp)
	mul $t3, $t1, $t2
	lw $t4, 52($sp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 40($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($t1)
	add $t2, $t5, $t4
	li $t1, 1
	neg $t3, $t1
	lw $t4, 0($t2)
	bne $t4, $t3, Tag65
	li $t1, 1
	j Tag66
Tag65:
	li $t1, 0
Tag66:
	move $t2, $t1
	beqz $t2, Tag67
	lw $t1, 24($sp)
	add $t2, $t1, 1
	sw $t2, 24($sp)
	li $t1, 4
	lw $t2, 24($sp)
	mul $t3, $t1, $t2
	lw $t4, 44($sp)
	add $t1, $t4, $t3
	lw $t2, 36($sp)
	sw $t2, 0($t1)
	li $t3, 4
	lw $t1, 24($sp)
	mul $t2, $t3, $t1
	lw $t4, 48($sp)
	add $t1, $t4, $t2
	lw $t3, 40($sp)
	sw $t3, 0($t1)
	li $t2, 4
	lw $t1, 36($sp)
	mul $t3, $t2, $t1
	lw $t4, 52($sp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 40($sp)
	mul $t4, $t2, $t3
	lw $t5, 0($t1)
	add $t2, $t5, $t4
	lw $t1, 32($sp)
	add $t3, $t1, 1
	sw $t3, 0($t2)
	lw $t1, 36($sp)
	lw $t2, 16($sp)
	bne $t1, $t2, Tag68
	lw $t3, 40($sp)
	lw $t1, 20($sp)
	bne $t3, $t1, Tag68
	li $t2, 1
	j Tag69
Tag68:
	li $t2, 0
Tag69:
	move $t1, $t2
	beqz $t1, Tag67
	li $t2, 1
	sw $t2, 28($sp)
Tag67:
	lw $t1, 28($sp)
	seq $t2, $t1, 1
	move $t3, $t2
	beqz $t3, Tag70
	j Tag21
Tag70:
	lw $t1, 4($sp)
	add $t2, $t1, 1
	sw $t2, 4($sp)
	j Tag19
Tag21:
	lw $t1, 28($sp)
	seq $t2, $t1, 1
	move $t3, $t2
	beqz $t3, Tag71
	la $t1, Tag73
	li $t2, 4
	lw $t3, 16($sp)
	mul $t4, $t2, $t3
	lw $t5, 52($sp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 20($sp)
	mul $t5, $t3, $t4
	lw $t0, 0($t2)
	add $t3, $t0, $t5
	lw $t2, 0($t3)
	move $a0, $t2
	li $v0, 1
	syscall
	la $a0, fs_newLine
	li $v0, 4
	syscall
	j Tag72
Tag71:
	la $t0, Tag74
	move $a0, $t0
	li $v0, 4
	syscall
Tag72:
	li $t0, 0
	move $v0, $t0
	lw $t5, 3120($sp)	# level load register
	lw $t4, 3116($sp)	# level load register
	lw $t3, 3112($sp)	# level load register
	lw $t2, 3108($sp)	# level load register
	lw $t1, 3104($sp)	# level load register
	lw $t0, 3100($sp)	# level load register
	lw $ra, 3164($sp)
	addiu $sp, $sp, 3168
	jr $ra
	lw $t5, 3120($sp)	# level load register
	lw $t4, 3116($sp)	# level load register
	lw $t3, 3112($sp)	# level load register
	lw $t2, 3108($sp)	# level load register
	lw $t1, 3104($sp)	# level load register
	lw $t0, 3100($sp)	# level load register
	lw $ra, 3164($sp)
	addiu $sp, $sp, 3168
	jr $ra
########################################
############### RUN-TIME ###############
########################################

#malloc
Tag1:
	# a0 -- size in bytes (already x4)
	li $v0, 9
	syscall
	jr $ra

#getchar
Tag2:
	li $v0, 12
	syscall
	jr $ra
	
exit:
	li $v0, 10
	syscall
	jr $ra

## Daniel J. Ellard -- 03/13/94
## printf.asm--
## an implementation of a simple printf work-alike.

## printf--
## A simple printf-like function. Understands just the basic forms
## of the %s, %d, %c, and %% formats, and can only have 3 embedded
## formats (so that all of the parameters are passed in registers).
## If there are more than 3 embedded formats, all but the first 3 are
## completely ignored (not even printed).
## Register Usage:
## $a0,$s0 - pointer to format string
## $a1,$s1 - format argument 1 (optional)
## $a2,$s2 - format argument 2 (optional)
## $a3,$s3 - format argument 3 (optional)
## $s4 - count of formats processed.
## $s5 - char at $s4.
## $s6 - pointer to printf buffer
##
Tag0:
	subu $sp, $sp, 56 # set up the stack frame,
	sw $ra, 32($sp) # saving the local environment.
	sw $fp, 28($sp)
	sw $s0, 24($sp)
	sw $s1, 20($sp)
	sw $s2, 16($sp)
	sw $s3, 12($sp)
	sw $s4, 8($sp)
	sw $s5, 4($sp)
	sw $s6, 0($sp)
	sw $s7, 36($sp)
	sw $t0, 40($sp)
	sw $t1, 44($sp)
	sw $t2, 48($sp)
	sw $t3, 52($sp)
	addu $fp, $sp, 52

# grab the arguments:
	move $s0, $a0 # fmt string
	move $s1, $a1 # arg1 (optional)
	move $s2, $a2 # arg2 (optional)
	move $s3, $a3 # arg3 (optional)
	lw $s7, 16($v1)# arg4 (optional)
	lw $t0, 20($v1)# arg5 (optional)

	li $s4, 0 # set # of formats = 0
	la $s6, printf_buf # set s6 = base of printf buffer.

printf_loop: # process each character in the fmt:
	lb $s5, 0($s0) # get the next character, and then
	addu $s0, $s0, 1 # bump up $s0 to the next character.

	beq $s5, '%', printf_fmt # if the fmt character, then do fmt.
	beq $0, $s5, printf_end # if zero, then go to end.

printf_putc:
	sb $s5, 0($s6) # otherwise, just put this char
	sb $0, 1($s6) # into the printf buffer,
	move $a0, $s6 # and then print it with the
	li $v0, 4 # print_str syscall
	syscall

	b printf_loop # loop on.

printf_fmt:
	lb $s5, 0($s0) # see what the fmt character is,
	addu $s0, $s0, 1 # and bump up the pointer.

	beq $s4, 5, printf_loop # if we've already processed 3 args,
# then *ignore* this fmt.
	beq $s5, '0', printf_pre
	beq $s5, 'd', printf_int # if 'd', print as a decimal integer.
	beq $s5, 's', printf_str # if 's', print as a string.
	beq $s5, 'c', printf_char # if 'c', print as a ASCII char.
	beq $s5, '%', printf_perc # if '%', print a '%'
	b printf_loop # otherwise, just continue.

printf_shift_args: # shift over the fmt args,
	move $s1, $s2 # $s1 = $s2
	move $s2, $s3 # $s2 = $s3
	move $s3, $s7 # $s3 = $s7
	move $s7, $t0

	add $s4, $s4, 1 # increment # of args processed.

	b printf_loop # and continue the main loop.
	
printf_pre:
	lb $s5, ($s0)
	sub $s5, $s5, 48
	add $s0, $s0, 2
	
	li $t1, 0
	move $t2, $s1
printf_digits_begin:
	beqz $t2, printf_digits_end
	add $t1, $t1, 1
	div $t2, $t2, 10
	b printf_digits_begin
	
printf_digits_end:
	sub $t1, $s5, $t1
	
printf_pre_output_zero_begin:
	beqz $t1, printf_pre_output_zero_end
	sub $t1, $t1, 1
	li $a0, 0
	li $v0, 1
	syscall
	b printf_pre_output_zero_begin
	
printf_pre_output_zero_end:
	move $a0, $s1
	li $v0, 1
	syscall
	b printf_shift_args
	

printf_int: # deal with a %d:
	move $a0, $s1 # do a print_int syscall of $s1.
	li $v0, 1
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_str: # deal with a %s:
	move $a0, $s1 # do a print_string syscall of $s1.
	li $v0, 4
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_char: # deal with a %c:
	sb $s1, 0($s6) # fill the buffer in with byte $s1,
	sb $0, 1($s6) # and then a null.
	move $a0, $s6 # and then do a print_str syscall
	li $v0, 4 # on the buffer.
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_perc: # deal with a %%:
	li $s5, '%' # (this is redundant)
	sb $s5, 0($s6) # fill the buffer in with byte %,
	sb $0, 1($s6) # and then a null.
	move $a0, $s6 # and then do a print_str syscall
	li $v0, 4 # on the buffer.
	syscall
	b printf_loop # branch to printf_loop

printf_end:
	lw $t1, 44($sp)
	lw $t2, 48($sp)
	lw $t3, 52($sp)
	lw $s7, 36($sp)
	lw $t0, 40($sp)
	lw $ra, 32($sp) # restore the prior environment:
	lw $fp, 28($sp)
	lw $s0, 24($sp)
	lw $s1, 20($sp)
	lw $s2, 16($sp)
	lw $s3, 12($sp)
	lw $s4, 8($sp)
	lw $s5, 4($sp)
	lw $s6, 0($sp)
	addu $sp, $sp, 56 # release the stack frame.
	jr $ra # return.

.data
	printf_buf: .space 2

