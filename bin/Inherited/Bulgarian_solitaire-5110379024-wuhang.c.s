	.data
Tag17:
	.asciiz "%d "
	.align 2
Tag18:
	.asciiz "%c"
	.align 2
Tag54:
	.asciiz "Sorry, the number n must be a number s.t. there exists i satisfying n=1+2+...+i\n"
	.align 2
Tag55:
	.asciiz "Let's start!\n"
	.align 2
Tag56:
	.asciiz "%d\n"
	.align 2
Tag66:
	.asciiz "step %d:\n"
	.align 2
Tag67:
	.asciiz "Total: %d step(s)\n"
	.align 2
	.align 2
	fs_whiteSpace: .asciiz " "
	.align 2
	fs_newLine: .asciiz "\n"
	.align 2
	.globl args
args:	.space 12
	.align 2
	.globl stat
stat:	.space 148
	.text
	.align 2
	.globl main
main:
	la $gp, stat
	la $v1, args
	li $t1, 0
	sw $t1, 0($gp)
	li $t2, 210
	sw $t2, 0($gp)
	li $t1, 0
	sw $t1, 4($gp)
	li $t2, 0
	sw $t2, 8($gp)
	li $t1, 400
	move $a0, $t1	# save arguments
	jal Tag1
	move $t2, $v0
	sw $t2, 12($gp)
	li $t1, 0
	sw $t1, 16($gp)
	li $t2, 48271
	sw $t2, 16($gp)
	li $t1, 0
	sw $t1, 20($gp)
	li $t2, 2147483647
	sw $t2, 20($gp)
	li $t1, 0
	sw $t1, 24($gp)
	li $t2, 0
	sw $t2, 28($gp)
	li $t1, 0
	sw $t1, 32($gp)
	li $t2, 1
	sw $t2, 32($gp)
	jal Tag52
	jal exit
Tag3:
	addi $sp, $sp, -148	# enter
	sw $ra, 144($sp)
	sw $t6, 104($sp)	# level save register
	sw $t5, 100($sp)	# level save register
	sw $t4, 96($sp)	# level save register
	sw $t3, 92($sp)	# level save register
	sw $t2, 88($sp)	# level save register
	sw $t1, 84($sp)	# level save register
	sw $t0, 80($sp)	# level save register
	lw $t2, 32($gp)
	lw $t3, 24($gp)
	rem $t4, $t2, $t3
	lw $t5, 16($gp)
	mul $t2, $t5, $t4
	lw $t3, 32($gp)
	lw $t4, 24($gp)
	div $t5, $t3, $t4
	lw $t6, 28($gp)
	mul $t3, $t6, $t5
	sub $t4, $t2, $t3
	sw $t4, 0($sp)
	lw $t2, 0($sp)
	sge $t3, $t2, 0
	move $t4, $t3
	beqz $t4, Tag4
	lw $t2, 0($sp)
	sw $t2, 32($gp)
	j Tag5
Tag4:
	lw $t2, 0($sp)
	lw $t3, 20($gp)
	add $t1, $t2, $t3
	sw $t1, 32($gp)
Tag5:
	lw $t1, 32($gp)
	move $v0, $t1
	lw $t6, 104($sp)	# level load register
	lw $t5, 100($sp)	# level load register
	lw $t4, 96($sp)	# level load register
	lw $t3, 92($sp)	# level load register
	lw $t2, 88($sp)	# level load register
	lw $t1, 84($sp)	# level load register
	lw $t0, 80($sp)	# level load register
	lw $ra, 144($sp)
	addiu $sp, $sp, 148
	jr $ra
	lw $t6, 104($sp)	# level load register
	lw $t5, 100($sp)	# level load register
	lw $t4, 96($sp)	# level load register
	lw $t3, 92($sp)	# level load register
	lw $t2, 88($sp)	# level load register
	lw $t1, 84($sp)	# level load register
	lw $t0, 80($sp)	# level load register
	lw $ra, 144($sp)
	addiu $sp, $sp, 148
	jr $ra
Tag6:
	addi $sp, $sp, -76	# enter
	sw $ra, 72($sp)
	sw $t2, 16($sp)	# level save register
	sw $t1, 12($sp)	# level save register
	sw $t0, 8($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	lw $t2, 0($sp)
	sw $t2, 32($gp)
	lw $t2, 16($sp)	# level load register
	lw $t1, 12($sp)	# level load register
	lw $t0, 8($sp)	# level load register
	lw $ra, 72($sp)
	addiu $sp, $sp, 76
	jr $ra
Tag7:
	addi $sp, $sp, -172	# enter
	sw $ra, 168($sp)
	sw $t6, 128($sp)	# level save register
	sw $t5, 124($sp)	# level save register
	sw $t4, 120($sp)	# level save register
	sw $t3, 116($sp)	# level save register
	sw $t2, 112($sp)	# level save register
	sw $t1, 108($sp)	# level save register
	sw $t0, 104($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	sw $a1, 4($sp)	# load argument
	li $t2, 4
	lw $t3, 0($sp)
	mul $t4, $t2, $t3
	lw $t5, 12($gp)
	add $t2, $t5, $t4
	lw $t3, 0($t2)
	sw $t3, 8($sp)
	li $t2, 4
	lw $t3, 0($sp)
	mul $t4, $t2, $t3
	lw $t5, 12($gp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 4($sp)
	mul $t5, $t3, $t4
	lw $t6, 12($gp)
	add $t3, $t6, $t5
	lw $t4, 0($t3)
	sw $t4, 0($t2)
	li $t3, 4
	lw $t2, 4($sp)
	mul $t4, $t3, $t2
	lw $t5, 12($gp)
	add $t2, $t5, $t4
	lw $t1, 8($sp)
	sw $t1, 0($t2)
	lw $t6, 128($sp)	# level load register
	lw $t5, 124($sp)	# level load register
	lw $t4, 120($sp)	# level load register
	lw $t3, 116($sp)	# level load register
	lw $t2, 112($sp)	# level load register
	lw $t1, 108($sp)	# level load register
	lw $t0, 104($sp)	# level load register
	lw $ra, 168($sp)
	addiu $sp, $sp, 172
	jr $ra
Tag8:
	addi $sp, $sp, -136	# enter
	sw $ra, 132($sp)
	sw $t5, 88($sp)	# level save register
	sw $t4, 84($sp)	# level save register
	sw $t3, 80($sp)	# level save register
	sw $t2, 76($sp)	# level save register
	sw $t1, 72($sp)	# level save register
	sw $t0, 68($sp)	# level save register
	sw $a0, 0($sp)	# load argument
Tag9:
	lw $t2, 4($gp)
	lw $t3, 0($sp)
	sle $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag11
	lw $t2, 4($gp)
	add $t3, $t2, 1
	lw $t4, 4($gp)
	mul $t2, $t4, $t3
	div $t5, $t2, 2
	lw $t3, 0($sp)
	seq $t2, $t3, $t5
	move $t4, $t2
	beqz $t4, Tag12
	li $t2, 1
	move $v0, $t2
	lw $t5, 88($sp)	# level load register
	lw $t4, 84($sp)	# level load register
	lw $t3, 80($sp)	# level load register
	lw $t2, 76($sp)	# level load register
	lw $t1, 72($sp)	# level load register
	lw $t0, 68($sp)	# level load register
	lw $ra, 132($sp)
	addiu $sp, $sp, 136
	jr $ra
Tag12:
	lw $t2, 4($gp)
	add $t3, $t2, 1
	sw $t3, 4($gp)
	j Tag9
Tag11:
	li $t0, 0
	move $v0, $t0
	lw $t5, 88($sp)	# level load register
	lw $t4, 84($sp)	# level load register
	lw $t3, 80($sp)	# level load register
	lw $t2, 76($sp)	# level load register
	lw $t1, 72($sp)	# level load register
	lw $t0, 68($sp)	# level load register
	lw $ra, 132($sp)
	addiu $sp, $sp, 136
	jr $ra
	lw $t5, 88($sp)	# level load register
	lw $t4, 84($sp)	# level load register
	lw $t3, 80($sp)	# level load register
	lw $t2, 76($sp)	# level load register
	lw $t1, 72($sp)	# level load register
	lw $t0, 68($sp)	# level load register
	lw $ra, 132($sp)
	addiu $sp, $sp, 136
	jr $ra
Tag13:
	addi $sp, $sp, -144	# enter
	sw $ra, 140($sp)
	sw $t6, 100($sp)	# level save register
	sw $t5, 96($sp)	# level save register
	sw $t4, 92($sp)	# level save register
	sw $t3, 88($sp)	# level save register
	sw $t2, 84($sp)	# level save register
	sw $t1, 80($sp)	# level save register
	sw $t0, 76($sp)	# level save register
	li $t2, 0
	sw $t2, 0($sp)
Tag14:
	lw $t2, 0($sp)
	lw $t3, 8($gp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag16
	la $t2, Tag17
	li $t3, 4
	lw $t4, 0($sp)
	mul $t5, $t3, $t4
	lw $t6, 12($gp)
	add $t3, $t6, $t5
	lw $t4, 0($t3)
	move $a0, $t4
	li $v0, 1
	syscall
	la $a0, fs_whiteSpace
	li $v0, 4
	syscall
	lw $t3, 0($sp)
	add $t2, $t3, 1
	sw $t2, 0($sp)
	j Tag14
Tag16:
	la $t0, Tag18
	li $t1, 10
	move $a0, $t0	# save arguments
	move $a1, $t1	# save arguments
	jal Tag0
	move $t2, $v0
	lw $t6, 100($sp)	# level load register
	lw $t5, 96($sp)	# level load register
	lw $t4, 92($sp)	# level load register
	lw $t3, 88($sp)	# level load register
	lw $t2, 84($sp)	# level load register
	lw $t1, 80($sp)	# level load register
	lw $t0, 76($sp)	# level load register
	lw $ra, 140($sp)
	addiu $sp, $sp, 144
	jr $ra
Tag19:
	addi $sp, $sp, -476	# enter
	sw $ra, 472($sp)
	sw $t6, 432($sp)	# level save register
	sw $t5, 428($sp)	# level save register
	sw $t4, 424($sp)	# level save register
	sw $t3, 420($sp)	# level save register
	sw $t2, 416($sp)	# level save register
	sw $t1, 412($sp)	# level save register
	sw $t0, 408($sp)	# level save register
	li $t2, 0
	sw $t2, 4($sp)
	li $t3, 400
	move $a0, $t3	# save arguments
	jal Tag1
	move $t2, $v0
	sw $t2, 8($sp)
	lw $t3, 8($gp)
	lw $t2, 4($gp)
	sne $t4, $t3, $t2
	move $t5, $t4
	beqz $t5, Tag20
	li $t2, 0
	move $v0, $t2
	lw $t6, 432($sp)	# level load register
	lw $t5, 428($sp)	# level load register
	lw $t4, 424($sp)	# level load register
	lw $t3, 420($sp)	# level load register
	lw $t2, 416($sp)	# level load register
	lw $t1, 412($sp)	# level load register
	lw $t0, 408($sp)	# level load register
	lw $ra, 472($sp)
	addiu $sp, $sp, 476
	jr $ra
Tag20:
	lw $t2, 4($sp)
	lw $t3, 8($gp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag23
	li $t2, 4
	lw $t3, 4($sp)
	mul $t4, $t2, $t3
	lw $t5, 8($sp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 4($sp)
	mul $t5, $t3, $t4
	lw $t6, 12($gp)
	add $t3, $t6, $t5
	lw $t4, 0($t3)
	sw $t4, 0($t2)
	lw $t3, 4($sp)
	add $t2, $t3, 1
	sw $t2, 4($sp)
	j Tag20
Tag23:
	li $t2, 0
	sw $t2, 0($sp)
Tag24:
	lw $t2, 8($gp)
	sub $t3, $t2, 1
	lw $t4, 0($sp)
	slt $t2, $t4, $t3
	move $t5, $t2
	beqz $t5, Tag26
	lw $t2, 0($sp)
	add $t3, $t2, 1
	sw $t3, 4($sp)
Tag27:
	lw $t2, 4($sp)
	lw $t3, 8($gp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag29
	li $t2, 4
	lw $t3, 0($sp)
	mul $t4, $t2, $t3
	lw $t5, 8($sp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 4($sp)
	mul $t5, $t3, $t4
	lw $t6, 8($sp)
	add $t3, $t6, $t5
	lw $t4, 0($t2)
	lw $t5, 0($t3)
	sgt $t2, $t4, $t5
	move $t3, $t2
	beqz $t3, Tag30
	li $t2, 4
	lw $t3, 0($sp)
	mul $t4, $t2, $t3
	lw $t5, 8($sp)
	add $t2, $t5, $t4
	lw $t3, 0($t2)
	sw $t3, 12($sp)
	li $t2, 4
	lw $t3, 0($sp)
	mul $t4, $t2, $t3
	lw $t5, 8($sp)
	add $t2, $t5, $t4
	li $t3, 4
	lw $t4, 4($sp)
	mul $t5, $t3, $t4
	lw $t6, 8($sp)
	add $t3, $t6, $t5
	lw $t4, 0($t3)
	sw $t4, 0($t2)
	li $t3, 4
	lw $t2, 4($sp)
	mul $t4, $t3, $t2
	lw $t5, 8($sp)
	add $t2, $t5, $t4
	lw $t3, 12($sp)
	sw $t3, 0($t2)
Tag30:
	lw $t2, 4($sp)
	add $t3, $t2, 1
	sw $t3, 4($sp)
	j Tag27
Tag29:
	lw $t2, 0($sp)
	add $t3, $t2, 1
	sw $t3, 0($sp)
	j Tag24
Tag26:
	li $t2, 0
	sw $t2, 0($sp)
Tag31:
	lw $t2, 0($sp)
	lw $t3, 8($gp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag33
	li $t2, 4
	lw $t3, 0($sp)
	mul $t4, $t2, $t3
	lw $t5, 8($sp)
	add $t2, $t5, $t4
	lw $t3, 0($sp)
	add $t4, $t3, 1
	lw $t5, 0($t2)
	sne $t3, $t5, $t4
	move $t2, $t3
	beqz $t2, Tag34
	li $t3, 0
	move $v0, $t3
	lw $t6, 432($sp)	# level load register
	lw $t5, 428($sp)	# level load register
	lw $t4, 424($sp)	# level load register
	lw $t3, 420($sp)	# level load register
	lw $t2, 416($sp)	# level load register
	lw $t1, 412($sp)	# level load register
	lw $t0, 408($sp)	# level load register
	lw $ra, 472($sp)
	addiu $sp, $sp, 476
	jr $ra
Tag34:
	lw $t2, 0($sp)
	add $t3, $t2, 1
	sw $t3, 0($sp)
	j Tag31
Tag33:
	li $t0, 1
	move $v0, $t0
	lw $t6, 432($sp)	# level load register
	lw $t5, 428($sp)	# level load register
	lw $t4, 424($sp)	# level load register
	lw $t3, 420($sp)	# level load register
	lw $t2, 416($sp)	# level load register
	lw $t1, 412($sp)	# level load register
	lw $t0, 408($sp)	# level load register
	lw $ra, 472($sp)
	addiu $sp, $sp, 476
	jr $ra
	lw $t6, 432($sp)	# level load register
	lw $t5, 428($sp)	# level load register
	lw $t4, 424($sp)	# level load register
	lw $t3, 420($sp)	# level load register
	lw $t2, 416($sp)	# level load register
	lw $t1, 412($sp)	# level load register
	lw $t0, 408($sp)	# level load register
	lw $ra, 472($sp)
	addiu $sp, $sp, 476
	jr $ra
Tag35:
	addi $sp, $sp, -276	# enter
	sw $ra, 272($sp)
	sw $t5, 228($sp)	# level save register
	sw $t4, 224($sp)	# level save register
	sw $t3, 220($sp)	# level save register
	sw $t2, 216($sp)	# level save register
	sw $t1, 212($sp)	# level save register
	sw $t0, 208($sp)	# level save register
	li $t2, 0
	sw $t2, 0($sp)
Tag36:
	lw $t2, 0($sp)
	lw $t3, 8($gp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag38
	li $t2, 4
	lw $t3, 0($sp)
	mul $t4, $t2, $t3
	lw $t5, 12($gp)
	add $t2, $t5, $t4
	lw $t3, 0($t2)
	seq $t4, $t3, $zero
	move $t2, $t4
	beqz $t2, Tag42
	lw $t3, 0($sp)
	add $t2, $t3, 1
	sw $t2, 4($sp)
Tag40:
	lw $t2, 4($sp)
	lw $t3, 8($gp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag42
	li $t2, 4
	lw $t3, 4($sp)
	mul $t4, $t2, $t3
	lw $t5, 12($gp)
	add $t2, $t5, $t4
	lw $t3, 0($t2)
	sne $t4, $t3, 0
	move $t2, $t4
	beqz $t2, Tag43
	lw $t3, 0($sp)
	lw $t2, 4($sp)
	move $a0, $t3	# save arguments
	move $a1, $t2	# save arguments
	jal Tag7
	move $t4, $v0
	j Tag42
Tag43:
	lw $t2, 4($sp)
	add $t3, $t2, 1
	sw $t3, 4($sp)
	j Tag40
Tag42:
	lw $t2, 0($sp)
	add $t3, $t2, 1
	sw $t3, 0($sp)
	j Tag36
Tag38:
	li $t2, 0
	sw $t2, 0($sp)
Tag44:
	lw $t2, 0($sp)
	lw $t3, 8($gp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag46
	li $t2, 4
	lw $t3, 0($sp)
	mul $t4, $t2, $t3
	lw $t5, 12($gp)
	add $t2, $t5, $t4
	lw $t3, 0($t2)
	seq $t4, $t3, $zero
	move $t2, $t4
	beqz $t2, Tag47
	lw $t3, 0($sp)
	sw $t3, 8($gp)
	j Tag46
Tag47:
	lw $t2, 0($sp)
	add $t3, $t2, 1
	sw $t3, 0($sp)
	j Tag44
Tag46:
	lw $t5, 228($sp)	# level load register
	lw $t4, 224($sp)	# level load register
	lw $t3, 220($sp)	# level load register
	lw $t2, 216($sp)	# level load register
	lw $t1, 212($sp)	# level load register
	lw $t0, 208($sp)	# level load register
	lw $ra, 272($sp)
	addiu $sp, $sp, 276
	jr $ra
Tag48:
	addi $sp, $sp, -164	# enter
	sw $ra, 160($sp)
	sw $t5, 116($sp)	# level save register
	sw $t4, 112($sp)	# level save register
	sw $t3, 108($sp)	# level save register
	sw $t2, 104($sp)	# level save register
	sw $t1, 100($sp)	# level save register
	sw $t0, 96($sp)	# level save register
	li $t2, 0
	sw $t2, 0($sp)
Tag49:
	lw $t2, 0($sp)
	lw $t3, 8($gp)
	slt $t4, $t2, $t3
	move $t5, $t4
	beqz $t5, Tag51
	li $t2, 4
	lw $t3, 0($sp)
	mul $t4, $t2, $t3
	lw $t5, 12($gp)
	add $t2, $t5, $t4
	lw $t3, 0($t2)
	sub $t4, $t3, 1
	sw $t4, 0($t2)
	lw $t3, 0($sp)
	add $t2, $t3, 1
	sw $t2, 0($sp)
	j Tag49
Tag51:
	li $t0, 4
	lw $t2, 8($gp)
	mul $t3, $t0, $t2
	lw $t4, 12($gp)
	add $t0, $t4, $t3
	lw $t2, 8($gp)
	sw $t2, 0($t0)
	lw $t3, 8($gp)
	lw $t0, 8($gp)
	add $t2, $t0, 1
	sw $t2, 8($gp)
	lw $t5, 116($sp)	# level load register
	lw $t4, 112($sp)	# level load register
	lw $t3, 108($sp)	# level load register
	lw $t2, 104($sp)	# level load register
	lw $t1, 100($sp)	# level load register
	lw $t0, 96($sp)	# level load register
	lw $ra, 160($sp)
	addiu $sp, $sp, 164
	jr $ra
Tag52:
	addi $sp, $sp, -464	# enter
	sw $ra, 460($sp)
	sw $t5, 416($sp)	# level save register
	sw $t4, 412($sp)	# level save register
	sw $t3, 408($sp)	# level save register
	sw $t2, 404($sp)	# level save register
	sw $t1, 400($sp)	# level save register
	sw $t0, 396($sp)	# level save register
	li $t2, 0
	sw $t2, 0($sp)
	li $t3, 0
	sw $t3, 4($sp)
	li $t2, 0
	sw $t2, 8($sp)
	lw $t3, 20($gp)
	lw $t2, 16($gp)
	div $t4, $t3, $t2
	sw $t4, 24($gp)
	lw $t2, 20($gp)
	lw $t3, 16($gp)
	rem $t4, $t2, $t3
	sw $t4, 28($gp)
	lw $t2, 0($gp)
	move $a0, $t2	# save arguments
	jal Tag8
	move $t3, $v0
	move $t4, $t3
	seq $t2, $t4, $zero
	move $t3, $t2
	beqz $t3, Tag53
	la $t2, Tag54
	move $a0, $t2
	li $v0, 4
	syscall
	li $t4, 1
	move $v0, $t4
	lw $t5, 416($sp)	# level load register
	lw $t4, 412($sp)	# level load register
	lw $t3, 408($sp)	# level load register
	lw $t2, 404($sp)	# level load register
	lw $t1, 400($sp)	# level load register
	lw $t0, 396($sp)	# level load register
	lw $ra, 460($sp)
	addiu $sp, $sp, 464
	jr $ra
Tag53:
	la $t2, Tag55
	move $a0, $t2
	li $v0, 4
	syscall
	li $t4, 3654898
	move $a0, $t4	# save arguments
	jal Tag6
	move $t2, $v0
	jal Tag3
	move $t3, $v0
	rem $t2, $t3, 10
	add $t4, $t2, 1
	sw $t4, 8($gp)
	la $t2, Tag56
	lw $t3, 8($gp)
	move $a0, $t3
	li $v0, 1
	syscall
	la $a0, fs_newLine
	li $v0, 4
	syscall
Tag57:
	lw $t2, 8($gp)
	sub $t3, $t2, 1
	lw $t4, 0($sp)
	slt $t2, $t4, $t3
	move $t5, $t2
	beqz $t5, Tag59
	li $t2, 4
	lw $t3, 0($sp)
	mul $t4, $t2, $t3
	lw $t5, 12($gp)
	add $t2, $t5, $t4
	jal Tag3
	move $t3, $v0
	rem $t4, $t3, 10
	add $t5, $t4, 1
	sw $t5, 0($t2)
Tag60:
	li $t2, 4
	lw $t3, 0($sp)
	mul $t4, $t2, $t3
	lw $t5, 12($gp)
	add $t2, $t5, $t4
	lw $t3, 0($t2)
	lw $t4, 4($sp)
	add $t2, $t3, $t4
	lw $t5, 0($gp)
	sgt $t3, $t2, $t5
	move $t4, $t3
	beqz $t4, Tag62
	li $t2, 4
	lw $t3, 0($sp)
	mul $t4, $t2, $t3
	lw $t5, 12($gp)
	add $t2, $t5, $t4
	jal Tag3
	move $t3, $v0
	rem $t4, $t3, 10
	add $t5, $t4, 1
	sw $t5, 0($t2)
	j Tag60
Tag62:
	li $t2, 4
	lw $t3, 0($sp)
	mul $t4, $t2, $t3
	lw $t5, 12($gp)
	add $t2, $t5, $t4
	lw $t3, 4($sp)
	lw $t4, 0($t2)
	add $t5, $t3, $t4
	sw $t5, 4($sp)
	lw $t2, 0($sp)
	add $t3, $t2, 1
	sw $t3, 0($sp)
	j Tag57
Tag59:
	lw $t2, 8($gp)
	sub $t3, $t2, 1
	li $t4, 4
	mul $t2, $t4, $t3
	lw $t5, 12($gp)
	add $t3, $t5, $t2
	lw $t4, 0($gp)
	lw $t2, 4($sp)
	sub $t0, $t4, $t2
	sw $t0, 0($t3)
	jal Tag13
	move $t2, $v0
	jal Tag35
	move $t0, $v0
Tag63:
	jal Tag19
	move $t0, $v0
	move $t2, $t0
	seq $t3, $t2, $zero
	move $t0, $t3
	beqz $t0, Tag65
	la $t2, Tag66
	lw $t0, 8($sp)
	add $t3, $t0, 1
	sw $t3, 8($sp)
	lw $t0, 8($sp)
	move $a0, $t2	# save arguments
	move $a1, $t0	# save arguments
	jal Tag0
	move $t3, $v0
	jal Tag48
	move $t4, $v0
	jal Tag35
	move $t0, $v0
	jal Tag13
	move $t2, $v0
	j Tag63
Tag65:
	la $t0, Tag67
	lw $t2, 8($sp)
	move $a0, $t0	# save arguments
	move $a1, $t2	# save arguments
	jal Tag0
	move $t3, $v0
	li $t1, 0
	move $v0, $t1
	lw $t5, 416($sp)	# level load register
	lw $t4, 412($sp)	# level load register
	lw $t3, 408($sp)	# level load register
	lw $t2, 404($sp)	# level load register
	lw $t1, 400($sp)	# level load register
	lw $t0, 396($sp)	# level load register
	lw $ra, 460($sp)
	addiu $sp, $sp, 464
	jr $ra
	lw $t5, 416($sp)	# level load register
	lw $t4, 412($sp)	# level load register
	lw $t3, 408($sp)	# level load register
	lw $t2, 404($sp)	# level load register
	lw $t1, 400($sp)	# level load register
	lw $t0, 396($sp)	# level load register
	lw $ra, 460($sp)
	addiu $sp, $sp, 464
	jr $ra
########################################
############### RUN-TIME ###############
########################################

#malloc
Tag1:
	# a0 -- size in bytes (already x4)
	li $v0, 9
	syscall
	jr $ra

#getchar
Tag2:
	li $v0, 12
	syscall
	jr $ra
	
exit:
	li $v0, 10
	syscall
	jr $ra

## Daniel J. Ellard -- 03/13/94
## printf.asm--
## an implementation of a simple printf work-alike.

## printf--
## A simple printf-like function. Understands just the basic forms
## of the %s, %d, %c, and %% formats, and can only have 3 embedded
## formats (so that all of the parameters are passed in registers).
## If there are more than 3 embedded formats, all but the first 3 are
## completely ignored (not even printed).
## Register Usage:
## $a0,$s0 - pointer to format string
## $a1,$s1 - format argument 1 (optional)
## $a2,$s2 - format argument 2 (optional)
## $a3,$s3 - format argument 3 (optional)
## $s4 - count of formats processed.
## $s5 - char at $s4.
## $s6 - pointer to printf buffer
##
Tag0:
	subu $sp, $sp, 56 # set up the stack frame,
	sw $ra, 32($sp) # saving the local environment.
	sw $fp, 28($sp)
	sw $s0, 24($sp)
	sw $s1, 20($sp)
	sw $s2, 16($sp)
	sw $s3, 12($sp)
	sw $s4, 8($sp)
	sw $s5, 4($sp)
	sw $s6, 0($sp)
	sw $s7, 36($sp)
	sw $t0, 40($sp)
	sw $t1, 44($sp)
	sw $t2, 48($sp)
	sw $t3, 52($sp)
	addu $fp, $sp, 52

# grab the arguments:
	move $s0, $a0 # fmt string
	move $s1, $a1 # arg1 (optional)
	move $s2, $a2 # arg2 (optional)
	move $s3, $a3 # arg3 (optional)
	lw $s7, 16($v1)# arg4 (optional)
	lw $t0, 20($v1)# arg5 (optional)

	li $s4, 0 # set # of formats = 0
	la $s6, printf_buf # set s6 = base of printf buffer.

printf_loop: # process each character in the fmt:
	lb $s5, 0($s0) # get the next character, and then
	addu $s0, $s0, 1 # bump up $s0 to the next character.

	beq $s5, '%', printf_fmt # if the fmt character, then do fmt.
	beq $0, $s5, printf_end # if zero, then go to end.

printf_putc:
	sb $s5, 0($s6) # otherwise, just put this char
	sb $0, 1($s6) # into the printf buffer,
	move $a0, $s6 # and then print it with the
	li $v0, 4 # print_str syscall
	syscall

	b printf_loop # loop on.

printf_fmt:
	lb $s5, 0($s0) # see what the fmt character is,
	addu $s0, $s0, 1 # and bump up the pointer.

	beq $s4, 5, printf_loop # if we've already processed 3 args,
# then *ignore* this fmt.
	beq $s5, '0', printf_pre
	beq $s5, 'd', printf_int # if 'd', print as a decimal integer.
	beq $s5, 's', printf_str # if 's', print as a string.
	beq $s5, 'c', printf_char # if 'c', print as a ASCII char.
	beq $s5, '%', printf_perc # if '%', print a '%'
	b printf_loop # otherwise, just continue.

printf_shift_args: # shift over the fmt args,
	move $s1, $s2 # $s1 = $s2
	move $s2, $s3 # $s2 = $s3
	move $s3, $s7 # $s3 = $s7
	move $s7, $t0

	add $s4, $s4, 1 # increment # of args processed.

	b printf_loop # and continue the main loop.
	
printf_pre:
	lb $s5, ($s0)
	sub $s5, $s5, 48
	add $s0, $s0, 2
	
	li $t1, 0
	move $t2, $s1
printf_digits_begin:
	beqz $t2, printf_digits_end
	add $t1, $t1, 1
	div $t2, $t2, 10
	b printf_digits_begin
	
printf_digits_end:
	sub $t1, $s5, $t1
	
printf_pre_output_zero_begin:
	beqz $t1, printf_pre_output_zero_end
	sub $t1, $t1, 1
	li $a0, 0
	li $v0, 1
	syscall
	b printf_pre_output_zero_begin
	
printf_pre_output_zero_end:
	move $a0, $s1
	li $v0, 1
	syscall
	b printf_shift_args
	

printf_int: # deal with a %d:
	move $a0, $s1 # do a print_int syscall of $s1.
	li $v0, 1
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_str: # deal with a %s:
	move $a0, $s1 # do a print_string syscall of $s1.
	li $v0, 4
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_char: # deal with a %c:
	sb $s1, 0($s6) # fill the buffer in with byte $s1,
	sb $0, 1($s6) # and then a null.
	move $a0, $s6 # and then do a print_str syscall
	li $v0, 4 # on the buffer.
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_perc: # deal with a %%:
	li $s5, '%' # (this is redundant)
	sb $s5, 0($s6) # fill the buffer in with byte %,
	sb $0, 1($s6) # and then a null.
	move $a0, $s6 # and then do a print_str syscall
	li $v0, 4 # on the buffer.
	syscall
	b printf_loop # branch to printf_loop

printf_end:
	lw $t1, 44($sp)
	lw $t2, 48($sp)
	lw $t3, 52($sp)
	lw $s7, 36($sp)
	lw $t0, 40($sp)
	lw $ra, 32($sp) # restore the prior environment:
	lw $fp, 28($sp)
	lw $s0, 24($sp)
	lw $s1, 20($sp)
	lw $s2, 16($sp)
	lw $s3, 12($sp)
	lw $s4, 8($sp)
	lw $s5, 4($sp)
	lw $s6, 0($sp)
	addu $sp, $sp, 56 # release the stack frame.
	jr $ra # return.

.data
	printf_buf: .space 2

