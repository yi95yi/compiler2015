	.data
Tag31:
	.asciiz "%d\n"
	.align 2
	.align 2
	fs_whiteSpace: .asciiz " "
	.align 2
	fs_newLine: .asciiz "\n"
	.align 2
	.globl args
args:	.space 12
	.align 2
	.globl stat
stat:	.space 116
	.text
	.align 2
	.globl main
main:
	la $gp, stat
	la $v1, args
	li $t1, 0
	sw $t1, 0($gp)
	li $t2, 2000
	sw $t2, 0($gp)
	li $t1, 0
	sw $t1, 4($gp)
	li $t2, 0
	sw $t2, 8($gp)
	li $t1, 12345
	sw $t1, 8($gp)
	li $t2, 0
	sw $t2, 12($gp)
	li $t1, 32768
	sw $t1, 12($gp)
	li $t2, 0
	sw $t2, 16($gp)
	li $t1, 86421
	sw $t1, 16($gp)
	li $t2, 0
	sw $t2, 20($gp)
	li $t1, 0
	sw $t1, 24($gp)
	jal Tag24
	jal exit
Tag3:
	addi $sp, $sp, -108	# enter
	sw $ra, 104($sp)
	sw $t5, 60($sp)	# level save register
	sw $t4, 56($sp)	# level save register
	sw $t3, 52($sp)	# level save register
	sw $t2, 48($sp)	# level save register
	sw $t1, 44($sp)	# level save register
	sw $t0, 40($sp)	# level save register
	lw $t2, 4($gp)
	lw $t3, 16($gp)
	mul $t4, $t2, $t3
	lw $t5, 8($gp)
	add $t2, $t4, $t5
	sw $t2, 0($sp)
	lw $t3, 0($sp)
	lw $t2, 12($gp)
	rem $t4, $t3, $t2
	sw $t4, 16($gp)
	lw $t2, 0($sp)
	move $v0, $t2
	lw $t5, 60($sp)	# level load register
	lw $t4, 56($sp)	# level load register
	lw $t3, 52($sp)	# level load register
	lw $t2, 48($sp)	# level load register
	lw $t1, 44($sp)	# level load register
	lw $t0, 40($sp)	# level load register
	lw $ra, 104($sp)
	addiu $sp, $sp, 108
	jr $ra
	lw $t5, 60($sp)	# level load register
	lw $t4, 56($sp)	# level load register
	lw $t3, 52($sp)	# level load register
	lw $t2, 48($sp)	# level load register
	lw $t1, 44($sp)	# level load register
	lw $t0, 40($sp)	# level load register
	lw $ra, 104($sp)
	addiu $sp, $sp, 108
	jr $ra
Tag4:
	addi $sp, $sp, -100	# enter
	sw $ra, 96($sp)
	sw $t3, 44($sp)	# level save register
	sw $t2, 40($sp)	# level save register
	sw $t1, 36($sp)	# level save register
	sw $t0, 32($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	lw $t1, 0($sp)
	seq $t2, $t1, 0
	move $t3, $t2
	beqz $t3, Tag5
	li $t1, 0
	move $v0, $t1
	lw $t3, 44($sp)	# level load register
	lw $t2, 40($sp)	# level load register
	lw $t1, 36($sp)	# level load register
	lw $t0, 32($sp)	# level load register
	lw $ra, 96($sp)
	addiu $sp, $sp, 100
	jr $ra
Tag5:
	lw $t1, 0($sp)
	add $t2, $t1, 8
	lw $t0, 0($t2)
	move $v0, $t0
	lw $t3, 44($sp)	# level load register
	lw $t2, 40($sp)	# level load register
	lw $t1, 36($sp)	# level load register
	lw $t0, 32($sp)	# level load register
	lw $ra, 96($sp)
	addiu $sp, $sp, 100
	jr $ra
	lw $t3, 44($sp)	# level load register
	lw $t2, 40($sp)	# level load register
	lw $t1, 36($sp)	# level load register
	lw $t0, 32($sp)	# level load register
	lw $ra, 96($sp)
	addiu $sp, $sp, 100
	jr $ra
Tag6:
	addi $sp, $sp, -120	# enter
	sw $ra, 116($sp)
	sw $t4, 68($sp)	# level save register
	sw $t3, 64($sp)	# level save register
	sw $t2, 60($sp)	# level save register
	sw $t1, 56($sp)	# level save register
	sw $t0, 52($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	lw $t1, 0($sp)
	add $t2, $t1, 8
	lw $t3, 0($sp)
	add $t1, $t3, 12
	lw $t4, 0($t1)
	move $a0, $t4	# save arguments
	jal Tag4
	move $t3, $v0
	lw $t1, 0($sp)
	add $t4, $t1, 16
	lw $t0, 0($t4)
	move $a0, $t0	# save arguments
	jal Tag4
	move $t1, $v0
	add $t4, $t3, $t1
	add $t0, $t4, 1
	sw $t0, 0($t2)
	lw $t4, 68($sp)	# level load register
	lw $t3, 64($sp)	# level load register
	lw $t2, 60($sp)	# level load register
	lw $t1, 56($sp)	# level load register
	lw $t0, 52($sp)	# level load register
	lw $ra, 116($sp)
	addiu $sp, $sp, 120
	jr $ra
Tag7:
	addi $sp, $sp, -140	# enter
	sw $ra, 136($sp)
	sw $t4, 88($sp)	# level save register
	sw $t3, 84($sp)	# level save register
	sw $t2, 80($sp)	# level save register
	sw $t1, 76($sp)	# level save register
	sw $t0, 72($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	lw $t1, 0($sp)
	add $t2, $t1, 16
	lw $t3, 0($t2)
	sw $t3, 4($sp)
	lw $t1, 0($sp)
	add $t2, $t1, 16
	lw $t3, 4($sp)
	add $t1, $t3, 12
	lw $t4, 0($t1)
	sw $t4, 0($t2)
	lw $t1, 4($sp)
	add $t2, $t1, 12
	lw $t3, 0($sp)
	sw $t3, 0($t2)
	lw $t1, 0($sp)
	move $a0, $t1	# save arguments
	jal Tag6
	move $t2, $v0
	lw $t3, 4($sp)
	move $a0, $t3	# save arguments
	jal Tag6
	move $t1, $v0
	lw $t2, 4($sp)
	move $v0, $t2
	lw $t4, 88($sp)	# level load register
	lw $t3, 84($sp)	# level load register
	lw $t2, 80($sp)	# level load register
	lw $t1, 76($sp)	# level load register
	lw $t0, 72($sp)	# level load register
	lw $ra, 136($sp)
	addiu $sp, $sp, 140
	jr $ra
	lw $t4, 88($sp)	# level load register
	lw $t3, 84($sp)	# level load register
	lw $t2, 80($sp)	# level load register
	lw $t1, 76($sp)	# level load register
	lw $t0, 72($sp)	# level load register
	lw $ra, 136($sp)
	addiu $sp, $sp, 140
	jr $ra
Tag8:
	addi $sp, $sp, -140	# enter
	sw $ra, 136($sp)
	sw $t4, 88($sp)	# level save register
	sw $t3, 84($sp)	# level save register
	sw $t2, 80($sp)	# level save register
	sw $t1, 76($sp)	# level save register
	sw $t0, 72($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	lw $t1, 0($sp)
	add $t2, $t1, 12
	lw $t3, 0($t2)
	sw $t3, 4($sp)
	lw $t1, 0($sp)
	add $t2, $t1, 12
	lw $t3, 4($sp)
	add $t1, $t3, 16
	lw $t4, 0($t1)
	sw $t4, 0($t2)
	lw $t1, 4($sp)
	add $t2, $t1, 16
	lw $t3, 0($sp)
	sw $t3, 0($t2)
	lw $t1, 0($sp)
	move $a0, $t1	# save arguments
	jal Tag6
	move $t2, $v0
	lw $t3, 4($sp)
	move $a0, $t3	# save arguments
	jal Tag6
	move $t1, $v0
	lw $t2, 4($sp)
	move $v0, $t2
	lw $t4, 88($sp)	# level load register
	lw $t3, 84($sp)	# level load register
	lw $t2, 80($sp)	# level load register
	lw $t1, 76($sp)	# level load register
	lw $t0, 72($sp)	# level load register
	lw $ra, 136($sp)
	addiu $sp, $sp, 140
	jr $ra
	lw $t4, 88($sp)	# level load register
	lw $t3, 84($sp)	# level load register
	lw $t2, 80($sp)	# level load register
	lw $t1, 76($sp)	# level load register
	lw $t0, 72($sp)	# level load register
	lw $ra, 136($sp)
	addiu $sp, $sp, 140
	jr $ra
Tag9:
	addi $sp, $sp, -336	# enter
	sw $ra, 332($sp)
	sw $t4, 284($sp)	# level save register
	sw $t3, 280($sp)	# level save register
	sw $t2, 276($sp)	# level save register
	sw $t1, 272($sp)	# level save register
	sw $t0, 268($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	sw $a1, 4($sp)	# load argument
	lw $t1, 0($sp)
	seq $t2, $t1, 0
	move $t3, $t2
	beqz $t3, Tag10
	lw $t1, 4($sp)
	move $v0, $t1
	lw $t4, 284($sp)	# level load register
	lw $t3, 280($sp)	# level load register
	lw $t2, 276($sp)	# level load register
	lw $t1, 272($sp)	# level load register
	lw $t0, 268($sp)	# level load register
	lw $ra, 332($sp)
	addiu $sp, $sp, 336
	jr $ra
Tag10:
	lw $t1, 0($sp)
	add $t2, $t1, 0
	lw $t3, 4($sp)
	add $t1, $t3, 0
	lw $t4, 0($t2)
	lw $t3, 0($t1)
	sgt $t2, $t4, $t3
	move $t1, $t2
	beqz $t1, Tag11
	lw $t2, 0($sp)
	add $t1, $t2, 12
	lw $t3, 0($sp)
	add $t2, $t3, 12
	lw $t4, 0($t2)
	lw $t3, 4($sp)
	move $a0, $t4	# save arguments
	move $a1, $t3	# save arguments
	jal Tag9
	move $t2, $v0
	sw $t2, 0($t1)
	j Tag12
Tag11:
	lw $t1, 0($sp)
	add $t2, $t1, 16
	lw $t3, 0($sp)
	add $t1, $t3, 16
	lw $t4, 0($t1)
	lw $t3, 4($sp)
	move $a0, $t4	# save arguments
	move $a1, $t3	# save arguments
	jal Tag9
	move $t1, $v0
	sw $t1, 0($t2)
Tag12:
	lw $t1, 0($sp)
	move $a0, $t1	# save arguments
	jal Tag6
	move $t2, $v0
	lw $t3, 0($sp)
	add $t1, $t3, 12
	lw $t2, 0($t1)
	beqz $t2, Tag14
	lw $t1, 0($sp)
	add $t2, $t1, 12
	lw $t3, 0($t2)
	add $t1, $t3, 4
	lw $t2, 0($sp)
	add $t3, $t2, 4
	lw $t4, 0($t1)
	lw $t2, 0($t3)
	ble $t4, $t2, Tag14
	li $t1, 1
	j Tag15
Tag14:
	li $t1, 0
Tag15:
	move $t2, $t1
	beqz $t2, Tag13
	lw $t1, 0($sp)
	move $a0, $t1	# save arguments
	jal Tag8
	move $t2, $v0
	move $t3, $t2
	move $v0, $t3
	lw $t4, 284($sp)	# level load register
	lw $t3, 280($sp)	# level load register
	lw $t2, 276($sp)	# level load register
	lw $t1, 272($sp)	# level load register
	lw $t0, 268($sp)	# level load register
	lw $ra, 332($sp)
	addiu $sp, $sp, 336
	jr $ra
Tag13:
	lw $t1, 0($sp)
	add $t2, $t1, 16
	lw $t3, 0($t2)
	beqz $t3, Tag17
	lw $t1, 0($sp)
	add $t2, $t1, 16
	lw $t3, 0($t2)
	add $t1, $t3, 4
	lw $t2, 0($sp)
	add $t3, $t2, 4
	lw $t4, 0($t1)
	lw $t2, 0($t3)
	ble $t4, $t2, Tag17
	li $t1, 1
	j Tag18
Tag17:
	li $t1, 0
Tag18:
	move $t2, $t1
	beqz $t2, Tag16
	lw $t1, 0($sp)
	move $a0, $t1	# save arguments
	jal Tag7
	move $t2, $v0
	move $t3, $t2
	move $v0, $t3
	lw $t4, 284($sp)	# level load register
	lw $t3, 280($sp)	# level load register
	lw $t2, 276($sp)	# level load register
	lw $t1, 272($sp)	# level load register
	lw $t0, 268($sp)	# level load register
	lw $ra, 332($sp)
	addiu $sp, $sp, 336
	jr $ra
Tag16:
	lw $t1, 0($sp)
	move $v0, $t1
	lw $t4, 284($sp)	# level load register
	lw $t3, 280($sp)	# level load register
	lw $t2, 276($sp)	# level load register
	lw $t1, 272($sp)	# level load register
	lw $t0, 268($sp)	# level load register
	lw $ra, 332($sp)
	addiu $sp, $sp, 336
	jr $ra
	lw $t4, 284($sp)	# level load register
	lw $t3, 280($sp)	# level load register
	lw $t2, 276($sp)	# level load register
	lw $t1, 272($sp)	# level load register
	lw $t0, 268($sp)	# level load register
	lw $ra, 332($sp)
	addiu $sp, $sp, 336
	jr $ra
Tag19:
	addi $sp, $sp, -228	# enter
	sw $ra, 224($sp)
	sw $t5, 180($sp)	# level save register
	sw $t4, 176($sp)	# level save register
	sw $t3, 172($sp)	# level save register
	sw $t2, 168($sp)	# level save register
	sw $t1, 164($sp)	# level save register
	sw $t0, 160($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	sw $a1, 4($sp)	# load argument
	li $t1, 0
	sw $t1, 8($sp)
	lw $t2, 0($sp)
	add $t1, $t2, 12
	lw $t3, 0($t1)
	sne $t2, $t3, 0
	move $t1, $t2
	beqz $t1, Tag20
	lw $t2, 0($sp)
	add $t1, $t2, 12
	lw $t3, 0($t1)
	add $t2, $t3, 8
	lw $t1, 0($t2)
	sw $t1, 8($sp)
Tag20:
	lw $t1, 8($sp)
	lw $t2, 4($sp)
	sgt $t3, $t1, $t2
	move $t4, $t3
	beqz $t4, Tag21
	lw $t1, 0($sp)
	add $t2, $t1, 12
	lw $t3, 0($t2)
	lw $t1, 4($sp)
	move $a0, $t3	# save arguments
	move $a1, $t1	# save arguments
	jal Tag19
	move $t2, $v0
	move $t4, $t2
	move $v0, $t4
	lw $t5, 180($sp)	# level load register
	lw $t4, 176($sp)	# level load register
	lw $t3, 172($sp)	# level load register
	lw $t2, 168($sp)	# level load register
	lw $t1, 164($sp)	# level load register
	lw $t0, 160($sp)	# level load register
	lw $ra, 224($sp)
	addiu $sp, $sp, 228
	jr $ra
Tag21:
	lw $t1, 8($sp)
	lw $t2, 4($sp)
	slt $t3, $t1, $t2
	move $t4, $t3
	beqz $t4, Tag22
	lw $t1, 0($sp)
	add $t2, $t1, 16
	lw $t3, 0($t2)
	lw $t1, 4($sp)
	lw $t2, 8($sp)
	sub $t4, $t1, $t2
	sub $t5, $t4, 1
	move $a0, $t3	# save arguments
	move $a1, $t5	# save arguments
	jal Tag19
	move $t1, $v0
	move $t2, $t1
	move $v0, $t2
	lw $t5, 180($sp)	# level load register
	lw $t4, 176($sp)	# level load register
	lw $t3, 172($sp)	# level load register
	lw $t2, 168($sp)	# level load register
	lw $t1, 164($sp)	# level load register
	lw $t0, 160($sp)	# level load register
	lw $ra, 224($sp)
	addiu $sp, $sp, 228
	jr $ra
Tag22:
	lw $t1, 0($sp)
	add $t2, $t1, 0
	lw $t0, 0($t2)
	move $v0, $t0
	lw $t5, 180($sp)	# level load register
	lw $t4, 176($sp)	# level load register
	lw $t3, 172($sp)	# level load register
	lw $t2, 168($sp)	# level load register
	lw $t1, 164($sp)	# level load register
	lw $t0, 160($sp)	# level load register
	lw $ra, 224($sp)
	addiu $sp, $sp, 228
	jr $ra
	lw $t5, 180($sp)	# level load register
	lw $t4, 176($sp)	# level load register
	lw $t3, 172($sp)	# level load register
	lw $t2, 168($sp)	# level load register
	lw $t1, 164($sp)	# level load register
	lw $t0, 160($sp)	# level load register
	lw $ra, 224($sp)
	addiu $sp, $sp, 228
	jr $ra
Tag23:
	addi $sp, $sp, -148	# enter
	sw $ra, 144($sp)
	sw $t4, 96($sp)	# level save register
	sw $t3, 92($sp)	# level save register
	sw $t2, 88($sp)	# level save register
	sw $t1, 84($sp)	# level save register
	sw $t0, 80($sp)	# level save register
	li $t1, 20
	move $t2, $t1    #sizeof
	move $a0, $t2	# save arguments
	jal Tag1
	move $t3, $v0
	sw $t3, 0($sp)
	lw $t1, 0($sp)
	add $t2, $t1, 12
	lw $t3, 0($sp)
	add $t1, $t3, 16
	li $t4, 0
	sw $t4, 0($t1)
	lw $t3, 0($t1)
	sw $t3, 0($t2)
	lw $t1, 0($sp)
	add $t2, $t1, 8
	li $t3, 1
	sw $t3, 0($t2)
	lw $t1, 0($sp)
	add $t2, $t1, 4
	jal Tag3
	move $t3, $v0
	sw $t3, 0($t2)
	lw $t1, 0($sp)
	add $t2, $t1, 0
	jal Tag3
	move $t3, $v0
	sw $t3, 0($t2)
	lw $t1, 0($sp)
	move $v0, $t1
	lw $t4, 96($sp)	# level load register
	lw $t3, 92($sp)	# level load register
	lw $t2, 88($sp)	# level load register
	lw $t1, 84($sp)	# level load register
	lw $t0, 80($sp)	# level load register
	lw $ra, 144($sp)
	addiu $sp, $sp, 148
	jr $ra
	lw $t4, 96($sp)	# level load register
	lw $t3, 92($sp)	# level load register
	lw $t2, 88($sp)	# level load register
	lw $t1, 84($sp)	# level load register
	lw $t0, 80($sp)	# level load register
	lw $ra, 144($sp)
	addiu $sp, $sp, 148
	jr $ra
Tag24:
	addi $sp, $sp, -260	# enter
	sw $ra, 256($sp)
	sw $t5, 212($sp)	# level save register
	sw $t4, 208($sp)	# level save register
	sw $t3, 204($sp)	# level save register
	sw $t2, 200($sp)	# level save register
	sw $t1, 196($sp)	# level save register
	sw $t0, 192($sp)	# level save register
	jal Tag2
	move $t1, $v0
	sub $t2, $t1, 48
	li $t3, 10000
	mul $t1, $t3, $t2
	jal Tag2
	move $t4, $v0
	sub $t2, $t4, 48
	li $t3, 1000
	mul $t4, $t3, $t2
	add $t5, $t1, $t4
	jal Tag2
	move $t2, $v0
	sub $t1, $t2, 48
	li $t3, 100
	mul $t2, $t3, $t1
	add $t4, $t5, $t2
	jal Tag2
	move $t1, $v0
	sub $t2, $t1, 48
	li $t3, 10
	mul $t1, $t3, $t2
	add $t5, $t4, $t1
	jal Tag2
	move $t2, $v0
	add $t1, $t5, $t2
	sub $t3, $t1, 48
	sw $t3, 4($gp)
	jal Tag23
	move $t1, $v0
	sw $t1, 24($gp)
	li $t2, 0
	sw $t2, 20($gp)
Tag25:
	lw $t1, 20($gp)
	lw $t2, 0($gp)
	slt $t3, $t1, $t2
	move $t4, $t3
	beqz $t4, Tag27
	lw $t1, 24($gp)
	jal Tag23
	move $t2, $v0
	move $a0, $t1	# save arguments
	move $a1, $t2	# save arguments
	jal Tag9
	move $t3, $v0
	sw $t3, 24($gp)
	lw $t1, 20($gp)
	lw $t2, 20($gp)
	add $t1, $t2, 1
	sw $t1, 20($gp)
	j Tag25
Tag27:
	li $t1, 0
	sw $t1, 20($gp)
Tag28:
	lw $t1, 20($gp)
	lw $t2, 0($gp)
	sle $t3, $t1, $t2
	move $t4, $t3
	beqz $t4, Tag30
	la $t1, Tag31
	lw $t2, 24($gp)
	lw $t3, 20($gp)
	move $a0, $t2	# save arguments
	move $a1, $t3	# save arguments
	jal Tag19
	move $t4, $v0
	move $a0, $t4
	li $v0, 1
	syscall
	la $a0, fs_newLine
	li $v0, 4
	syscall
	lw $t2, 20($gp)
	lw $t1, 20($gp)
	add $t2, $t1, 1
	sw $t2, 20($gp)
	j Tag28
Tag30:
	li $t0, 0
	move $v0, $t0
	lw $t5, 212($sp)	# level load register
	lw $t4, 208($sp)	# level load register
	lw $t3, 204($sp)	# level load register
	lw $t2, 200($sp)	# level load register
	lw $t1, 196($sp)	# level load register
	lw $t0, 192($sp)	# level load register
	lw $ra, 256($sp)
	addiu $sp, $sp, 260
	jr $ra
	lw $t5, 212($sp)	# level load register
	lw $t4, 208($sp)	# level load register
	lw $t3, 204($sp)	# level load register
	lw $t2, 200($sp)	# level load register
	lw $t1, 196($sp)	# level load register
	lw $t0, 192($sp)	# level load register
	lw $ra, 256($sp)
	addiu $sp, $sp, 260
	jr $ra
########################################
############### RUN-TIME ###############
########################################

#malloc
Tag1:
	# a0 -- size in bytes (already x4)
	li $v0, 9
	syscall
	jr $ra

#getchar
Tag2:
	li $v0, 12
	syscall
	jr $ra
	
exit:
	li $v0, 10
	syscall
	jr $ra

## Daniel J. Ellard -- 03/13/94
## printf.asm--
## an implementation of a simple printf work-alike.

## printf--
## A simple printf-like function. Understands just the basic forms
## of the %s, %d, %c, and %% formats, and can only have 3 embedded
## formats (so that all of the parameters are passed in registers).
## If there are more than 3 embedded formats, all but the first 3 are
## completely ignored (not even printed).
## Register Usage:
## $a0,$s0 - pointer to format string
## $a1,$s1 - format argument 1 (optional)
## $a2,$s2 - format argument 2 (optional)
## $a3,$s3 - format argument 3 (optional)
## $s4 - count of formats processed.
## $s5 - char at $s4.
## $s6 - pointer to printf buffer
##
Tag0:
	subu $sp, $sp, 56 # set up the stack frame,
	sw $ra, 32($sp) # saving the local environment.
	sw $fp, 28($sp)
	sw $s0, 24($sp)
	sw $s1, 20($sp)
	sw $s2, 16($sp)
	sw $s3, 12($sp)
	sw $s4, 8($sp)
	sw $s5, 4($sp)
	sw $s6, 0($sp)
	sw $s7, 36($sp)
	sw $t0, 40($sp)
	sw $t1, 44($sp)
	sw $t2, 48($sp)
	sw $t3, 52($sp)
	addu $fp, $sp, 52

# grab the arguments:
	move $s0, $a0 # fmt string
	move $s1, $a1 # arg1 (optional)
	move $s2, $a2 # arg2 (optional)
	move $s3, $a3 # arg3 (optional)
	lw $s7, 16($v1)# arg4 (optional)
	lw $t0, 20($v1)# arg5 (optional)

	li $s4, 0 # set # of formats = 0
	la $s6, printf_buf # set s6 = base of printf buffer.

printf_loop: # process each character in the fmt:
	lb $s5, 0($s0) # get the next character, and then
	addu $s0, $s0, 1 # bump up $s0 to the next character.

	beq $s5, '%', printf_fmt # if the fmt character, then do fmt.
	beq $0, $s5, printf_end # if zero, then go to end.

printf_putc:
	sb $s5, 0($s6) # otherwise, just put this char
	sb $0, 1($s6) # into the printf buffer,
	move $a0, $s6 # and then print it with the
	li $v0, 4 # print_str syscall
	syscall

	b printf_loop # loop on.

printf_fmt:
	lb $s5, 0($s0) # see what the fmt character is,
	addu $s0, $s0, 1 # and bump up the pointer.

	beq $s4, 5, printf_loop # if we've already processed 3 args,
# then *ignore* this fmt.
	beq $s5, '0', printf_pre
	beq $s5, 'd', printf_int # if 'd', print as a decimal integer.
	beq $s5, 's', printf_str # if 's', print as a string.
	beq $s5, 'c', printf_char # if 'c', print as a ASCII char.
	beq $s5, '%', printf_perc # if '%', print a '%'
	b printf_loop # otherwise, just continue.

printf_shift_args: # shift over the fmt args,
	move $s1, $s2 # $s1 = $s2
	move $s2, $s3 # $s2 = $s3
	move $s3, $s7 # $s3 = $s7
	move $s7, $t0

	add $s4, $s4, 1 # increment # of args processed.

	b printf_loop # and continue the main loop.
	
printf_pre:
	lb $s5, ($s0)
	sub $s5, $s5, 48
	add $s0, $s0, 2
	
	li $t1, 0
	move $t2, $s1
printf_digits_begin:
	beqz $t2, printf_digits_end
	add $t1, $t1, 1
	div $t2, $t2, 10
	b printf_digits_begin
	
printf_digits_end:
	sub $t1, $s5, $t1
	
printf_pre_output_zero_begin:
	beqz $t1, printf_pre_output_zero_end
	sub $t1, $t1, 1
	li $a0, 0
	li $v0, 1
	syscall
	b printf_pre_output_zero_begin
	
printf_pre_output_zero_end:
	move $a0, $s1
	li $v0, 1
	syscall
	b printf_shift_args
	

printf_int: # deal with a %d:
	move $a0, $s1 # do a print_int syscall of $s1.
	li $v0, 1
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_str: # deal with a %s:
	move $a0, $s1 # do a print_string syscall of $s1.
	li $v0, 4
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_char: # deal with a %c:
	sb $s1, 0($s6) # fill the buffer in with byte $s1,
	sb $0, 1($s6) # and then a null.
	move $a0, $s6 # and then do a print_str syscall
	li $v0, 4 # on the buffer.
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_perc: # deal with a %%:
	li $s5, '%' # (this is redundant)
	sb $s5, 0($s6) # fill the buffer in with byte %,
	sb $0, 1($s6) # and then a null.
	move $a0, $s6 # and then do a print_str syscall
	li $v0, 4 # on the buffer.
	syscall
	b printf_loop # branch to printf_loop

printf_end:
	lw $t1, 44($sp)
	lw $t2, 48($sp)
	lw $t3, 52($sp)
	lw $s7, 36($sp)
	lw $t0, 40($sp)
	lw $ra, 32($sp) # restore the prior environment:
	lw $fp, 28($sp)
	lw $s0, 24($sp)
	lw $s1, 20($sp)
	lw $s2, 16($sp)
	lw $s3, 12($sp)
	lw $s4, 8($sp)
	lw $s5, 4($sp)
	lw $s6, 0($sp)
	addu $sp, $sp, 56 # release the stack frame.
	jr $ra # return.

.data
	printf_buf: .space 2

