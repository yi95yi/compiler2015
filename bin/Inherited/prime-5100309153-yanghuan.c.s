	.data
Tag17:
	.asciiz "%d"
	.align 2
Tag21:
	.asciiz "%c"
	.align 2
Tag22:
	.asciiz "%d"
	.align 2
Tag23:
	.asciiz "%c"
	.align 2
Tag45:
	.asciiz "Total: %d\n"
	.align 2
	.align 2
	fs_whiteSpace: .asciiz " "
	.align 2
	fs_newLine: .asciiz "\n"
	.align 2
	.globl args
args:	.space 32
	.align 2
	.globl stat
stat:	.space 180
	.text
	.align 2
	.globl main
main:
	la $gp, stat
	la $v1, args
	li $t1, 0
	sw $t1, 0($gp)
	li $t2, 0
	sw $t2, 4($gp)
	li $t1, 0
	sw $t1, 8($gp)
	li $t2, 0
	sw $t2, 12($gp)
	li $t1, 0
	sw $t1, 16($gp)
	li $t2, 0
	sw $t2, 20($gp)
	li $t1, 4004
	move $a0, $t1	# save arguments
	jal Tag1
	move $t2, $v0
	sw $t2, 24($gp)
	li $t1, 680
	move $a0, $t1	# save arguments
	jal Tag1
	move $t2, $v0
	sw $t2, 28($gp)
	li $t1, 4004
	move $a0, $t1	# save arguments
	jal Tag1
	move $t2, $v0
	sw $t2, 32($gp)
	li $t1, 8
	move $a0, $t1	# save arguments
	jal Tag1
	move $t2, $v0
	sw $t2, 36($gp)
	li $t1, 115600
	move $a0, $t1	# save arguments
	jal Tag1
	move $t2, $v0
	sw $t2, 40($gp)
	jal Tag24
	jal exit
Tag3:
	addi $sp, $sp, -320	# enter
	sw $ra, 316($sp)
	sw $t4, 268($sp)	# level save register
	sw $t3, 264($sp)	# level save register
	sw $t2, 260($sp)	# level save register
	sw $t1, 256($sp)	# level save register
	sw $t0, 252($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	sw $a1, 4($sp)	# load argument
	sw $a2, 8($sp)	# load argument
	sw $a3, 12($sp)	# load argument
	lw $t2, 16($v1)
	sw $t2, 16($sp)
	li $t1, 2
	sw $t1, 20($sp)
	li $t2, 2
	sw $t2, 24($sp)
Tag4:
	lw $t1, 24($sp)
	lw $t2, 0($sp)
	sle $t3, $t1, $t2
	move $t4, $t3
	beqz $t4, Tag6
	li $t1, 4
	lw $t2, 24($sp)
	mul $t3, $t1, $t2
	lw $t4, 8($sp)
	add $t1, $t4, $t3
	lw $t2, 0($t1)
	seq $t3, $t2, 1
	move $t1, $t3
	beqz $t1, Tag7
	lw $t2, 4($sp)
	lw $t1, 4($sp)
	lw $t3, 0($t1)
	add $t4, $t3, 1
	sw $t4, 0($t2)
	lw $t1, 4($sp)
	li $t2, 4
	lw $t3, 0($t1)
	mul $t4, $t2, $t3
	lw $t1, 16($sp)
	add $t2, $t1, $t4
	lw $t3, 24($sp)
	sw $t3, 0($t2)
	li $t1, 4
	lw $t2, 24($sp)
	mul $t3, $t1, $t2
	lw $t4, 12($sp)
	add $t1, $t4, $t3
	lw $t2, 4($sp)
	lw $t3, 0($t2)
	sw $t3, 0($t1)
Tag7:
	lw $t1, 24($sp)
	lw $t2, 20($sp)
	mul $t3, $t1, $t2
	lw $t4, 0($sp)
	sle $t1, $t3, $t4
	move $t2, $t1
	beqz $t2, Tag10
	lw $t1, 24($sp)
	lw $t2, 20($sp)
	mul $t3, $t1, $t2
	li $t4, 4
	mul $t1, $t4, $t3
	lw $t2, 8($sp)
	add $t3, $t2, $t1
	li $t4, 0
	sw $t4, 0($t3)
	lw $t1, 20($sp)
	add $t2, $t1, 1
	sw $t2, 20($sp)
	j Tag7
Tag10:
	li $t1, 2
	sw $t1, 20($sp)
	lw $t2, 24($sp)
	add $t1, $t2, 1
	sw $t1, 24($sp)
	j Tag4
Tag6:
	lw $t4, 268($sp)	# level load register
	lw $t3, 264($sp)	# level load register
	lw $t2, 260($sp)	# level load register
	lw $t1, 256($sp)	# level load register
	lw $t0, 252($sp)	# level load register
	lw $ra, 316($sp)
	addiu $sp, $sp, 320
	jr $ra
Tag11:
	addi $sp, $sp, -596	# enter
	sw $ra, 592($sp)
	sw $s2, 568($sp)	# level save register
	sw $s1, 564($sp)	# level save register
	sw $s0, 560($sp)	# level save register
	sw $t7, 556($sp)	# level save register
	sw $t6, 552($sp)	# level save register
	sw $t5, 548($sp)	# level save register
	sw $t4, 544($sp)	# level save register
	sw $t3, 540($sp)	# level save register
	sw $t2, 536($sp)	# level save register
	sw $t1, 532($sp)	# level save register
	sw $t0, 528($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	sw $a1, 4($sp)	# load argument
	sw $a2, 8($sp)	# load argument
	sw $a3, 12($sp)	# load argument
	lw $t2, 16($v1)
	sw $t2, 16($sp)
	lw $t3, 20($v1)
	sw $t3, 20($sp)
	lw $t2, 24($v1)
	sw $t2, 24($sp)
	li $t1, 680
	lw $t2, 4($sp)
	mul $t3, $t1, $t2
	lw $t4, 24($sp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 8($sp)
	mul $t4, $t2, $t3
	add $t5, $t1, $t4
	li $t2, 1
	neg $t1, $t2
	lw $t3, 0($t5)
	seq $t2, $t3, $t1
	move $t4, $t2
	beqz $t4, Tag14
	li $t1, 4
	lw $t2, 8($sp)
	mul $t3, $t1, $t2
	lw $t4, 20($sp)
	add $t1, $t4, $t3
	lw $t2, 0($t1)
	mul $t3, $t2, 2
	li $t1, 4
	lw $t2, 4($sp)
	mul $t4, $t1, $t2
	lw $t5, 20($sp)
	add $t1, $t5, $t4
	lw $t2, 0($t1)
	sub $t4, $t3, $t2
	lw $t1, 0($sp)
	sle $t2, $t4, $t1
	move $t3, $t2
	beqz $t3, Tag14
	li $t1, 4
	lw $t2, 8($sp)
	mul $t3, $t1, $t2
	lw $t4, 20($sp)
	add $t1, $t4, $t3
	lw $t2, 0($t1)
	mul $t3, $t2, 2
	li $t1, 4
	lw $t2, 4($sp)
	mul $t4, $t1, $t2
	lw $t5, 20($sp)
	add $t1, $t5, $t4
	lw $t2, 0($t1)
	sub $t4, $t3, $t2
	li $t1, 4
	mul $t2, $t1, $t4
	lw $t3, 12($sp)
	add $t1, $t3, $t2
	lw $t4, 0($t1)
	beqz $t4, Tag14
	li $t1, 680
	lw $t2, 4($sp)
	mul $t3, $t1, $t2
	lw $t4, 24($sp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 8($sp)
	mul $t4, $t2, $t3
	add $t5, $t1, $t4
	lw $t2, 0($sp)
	lw $t1, 8($sp)
	li $t3, 4
	lw $t4, 8($sp)
	mul $t6, $t3, $t4
	lw $t7, 20($sp)
	add $t3, $t7, $t6
	lw $t4, 0($t3)
	mul $t6, $t4, 2
	li $t3, 4
	lw $t4, 4($sp)
	mul $t7, $t3, $t4
	lw $s0, 20($sp)
	add $t3, $s0, $t7
	lw $t4, 0($t3)
	sub $t7, $t6, $t4
	li $t3, 4
	mul $t4, $t3, $t7
	lw $t6, 16($sp)
	add $t3, $t6, $t4
	lw $t7, 0($t3)
	lw $t4, 12($sp)
	lw $t3, 16($sp)
	lw $t6, 20($sp)
	lw $s0, 24($sp)
	move $a0, $t2	# save arguments
	move $a1, $t1	# save arguments
	move $a2, $t7	# save arguments
	move $a3, $t4	# save arguments
	sw $t3, 16($v1)	# save arguments
	sw $t6, 20($v1)	# save arguments
	sw $s0, 24($v1)	# save arguments
	jal Tag11
	move $s1, $v0
	add $s2, $s1, 1
	sw $s2, 0($t5)
Tag14:
	li $t1, 680
	lw $t2, 4($sp)
	mul $t3, $t1, $t2
	lw $t4, 24($sp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 8($sp)
	mul $t4, $t2, $t3
	add $t5, $t1, $t4
	li $t2, 1
	neg $t1, $t2
	lw $t3, 0($t5)
	seq $t2, $t3, $t1
	move $t4, $t2
	beqz $t4, Tag15
	li $t1, 680
	lw $t2, 4($sp)
	mul $t3, $t1, $t2
	lw $t4, 24($sp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 8($sp)
	mul $t4, $t2, $t3
	add $t5, $t1, $t4
	li $t2, 1
	sw $t2, 0($t5)
Tag15:
	li $t1, 680
	lw $t2, 4($sp)
	mul $t3, $t1, $t2
	lw $t4, 24($sp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 8($sp)
	mul $t4, $t2, $t3
	add $t0, $t1, $t4
	lw $t2, 0($t0)
	move $v0, $t2
	lw $s2, 568($sp)	# level load register
	lw $s1, 564($sp)	# level load register
	lw $s0, 560($sp)	# level load register
	lw $t7, 556($sp)	# level load register
	lw $t6, 552($sp)	# level load register
	lw $t5, 548($sp)	# level load register
	lw $t4, 544($sp)	# level load register
	lw $t3, 540($sp)	# level load register
	lw $t2, 536($sp)	# level load register
	lw $t1, 532($sp)	# level load register
	lw $t0, 528($sp)	# level load register
	lw $ra, 592($sp)
	addiu $sp, $sp, 596
	jr $ra
	lw $s2, 568($sp)	# level load register
	lw $s1, 564($sp)	# level load register
	lw $s0, 560($sp)	# level load register
	lw $t7, 556($sp)	# level load register
	lw $t6, 552($sp)	# level load register
	lw $t5, 548($sp)	# level load register
	lw $t4, 544($sp)	# level load register
	lw $t3, 540($sp)	# level load register
	lw $t2, 536($sp)	# level load register
	lw $t1, 532($sp)	# level load register
	lw $t0, 528($sp)	# level load register
	lw $ra, 592($sp)
	addiu $sp, $sp, 596
	jr $ra
Tag16:
	addi $sp, $sp, -180	# enter
	sw $ra, 176($sp)
	sw $t4, 128($sp)	# level save register
	sw $t3, 124($sp)	# level save register
	sw $t2, 120($sp)	# level save register
	sw $t1, 116($sp)	# level save register
	sw $t0, 112($sp)	# level save register
	sw $a0, 0($sp)	# load argument
	sw $a1, 4($sp)	# load argument
	sw $a2, 8($sp)	# load argument
	la $t1, Tag17
	lw $t2, 0($sp)
	move $a0, $t2
	li $v0, 1
	syscall
Tag18:
	lw $t1, 8($sp)
	sgt $t2, $t1, 0
	move $t3, $t2
	beqz $t3, Tag20
	la $t1, Tag21
	li $t2, 32
	move $a0, $t1	# save arguments
	move $a1, $t2	# save arguments
	jal Tag0
	move $t3, $v0
	la $t4, Tag22
	lw $t1, 4($sp)
	move $a0, $t1
	li $v0, 1
	syscall
	lw $t3, 4($sp)
	mul $t1, $t3, 2
	lw $t2, 0($sp)
	sub $t3, $t1, $t2
	sw $t3, 4($sp)
	lw $t1, 0($sp)
	lw $t2, 4($sp)
	add $t3, $t1, $t2
	div $t4, $t3, 2
	sw $t4, 0($sp)
	lw $t1, 8($sp)
	sub $t2, $t1, 1
	sw $t2, 8($sp)
	j Tag18
Tag20:
	la $t0, Tag23
	li $t1, 10
	move $a0, $t0	# save arguments
	move $a1, $t1	# save arguments
	jal Tag0
	move $t2, $v0
	lw $t4, 128($sp)	# level load register
	lw $t3, 124($sp)	# level load register
	lw $t2, 120($sp)	# level load register
	lw $t1, 116($sp)	# level load register
	lw $t0, 112($sp)	# level load register
	lw $ra, 176($sp)
	addiu $sp, $sp, 180
	jr $ra
Tag24:
	addi $sp, $sp, -760	# enter
	sw $ra, 756($sp)
	sw $s1, 728($sp)	# level save register
	sw $s0, 724($sp)	# level save register
	sw $t7, 720($sp)	# level save register
	sw $t6, 716($sp)	# level save register
	sw $t5, 712($sp)	# level save register
	sw $t4, 708($sp)	# level save register
	sw $t3, 704($sp)	# level save register
	sw $t2, 700($sp)	# level save register
	sw $t1, 696($sp)	# level save register
	sw $t0, 692($sp)	# level save register
	li $t1, 1000
	sw $t1, 0($gp)
	jal Tag2
	move $t2, $v0
	sub $t1, $t2, 48
	li $t3, 100
	mul $t2, $t3, $t1
	jal Tag2
	move $t4, $v0
	sub $t1, $t4, 48
	li $t3, 10
	mul $t4, $t3, $t1
	add $t5, $t2, $t4
	jal Tag2
	move $t1, $v0
	add $t2, $t5, $t1
	sub $t3, $t2, 48
	sw $t3, 4($gp)
	li $t1, 0
	sw $t1, 16($gp)
	li $t2, 0
	sw $t2, 20($gp)
	lw $t1, 36($gp)
	li $t2, 0
	sw $t2, 0($t1)
	li $t3, 0
	sw $t3, 8($gp)
Tag25:
	lw $t1, 0($gp)
	add $t2, $t1, 1
	lw $t3, 8($gp)
	slt $t1, $t3, $t2
	move $t4, $t1
	beqz $t4, Tag27
	li $t1, 4
	lw $t2, 8($gp)
	mul $t3, $t1, $t2
	lw $t4, 24($gp)
	add $t1, $t4, $t3
	li $t2, 1
	sw $t2, 0($t1)
	li $t3, 4
	lw $t1, 8($gp)
	mul $t2, $t3, $t1
	lw $t4, 32($gp)
	add $t1, $t4, $t2
	li $t3, 0
	sw $t3, 0($t1)
	lw $t2, 8($gp)
	add $t1, $t2, 1
	sw $t1, 8($gp)
	j Tag25
Tag27:
	li $t1, 0
	sw $t1, 8($gp)
Tag28:
	lw $t1, 4($gp)
	add $t2, $t1, 1
	lw $t3, 8($gp)
	slt $t1, $t3, $t2
	move $t4, $t1
	beqz $t4, Tag30
	li $t1, 4
	lw $t2, 8($gp)
	mul $t3, $t1, $t2
	lw $t4, 28($gp)
	add $t1, $t4, $t3
	li $t2, 0
	sw $t2, 0($t1)
	lw $t3, 8($gp)
	add $t1, $t3, 1
	sw $t1, 8($gp)
	j Tag28
Tag30:
	li $t1, 0
	sw $t1, 8($gp)
Tag31:
	lw $t1, 8($gp)
	lw $t2, 4($gp)
	sle $t3, $t1, $t2
	move $t4, $t3
	beqz $t4, Tag33
	li $t1, 0
	sw $t1, 12($gp)
Tag34:
	lw $t1, 12($gp)
	lw $t2, 4($gp)
	sle $t3, $t1, $t2
	move $t4, $t3
	beqz $t4, Tag36
	li $t1, 680
	lw $t2, 8($gp)
	mul $t3, $t1, $t2
	lw $t4, 40($gp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 12($gp)
	mul $t4, $t2, $t3
	add $t5, $t1, $t4
	li $t2, 1
	neg $t1, $t2
	sw $t1, 0($t5)
	lw $t2, 12($gp)
	add $t1, $t2, 1
	sw $t1, 12($gp)
	j Tag34
Tag36:
	lw $t1, 8($gp)
	add $t2, $t1, 1
	sw $t2, 8($gp)
	j Tag31
Tag33:
	lw $t1, 0($gp)
	lw $t2, 36($gp)
	lw $t3, 24($gp)
	lw $t4, 32($gp)
	lw $t5, 28($gp)
	move $a0, $t1	# save arguments
	move $a1, $t2	# save arguments
	move $a2, $t3	# save arguments
	move $a3, $t4	# save arguments
	sw $t5, 16($v1)	# save arguments
	jal Tag3
	move $t6, $v0
	lw $t7, 36($gp)
	lw $t1, 0($t7)
	sw $t1, 16($gp)
	li $t2, 1
	sw $t2, 8($gp)
Tag37:
	lw $t1, 8($gp)
	lw $t2, 16($gp)
	slt $t3, $t1, $t2
	move $t4, $t3
	beqz $t4, Tag39
	lw $t1, 8($gp)
	add $t2, $t1, 1
	sw $t2, 12($gp)
Tag40:
	lw $t1, 12($gp)
	lw $t2, 16($gp)
	sle $t3, $t1, $t2
	move $t4, $t3
	beqz $t4, Tag42
	li $t1, 680
	lw $t2, 8($gp)
	mul $t3, $t1, $t2
	lw $t4, 40($gp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 12($gp)
	mul $t4, $t2, $t3
	add $t5, $t1, $t4
	li $t2, 1
	neg $t1, $t2
	lw $t3, 0($t5)
	seq $t2, $t3, $t1
	move $t4, $t2
	beqz $t4, Tag44
	li $t1, 680
	lw $t2, 8($gp)
	mul $t3, $t1, $t2
	lw $t4, 40($gp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 12($gp)
	mul $t4, $t2, $t3
	add $t5, $t1, $t4
	lw $t2, 0($gp)
	lw $t1, 8($gp)
	lw $t3, 12($gp)
	lw $t4, 24($gp)
	lw $t6, 32($gp)
	lw $t7, 28($gp)
	lw $s0, 40($gp)
	move $a0, $t2	# save arguments
	move $a1, $t1	# save arguments
	move $a2, $t3	# save arguments
	move $a3, $t4	# save arguments
	sw $t6, 16($v1)	# save arguments
	sw $t7, 20($v1)	# save arguments
	sw $s0, 24($v1)	# save arguments
	jal Tag11
	move $s1, $v0
	sw $s1, 0($t5)
	li $t1, 680
	lw $t2, 8($gp)
	mul $t3, $t1, $t2
	lw $t4, 40($gp)
	add $t1, $t4, $t3
	li $t2, 4
	lw $t3, 12($gp)
	mul $t4, $t2, $t3
	add $t5, $t1, $t4
	lw $t2, 0($t5)
	sgt $t1, $t2, 1
	move $t3, $t1
	beqz $t3, Tag44
	li $t1, 4
	lw $t2, 8($gp)
	mul $t3, $t1, $t2
	lw $t4, 28($gp)
	add $t1, $t4, $t3
	lw $t2, 0($t1)
	li $t3, 4
	lw $t1, 12($gp)
	mul $t4, $t3, $t1
	lw $t5, 28($gp)
	add $t1, $t5, $t4
	lw $t3, 0($t1)
	li $t4, 680
	lw $t1, 8($gp)
	mul $t5, $t4, $t1
	lw $t6, 40($gp)
	add $t1, $t6, $t5
	li $t4, 4
	lw $t5, 12($gp)
	mul $t6, $t4, $t5
	add $t7, $t1, $t6
	lw $t4, 0($t7)
	move $a0, $t2	# save arguments
	move $a1, $t3	# save arguments
	move $a2, $t4	# save arguments
	jal Tag16
	move $t1, $v0
	lw $t5, 20($gp)
	add $t1, $t5, 1
	sw $t1, 20($gp)
Tag44:
	lw $t1, 12($gp)
	add $t2, $t1, 1
	sw $t2, 12($gp)
	j Tag40
Tag42:
	lw $t1, 8($gp)
	add $t2, $t1, 1
	sw $t2, 8($gp)
	j Tag37
Tag39:
	la $t1, Tag45
	lw $t2, 20($gp)
	move $a0, $t1	# save arguments
	move $a1, $t2	# save arguments
	jal Tag0
	move $t3, $v0
	li $t0, 0
	move $v0, $t0
	lw $s1, 728($sp)	# level load register
	lw $s0, 724($sp)	# level load register
	lw $t7, 720($sp)	# level load register
	lw $t6, 716($sp)	# level load register
	lw $t5, 712($sp)	# level load register
	lw $t4, 708($sp)	# level load register
	lw $t3, 704($sp)	# level load register
	lw $t2, 700($sp)	# level load register
	lw $t1, 696($sp)	# level load register
	lw $t0, 692($sp)	# level load register
	lw $ra, 756($sp)
	addiu $sp, $sp, 760
	jr $ra
	lw $s1, 728($sp)	# level load register
	lw $s0, 724($sp)	# level load register
	lw $t7, 720($sp)	# level load register
	lw $t6, 716($sp)	# level load register
	lw $t5, 712($sp)	# level load register
	lw $t4, 708($sp)	# level load register
	lw $t3, 704($sp)	# level load register
	lw $t2, 700($sp)	# level load register
	lw $t1, 696($sp)	# level load register
	lw $t0, 692($sp)	# level load register
	lw $ra, 756($sp)
	addiu $sp, $sp, 760
	jr $ra
########################################
############### RUN-TIME ###############
########################################

#malloc
Tag1:
	# a0 -- size in bytes (already x4)
	li $v0, 9
	syscall
	jr $ra

#getchar
Tag2:
	li $v0, 12
	syscall
	jr $ra
	
exit:
	li $v0, 10
	syscall
	jr $ra

## Daniel J. Ellard -- 03/13/94
## printf.asm--
## an implementation of a simple printf work-alike.

## printf--
## A simple printf-like function. Understands just the basic forms
## of the %s, %d, %c, and %% formats, and can only have 3 embedded
## formats (so that all of the parameters are passed in registers).
## If there are more than 3 embedded formats, all but the first 3 are
## completely ignored (not even printed).
## Register Usage:
## $a0,$s0 - pointer to format string
## $a1,$s1 - format argument 1 (optional)
## $a2,$s2 - format argument 2 (optional)
## $a3,$s3 - format argument 3 (optional)
## $s4 - count of formats processed.
## $s5 - char at $s4.
## $s6 - pointer to printf buffer
##
Tag0:
	subu $sp, $sp, 56 # set up the stack frame,
	sw $ra, 32($sp) # saving the local environment.
	sw $fp, 28($sp)
	sw $s0, 24($sp)
	sw $s1, 20($sp)
	sw $s2, 16($sp)
	sw $s3, 12($sp)
	sw $s4, 8($sp)
	sw $s5, 4($sp)
	sw $s6, 0($sp)
	sw $s7, 36($sp)
	sw $t0, 40($sp)
	sw $t1, 44($sp)
	sw $t2, 48($sp)
	sw $t3, 52($sp)
	addu $fp, $sp, 52

# grab the arguments:
	move $s0, $a0 # fmt string
	move $s1, $a1 # arg1 (optional)
	move $s2, $a2 # arg2 (optional)
	move $s3, $a3 # arg3 (optional)
	lw $s7, 16($v1)# arg4 (optional)
	lw $t0, 20($v1)# arg5 (optional)

	li $s4, 0 # set # of formats = 0
	la $s6, printf_buf # set s6 = base of printf buffer.

printf_loop: # process each character in the fmt:
	lb $s5, 0($s0) # get the next character, and then
	addu $s0, $s0, 1 # bump up $s0 to the next character.

	beq $s5, '%', printf_fmt # if the fmt character, then do fmt.
	beq $0, $s5, printf_end # if zero, then go to end.

printf_putc:
	sb $s5, 0($s6) # otherwise, just put this char
	sb $0, 1($s6) # into the printf buffer,
	move $a0, $s6 # and then print it with the
	li $v0, 4 # print_str syscall
	syscall

	b printf_loop # loop on.

printf_fmt:
	lb $s5, 0($s0) # see what the fmt character is,
	addu $s0, $s0, 1 # and bump up the pointer.

	beq $s4, 5, printf_loop # if we've already processed 3 args,
# then *ignore* this fmt.
	beq $s5, '0', printf_pre
	beq $s5, 'd', printf_int # if 'd', print as a decimal integer.
	beq $s5, 's', printf_str # if 's', print as a string.
	beq $s5, 'c', printf_char # if 'c', print as a ASCII char.
	beq $s5, '%', printf_perc # if '%', print a '%'
	b printf_loop # otherwise, just continue.

printf_shift_args: # shift over the fmt args,
	move $s1, $s2 # $s1 = $s2
	move $s2, $s3 # $s2 = $s3
	move $s3, $s7 # $s3 = $s7
	move $s7, $t0

	add $s4, $s4, 1 # increment # of args processed.

	b printf_loop # and continue the main loop.
	
printf_pre:
	lb $s5, ($s0)
	sub $s5, $s5, 48
	add $s0, $s0, 2
	
	li $t1, 0
	move $t2, $s1
printf_digits_begin:
	beqz $t2, printf_digits_end
	add $t1, $t1, 1
	div $t2, $t2, 10
	b printf_digits_begin
	
printf_digits_end:
	sub $t1, $s5, $t1
	
printf_pre_output_zero_begin:
	beqz $t1, printf_pre_output_zero_end
	sub $t1, $t1, 1
	li $a0, 0
	li $v0, 1
	syscall
	b printf_pre_output_zero_begin
	
printf_pre_output_zero_end:
	move $a0, $s1
	li $v0, 1
	syscall
	b printf_shift_args
	

printf_int: # deal with a %d:
	move $a0, $s1 # do a print_int syscall of $s1.
	li $v0, 1
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_str: # deal with a %s:
	move $a0, $s1 # do a print_string syscall of $s1.
	li $v0, 4
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_char: # deal with a %c:
	sb $s1, 0($s6) # fill the buffer in with byte $s1,
	sb $0, 1($s6) # and then a null.
	move $a0, $s6 # and then do a print_str syscall
	li $v0, 4 # on the buffer.
	syscall
	b printf_shift_args # branch to printf_shift_args

printf_perc: # deal with a %%:
	li $s5, '%' # (this is redundant)
	sb $s5, 0($s6) # fill the buffer in with byte %,
	sb $0, 1($s6) # and then a null.
	move $a0, $s6 # and then do a print_str syscall
	li $v0, 4 # on the buffer.
	syscall
	b printf_loop # branch to printf_loop

printf_end:
	lw $t1, 44($sp)
	lw $t2, 48($sp)
	lw $t3, 52($sp)
	lw $s7, 36($sp)
	lw $t0, 40($sp)
	lw $ra, 32($sp) # restore the prior environment:
	lw $fp, 28($sp)
	lw $s0, 24($sp)
	lw $s1, 20($sp)
	lw $s2, 16($sp)
	lw $s3, 12($sp)
	lw $s4, 8($sp)
	lw $s5, 4($sp)
	lw $s6, 0($sp)
	addu $sp, $sp, 56 # release the stack frame.
	jr $ra # return.

.data
	printf_buf: .space 2

