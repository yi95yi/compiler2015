#!/bin/bash
#please change the $name variable to be your own bitbucket id
#please make sure the compiler could be compiled correctly and finaltermvars.sh is set
#build-answers.sh should be run in the normaldata directory

echo '========== Testing start =========='



InhDir=$(pwd)/Inherited
InhInpDir=${InhDir}/Inherited-InputSet
InhAnsDir=${InhDir}/Inherited-AnswerSet
NewDir=$(pwd)/New
NewInpDir=${NewDir}/New-InputSet
NewAnsDir=${NewDir}/New-AnswerSet

echo $InhDir
echo $InhInpDir
echo $InhAnsDir
echo $NewDir
echo $NewInpDir
echo $NewAnsDir

CCHK="java -classpath ../antlr-4.5-complete.jar: main.Main"

$CCHK < $NewDir/cmpstr.c > cmpstr.s
$CCHK $NewDir/bubble.c > bubble.s

echo "========== test completed =========="
